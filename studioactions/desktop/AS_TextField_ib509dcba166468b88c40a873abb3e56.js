function AS_TextField_ib509dcba166468b88c40a873abb3e56(eventobject, changedtext) {
    var amount = this.view.frmPayDueAmount.flxContainer.flxMainContainer.flxBillPay.PayDueAmount.tbxAmount.text;
    var a = replace(/$/g, '');
    int amt = parseInt(a);
    if (amt < 1000) {
        this.view.frmPayDueAmount.flxContainer.flxMainContainer.flxBillPay.PayDueAmount.lblAmount.isVisible = "false";
        this.view.frmPayDueAmount.flxContainer.flxMainContainer.flxBillPay.PayDueAmount.flxInfoDueAmount.isVisible = "true";
    }
}