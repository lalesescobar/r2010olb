describe("SampleSuite", function() {
	it("DbxLogin", async function() {
	
	  
	  var userName='dbpolbuser';
	  var passWord='Kony@1234';
	
	  // Verifying may be later screen
	 /* 
	  var mayBeLater=await kony.automation.playback.waitFor(["frmLogout","CustomFeedbackPopup","btnNo"],5000);
	  if(mayBeLater){
	    kony.automation.button.click(["frmLogout","CustomFeedbackPopup","btnNo"]);
	    await kony.automation.playback.waitFor(["frmLogout","logOutMsg","AlterneteActionsLoginNow"]);
	    kony.automation.flexcontainer.click(["frmLogout","logOutMsg","AlterneteActionsLoginNow"]);
	  }
	*/
	  // Login to the application
	  await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxUserName"],10000);
	  kony.automation.textbox.enterText(["frmLogin","loginComponent","tbxUserName"],userName);
	  await kony.automation.playback.waitFor(["frmLogin","loginComponent","tbxPassword"],10000);
	  kony.automation.textbox.enterText(["frmLogin","loginComponent","tbxPassword"],passWord);
	  
	  await kony.automation.playback.wait(10000);
	  
	  await kony.automation.playback.waitFor(["frmLogin","loginComponent","btnLogin"],20000);
	  kony.automation.button.click(["frmLogin","loginComponent","btnLogin"]);
	  /*
	  // Verifying Terms and conditions screen
	  var termsconditions=await kony.automation.playback.waitFor(["frmPreTermsandCondition","flxAgree"],15000);
	  if(termsconditions){
	    kony.automation.flexcontainer.click(["frmPreTermsandCondition","flxAgree"]);
	    await kony.automation.playback.waitFor(["frmPreTermsandCondition","btnProceed"]);
	    kony.automation.button.click(["frmPreTermsandCondition","btnProceed"]);
	  }
	  */
	  
	 
	 // await kony.automation.playback.wait(10000);
	  
	  await kony.automation.playback.waitFor(["frmDashboard"]);
	 
	},70000);
	
	
	it("Login_Signout_ARB-7779-TS33-TC01", async function() {
	  //await kony.automation.playback.wait(20000);
	  await kony.automation.playback.waitFor(["frmDashboard","customheader","headermenu","btnLogout"]);
	  kony.automation.button.click(["frmDashboard","customheader","headermenu","btnLogout"]);
	  await kony.automation.playback.waitFor(["frmAccountsLanding","CustomPopup","btnYes"]);
	  kony.automation.button.click(["frmDashboard","CustomPopup","btnYes"]);
	  
	});
});