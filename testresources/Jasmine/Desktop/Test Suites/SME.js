describe("SME", function() {
	it("SMELogin", async function() {
	  
	  
	  await kony.automation.playback.wait(2000);
		kony.automation.textbox.enterText(["frmLogin","loginComponent","tbxUserName"],"DBXUSER01SB");
		await kony.automation.playback.wait(2000);
		kony.automation.textbox.enterText(["frmLogin","loginComponent","tbxPassword"],"Kony@1234");
		await kony.automation.playback.wait(2000);
		kony.automation.button.click(["frmLogin","loginComponent","btnLogin"]);
	  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
	  /*
	  
	  await kony.automation.playback.wait(2000);
		kony.automation.textbox.enterText(["frmLogin","main","tbxUserName"],"DBXUSER01SB");
		await kony.automation.playback.wait(2000);
		kony.automation.textbox.enterText(["frmLogin","main","tbxPassword"],"Kony@1234");
		await kony.automation.playback.wait(2000);
		kony.automation.button.click(["frmLogin","main","btnLogin"]);
	    */
	});
	
	it("SMELogout", async function() {
	  
	  await kony.automation.playback.wait(5000);
	  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.wait(5000);
	  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
	  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","flxLogout"]);
	  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","flxLogout"]);
	  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","flxLogout"]);
	  await kony.automation.playback.waitFor(["frmBBAccountsLanding","CustomPopup","btnYes"]);
	  await kony.automation.playback.wait(2000);
	  kony.automation.button.click(["frmBBAccountsLanding","CustomPopup","btnYes"]);
	  
	});
});