it("SMELogout", async function() {
  
  await kony.automation.playback.wait(5000);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.wait(5000);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","topmenu","flxMenu"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","customheader","customhamburger","flxLogout"]);
  await kony.automation.scrollToWidget(["frmBBAccountsLanding","customheader","customhamburger","flxLogout"]);
  kony.automation.flexcontainer.click(["frmBBAccountsLanding","customheader","customhamburger","flxLogout"]);
  await kony.automation.playback.waitFor(["frmBBAccountsLanding","CustomPopup","btnYes"]);
  await kony.automation.playback.wait(2000);
  kony.automation.button.click(["frmBBAccountsLanding","CustomPopup","btnYes"]);
  
});