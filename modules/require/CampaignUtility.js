define(['OLBConstants'], function(OLBConstants){

  return {

    CAMPAIGN_CONSTANTS : {
      PLACEHOLDERCODE : "POPUP",
      PREFIX : "OLB_",
      VERB : "getCampaign",
    },

    fetchPopupCampaigns : function(){
    let self = this;
    if(OLBConstants.CLIENT_PROPERTIES && OLBConstants.CLIENT_PROPERTIES.OLB_ENABLE_POPUP_CAMPAIGNS && OLBConstants.CLIENT_PROPERTIES.OLB_ENABLE_POPUP_CAMPAIGNS.toUpperCase() === "TRUE"){
      let directMktManager = applicationManager.getDirectMarketingManager();
      let formname = applicationManager.getNavigationManager().currentForm;
      let context = self.fetchContext(formname);
      directMktManager.getPopupAds(context,self.CAMPAIGN_CONSTANTS.VERB, self.campaignsSuccess, self.campaignsFailure);
    }
    },

    fetchContext : function(formname){
      let self = this;
      let context = {};
      context["scale"] = applicationManager.getDeviceUtilManager().getDeviceScale();
      context["placeholderCode"] = self.CAMPAIGN_CONSTANTS.PLACEHOLDERCODE;
      context["eventCode"] = self.CAMPAIGN_CONSTANTS.PREFIX + formname;
//       let breakpoint = kony.application.getCurrentBreakpoint();
//       let scale = (breakpoint === 640) ? "3x" : 1366;
      context["scale"] = 1366;
      return context;
    },

    campaignsSuccess : function(res) {
      applicationManager.getNavigationManager().updateForm({
        "campaign" : res,
      });
    },

    campaignsFailure : function(res) {
      applicationManager.getNavigationManager().updateForm({
        "campaign" : undefined,
      });
      kony.print("error", res);
    },

    removeExistingCampaignpopup : function(view){
      if(view.flxDialogs && view.flxDialogs.campaignpopup){
        view.flxDialogs.campaignpopup.removeFromParent();
      } else if(view.campaignpopup){
        view.campaignpopup.removeFromParent();
      }
    },

    showCampaign : function(data, view, mainFlexId){
      let scopeObj = this;
      // mainFlexId can be undefined if popup is added in flxDialogs
      // flxDialogs will take care of page height adjustments - needs to be tested
      //this.removeExistingCampaignpopup(view);
      if(view.campaignpopup){
        view.campaignpopup.removeFromParent();
      }
      let hasCampaign = data && data.length > 0;
      if(hasCampaign){
        let campaignpopup = new com.olb.common.campaignpopup({
          "autogrowMode": kony.flex.AUTOGROW_NONE,
          "clipBounds": true,
          "height": "100%",
          "id": "campaignpopup",
          "isVisible": hasCampaign,
          "layoutType": kony.flex.FREE_FORM,
          "left": "0dp",
          "masterType": constants.MASTER_TYPE_DEFAULT,
          "isModalContainer": false,
          "skin": "sknBackground000000Op35",
          "top": "0dp",
          "width": "100%",
          "zIndex": 2000,
          "overrides": {
            "lblCampaignId" : {
              "text" : data[0].campaignId
            },
            "lblHeading" : {
              "text" : data[0].bannerTitle
            },
            "lblmessage" : {
              "text" : data[0].bannerDescription
            },
            "imgCampaign" : {
              "src" : data[0].imageURL,
              "onTouchEnd" : function(eventObject){
                let parent = eventObject.parent.parent;
                scopeObj.ignoreCamapign(parent.lblCampaignId.text);
                parent.onClickofCampaign(parent.lblTargetUrl.text.trim());
              }
            },
            "lblTargetUrl": {
              "text": data[0].destinationURL
            },
            "lblCTATargetUrl": {
              "text": data[0].callToActionTargetURL
            },
            "lblcross" : {
              "isVisible" : data[0].showCloseIcon && data[0].showCloseIcon.toUpperCase() === "TRUE",
              "onTouchEnd" : function(eventObject){
                let parent = eventObject.parent.parent.parent;
                scopeObj.ignoreCamapign(parent.lblCampaignId.text);
                parent.closepopup();
              }
            },
            "btnYes" : {
              "text" : data[0].callToActionButtonLabel ? data[0].callToActionButtonLabel : kony.i18n.getLocalizedString("i18n.Campaign.Primary"),
              "onClick" : function(eventObject){
                let parent = eventObject.parent.parent.parent;
                scopeObj.ignoreCamapign(parent.lblCampaignId.text);
                parent.onClickofCampaign(parent.lblCTATargetUrl.text.trim());
              }
            },
            "btnNo" : {
              "isVisible" : data[0].showReadLaterButton && data[0].showReadLaterButton.toUpperCase() === "TRUE",
              "onClick" : function(eventObject){
                let parent = eventObject.parent.parent.parent;
                parent.closepopup();
              }
            },
          }
        }, {
          "overrides": {}
        }, {
          "overrides": {}
        });
        campaignpopup.btnYes.toolTip = data[0].callToActionButtonLabel ? data[0].callToActionButtonLabel : kony.i18n.getLocalizedString("i18n.Campaign.Primary");
        campaignpopup.btnNo.toolTip = "Read Later";
        view.forceLayout();
        this.getBreakpointChanges(campaignpopup);
        view.add(campaignpopup); 
        campaignpopup.adjustHeight(mainFlexId);
        campaignpopup.forceLayout();
        view.forceLayout();
      }
    },

    getBreakpointChanges : function(campaignpopup){
      if(campaignpopup) {
        let breakpoint = kony.application.getCurrentBreakpoint();
        let isMobile = (breakpoint === 640);
        campaignpopup.lblHeading.skin = isMobile ? "sknLabel003e7524px" : "sknLabel003e7540px";
        campaignpopup.lblHeading.left = isMobile ? "20px" : "30px";
        campaignpopup.lblHeading.right = isMobile ? "40px" : "60px";
        campaignpopup.lblmessage.skin = isMobile ? "sknLabel42424215px" : "sknLabel42424220px";
        campaignpopup.lblmessage.left = isMobile ? "20px" : "30px";
        campaignpopup.lblmessage.right = isMobile ? "40px" : "60px";
        campaignpopup.lblcross.right = isMobile ? "10px" : "20px";
        campaignpopup.flxButtons.layoutType = isMobile ? kony.flex.FLOW_VERTICAL : kony.flex.FLOW_HORIZONTAL;
        campaignpopup.flxButtons.reverseLayoutDirection = !isMobile;
        campaignpopup.btnYes.width = isMobile ? "80%" : "30%";
        campaignpopup.btnYes.centerX = isMobile ? "50%" : "";
        campaignpopup.btnNo.width = isMobile ? "80%" : "30%";
        campaignpopup.btnNo.centerX = isMobile ? "50%" : "";
        campaignpopup.btnNo.top = isMobile ? "10px" : "0px";
        campaignpopup.flxpopup.width = isMobile ? "300px" : "560px";
        campaignpopup.imgCampaign.height = isMobile ? "175px" : "323px";
      }
    },

    onBreakpointChange : function(campaignpopup, mainFlexId){
      if(campaignpopup){
        this.getBreakpointChanges(campaignpopup);
        campaignpopup.adjustHeight(mainFlexId);
      }
    },

    ignoreCamapign : function(campaignId) {
      let directMktManager = applicationManager.getDirectMarketingManager();
      function callback(res) {
        if(!res.isSuccess)
          kony.web.logger("Failed to ignore pop up campaign");
      }
      directMktManager.ignorePopupAds(campaignId, callback);
    },

    onClickofInAppCampaign : function(url){
      // this is a common function to handle all IN-APP Ads
      var scopeObj = this;	
      let actionType = scopeObj.getQueryString("actionType",url);
      let moduleName = scopeObj.getQueryString("moduleName",url);
      if(actionType === "internal" && moduleName ==="Enable_Paperless_Statements"){
        var profileModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ProfileModule");
        profileModule.presentationController.showPreferredAccounts();
      }  else if(actionType === "internal" && moduleName){
        applicationManager.getNavigationManager().navigateTo(moduleName);
      } else {
        kony.application.openURL(url) ;
      }
    },

    getQueryString: function(field, url) {
      var href = url;
      var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
      var string = reg.exec(href);
      return string ? string[1] : null;
    },

  };

});