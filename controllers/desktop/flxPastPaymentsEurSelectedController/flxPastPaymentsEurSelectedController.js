define({
  showUnselectedRow: function () {
    var rowIndex = kony.application.getCurrentForm().segPastPaymentsEur.selectedRowIndex[1];
    var data = kony.application.getCurrentForm().segPastPaymentsEur.data;
    var pre_val;
    var required_values = [];
    var array_close = ["O", false, "sknFlxIdentifier","sknffffff15pxolbfonticons", "50dp", "sknflxffffffnoborder"];
    var array_open = ["P", true,"sknflx4a902", "sknLbl4a90e215px", "280dp", "sknFlxf7f7f7"];
    if (previous_index === rowIndex) {
      data[rowIndex].lblDropdown == "P" ? required_values = array_close : required_values = array_open;
      this.toggle(rowIndex, required_values);
    }
    else {
      if (previous_index >= 0) { 
         if(previous_index < data.length){pre_val = previous_index; this.toggle(pre_val, array_close); }}
      pre_val = rowIndex; this.toggle(rowIndex, array_open);
    }
    previous_index = rowIndex;
  },
  toggle: function (index, array) {
    var data = kony.application.getCurrentForm().segPastPaymentsEur.data;
    data[index].lblDropdown = array[0];
    data[index].flxIdentifier.isVisible = array[1];
    data[index].flxIdentifier.skin = array[2];
    data[index].lblIdentifier.skin = array[3];
    data[index].flxPastPaymentsEurSelected.height = array[4];
    data[index].flxPastPaymentsEurSelected.skin = array[5];
    kony.application.getCurrentForm().segPastPaymentsEur.setDataAt(data[index], index);
  },
});
