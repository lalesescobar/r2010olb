define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropDown **/
    AS_FlexContainer_e21cbeba6e7b4eca9aff0dcb2f73b107: function AS_FlexContainer_e21cbeba6e7b4eca9aff0dcb2f73b107(eventobject, context) {
        var self = this;
        //this.pendingRowOnClick();
        this.showSelectedRow();
    },
    /** onClick defined for btnAction **/
    AS_Button_b4d88f559bd144838fdb09614ba82e2b: function AS_Button_b4d88f559bd144838fdb09614ba82e2b(eventobject, context) {
        var self = this;
        this.executeOnParent("viewTransactionReport");
    }
});