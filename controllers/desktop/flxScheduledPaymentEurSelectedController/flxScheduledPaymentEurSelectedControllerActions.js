define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropDown **/
    AS_FlexContainer_f0181bc63adf472285554192152b40ab: function AS_FlexContainer_f0181bc63adf472285554192152b40ab(eventobject, context) {
        var self = this;
        //this.pendingRowOnClick();
        this.showSelectedRow();
    },
    /** onClick defined for btnAction **/
    AS_Button_adc04f43c09a46d0968591ccb6714687: function AS_Button_adc04f43c09a46d0968591ccb6714687(eventobject, context) {
        var self = this;
        this.executeOnParent("viewTransactionReport");
    }
});