define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDownloadAttachment **/
    AS_FlexContainer_c76c718bcb934f8690568e8f1c9a6483: function AS_FlexContainer_c76c718bcb934f8690568e8f1c9a6483(eventobject, context) {
        var self = this;
        return self.downloadSingleFile.call(this);
    }
});