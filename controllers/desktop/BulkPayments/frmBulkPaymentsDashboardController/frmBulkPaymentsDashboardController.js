define(['CommonUtilities', 'ViewConstants', 'FormControllerUtility','CampaignUtility'], function (CommonUtilities, ViewConstants, FormControllerUtility,CampaignUtility) {
  
 var orientationHandler = new OrientationHandler();
  
  return {
    
    /** Global Variables **/
    bulkPaymentsModule: null,  
	fetchParams: {},
	dashboardSortParams: {},
    fetchParams: {},
    filterParams: {},

    /**
     * Method to update form using given context
     * @param {object} context depending on the context the appropriate function is executed to update view
     */
    
    updateFormUI : function (context) {
      if (context.progressBar === true) {
        FormControllerUtility.showProgressBar(this.view);
      }
      else if (context.progressBar === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
      else if (context.showRecipients === false) {
        this.errorFlow(context);
      }
      else if (context.key) {
        if(context.key===kony.i18n.getLocalizedString("i18n.stopChecks.ViewRequests"))
        {
          this.showBulkPaymentsDashboardSelectedTab(
            1,
            kony.i18n.getLocalizedString("i18n.kony.BulkPayments.bulkPaymentHeader"),
            kony.i18n.getLocalizedString("i18n.stopChecks.ViewRequests")
          );
          this.activeTab = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.onGoingPayments");
          this.view.TabPaneNew.TabSearchBarNew.tbxSearch.text = "";
		  this.filterParams.OngoingPaymentSearch = "";
          this.hideOrShowCloseIcon();
          this.view.TabPaneNew.PaginationContainer.isVisible = true;
          this.invokeonGoingPayments();
        }
        else if(context.key===kony.i18n.getLocalizedString("i18n.kony.BulkPayments.viewUploadedfiles"))
        {
          this.showBulkPaymentsDashboardSelectedTab(
            2,
            kony.i18n.getLocalizedString("i18n.kony.BulkPayments.bulkPaymentHeader"),
            kony.i18n.getLocalizedString("i18n.kony.BulkPayments.viewUploadedfiles")
          );
		  this.activeTab = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.uploadedFiles");
          this.view.TabPaneNew.TabSearchBarNew.tbxSearch.text = "";
		  this.filterParams.UploadingFilesSearch = "";
          this.hideOrShowCloseIcon();
          this.view.TabPaneNew.PaginationContainer.isVisible = true;
          this.invokeUploadFiles();
        }
        else if(context.key===kony.i18n.getLocalizedString("i18n.kony.BulkPayments.viewHistory"))
        {
          this.showBulkPaymentsDashboardSelectedTab(
            3,
            kony.i18n.getLocalizedString("i18n.kony.BulkPayments.bulkPaymentHeader"),
            kony.i18n.getLocalizedString("i18n.kony.BulkPayments.viewHistory")
          );
		  this.activeTab = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.paymentHistory");
          this.view.TabPaneNew.TabSearchBarNew.tbxSearch.text = "";
		  this.filterParams.PaymentHistorySearch = "";
          this.hideOrShowCloseIcon();
          this.view.TabPaneNew.PaginationContainer.isVisible = true;
          this.invokePaymentHistory();
        }		
      }
	  else if (context.fetchOnGoingPaymentsSuccess) {
		 this.setUpOngoingPaymentsview(context.fetchOnGoingPaymentsSuccess);
	  }
	  else if (context.fetchUploadedFilesSuccess) {
		 this.setUpUploadingFilesview(context.fetchUploadedFilesSuccess);
	  }
	  else if (context.fetchHistorySuccess) {
		 this.setUpPaymentHistoryview(context.fetchHistorySuccess);
	  }
	  if (context.serverError === true) {
          this.showServerErrorMessage({ show: true , errorMessage: context.errorMessage});
      }
      if (context.campaignRes) {
        this.campaignSuccess(context.campaignRes);
      }
      if (context.campaignError) {
        this.view.flxBannerContainerDesktop.setVisibility(false);
        this.view.flxBannerContainerMobile.setVisibility(false);
      }

      
      this.view.forceLayout();
      this.adjustScreen(0);
    },
    
    /**
     * onPreShow :  onPreshow event Function for the form
     * @member of {frmBulkPaymentsDashboardController}
     * @param {}
     * @return {}
     * @throws {}
     */
    
    onPreShow : function () {
      
      var scopeObj = this;
      this.view.customheader.forceCloseHamburger();
      this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX_POINTER;
      applicationManager.getNavigationManager().applyUpdates(this);
      this.view.onBreakpointChange = function () {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      };
      var onSegReload = function() {
         scopeObj.view.forceLayout();
         scopeObj.adjustScreen(50);
      };
      this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.onClick = function() {
        if (this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.isVisible) {
          this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.origin = true;
        }
      }.bind(this);
      this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxIphoneDropdown.onClick = function() {
        this.setDropdownVisiblility();
      }.bind(this);
      this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.segViewTypes.onRowClick=function(){
		this.segViewTypesRowClick(this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.segViewTypes.selectedRowItems[0].lblViewTypeName.text);
	  }.bind(this);
      this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxIphoneDropdown.skin="slFBox";
	  this.view.TabPaneNew.TabBodyNew.setSegmentReloadAction(onSegReload);
      this.view.TabPaneNew.TabBodyNew.segTemplates.sectionHeaderTemplate = "flxBulkPaymentsViewHeader";
      this.view.TabPaneNew.TabBodyNew.segTemplates.rowTemplate = "flxBulkPaymentsViewRowTemplate";
      this.view.TabPaneNew.TabBodyNew.setExpandableRowHeight(202);
	  this.view.TabPaneNew.TabSearchBarNew.tbxSearch.onKeyUp = this.hideOrShowCloseIcon.bind(this);
      this.loadDashboard();
      this.initializeDashboardSortParams();
      this.initializeFetchParams();
      this.initializeFilterParams();
      this.setupRightContainerForDashboard();
      this.setupPermissionBasedView();
      this.view.forceLayout();
      this.adjustScreen(0);
    },
	
    hideOrShowCloseIcon: function() {
      if (this.view.TabPaneNew.TabSearchBarNew.tbxSearch.text.trim() === "") {
        this.view.TabPaneNew.TabSearchBarNew.imgClear.setVisibility(false);
      } else {
        this.view.TabPaneNew.TabSearchBarNew.imgClear.setVisibility(true);
      }
      this.view.TabPaneNew.TabSearchBarNew.forceLayout();
    },

    initializeDashboardSortParams: function() {
      this.dashboardSortParams = {
        "OnGoingPayments": {
          "Description": "sortingfinal.png",
          "Date": "sortingfinal.png",
          "Status": "sortingfinal.png"
        },
        "UploadedFiles": {
          "Description": "sortingfinal.png",
          "Date": "sortingfinal.png",
          "Status": "sortingfinal.png"
        },
        "PaymentHistory": {
          "Description": "sortingfinal.png",
          "Date": "sortingfinal.png",
          "TotalAmount": "sortingfinal.png"
        }
      };
    },

    initializeFetchParams: function() {
      this.fetchParams = {
        "searchString": "",
        "sortByParam": "",
        "sortOrder": "",
        "pageSize": "",
        "pageOffset": "",
        "filterByParam": "",
        "filterByValue": ""
      };
    },
    initializeFilterParams: function() {
      this.filterParams = {
        "OnGoingPayments": ".",
        "UploadedFiles": ".",
        "PaymentHistory": ".",
      };
    },
    /**
     * onPostShow :  postShow event Function for the form
     * @member of {frmBulkPaymentsDashboardController}
     * @param {}
     * @return {}
     * @throws {}
     */
    
    onPostShow : function () {
      this.view.forceLayout();
      this.adjustScreen(0);       
    },
    
    errorFlow: function(context) {
      
    },

    /**
     * onInit : onInit event Function for the form
     * @member of {frmBulkPaymentsDashboardController}
     * @param {}
     * @return {}
     * @throws {}
     */
    
    onInit: function () {      
      FormControllerUtility.updateWidgetsHeightInInfo(this.view, ['customheader', 'flxMain', 'flxHeader', 'flxFooter','flxContentContainer','flxHeaderMain']);
      this.bulkPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BulkPayments");
    },
    
    /**
     * showOnGoingPayments : method to show ongoing payments
     * @member of {frmBulkPaymentsDashboardController}
     * @param {}
     * @return {}
     * @throws {}
     */    
    invokeonGoingPayments : function(){      
      this.updateFetchParams();
	  this.setPaginationComponentForOngoingPayments(kony.i18n.getLocalizedString("i18n.common.transactions"));
	  this.fetchOngoingPayments("", "DESC");
    },

    /**
     * showUploadFiles : method to show uploaded files
     * @member of {frmBulkPaymentsDashboardController}
     * @param {}
     * @return {}
     * @throws {}
     */    
    invokeUploadFiles : function(){      
      this.updateFetchParams();
	  this.setPaginationComponentForUploadedFiles(kony.i18n.getLocalizedString("i18n.BulkWires.Files"));
	  this.fetchUploadedFiles("", "DESC");
     },

    /**
     * showPaymentHistory : method to show payment history
     * @member of {frmBulkPaymentsDashboardController}
     * @param {}
     * @return {}
     * @throws {}
     */    
    invokePaymentHistory : function(){      
      this.updateFetchParams();
	  this.setPaginationComponentForPaymentHistory(kony.i18n.getLocalizedString("i18n.common.transactions"));
	  this.fetchHistory("", "DESC");
    },
    
    /**
     * onBreakpointChange : Handles ui changes on .
     * @member of {frmACHDashboardController}
     * @param {integer} width - current browser width
     * @return {}
     * @throws {}
     */    
    onBreakpointChange: function (width) {      
      orientationHandler.onOrientationChange(this.onBreakpointChange); 
      var break_point = kony.application.getCurrentBreakpoint();
      var scope = this;
      var responsiveFonts = new ResponsiveFonts();
      this.view.customheader.onBreakpointChangeComponent(width); 
      this.bulkPaymentsModule.presentationController.getBulkPayCampaigns(); 
      this.adjustScreen(0);
    },
    
    setRightContainerUI: function () {
      this.view.dbRightContainerNew.flxActionHeaderSeperator.setVisibility(false);
      this.view.dbRightContainerNew.lblActionHeader.setVisibility(false);
      if(this.view.dbRightContainerNew.btnAction1.isVisible === true && this.view.dbRightContainerNew.btnAction2.isVisible === true) {
        this.view.dbRightContainerNew.flxActionSeperator.isVisible = true;
      }
      else {
        this.view.dbRightContainerNew.flxActionSeperator.isVisible = false;
      }
      if(this.view.dbRightContainerNew.btnAction3.isVisible === true && (this.view.dbRightContainerNew.btnAction2.isVisible === true || this.view.dbRightContainerNew.btnAction1.isVisible === true)){
        this.view.dbRightContainerNew.flxACtionSeperator2.isVisible = true;
      }
      else {
        this.view.dbRightContainerNew.flxACtionSeperator2.isVisible = false;
      }
    },

    setupRightContainerForDashboard: function () {
      var scopeObj = this;
      this.view.dbRightContainerNew.btnAction1.isVisible = this.checkAtLeastOnePermission(applicationManager.getConfigurationManager().getBulkPaymentFileUploadPermissionsList()) ;
      this.view.dbRightContainerNew.btnAction1.text = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.uploadFilesAndMakePayment");      
      this.view.dbRightContainerNew.btnAction1.onClick = function() {
         scopeObj.bulkPaymentsModule.presentationController.noServiceNavigate("frmBulkPaymentsUploadFile",BBConstants.BULKPAYMENTS_UPLOAD_FILE);
       };
      
      this.view.dbRightContainerNew.flxAction2.isVisible = this.checkAtLeastOnePermission(applicationManager.getConfigurationManager().getBulkPaymentFilesViewPermissionsList()) ;
      this.view.dbRightContainerNew.btnAction2.text = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.downloadTemplateFiles");
      this.view.dbRightContainerNew.btnAction2.onClick =  function() {
         scopeObj.bulkPaymentsModule.presentationController.downloadSampleFile();
      };
    
      this.view.dbRightContainerNew.flxAction3.isVisible = false;     
      this.setRightContainerUI();
      this.view.dbRightContainerNew.setVisibility(true);
    },
    
    checkAtLeastOnePermission : function (permissions) {
        return permissions.some(this.checkUserPermission);
    },
    
    checkUserPermission : function (permission) {
        return applicationManager.getConfigurationManager().checkUserPermission(permission);
    },

    campaignSuccess : function(data){
      var CampaignManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('CampaignManagement');
      CampaignManagementModule.presentationController.updateCampaignDetails(data);
      var self =this;
      if(data.length === 0){     
        this.view.dbRightContainerNew.flxBannerWrapper.setVisibility(false);
      }
      else {
        this.view.dbRightContainerNew.flxBannerWrapper.setVisibility(true);       
        this.view.dbRightContainerNew.imgBanner.src = data[0].imageURL;
        this.view.dbRightContainerNew.imgBanner.onTouchStart =function(){
          CampaignUtility.onClickofInAppCampaign(data[0].destinationURL); 
        };
      }      
      this.adjustScreen(50);
    },

    /**
     * adjustScreen : Handles ui changes based on the screen size
     * @member of {frmBulkPaymentsDashboardController}
     * @param {integer} data - difference to be added to the screen
     * @return {}
     * @throws {}
     */

    adjustScreen : function (data) {
      this.view.flxFooter.isVisible = true;
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.info.frame.height + this.view.flxContentContainer.info.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.info.frame.height;
        if (diff > 0) 
          this.view.flxFooter.top = mainheight + diff + data + "dp";
        else
          this.view.flxFooter.top = mainheight + data + "dp";
        this.view.forceLayout();
      } else {
        this.view.flxFooter.top = mainheight + data + "dp";
        this.view.forceLayout();
      }
    },
    
    loadDashboard: function() {
            this.view.TabPaneNew.TabsHeaderNew.btnTab1.onClick = this.onClickAnyTab;
            this.view.TabPaneNew.TabsHeaderNew.btnTab2.onClick = this.onClickAnyTab;
            this.view.TabPaneNew.TabsHeaderNew.btnTab3.onClick = this.onClickAnyTab;
            this.view.TabPaneNew.TabsHeaderNew.btnTab1.text = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.onGoingPayments");
            this.view.TabPaneNew.TabsHeaderNew.btnTab1.toolTip = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.onGoingPayments");
			this.view.TabPaneNew.TabsHeaderNew.btnTab1.hoverSkin = "sknBtn72727215pxLatoBgf8f7f8";
            this.view.TabPaneNew.TabsHeaderNew.btnTab2.text = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.uploadedFiles");
            this.view.TabPaneNew.TabsHeaderNew.btnTab2.toolTip = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.uploadedFiles");
			this.view.TabPaneNew.TabsHeaderNew.btnTab2.hoverSkin = "sknBtn72727215pxLatoBgf8f7f8";
            this.view.TabPaneNew.TabsHeaderNew.btnTab3.text = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.paymentHistory");
            this.view.TabPaneNew.TabsHeaderNew.btnTab3.toolTip = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.paymentHistory");
			this.view.TabPaneNew.TabsHeaderNew.btnTab3.hoverSkin = "sknBtn72727215pxLatoBgf8f7f8";
	 },
    onClickAnyTab: function(eventobject) {
      this.view.TabPaneNew.TabsHeaderNew.clickTab(eventobject);
      this.onTabClick(eventobject);
    },
    onTabClick: function(eventobject) {
	  this.view.TabPaneNew.TabSearchBarNew.tbxSearch.text = "";
      if (eventobject.text === kony.i18n.getLocalizedString("i18n.kony.BulkPayments.onGoingPayments")) {
        this.activeTab = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.onGoingPayments");
        this.view.customheader.customhamburger.activateMenu(kony.i18n.getLocalizedString("i18n.kony.BulkPayments.bulkPaymentHeader"),
            kony.i18n.getLocalizedString("i18n.stopChecks.ViewRequests"));
        this.view.TabPaneNew.PaginationContainer.isVisible = true;
		this.filterParams.OngoingPaymentSearch = "";
        this.invokeonGoingPayments();
      } else if (eventobject.text === kony.i18n.getLocalizedString("i18n.kony.BulkPayments.uploadedFiles")) {
        this.activeTab = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.uploadedFiles");
        this.view.customheader.customhamburger.activateMenu(kony.i18n.getLocalizedString("i18n.kony.BulkPayments.bulkPaymentHeader"), kony.i18n.getLocalizedString("i18n.kony.BulkPayments.viewUploadedfiles"));
        this.view.TabPaneNew.PaginationContainer.isVisible = true;
		this.filterParams.UploadingFilesSearch = "";
        this.invokeUploadFiles();
      } else if (eventobject.text === kony.i18n.getLocalizedString("i18n.kony.BulkPayments.paymentHistory")) {
        this.activeTab = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.paymentHistory");
        this.view.customheader.customhamburger.activateMenu(kony.i18n.getLocalizedString("i18n.kony.BulkPayments.bulkPaymentHeader"), kony.i18n.getLocalizedString("i18n.kony.BulkPayments.viewHistory"));
        this.view.TabPaneNew.PaginationContainer.isVisible = true;
		this.filterParams.PaymentHistorySearch = "";
        this.invokePaymentHistory();
      }
      this.adjustScreen(200);
      this.view.forceLayout();
    },

	
    setUpOngoingPaymentsview: function(response) {
	  FormControllerUtility.showProgressBar(this.view);
      this.view.TabPaneNew.TabSearchBarNew.flxSearch.width="68%";
      this.view.TabPaneNew.TabSearchBarNew.flxDropDown.isVisible=true;
	  this.view.TabPaneNew.TabSearchBarNew.tbxSearch.text = this.fetchParams.searchString;
	  this.view.TabPaneNew.setSearchData([
        ["lblOPFromAccountValue", "lblOPnitiatedByValue", "lblOPPaymentIDValue"]
      ]);
       this.view.TabPaneNew.TabSearchBarNew.tbxSearch.onDone = function() {
        this.fetchParams.pageSize = "";
        this.fetchParams.pageOffset = "";
        this.filterParams.OngoingPaymentSearch = ".";
        this.view.TabPaneNew.PaginationContainer.isVisible = false;
        this.invokeonGoingPayments();
      }.bind(this);
      this.view.TabPaneNew.TabSearchBarNew.imgClear.onTouchStart = function() {
        this.view.TabPaneNew.TabSearchBarNew.tbxSearch.text = "";
        this.hideOrShowCloseIcon();
        this.invokeonGoingPayments();
      }.bind(this);
      var sectionData = {
        "flxOngoingPayments": {
          "isVisible": true
        },
        "flxUploadingFiles": {
          "isVisible": false
        },
        "flxPaymentHistory": {
          "isVisible": false
        },
        "btnDescription": {
          "text": "Description"
        },
		
		"imgSortDesc": this.dashboardSortParams.OnGoingPayments.Description,
        "imgDate": this.dashboardSortParams.OnGoingPayments.Date,
		"imgStatus": this.dashboardSortParams.OnGoingPayments.Status,
     
        "btnDate": {
          "text": "Date"
        },
        
        "btnStatus": {
          "text": "Status"
        },
       
        "lblActions": {
          "text": kony.i18n.getLocalizedString("i18n.wireTransfers.Actions")
        },
        "imgFlxBottomSeparator": {
          "text": "-"
        },
        "imgFlxTopSeparator": {
          "text": "-"
        },
		"flxDescription": {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            var img = this.dashboardSortParams.OnGoingPayments.Description;
            if(img === "sortingfinal.png")
            {
              img = "sorting_next.png";
              var order = "DESC";
            }
            else{
              img = (img === "sorting_next.png") ? "sorting_previous.png" : "sorting_next.png";
              var order = (img === "sorting_next.png") ? "DESC" : "ASC";
            }
            this.dashboardSortParams.OnGoingPayments.Description = img;
            this.fetchOngoingPayments("description", order);
          }.bind(this)
        },
        "flxStatus": {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            var img = this.dashboardSortParams.OnGoingPayments.Status;
            if(img === "sortingfinal.png")
            {
              img = "sorting_next.png";
              var order = "DESC";
            }
            else{
              img = (img === "sorting_next.png") ? "sorting_previous.png" : "sorting_next.png";
              var order = (img === "sorting_next.png") ? "DESC" : "ASC";
            }
            this.dashboardSortParams.OnGoingPayments.Status = img;
            this.fetchOngoingPayments("status", order);
          }.bind(this)
        }, 
        "flxDate": {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            var img = this.dashboardSortParams.OnGoingPayments.Date;
            if(img === "sortingfinal.png")
            {
              img = "sorting_next.png";
              var order = "DESC";
            }
            else{
              img = (img === "sorting_next.png") ? "sorting_previous.png" : "sorting_next.png";
              var order = (img === "sorting_next.png") ? "DESC" : "ASC";
            }
            this.dashboardSortParams.OnGoingPayments.Date = img;
            this.fetchOngoingPayments("paymentDate", order);
          }.bind(this)
        }, 
      };
      var defaultValues = {
        flxMain: {
          "height": "51dp"
        },
        flxBulkPaymentsViewHeader: {
          "skin": "bbSKnFlxffffff"
        },
        flxNoRecords: {
          "isVisible": false
        },
        flxBPViewHeader: {
          "isVisible": true,
          "skin": "bbSKnFlxffffff"
        },
		flxTopSeparator: {
           "isVisible": true
		},
		flxBottomSeperator: {
            "isVisible": true
        },
        flxOngoingPaymentsHeader: {
          "isVisible": true
        },
        flxOngoingPaymentsDetails: {
          "isVisible": true
        },
        flxUploadingFilesDetails: {
          "isVisible": false
        },
        flxUploadingFilesHeader: {
          "isVisible": false
        },
        flxPaymentHistoryHeader: {
          "isVisible": false
        },
        flxPaymentHistoryDetails: {
          "isVisible": false
        },
        imgDropDown: {
          "skin": "sknLblFontTypeIcon1a98ff12pxOther",
          "text": "O"
        },
        flxOpActions: {
          "onClick": function(eventobject, context) {
              this.onNavigatetoReviewDetails(eventobject, context);
         }.bind(this)
        },
      };
      var rowDataMap = {
        lblDate: "lblDate",
        lblOpDescription: "lblOpDescription",
        lblOpStatus: "lblOpStatus",
        lblOPFromAccount: "lblOPFromAccount",
        lblOPTotalTransactions: "lblOPTotalTransactions",
        lblOPTotalAmount: "lblOPTotalAmount",
        lblOPInitiatedBy: "lblOPInitiatedBy",
        lblOPPaymentID: "lblOPPaymentID",
        lblValueDate: "lblValueDate",
        lblOPFromAccountValue: "lblOPFromAccountValue",
        lblOPTotalTransactionsValue: "lblOPTotalTransactionsValue",
        lblOPTotalAmountValue: "lblOPTotalAmountValue",
        lblOPnitiatedByValue: "lblOPnitiatedByValue",
        lblOPPaymentIDValue: "lblOPPaymentIDValue",
        lblValueDateValue: "lblValueDateValue",
		flxTopSeperator: "flxTopSeperator",
		flxBottomSeperator: "flxBottomSeperator",
        lblOpActions: "lblOpActions",
	};

      this.view.TabPaneNew.TabBodyNew.setSectionData([sectionData]);
      this.view.TabPaneNew.TabBodyNew.setRowDataMap([rowDataMap]);
      this.view.TabPaneNew.TabBodyNew.setDefaultValues([defaultValues]);
      if (response.length === 0) {
		this.view.TabPaneNew.PaginationContainer.isVisible = false;
		this.showNoBulkPayments(kony.i18n.getLocalizedString("konybb.i18n.BulkPayments.noOngoingPayments"));
	  } 
      else {
        this.view.TabPaneNew.PaginationContainer.isVisible = true;
        this.updatePaginationContainerUI(response);
        if (response.length === this.view.TabPaneNew.PaginationContainer.getPageSize() + 1) {
          response.pop();
        }
        if(this.filterParams.OngoingPaymentSearch === ".") {
          this.view.TabPaneNew.PaginationContainer.isVisible = false;
        }
        this.view.TabPaneNew.TabBodyNew.addDataForSections([response]);
      }
 	  this.view.TabPaneNew.TabSearchBarNew.tbxSearch.placeholder = kony.i18n.getLocalizedString("i18n.konybb.SearchPlaceholder");
	  FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
      this.adjustScreen(50);
    },
    
    setUpUploadingFilesview: function(response) {
      FormControllerUtility.showProgressBar(this.view);
      this.view.TabPaneNew.TabSearchBarNew.flxSearch.width="99%";
      this.view.TabPaneNew.TabSearchBarNew.flxDropDown.isVisible=false;
      this.view.TabPaneNew.TabSearchBarNew.tbxSearch.text = this.fetchParams.searchString;
	  this.view.TabPaneNew.setSearchData([
        ["lblFileNameValue", "lblUploadedByValue", "lblSystemFileNameValue"]
      ]);
      this.view.TabPaneNew.TabSearchBarNew.tbxSearch.onDone = function() {
        this.fetchParams.pageSize = "";
        this.fetchParams.pageOffset = "";
        this.filterParams.UploadingFilesSearch = ".";
        this.view.TabPaneNew.PaginationContainer.isVisible = false;
        this.invokeUploadFiles();
      }.bind(this);
      this.view.TabPaneNew.TabSearchBarNew.imgClear.onTouchStart = function() {
        this.view.TabPaneNew.TabSearchBarNew.tbxSearch.text = "";
        this.hideOrShowCloseIcon();
        this.invokeUploadFiles();
      }.bind(this);
      var sectionData = {
        "flxOngoingPayments": {
          "isVisible": false
        },
        "flxUploadingFiles": {
          "isVisible": true
        },
        "flxPaymentHistory": {
          "isVisible": false
        },
        "btnFileDescription": {
          "text": "Description"
        },
       
        "imgSortFileDesc": this.dashboardSortParams.UploadedFiles.Description,
        "imgUFDate": this.dashboardSortParams.UploadedFiles.Date,
		"imgUFStatus": this.dashboardSortParams.UploadedFiles.Status,
        "flxFileDescription": {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            var img = this.dashboardSortParams.UploadedFiles.Description;
            if(img === "sortingfinal.png")
            {
              img = "sorting_next.png";
              var order = "DESC";
            }
            else{
              img = (img === "sorting_next.png") ? "sorting_previous.png" : "sorting_next.png";
              var order = (img === "sorting_next.png") ? "DESC" : "ASC";
            }
            this.dashboardSortParams.UploadedFiles.Description = img;
            this.fetchUploadedFiles("description", order);
          }.bind(this)
        },
        "flxUFStatus": {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            var img = this.dashboardSortParams.UploadedFiles.Status;
            if(img === "sortingfinal.png")
            {
              img = "sorting_next.png";
              var order = "DESC";
            }
            else{
              img = (img === "sorting_next.png") ? "sorting_previous.png" : "sorting_next.png";
              var order = (img === "sorting_next.png") ? "DESC" : "ASC";
            }
            this.dashboardSortParams.UploadedFiles.Status = img;
            this.fetchUploadedFiles("status", order);
          }.bind(this)
        },
        "flxUFDate": {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            var img = this.dashboardSortParams.UploadedFiles.Date;
            if(img === "sortingfinal.png")
            {
              img = "sorting_next.png";
              var order = "DESC";
            }
            else{
              img = (img === "sorting_next.png") ? "sorting_previous.png" : "sorting_next.png";
              var order = (img === "sorting_next.png") ? "DESC" : "ASC";
            }
            this.dashboardSortParams.UploadedFiles.Date = img;
            this.fetchUploadedFiles("uploadedDate", order);
          }.bind(this)
        },
        "btnUFDate": {
          "text": "Date"
        },
        "btnUFStatus": {
          "text": "Status "
        },
        "btnUFUploadID": {
          "text": "Upload ID"
        },
        "imgUFUploadID": {
          "isVisible": false
        },
        "imgFlxBottomSeparator": {
          "text": "-"
        },
        "imgFlxTopSeparator": {
          "text": "-"
        },
      };
      var defaultValues = {
        flxMain: {
          "height": "51dp"
        },
        flxBulkPaymentsViewHeader: {
          "skin": "bbSKnFlxffffff"
        },
        flxNoRecords: {
          "isVisible": false
        },
        flxBPViewHeader: {
          "isVisible": true,
          "skin": "bbSKnFlxffffff"
        },
		flxTopSeparator: {
           "isVisible": true
		},
		flxBottomSeperator: {
            "isVisible": true
        },
        flxOngoingPaymentsHeader: {
          "isVisible": false
        },
        flxOngoingPaymentsDetails: {
          "isVisible": false
        },
        flxUploadingFilesDetails: {
          "isVisible": true
        },
        flxUploadingFilesHeader: {
          "isVisible": true
        },
        flxPaymentHistoryHeader: {
          "isVisible": false
        },
        flxPaymentHistoryDetails: {
          "isVisible": false
        },
        imgDropDown: {
          "skin": "sknLblFontTypeIcon1a98ff12pxOther",
          "text": "O"
        },
      };
      var rowDataMap = {
        lblUfDate: "lblUfDate",
        lblFileDescription: "lblFileDescription",
        lblUfStatus: "lblUfStatus",
        lblUploadID: "lblUploadID",
        lblFileName: "lblFileName",
        lblUploadedBy: "lblUploadedBy",
        lblSystemFileName: "lblSystemFileName",

        lblFileNameValue: "lblFileNameValue",
        lblUploadedByValue: "lblUploadedByValue",
        lblSystemFileNameValue: "lblSystemFileNameValue",
		flxTopSeperator: "flxTopSeperator",
		flxBottomSeperator: "flxBottomSeperator",

      };
      
      this.view.TabPaneNew.TabBodyNew.setSectionData([sectionData]);
      this.view.TabPaneNew.TabBodyNew.setRowDataMap([rowDataMap]);
      this.view.TabPaneNew.TabBodyNew.setDefaultValues([defaultValues]);
      if (response.length === 0) {
		this.view.TabPaneNew.PaginationContainer.isVisible = false;
		this.showNoBulkPayments(kony.i18n.getLocalizedString("konybb.i18n.BulkPayments.noUploadedFiles"));
	  } 
      else {
        this.view.TabPaneNew.PaginationContainer.isVisible = true;
        this.updatePaginationContainerUI(response);
 		if (response.length === this.view.TabPaneNew.PaginationContainer.getPageSize() + 1) {
          response.pop();
        }
        if(this.filterParams.UploadingFilesSearch === ".") {
          this.view.TabPaneNew.PaginationContainer.isVisible = false;
        }
        this.view.TabPaneNew.TabBodyNew.addDataForSections([response]);
      }
      this.view.TabPaneNew.TabSearchBarNew.tbxSearch.placeholder = kony.i18n.getLocalizedString("i18n.konybb.SearchPlaceholder");
	  FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
      this.adjustScreen(50);
    },
    
    setUpPaymentHistoryview: function(response) {
      FormControllerUtility.showProgressBar(this.view);
      this.view.TabPaneNew.TabSearchBarNew.flxSearch.width="99%";
      this.view.TabPaneNew.TabSearchBarNew.flxDropDown.isVisible=false;
      this.view.TabPaneNew.TabSearchBarNew.tbxSearch.text = this.fetchParams.searchString;
      this.view.TabPaneNew.setSearchData([
        ["lblFromAccountValue", "lblInitiatedByValue", "lblPaymentIDValue", "lblPaymentStatusValue"]
      ]);
      this.view.TabPaneNew.TabSearchBarNew.tbxSearch.onDone = function() {
        this.fetchParams.pageSize = "";
        this.fetchParams.pageOffset = "";
        this.filterParams.PaymentHistorySearch = ".";
        this.view.TabPaneNew.PaginationContainer.isVisible = false;
        this.invokePaymentHistory();
      }.bind(this);
      this.view.TabPaneNew.TabSearchBarNew.imgClear.onTouchStart = function() {
      this.view.TabPaneNew.TabSearchBarNew.tbxSearch.text = "";
      this.hideOrShowCloseIcon();
      this.invokePaymentHistory();
      }.bind(this);
      var sectionData = {
        "flxOngoingPayments": {
          "isVisible": false
        },
        "flxUploadingFiles": {
          "isVisible": false
        },
        "flxPaymentHistory": {
          "isVisible": true
        },
        "btnPHDescription": {
          "text": "Description"
        },
        "btnPHDate": {
          "text": "Date"
        },
        "imgSortPHDesc": this.dashboardSortParams.PaymentHistory.Description,
        "imgPHDate": this.dashboardSortParams.PaymentHistory.Date,
        "imgPHStatus": this.dashboardSortParams.PaymentHistory.TotalAmount,
        "btnPHStatus": {
          "text": "Total Amount"
        },
        "lblPHActions": {
          "text": kony.i18n.getLocalizedString("i18n.wireTransfers.Actions")
        },
        "imgFlxBottomSeparator": {
          "text": "-"
        },
        "imgFlxTopSeparator": {
          "text": "-"
        },
        "flxPHDescription": {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            var img = this.dashboardSortParams.PaymentHistory.Description;
            if(img === "sortingfinal.png")
            {
              img = "sorting_next.png";
              var order = "DESC";
            }
            else{
              img = (img === "sorting_next.png") ? "sorting_previous.png" : "sorting_next.png";
              var order = (img === "sorting_next.png") ? "DESC" : "ASC";
            }
            this.dashboardSortParams.PaymentHistory.Description = img;
            this.fetchHistory("description", order);
          }.bind(this)
        },
        "flxPHStatus": {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            var img = this.dashboardSortParams.PaymentHistory.Status;
            if(img === "sortingfinal.png")
            {
              img = "sorting_next.png";
              var order = "DESC";
            }
            else{
              img = (img === "sorting_next.png") ? "sorting_previous.png" : "sorting_next.png";
              var order = (img === "sorting_next.png") ? "DESC" : "ASC";
            }
            this.dashboardSortParams.PaymentHistory.Status = img;
            this.fetchHistory("totalAmount", order);
          }.bind(this)
        },
        "flxPHDate": {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            var img = this.dashboardSortParams.PaymentHistory.Date;
            if(img === "sortingfinal.png")
            {
              img = "sorting_next.png";
              var order = "DESC";
            }
            else{
              img = (img === "sorting_next.png") ? "sorting_previous.png" : "sorting_next.png";
              var order = (img === "sorting_next.png") ? "DESC" : "ASC";
            }
            this.dashboardSortParams.PaymentHistory.Date = img;
            this.fetchHistory("paymentDate", order);
          }.bind(this)
        },
		
      };
      var defaultValues = {
        flxMain: {
          "height": "51dp"
        },
        flxBulkPaymentsViewHeader: {
          "skin": "bbSKnFlxffffff"
        },
        flxNoRecords: {
          "isVisible": false
        },
        flxBPViewHeader: {
          "isVisible": true,
          "skin": "bbSKnFlxffffff"
        },
		flxTopSeparator: {
           "isVisible": true
		},
		flxBottomSeperator: {
           "isVisible": true
        },
        flxOngoingPaymentsHeader: {
          "isVisible": false
        },
        flxOngoingPaymentsDetails: {
          "isVisible": false
        },
        flxUploadingFilesDetails: {
          "isVisible": false
        },
        flxUploadingFilesHeader: {
          "isVisible": false
        },
        flxPaymentHistoryHeader: {
          "isVisible": true
        },
        flxPaymentHistoryDetails: {
          "isVisible": true
        },
        imgDropDown: {
          "skin": "sknLblFontTypeIcon1a98ff12pxOther",
          "text": "O"
        },
        flxPhActions: {
          "onClick": function(eventobject, context) {
              this.onNavigatetoReviewDetails(eventobject, context);
          }.bind(this)
        },
        lblPhActions: {
          "isVisible": true,
          "text": "View Payment"
        },
      };
      var rowDataMap = {
        lblPhDate: "lblPhDate",
        lblPhDescription: "lblPhDescription",
        lblPhStatus: "lblPhStatus",
        lblFromAccount: "lblFromAccount",
        lblTotalTransactions: "lblTotalTransactions",
        lblTotalAmount: "lblTotalAmount",
        lblInitiatedBy: "lblInitiatedBy",
        lblPaymentID: "lblPaymentID",
		lblPaymentStatus: "lblPaymentStatus",
        lblFromAccountValue: "lblFromAccountValue",
        lblTotalTransactionsValue: "lblTotalTransactionsValue",
        lblTotalAmountValue: "lblTotalAmountValue",
        lblInitiatedByValue: "lblInitiatedByValue",
        lblPaymentIDValue: "lblPaymentIDValue",
		lblPaymentStatusValue: "lblPaymentStatusValue",
		flxTopSeperator: "flxTopSeperator",
		flxBottomSeperator: "flxBottomSeperator",
      };
      
      this.view.TabPaneNew.TabBodyNew.setSectionData([sectionData]);
      this.view.TabPaneNew.TabBodyNew.setRowDataMap([rowDataMap]);
      this.view.TabPaneNew.TabBodyNew.setDefaultValues([defaultValues]);
      if (response.length === 0) {
		this.view.TabPaneNew.PaginationContainer.isVisible = false;
		this.showNoBulkPayments(kony.i18n.getLocalizedString("konybb.i18n.BulkPayments.noPaymentHistory"));
	  } 
      else {
        this.view.TabPaneNew.PaginationContainer.isVisible = true;
        this.updatePaginationContainerUI(response);
        if (response.length === this.view.TabPaneNew.PaginationContainer.getPageSize() + 1) {
          response.pop();
        }
        if(this.filterParams.PaymentHistorySearch === ".") {
          this.view.TabPaneNew.PaginationContainer.isVisible = false;
        }
        this.view.TabPaneNew.TabBodyNew.addDataForSections([response]);
      }
      this.view.TabPaneNew.TabSearchBarNew.tbxSearch.placeholder = kony.i18n.getLocalizedString("i18n.konybb.SearchPlaceholder");
	  FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
      this.adjustScreen(50);
    },
    
    onNavigatetoReviewDetails: function(eventobject, context) {
	  var row = context.rowIndex;
      var section = context.sectionIndex;
      var selectedRowData = this.view.TabPaneNew.TabBodyNew.getData()[section][1][row];
      var statusText;
      var btnText;
      if (!(kony.sdk.isNullOrUndefined(selectedRowData.lblPhStatus))) {
     	btnText=BBConstants.MAKER_VIEW_PAYMENT_HISTORY;
      } 
      else 
      {
     	statusText = selectedRowData.lblOpStatus.text;
        if((statusText === BBConstants.TRANSACTION_STATUS.PENDING_FOR_AUTH) || (statusText === BBConstants.TRANSACTION_STATUS.CANCELLED) || (statusText === BBConstants.TRANSACTION_STATUS.DISCARDED))
      {
          btnText=BBConstants.MAKER_VIEW_PAYMENT;
      }
        else
      {
          btnText=BBConstants.REVIEW_PAYMENT;
      }
      }
      var BulkPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BulkPayments");
      BulkPaymentsModule.presentationController.noServiceNavigate("frmBulkPaymentsReview", btnText,selectedRowData);
    },
     
    showBulkPaymentsDashboardSelectedTab(tabIndex, menuTabName, contentHeaderName) {
      FormControllerUtility.showProgressBar(this.view);
      this.view.customheader.customhamburger.activateMenu(kony.i18n.getLocalizedString("i18n.kony.BulkPayments.bulkPaymentHeader"), contentHeaderName);
      this.view.TabPaneNew.TabsHeaderNew.focusTab(tabIndex);
      this.loadDashboard();
      this.view.forceLayout();
      this.adjustScreen(0);
    },
    
	setDropdownVisiblility: function() {
		if (this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.origin) {
			this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.origin=false;
			return;
		}
      var obj_arr = [];
      obj_arr = [{
        "flxSeparator":{
          "isVisible": false
        },
        "lblViewTypeName": {
          text: "All",
          }
      }, {
        "flxSeparator":{
          "isVisible": false
        },
        "lblViewTypeName": {
          text: "Created",
         }
      }, {
        "flxSeparator":{
          "isVisible": false
        },
        "lblViewTypeName": {
          text: "Pending For Authorization",
         }
      }, { "flxSeparator":{
        "isVisible": false
      },
          "lblViewTypeName": {
            text: "Rejected",
            }
         }, {
           "flxSeparator":{
             "isVisible": false
           },
           "lblViewTypeName": {
             text: "Cancelled",
           }
         }, {
           "flxSeparator":{
             "isVisible": false
           },
           "lblViewTypeName": {
             text: "Processing Payments",
           }
      }];
      this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.segViewTypes.setData(obj_arr);
      if (!this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.isVisible) {
        this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.clipBounds = false;
        this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.clipBounds = false;
        this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.isVisible = true;
        this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.segViewTypes.setVisibility(true);
        this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxIphoneDropdown.flxImage.setVisibility(true);
        this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.imgDropdown.centerX = "50%";
        this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxIphoneDropdown.flxImage.imgDropdown.src = "listboxdownarrow.png";
      } else {
        this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.isVisible = false;
        this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.segViewTypes.setVisibility(false);
        this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.imgDropdown.centerX = "50%";
        this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxIphoneDropdown.flxImage.imgDropdown.src = "listboxuparrow.png";
      }
    },

    segViewTypesRowClick: function(lblvalue) {
      this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxIphoneDropdown.lblViewType.text = lblvalue;
      this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxDropdown.setVisibility(false);
      this.view.TabPaneNew.TabSearchBarNew.MobileCustomDropdown.flxIphoneDropdown.flxImage.imgDropdown.src = "listboxdownarrow.png";
      if (lblvalue !== "All") {
        this.fetchParams.filterByParam = "status";
        if (lblvalue == "Created") {
          this.fetchParams.filterByValue = "Created";
        }
        if (lblvalue == "Pending For Authorization") {
          this.fetchParams.filterByValue = "Pending";
        }
        if (lblvalue == "Rejected") {
          this.fetchParams.filterByValue = "Rejected";
        }
        if (lblvalue == "Cancelled") {
          this.fetchParams.filterByValue = "Cancelled";
        }
        if (lblvalue == "Processing Payments") {
          this.fetchParams.filterByValue = "Processing";
        }
        this.filterParams.OngoingPayments=".";
      } else {
        this.fetchParams.filterByParam = "";
        this.fetchParams.filterByValue = "";
      }
      this.invokeonGoingPayments();
    },
    
    setPaginationComponentForOngoingPayments: function(pageHeader) {
      this.view.TabPaneNew.PaginationContainer.setPageSize(BBConstants.PAGE_SIZE);
      this.view.TabPaneNew.PaginationContainer.setPageHeader(pageHeader);
	  this.view.TabPaneNew.PaginationContainer.setLowerLimit(1);
      this.view.TabPaneNew.PaginationContainer.setServiceDelegate(this.fetchOngoingPayments);
      this.view.TabPaneNew.PaginationContainer.setIntervalHeaderForBulkpayments();
    },

    setPaginationComponentForUploadedFiles: function(pageHeader) {
      this.view.TabPaneNew.PaginationContainer.setPageSize(BBConstants.PAGE_SIZE);
      this.view.TabPaneNew.PaginationContainer.setPageHeader(pageHeader);
	  this.view.TabPaneNew.PaginationContainer.setLowerLimit(1);
      this.view.TabPaneNew.PaginationContainer.setServiceDelegate(this.fetchUploadedFiles);
      this.view.TabPaneNew.PaginationContainer.setIntervalHeaderForBulkpayments();
    },

    setPaginationComponentForPaymentHistory: function(pageHeader) {
      this.view.TabPaneNew.PaginationContainer.setPageSize(BBConstants.PAGE_SIZE);
      this.view.TabPaneNew.PaginationContainer.setPageHeader(pageHeader);
	  this.view.TabPaneNew.PaginationContainer.setLowerLimit(1);
      this.view.TabPaneNew.PaginationContainer.setServiceDelegate(this.fetchHistory);
      this.view.TabPaneNew.PaginationContainer.setIntervalHeaderForBulkpayments();
    },

 	setupPermissionBasedView: function () {
      this.view.TabPaneNew.TabsHeaderNew.btnTab1.isVisible = this.checkAtLeastOnePermission(applicationManager.getConfigurationManager().getBulkPaymentRequestViewPermissionsList()) ;
      this.view.TabPaneNew.TabsHeaderNew.btnTab2.isVisible = this.checkAtLeastOnePermission(applicationManager.getConfigurationManager().getBulkPaymentFilesViewPermissionsList()) ;
      this.view.TabPaneNew.TabsHeaderNew.btnTab3.isVisible = this.checkAtLeastOnePermission(applicationManager.getConfigurationManager().getBulkPaymentRequestViewPermissionsList()) ;
      this.view.TabPaneNew.TabsHeaderNew.btnTab4.isVisible = false;
    },

	fetchOngoingPayments: function (sortParam, sortOrder) {
      this.view.TabPaneNew.TabSearchBarNew.tbxSearch.onDone = function() {
        this.fetchParams.pageSize = "";
        this.fetchParams.pageOffset = "";
        this.filterParams.OngoingPaymentSearch = ".";
        this.view.TabPaneNew.PaginationContainer.isVisible = false;
        this.invokeonGoingPayments();
      }.bind(this);
      this.updateFetchParams(sortParam, sortOrder);
      this.bulkPaymentsModule.presentationController.fetchOnGoingPayments(this.fetchParams);
    },

	fetchUploadedFiles: function (sortParam, sortOrder) {
      this.view.TabPaneNew.TabSearchBarNew.tbxSearch.onDone = function() {
        this.fetchParams.pageSize = "";
        this.fetchParams.pageOffset = "";
        this.filterParams.UploadingFilesSearch = ".";
        this.view.TabPaneNew.PaginationContainer.isVisible = false;
        this.invokeUploadFiles();
      }.bind(this);
      this.updateFetchParams(sortParam, sortOrder);
      this.bulkPaymentsModule.presentationController.fetchUploadedFiles(this.fetchParams);
    },

	fetchHistory: function (sortParam, sortOrder) {
      this.view.TabPaneNew.TabSearchBarNew.tbxSearch.onDone = function() {
        this.fetchParams.pageSize = "";
        this.fetchParams.pageOffset = "";
        this.filterParams.PaymentHistorySearch = ".";
        this.view.TabPaneNew.PaginationContainer.isVisible = false;
        this.invokePaymentHistory();
      }.bind(this);
	  this.updateFetchParams(sortParam, sortOrder);
      this.bulkPaymentsModule.presentationController.fetchHistory(this.fetchParams);
    },

	updateFetchParams : function(sortParam, sortOrder){
      this.fetchParams.searchString = CommonUtilities.validateSearchString(this.view.TabPaneNew.TabSearchBarNew.tbxSearch.text);
      if(!kony.sdk.isNullOrUndefined(sortParam))
        this.fetchParams.sortByParam = sortParam;
      if(!kony.sdk.isNullOrUndefined(sortOrder))
      this.fetchParams.sortOrder = sortOrder;
      if (!((this.filterParams.OngoingPaymentSearch === ".") || (this.filterParams.UploadingFilesSearch === ".") || (this.filterParams.paymentHistorySearch === "."))) {
        this.fetchParams.pageSize = this.view.TabPaneNew.PaginationContainer.getPageSize() + 1;
        this.fetchParams.pageOffset = this.view.TabPaneNew.PaginationContainer.getPageOffset();
      }
      if (!(this.activeTab === kony.i18n.getLocalizedString("i18n.kony.BulkPayments.onGoingPayments")) && (this.filterParams.OngoingPayments === ".")) {
        this.fetchParams.filterByParam = "";
        this.fetchParams.filterByValue = "";
      }
    },

	showServerErrorMessage: function (context) {
          if (context.show) {
              this.view.flxMainWrapper.setVisibility(true);
              this.view.lblDowntimeWarning.text = context.errorMessage ||  kony.i18n.getLocalizedString(context.errMsgi18nKey || "i18n.common.OoopsServerError");
              this.view.flxDowntimeWarning.setFocus();
          } else {
              this.view.flxMainWrapper.setVisibility(false);
          }
          FormControllerUtility.hideProgressBar(this.view);
          this.adjustScreen(30);
    },

	updatePaginationContainerUI: function(responseData) {
            var isMaxLimitReached = responseData.length <= this.view.TabPaneNew.PaginationContainer.getPageSize();
            this.view.TabPaneNew.PaginationContainer.setIsMaxLimitReached(isMaxLimitReached);
            this.view.TabPaneNew.PaginationContainer.updateUI();
    },
    
    showNoBulkPayments: function(msgText) {
      var dataMap = 
          {
            lblNoRecords: "lblMsg"
          };
      var NODATAFLEXHEIGHT = "450dp";
      var defValues =
          {
            flxMain: 
            {
              "height" : NODATAFLEXHEIGHT
            },
            flxBPViewHeader: 
            {
              "isVisible" : false
            },
            flxBPTemplateDetails: 
            {
              "isVisible" : false
            },
            flxNoRecords: 
            {
              "isVisible": true
            }
          };
      this.view.TabPaneNew.TabBodyNew.setRowDataMap([dataMap]);
      this.view.TabPaneNew.TabBodyNew.setDefaultValues([defValues]);
      this.view.TabPaneNew.TabBodyNew.addRowsData([[{"lblMsg": msgText}]]);
    },
  }
});