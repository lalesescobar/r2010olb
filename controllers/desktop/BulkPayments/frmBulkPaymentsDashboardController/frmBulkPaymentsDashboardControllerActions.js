define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onDeviceBack defined for frmBulkPaymentsDashboard **/
    AS_Form_he4b41b8533547eeaa9fad84d541b03c: function AS_Form_he4b41b8533547eeaa9fad84d541b03c(eventobject) {
        kony.print("Back Button Pressed");
    },
    /** preShow defined for frmBulkPaymentsDashboard **/
    AS_Form_dc483c4888974b7399d675773163e055: function AS_Form_dc483c4888974b7399d675773163e055(eventobject) {
        return this.onPreShow();
    },
    /** postShow defined for frmBulkPaymentsDashboard **/
    AS_Form_c6caedd2f16c41898fac709fcb86790e: function AS_Form_c6caedd2f16c41898fac709fcb86790e(eventobject) {
        this.onPostShow();
    },
    /** init defined for frmBulkPaymentsDashboard **/
    AS_Form_cd46a142022b49b7806ae44e9938c720: function AS_Form_cd46a142022b49b7806ae44e9938c720(eventobject) {
        this.onInit();
    },
    /** onTouchEnd defined for frmBulkPaymentsDashboard **/
    AS_Form_h3b597790b1d433aaf598000f1ce5617: function AS_Form_h3b597790b1d433aaf598000f1ce5617(eventobject, x, y) {
        hidePopups();
    }
});