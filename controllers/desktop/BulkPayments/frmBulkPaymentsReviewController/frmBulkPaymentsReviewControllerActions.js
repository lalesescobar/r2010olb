define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmBulkPaymentsReview **/
    AS_Form_d73a055aaedf48e6907a1a372f42e78d: function AS_Form_d73a055aaedf48e6907a1a372f42e78d(eventobject) {
        var self = this;
        this.onInit();
    },
    /** preShow defined for frmBulkPaymentsReview **/
    AS_Form_i5e7f04ab3f74cfebc1f92cd9cf46ac5: function AS_Form_i5e7f04ab3f74cfebc1f92cd9cf46ac5(eventobject) {
        var self = this;
        return this.onPreShow();
    },
    /** postShow defined for frmBulkPaymentsReview **/
    AS_Form_gf55215895974e91987f0dbb71552dbe: function AS_Form_gf55215895974e91987f0dbb71552dbe(eventobject) {
        var self = this;
        this.onPostShow();
    },
    /** onDeviceBack defined for frmBulkPaymentsReview **/
    AS_Form_a0745c8d7fb84daabe97fc2da77944f6: function AS_Form_a0745c8d7fb84daabe97fc2da77944f6(eventobject) {
        var self = this;
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmBulkPaymentsReview **/
    AS_Form_c27f34c779fe4459a0570f2e7167b0ae: function AS_Form_c27f34c779fe4459a0570f2e7167b0ae(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});