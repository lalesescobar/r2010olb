define(['CommonUtilities', 'ViewConstants', 'OLBConstants','FormControllerUtility','CampaignUtility'], function (CommonUtilities, ViewConstants,OLBConstants,FormControllerUtility,CampaignUtility) {
  
 var orientationHandler = new OrientationHandler();
  
  return {

	/** Global Variables **/
	bulkPaymentsModule: null, 
	isEditFlow :"",
	btnTextApprovalReq : "",
	bulkPaymentRecordData : {},
	isMakerFlow : 1,   
	isMakerHistoryFlow : 0, 
    fetchParams: {},
    paymentOrdersData:{},
    isackFlow:"",
    ackMsg:"",
    dashboardSortParams: {},
    
    /**
     * Method to update form using given context
     * @param {object} context depending on the context the appropriate function is executed to update view
     */
    
    updateFormUI : function (context) {
      if (context.progressBar === true) {
        FormControllerUtility.showProgressBar(this.view);
      }
      else if (context.progressBar === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
      else if (context.key === BBConstants.REVIEW_PAYMENT) {
        this.isMakerFlow = 1;
        this.isEditFlow = false;
		this.isMakerHistoryFlow = 0;
		this.btnTextApprovalReq = kony.i18n.getLocalizedString("i18n.stopChecks.ViewRequests");
        this.bulkPaymentRecordData = context.responseData; 
        this.fetchBulkPaymentOrders(context.responseData); 
	 }
      else if (context.key === BBConstants.MAKER_VIEW_PAYMENT) {
      	this.isMakerFlow = 1;
      	this.isEditFlow = true;
		this.isMakerHistoryFlow = 0;
		this.btnTextApprovalReq = kony.i18n.getLocalizedString("i18n.stopChecks.ViewRequests");
      	this.bulkPaymentRecordData = context.responseData;
      	this.fetchBulkPaymentOrders(context.responseData);         
     }  
	  else if (context.key === BBConstants.MAKER_VIEW_PAYMENT_HISTORY) {
        this.isMakerFlow = 1;
        this.isEditFlow = true;
        this.isMakerHistoryFlow = 1;
        this.btnTextApprovalReq = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.backtoPaymentsHistory");
        this.bulkPaymentRecordData = context.responseData;
        this.fetchBulkPaymentOrders(context.responseData);
      }
      else if (context.key === BBConstants.APPROVER_VIEW_PAYMENT) {		
      	this.isMakerFlow = 0;
      	this.isEditFlow = true;
      	if(context.responseData.isHistoryFlow){
      		this.btnTextApprovalReq = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.backToApprovalHistory");      		
      	}else{
      		this.btnTextApprovalReq = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.backToPendingApprovals");
      	}        	
		this.isMakerHistoryFlow = 0;      	
      	this.bulkPaymentRecordData = context.responseData; 
      	this.fetchBulkPaymentOrders(context.responseData);		
      }
      else if (context.key === BBConstants.REQUESTS_VIEW_PAYMENT) {      	
      	this.isMakerFlow = 0;
      	this.isEditFlow = true;
      	if(context.responseData.isHistoryFlow){
      		this.btnTextApprovalReq = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.backToRequestHistory");      		
      	}else{
      		this.btnTextApprovalReq = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.backToPendingRequests");
      	}      	
		this.isMakerHistoryFlow = 0;      	
      	this.bulkPaymentRecordData = context.responseData;
		this.fetchBulkPaymentOrders(context.responseData);
      }    
      else if(context.fetchPaymentOrdersSuccess){
        this.paymentOrdersData=context.fetchPaymentOrdersSuccess;
      	if(this.isMakerFlow){
      		this.getRecipientData(this.isEditFlow,this.btnTextApprovalReq);
            this.setAcknowledgementMessage();
      	} else{
      		this.onViewPaymentsScreenForApprovalDashboard(this.isEditFlow,this.btnTextApprovalReq);
      	}
      }      
      else if (context.getExistingBeneficiariesSuccess) {
		 this.mapBeneficiaryListingData(context.getExistingBeneficiariesSuccess);
	  }   
      else if (context.getExistingBeneficiariesFailure) {
        this.showBeneficiaryListingErrorMessage(context.getExistingBeneficiariesFailure.errorMessage);
      }
      else if (context.serverError === true) {
		  if (context.closePopup === true) {
            FormControllerUtility.hideProgressBar(this.view);
			this.view.flxCancelPopup.setVisibility(false);
            this.adjustScreen(0);
		  }
          this.showServerErrorMessage({ show: true , errorMessage: context.errorMessage});

      }
      else if(context.updateBulkPaymentRecordSuccess) {
      	this.onupdateBulkPaymentRecordSuccess(context.updateBulkPaymentRecordSuccess);
      } 
      else if (context.cancelBulkPaymentRecordSuccess) {
        this.oncancelBulkPaymentRecordSuccess(context.cancelBulkPaymentRecordSuccess);
	  }       
      else if (context.addPaymentOrderSuccess) {
        this.fetchBulkPaymentRecordDetailsById();
        isackFlow=true;
        ackMsg=kony.i18n.getLocalizedString("i18n.common.paymentSucessMsg");
      } 
      else if (context.deletePaymentOrderSuccess) {
        this.fetchBulkPaymentRecordDetailsById();
        isackFlow=true;
        ackMsg=kony.i18n.getLocalizedString("kony.i18n.common.ItemRemoved");
      } 
      else if (context.editPaymentOrderSuccess) {
        this.fetchBulkPaymentRecordDetailsById();
        isackFlow=true;
        ackMsg=kony.i18n.getLocalizedString("kony.i18n.common.recipientUpdate");
      } 
      else if(context.submitPaymentOrderSuccess){
        this.onSubmitForApproval(context.submitPaymentOrderSuccess);
      }
      else if(context.onApproveSuccess){
        this.showPaymentApproveRejectaAcknowledgment(context.onApproveSuccess, BBConstants.TRANSACTION_STATUS.APPROVED);
      }
      else if(context.onRejectSuccess){
        this.showPaymentApproveRejectaAcknowledgment(context.onRejectSuccess, BBConstants.TRANSACTION_STATUS.REJECTED);
      }
      else if(context.onRequestsHistorySuccess){
      	this.showRequestHistoryData(context.onRequestsHistorySuccess.RequestHistory);
      }
       else if(context.fetchBulkPaymentRecordDetailsByIdSuccess){
         this.bulkPaymentRecordData.totalAmount =context.fetchBulkPaymentRecordDetailsByIdSuccess.totalAmount;
         this.bulkPaymentRecordData.totalTransactions =context.fetchBulkPaymentRecordDetailsByIdSuccess.totalTransactions;
         this.fetchBulkPaymentOrders(this.bulkPaymentRecordData);
      }

      this.view.forceLayout();
      this.adjustScreen(10);
    },
    
    /**
     * onPreShow :  onPreshow event Function for the form
     * @member of {frmBulkPaymentsDashboardController}
     * @param {}
     * @return {}
     * @throws {}
     */
    
    onPreShow : function () {
      
      var scopeObj = this;
      this.view.customheader.forceCloseHamburger();
      this.view.customheader.customhamburger.activateMenu(kony.i18n.getLocalizedString("i18n.kony.BulkPayments.bulkPaymentHeader"),
                                                          kony.i18n.getLocalizedString("i18n.stopChecks.ViewRequests"));
      applicationManager.getNavigationManager().applyUpdates(this);
      this.view.onBreakpointChange = function () {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      };      
      this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX_POINTER;
      this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX_POINTER;
      this.view.flxPaymentReview.NonEditableBulkPaymentDetails.flxTitle.btnEdit.onClick=this.onEditReviewClick.bind(this);
      
      var onSegReload = function() {
        scopeObj.view.forceLayout();
        scopeObj.adjustScreen(10);
      };     
      this.view.TabBodyNew.setSegmentReloadAction(onSegReload);
      this.view.TabBodyNew.setExpandableRowHeight(202);
      this.view.TabBodyNew1.setSegmentReloadAction(onSegReload);
      this.view.TabBodyNew1.setExpandableRowHeight(202);
      this.initializeFetchParams();
      this.initializeDashboardSortParams();
      isackFlow=false;
	  this.view.forceLayout();  
      this.adjustScreen(30);      
    },
    
    /**
     * onPostShow :  postShow event Function for the form
     * @member of {frmBulkPaymentsDashboardController}
     * @param {}
     * @return {}
     * @throws {}
     */
    
    onPostShow : function () {      
      this.view.forceLayout();
      this.adjustScreen(10);       
    },

    /**
     * onInit : onInit event Function for the form
     * @member of {frmBulkPaymentsDashboardController}
     * @param {}
     * @return {}
     * @throws {}
     */
    
    onInit: function () {      
      FormControllerUtility.updateWidgetsHeightInInfo(this.view, ['customheader', 'flxMain', 'flxHeader', 'flxFooter','flxContentContainer','flxHeaderMain']);
      this.bulkPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BulkPayments");
    },  
   
    
    /**
     * onBreakpointChange : Handles ui changes on .
     * @member of {frmACHDashboardController}
     * @param {integer} width - current browser width
     * @return {}
     * @throws {}
     */
    
    onBreakpointChange: function (width) {      
      orientationHandler.onOrientationChange(this.onBreakpointChange); 
      var break_point = kony.application.getCurrentBreakpoint();
      var scope = this;
      var responsiveFonts = new ResponsiveFonts();
      this.view.customheader.onBreakpointChangeComponent(width);  
      this.adjustScreen(10);
    },
    
    /**
     * adjustScreen : Handles ui changes based on the screen size
     * @member of {frmBulkPaymentsDashboardController}
     * @param {integer} data - difference to be added to the screen
     * @return {}
     * @throws {}
     */
    
    adjustScreen : function (data) {  
      this.view.flxFooter.isVisible = true;
      this.view.forceLayout();
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.info.frame.height + this.view.flxContentContainer.info.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.info.frame.height;
        if (diff > 0)
          this.view.flxFooter.top = mainheight + diff + data + "dp";
        else
          this.view.flxFooter.top = mainheight + data + "dp";
        this.view.forceLayout();
      } else {
        this.view.flxFooter.top = mainheight + data + "dp";
        this.view.forceLayout();
      }
    },
    
    /**
     * resetUI : function that hides all the UI flexes in frmBulkPaymentReview    
     */


    resetUI: function() {
      this.view.flxAcknowledgementContainer.isVisible = false;
      this.view.flxErrorMessage.isVisible = false;
      this.view.flxAddRecipientDetails.isVisible = false;
      this.view.flxApprovalsHistoryInformation.isVisible = false;  
      this.view.flxAddRecipientsManually.isVisible = false;  
      this.view.flxPaymentReview.isVisible = false;
      this.view.flxAckMessage.isVisible = false;
      this.view.flxPopupConfirmation.isVisible = false;
      this.view.flxDisplayErrorMessage.isVisible = false;

      var breakpoint = kony.application.getCurrentBreakpoint();
      if(breakpoint <= 1024 || orientationHandler.isTablet)
      {
        this.view.CommonFormActionsNew.btnOption.width = "20%";
        this.view.CommonFormActionsNew.btnCancel.width = "20%";
        this.view.CommonFormActionsNew.btnNext.width = "20%";
        this.view.CommonFormActionsNew.btnBack.width = "20%";
      }

      else
      {
        this.view.CommonFormActionsNew.btnOption.width = "13%";
        this.view.CommonFormActionsNew.btnCancel.width = "13%";
        this.view.CommonFormActionsNew.btnNext.width = "13%";
        this.view.CommonFormActionsNew.btnBack.width = "13%";
      }
      this.view.CommonFormActionsNew.isVisible = true;
      this.view.formActionsNew.isVisible = false;
      this.view.flxAddPayment.isVisible=true;
      this.view.TabBodyNew1.isVisible = true;
      this.view.flxSearchRecipients.isVisible = true;
      this.view.CommonFormActionsExt.isVisible = false;
      this.view.PaginationContainer.isVisible = false;
      this.view.flxMainWrapper.setVisibility(false);

      this.adjustScreen(10);
      this.view.forceLayout();
    },

    resetRecipientsUI: function() {
      this.view.flxAddType.isVisible = true;
      this.view.flxBankType.isVisible = true;
      this.view.flxRecipientDetailsInfinity.isVisible = true;
      this.view.flxRecipientDetailsExternal.isVisible = true;  
      this.view.flxRecipientDetailsExisting.isVisible = true;  
      this.view.flxAmount.isVisible = true;
      this.view.flxRecipientName.isVisible = true; 
      this.view.flxFeesPaid.isVisible = true; 
      this.view.flxPaymentRef.isVisible = true; 
      this.view.flxAddToList.isVisible = false;
      this.view.flxInfo.isVisible = true;
       this.view.flxSwiftExisting.isVisible = true;
      
      var breakpoint = kony.application.getCurrentBreakpoint();
      if (breakpoint <= 1024 || orientationHandler.isTablet) {
        this.view.flxPaymentRef.bottom="0dp";
      }
      this.adjustScreen(10);
      this.view.forceLayout();
    },
    
    clearRecipientsFields: function() {
      this.view.tbxRecipAccNumber.text="";
      this.view.tbxRecipAccNumberExt.text="";
      this.view.tbxSwift.text="";
      this.view.tbxRecipientBankName.text="";
      this.view.tbxAmount.text="";
      var formatManager = applicationManager.getFormatUtilManager();
      this.view.tbxCurrency.text = formatManager.getCurrencySymbol(this.bulkPaymentRecordData.currency);
      this.view.tbxCurrency.setEnabled(false);
      this.view.tbxRecipientName.text="";
      this.view.tbxPaymentRef.text="";
      this.OnRadioBtnClickFeesPaidBy(1);
      this.view.lblSelect.text = OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED;
      this.adjustScreen(10);
      this.view.forceLayout();
    },
         
   
    initializeFetchParams: function() {
      this.fetchParams = {
        "limit": 10,
        "offset": 0,
        "order": "desc",
        "resetSorting": true,
        "sortBy": "createdOn"
      };

    },
    
    initializeDashboardSortParams: function() {
      this.dashboardSortParams = {
        "beneficiaryListing": {
          "Name": "sortingfinal.png",
          "Bank": "sortingfinal.png",
          "Type": "sortingfinal.png"
        }        
      };
    },
    
    updateFetchParams : function(sortParam, sortOrder){
     this.fetchParams.searchString = CommonUtilities.validateSearchString(this.view.tbxSearch.text);
      if(!kony.sdk.isNullOrUndefined(sortParam))
        this.fetchParams.sortByParam = sortParam;
      if(!kony.sdk.isNullOrUndefined(sortOrder))
        this.fetchParams.sortOrder = sortOrder;
      
      if(!kony.sdk.isNullOrUndefined(sortParam))
        this.fetchParams.sortBy = sortParam;
      if(!kony.sdk.isNullOrUndefined(sortOrder))
        this.fetchParams.order = sortOrder;
      this.fetchParams.pageSize = this.view.PaginationContainer.getPageSize() + 1;
      this.fetchParams.pageOffset = this.view.PaginationContainer.getPageOffset();
      this.fetchParams.offset = this.view.PaginationContainer.getPageOffset();
      
      this.fetchParams.filterByParam = "";
      this.fetchParams.filterByValue = "";
    },

    showServerErrorMessage: function(context) {
      if (context.show) {                
        this.view.flxDisplayErrorMessage.setVisibility(true);
        this.view.lblDisplayError.text = context.errorMessage || kony.i18n.getLocalizedString(context.errMsgi18nKey || "i18n.common.OoopsServerError");
        this.view.lblDisplayError.setFocus();
      } else {
        this.view.flxDisplayErrorMessage.setVisibility(false);
      }
      this.hidePopup();
      FormControllerUtility.hideProgressBar(this.view);
      this.adjustScreen(30);
    },

    
    setAcknowledgementMessage: function() {
     
      if(isackFlow===true){       
        this.view.flxAcknowledgementContainer.isVisible = true;  
        this.view.flxAcknowledgementNew.rTextSuccess.skin="sknRtxSSPLight42424224Px";
        this.view.flxAcknowledgementNew.rTextSuccess.text =ackMsg;
        this.view.flxAcknowledgementNew.flxImgdownload.onTouchEnd= function() {
          var scopeObj = this;
          scopeObj.view.flxAcknowledgementContainer.setVisibility(false);
          this.view.forceLayout();
        }.bind(this);
        
        isackFlow=false;
        this.adjustScreen(10);
        this.view.forceLayout();
      }
    },    
         
       
    mapBenificiaryData: function(isEditFlow,context) {
      if(!kony.sdk.isNullOrUndefined(isEditFlow)&&isEditFlow===true){
        var data = this.view.TabBodyNew1.getData()[context.sectionIndex][1];
        this.view.lblNameValue.text = data[context.rowIndex].lblViewRecipientName.text;
        this.view.lblAccountValue.text = data[context.rowIndex].accountNumber;
        this.view.lblBankNameValue.text = data[context.rowIndex].lblSwiftCodeValue.text;
        this.view.tbxAmount.text=data[context.rowIndex].amount;    
        if(!kony.sdk.isNullOrUndefined(data[context.rowIndex].paymentReference))
        {
          this.view.tbxPaymentRef.text = data[context.rowIndex].paymentReference;
        } 
        
        if (data[context.rowIndex].feesPaidBy === BBConstants.SHARED) {
          this.OnRadioBtnClickFeesPaidBy(3);
        } else if (data[context.rowIndex].feesPaidBy === BBConstants.BENEFICIARY){
          this.OnRadioBtnClickFeesPaidBy(2);
        }
        else{
          this.OnRadioBtnClickFeesPaidBy(1);
        }
        
        if (data[context.rowIndex].lblPaymentMethodValue.text === BBConstants.INTERNAL) {
          this.view.flxSwiftExisting.isVisible = false;
        } else {
          this.view.flxSwiftExisting.isVisible = true;
          this.view.lblSwiftValue.text = data[context.rowIndex].lblAccTypeValue.text;
        }
      }
      else{
        var data = this.view.TabBodyNew.getData()[0][1];
        var selectedRow = "";
        for (var i = 0; i < data.length; i++) {
          if (data[i].lblSelect.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) {
            selectedRow = i;
          }
        }
        this.view.lblNameValue.text = data[selectedRow].beneficiaryName;
        this.view.lblAccountValue.text = data[selectedRow].lblAccountNoValue.text;
        this.view.lblBankNameValue.text = data[selectedRow].bankName;
        if (data[selectedRow].lblAccountType.text === "Internal") {
          this.view.flxSwiftExisting.isVisible = false;
          this.view.lblSwiftValue.text = "";
        } else {
          this.view.flxSwiftExisting.isVisible = true;
          this.view.lblSwiftValue.text = data[selectedRow].lblAccTypeValue.text;
        }
      }
    },

    /**
     * function to display the add recipients screen  on continue
     */

    onClickContinueAddExistingRecipients : function() {
      this.resetUI();
      this.clearRecipientsFields();
      this.resetRecipientsUI();
      this.mapBenificiaryData();
      this.view.flxAddRecipientsManually.isVisible = true;
      this.view.lblContentHeader.text = kony.i18n.getLocalizedString("i18n.common.AddPaymentHeader");
      this.view.lblAddHeader.text = kony.i18n.getLocalizedString("i18n.common.PaymentDetails");   
      this.view.flxAddType.isVisible = false;
      this.view.flxBankType.isVisible = false;
      this.view.flxRecipientDetailsInfinity.isVisible = false;
      this.view.flxRecipientDetailsExternal.isVisible = false;
      this.view.flxRecipientName.isVisible = false;
      this.view.flxAddToList.isVisible = false;
      this.view.CommonFormActionsNew.btnBack.isVisible = true;
      this.view.CommonFormActionsNew.btnBack.text = kony.i18n.getLocalizedString("i18n.konybb.common.cancel");
      this.view.CommonFormActionsNew.btnBack.toolTip = kony.i18n.getLocalizedString("i18n.konybb.common.cancel");
      this.view.CommonFormActionsNew.btnBack.onClick = this.fetchBulkPaymentOrders.bind(this, this.bulkPaymentRecordData);
      this.view.CommonFormActionsNew.btnBack.skin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnBack.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnCancel.isVisible = true;
      this.view.CommonFormActionsNew.btnCancel.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.BACK");
      this.view.CommonFormActionsNew.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.BACK");
      this.view.CommonFormActionsNew.btnCancel.onClick = this.showAddRecipientsScreenUI.bind(this);
      this.view.CommonFormActionsNew.btnCancel.skin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnCancel.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnOption.isVisible = true;
      this.view.CommonFormActionsNew.btnOption.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.ADD");
      this.view.CommonFormActionsNew.btnOption.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.ADD");      
      this.view.CommonFormActionsNew.btnOption.skin = ViewConstants.SKINS.NORMAL;
      this.view.CommonFormActionsNew.btnOption.hoverSkin = ViewConstants.SKINS.NORMAL;
      FormControllerUtility.disableButton(this.view.CommonFormActionsNew.btnOption);
      this.view.CommonFormActionsNew.btnNext.isVisible = false;
      this.view.CommonFormActionsNew.btnOption.onClick = this.addExistingRecipientsPaymentOrder.bind(this);      
      this.view.flxFeesTypeRadio1.onClick = this.OnRadioBtnClickFeesPaidBy.bind(this,1);
      this.view.flxFeesTypeRadio2.onClick = this.OnRadioBtnClickFeesPaidBy.bind(this,2);
      this.view.flxFeesTypeRadio3.onClick = this.OnRadioBtnClickFeesPaidBy.bind(this,3);
      
      var breakpoint = kony.application.getCurrentBreakpoint();
      if (breakpoint <= 1024 || orientationHandler.isTablet) {
        this.view.flxPaymentRef.bottom="75dp";
      }
      
      var scopeObj = this;
      this.view.tbxAmount.onKeyUp = function() {
        if(!CommonUtilities.isEmptyString(scopeObj.view.tbxAmount.text))
        {
          FormControllerUtility.enableButton(scopeObj.view.CommonFormActionsNew.btnOption);
        }
        else
        {
          FormControllerUtility.disableButton(scopeObj.view.CommonFormActionsNew.btnOption);
        }
      };
      this.adjustScreen(0);
      this.view.forceLayout();
    },
    
    addExistingRecipientsPaymentOrder : function() {

      var feesPaid="";    
      if (this.view.imgFees1Type2.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) 
      {
        feesPaid= this.view.lblFeesOpt1.text;
      }
      else if (this.view.imgFees2Type2.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO)
      { 
        feesPaid= this.view.lblFeesOpt2.text;
      }
      else
      {
        feesPaid= this.view.lblFeesOpt3.text;
      }
      
      var data = this.view.TabBodyNew.getData()[0][1];
        var selectedRow = "";
        for (var i = 0; i < data.length; i++) {
          if (data[i].lblSelect.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) {
            selectedRow = i;
          }
        }
         var paymentMethod = data[selectedRow].lblAccountType.text;
      
      var requestParams={
        "recordId":this.bulkPaymentRecordData.recordId,
        "recipientName":this.view.lblNameValue.text,
        "accountNumber":this.view.lblAccountValue.text,
        "bankName":this.view.lblBankNameValue.text,
        "swift": this.view.lblSwiftValue.text,
        "currency": this.bulkPaymentRecordData.currency,
        "amount":this.view.tbxAmount.text,
        "feesPaidBy":feesPaid,
        "paymentReference": this.view.tbxPaymentRef.text,
        "paymentMethod": paymentMethod
      }

      this.bulkPaymentsModule.presentationController.addPaymentOrder(requestParams);
    },
    
    setPaginationComponentForBeneficiaries: function(pageHeader) {
      this.view.PaginationContainer.setPageSize(BBConstants.PAGE_SIZE);
      this.view.PaginationContainer.setPageHeader(pageHeader);
      this.view.PaginationContainer.setLowerLimit(1);
      this.view.PaginationContainer.setServiceDelegate(this.fetchBeneficiaries);      
    },
    
    fetchBeneficiaries: function() {
      this.updateFetchParams();
      this.view.PaginationContainer.setIntervalHeaderForBulkpayments();
      this.bulkPaymentsModule.presentationController.getExistingBeneficiaries(this.fetchParams);
    },
    
    invokefetchBeneficiaries: function() {
      this.setPaginationComponentForBeneficiaries(BBConstants.RECORDS);
      this.fetchBeneficiaries();
    },
    
    updatePaginationContainerUI: function(responseData) {
      var isMaxLimitReached = responseData.length+1 < this.view.PaginationContainer.getPageSize();
      this.view.PaginationContainer.setIsMaxLimitReached(isMaxLimitReached);
      this.view.PaginationContainer.updateUI();
    },
    
    /**
     * function to display the add recipients screen  
     */

    showAddRecipientsScreenUI : function() {

      this.resetUI();
      this.clearRecipientsFields();
      this.view.tbxSearchBox.text = "";
      this.view.imgClearIcon.isVisible =false;
      this.view.CommonFormActionsExt.isVisible = true;
      this.view.CommonFormActionsNew.isVisible = false;
      this.view.PaginationContainer.isVisible = true;
      this.view.flxAddRecipientDetails.isVisible = true;
      this.view.lblContentHeader.text = kony.i18n.getLocalizedString("i18n.common.AddPaymentHeader");
      this.view.lblRecordHeader1.text = kony.i18n.getLocalizedString("i18n.common.PaymentDetails");
      this.view.TabBodyNew.segTemplates.rowTemplate = "flxBulkPayementRowTemplate";
      this.view.TabBodyNew.segTemplates.sectionHeaderTemplate = "flxBulkPayementHeader";    
      this.view.CommonFormActionsExt.btnBack.isVisible = true;
      this.view.CommonFormActionsExt.btnBack.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.BACK");
      this.view.CommonFormActionsExt.btnBack.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.BACK");
      this.view.CommonFormActionsExt.btnBack.skin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsExt.btnBack.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsExt.btnNext.isVisible = true;
      this.view.CommonFormActionsExt.btnNext.text = kony.i18n.getLocalizedString("i18n.userManagement.Continue");
      this.view.CommonFormActionsExt.btnNext.toolTip = kony.i18n.getLocalizedString("i18n.userManagement.Continue"); 
      this.view.CommonFormActionsExt.btnNext.skin = ViewConstants.SKINS.NORMAL;
      this.view.CommonFormActionsExt.btnNext.hoverSkin = ViewConstants.SKINS.NORMAL;
      this.view.CommonFormActionsExt.btnNext.onClick = this.onClickContinueAddExistingRecipients.bind(this);
      this.view.CommonFormActionsExt.btnBack.onClick = this.fetchBulkPaymentOrders.bind(this, this.bulkPaymentRecordData);
      this.view.flxAddType2Radio1.onClick = this.showAddRecipientsManuallyScreenUI.bind(this);
      this.view.CommonFormActionsExt.btnCancel.isVisible = false;
      this.view.CommonFormActionsExt.btnOption.isVisible = false;
      this.view.tbxSearch.onDone = function() {
        this.updateFetchParams();
        this.bulkPaymentsModule.presentationController.getExistingBeneficiaries(this.fetchParams);
      }.bind(this);
      FormControllerUtility.disableButton(this.view.CommonFormActionsExt.btnNext);
      this.invokefetchBeneficiaries();
      this.view.forceLayout();
    },
    
    showBeneficiaryListingErrorMessage: function(msgText) {
      var dataMap = {
        lblNoRecords: "lblMsg"
      };
      var NODATAFLEXHEIGHT = "450dp";
      var defValues = {
        flxMain: {
          "height": NODATAFLEXHEIGHT
        },
        flxBulkPayementRowHeader: {
          "isVisible": false
        },
        flxBulkPayementRowDetails: {
          "isVisible": false
        },				
        flxNoRecords: {
          "isVisible": true
        }				
      };
      var sectionData = this.getBeneficiaryListingSectionData();
      this.view.TabBodyNew.setSectionData([sectionData]);            			
      this.view.TabBodyNew.setRowDataMap([dataMap]);
      this.view.TabBodyNew.setDefaultValues([defValues]);
      this.view.TabBodyNew.addRowsData([
        [{
          "lblMsg": 
          {
            "skin": ViewConstants.SKINS.RTEXT_ERROR_DESKTOP,
            "text" :msgText				
          },
        }]
      ]);
      this.view.PaginationContainer.isVisible = false;
      FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
    },
    
    getBeneficiaryListingSectionData :  function() {
      
      var sectionData = {
        flxAddRecipients: {
          "isVisible": true
        },
        flxEditRecipients: {
          "isVisible": false
        },
        flxViewRecipients: {
          "isVisible": false
        },
        btnRecipientName: {
          "text": "Beneficiary Name"
        },
        imgSortRecpName: this.dashboardSortParams.beneficiaryListing.Name,
        btnBankName: {
          "text": "Bank Name"
        },
        imgSortBankName: this.dashboardSortParams.beneficiaryListing.Bank,
        btnAccountType: {
          "text":"Payment Method"
        },
        imgSortAccountType: this.dashboardSortParams.beneficiaryListing.Type,                  
        btnSelectAll: {
          "text": "Select"
        },
        flxRecipientName: {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            var img = this.dashboardSortParams.beneficiaryListing.Name;
            if (img === "sortingfinal.png") {
              img = "sorting_next.png";
              var order = "desc";
            } else {
              img = (img === "sorting_next.png") ? "sorting_previous.png" : "sorting_next.png";
              var order = (img === "sorting_next.png") ? "desc" : "asc";
            }
            this.dashboardSortParams.beneficiaryListing.Name = img;
            this.updateFetchParams("beneficiaryName",order);
            this.bulkPaymentsModule.presentationController.getExistingBeneficiaries(this.fetchParams);
          }.bind(this)
        },
        flxBankName: {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            var img = this.dashboardSortParams.beneficiaryListing.Bank;
            if (img === "sortingfinal.png") {
              img = "sorting_next.png";
              var order = "desc";
            } else {
              img = (img === "sorting_next.png") ? "sorting_previous.png" : "sorting_next.png";
              var order = (img === "sorting_next.png") ? "desc" : "asc";
            }
            this.dashboardSortParams.beneficiaryListing.Bank = img;
            this.updateFetchParams("bankName",order);
            this.bulkPaymentsModule.presentationController.getExistingBeneficiaries(this.fetchParams);
          }.bind(this)
        },
        flxAccountType: {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            var img = this.dashboardSortParams.beneficiaryListing.Type;
            if (img === "sortingfinal.png") {
              img = "sorting_next.png";
              var order = "asc";
              var param="isInternationalAccount";

              this.dashboardSortParams.beneficiaryListing.Type = img;
              this.updateFetchParams(param, order);
              this.bulkPaymentsModule.presentationController.getExistingBeneficiaries(this.fetchParams);
            }
            else
            {
              this.view.TabBodyNew.sortData(context.sectionIndex, "","","lblAccountType", "ObjectText", "Desc");
              this.dashboardSortParams.beneficiaryListing.Type = "sortingfinal.png";
            }
          }.bind(this)
        }

      };
      
      return sectionData;
    },
    
     mapBeneficiaryListingData : function(response) {
     
      var sectionData = this.getBeneficiaryListingSectionData();
      var defaultValues = {
        flxMain: {
          "height": "51dp"
        },
        flxBulkPayementRowHeader: {
          "skin": "bbSKnFlxffffff"
        },
        flxRecipientsType: {
          "isVisible": false
        },
        flxSeperator: {
          "isVisible": false
        },
        flxAddRecipientsRowHeader: {
          "isVisible": true
        },
        flxEditRecipientsRowHeader: {
          "isVisible": false
        },
        flxViewRecipientsRowHeader: {
          "isVisible": false
        },
        flxNoRecords: {
          "isVisible": false
        },
        flxBulkPayementRowDetails: {
          "isVisible": true
        },
        imgDropDown: {
          "skin": ViewConstants.SKINS.DRP_DWN_OTHER,
          "text": ViewConstants.FONT_ICONS.THREE_DOTS_ACCOUNTS,
          "isVisible": true
        },
        lblSelect: {
          "isVisible": true
        },
        flxActions: {
          "isVisible": false
        },
        flxSelect: {
          "isVisible": true,
          "onClick": function(eventobject, context) {
            this.onSelectExistingRecipient(eventobject, context);
          }.bind(this)
        },
        lblAccountNo: {
          "text": kony.i18n.getLocalizedString("kony.i18n.common.accountNumber"),
          "isVisible": true,
        },        
        lblSwiftCode: {
          "text": "Nick Name",
          "isVisible": true,
        },
        lblPayRef: {
          "text": kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentReference"),
          "isVisible": false,
        },
        lblFees: {
          "text": kony.i18n.getLocalizedString("i18n.TransfersEur.FeesPaidBy"),
          "isVisible": false,
        },
        lblPayRefValue: {          
          "isVisible": false
        },
        lblFeesValue: {         
          "isVisible": false
        }
       
      };
      var rowDataMap = {
        lblRecipientName: "beneficiaryName",
        lblBankName: "bankName",
        lblAccountType: "lblAccountType",
        lblAccountNo: "lblAccountNo",
        lblAccountNoValue: "lblAccountNoValue",
        lblAccType: "lblSwiftName",
        lblAccTypeValue: "lblAccTypeValue",
        lblSwiftCode: "lblSwiftCode",
        lblSwiftCodeValue: "lblNickName"                  
      }; 
      
      if (kony.sdk.isNullOrUndefined(response)) response = [this.view.TabBodyNew.getData()[0][1]];
      this.updatePaginationContainerUI(response);
      this.view.TabBodyNew.setSectionData([sectionData]);
      this.view.TabBodyNew.setRowDataMap([rowDataMap]);
      this.view.TabBodyNew.setDefaultValues([defaultValues]);
      this.view.TabBodyNew.addDataForSections([response]);
      FormControllerUtility.hideProgressBar(this.view);
      this.adjustScreen(-50);
      this.view.forceLayout();
    },
    
    onSelectExistingRecipient: function(eventobject, context) {
      var row = context.rowIndex;
      var section = context.sectionIndex;
      var data = this.view.TabBodyNew.getData()[section][1];
      var lblSelect="";

      for(var i=0;i<data.length;i++)
      {
        lblSelect = data[i].lblSelect;
        lblSelect.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
        lblSelect.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
        this.view.TabBodyNew.updateKeyAt("lblSelect", lblSelect, i, section);
      }
      
      
      lblSelect = data[row].lblSelect;
      lblSelect.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
      lblSelect.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;  
      this.view.TabBodyNew.updateKeyAt("lblSelect", lblSelect, row, section);
      FormControllerUtility.enableButton(this.view.CommonFormActionsExt.btnNext);

    },

    OnRadioBtnClickFeesPaidBy: function(btnId) {
      var RadioBtn1 = this.view.imgFees1Type2;
      var RadioBtn2 = this.view.imgFees2Type2;
      var RadioBtn3 = this.view.imgFees3Type2;

      if (btnId===1)
      {
        RadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
        RadioBtn1.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;   
        RadioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
        RadioBtn2.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
        RadioBtn3.text =ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
        RadioBtn3.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;         
        this.adjustScreen(10);
      }
      else if (btnId===2)
      {
        RadioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
        RadioBtn2.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;   
        RadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
        RadioBtn1.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
        RadioBtn3.text =ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
        RadioBtn3.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;         
        this.adjustScreen(10);
      }			
      else {
        RadioBtn3.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
        RadioBtn3.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;   
        RadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
        RadioBtn1.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
        RadioBtn2.text =ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
        RadioBtn2.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;                
        this.adjustScreen(10);
      }

    },

    RadioBtnAction: function() {
      var RadioBtn1= this.view.imgRadioBtnRecipientType1;
      var RadioBtn2 =this.view.imgRadioBtnRecipientType2;
      if (RadioBtn1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) {
        RadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
        RadioBtn1.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
        RadioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
        RadioBtn2.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;
        this.view.flxRecipientDetailsInfinity.isVisible = false;
        this.view.flxRecipientDetailsExternal.isVisible = true;
        this.view.flxFeesPaid.isVisible = true;
        this.adjustScreen(10); 
      } else {
        RadioBtn2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
        RadioBtn2.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
        RadioBtn1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
        RadioBtn1.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;
        this.view.flxRecipientDetailsInfinity.isVisible = true;
        this.view.flxRecipientDetailsExternal.isVisible = false;
        this.view.flxFeesPaid.isVisible = false;
        this.adjustScreen(10); 
      }
      FormControllerUtility.disableButton(this.view.CommonFormActionsNew.btnNext);
      this.clearRecipientsFields();
    },
    
    /**
     * function to display the add recipients screen manually 
     */

    showAddRecipientsManuallyScreenUI : function() {

      this.resetUI();
      this.clearRecipientsFields();
      this.resetRecipientsUI();
      this.view.tbxSearchBox.text = "";
      this.view.imgClearIcon.isVisible =false;
      this.view.flxAddRecipientsManually.isVisible = true;
      this.view.lblContentHeader.text = kony.i18n.getLocalizedString("i18n.common.AddPaymentHeader");
      this.view.lblAddHeader.text = kony.i18n.getLocalizedString("i18n.common.PaymentDetails");   
      this.view.imgRadioBtnRecipientType1.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
      this.view.imgRadioBtnRecipientType1.skin = ViewConstants.SKINS.RADIOBTN_SELECTED;
      this.view.imgRadioBtnRecipientType2.text = ViewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
      this.view.imgRadioBtnRecipientType2.skin = ViewConstants.SKINS.RADIOBTN_UNSELECTED_FONT;
      this.view.flxRecipientDetailsInfinity.isVisible = true;
      this.view.flxRecipientDetailsExternal.isVisible = false;
      this.view.flxRecipientDetailsExisting.isVisible = false;
      this.view.flxFeesPaid.isVisible = false;
      this.view.flxTypeRadio1.onClick = this.RadioBtnAction.bind(this);
      this.view.flxTypeRadio2.onClick = this.RadioBtnAction.bind(this);
      this.view.CommonFormActionsNew.btnBack.isVisible = true;
      this.view.CommonFormActionsNew.btnBack.text = kony.i18n.getLocalizedString("i18n.konybb.common.cancel");
      this.view.CommonFormActionsNew.btnBack.toolTip = kony.i18n.getLocalizedString("i18n.konybb.common.cancel");
      this.view.CommonFormActionsNew.btnBack.skin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnBack.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnNext.isVisible = true;
      this.view.CommonFormActionsNew.btnNext.text = kony.i18n.getLocalizedString("i18n.ProfileManagement.ADD");
      this.view.CommonFormActionsNew.btnNext.toolTip = kony.i18n.getLocalizedString("i18n.ProfileManagement.ADD"); 
      this.view.CommonFormActionsNew.btnNext.skin = ViewConstants.SKINS.NORMAL;
      this.view.CommonFormActionsNew.btnNext.hoverSkin = ViewConstants.SKINS.NORMAL;      
      this.view.CommonFormActionsNew.btnNext.onClick = this.addNewRecipientsPaymentOrder.bind(this);      
      FormControllerUtility.disableButton(this.view.CommonFormActionsNew.btnNext);
      this.view.CommonFormActionsNew.btnBack.onClick = this.fetchBulkPaymentOrders.bind(this, this.bulkPaymentRecordData);
      this.view.flxAddTypeRadio2.onClick =  this.showAddRecipientsScreenUI.bind(this);
      this.view.flxFeesTypeRadio1.onClick = this.OnRadioBtnClickFeesPaidBy.bind(this,1);
      this.view.flxFeesTypeRadio2.onClick = this.OnRadioBtnClickFeesPaidBy.bind(this,2);
      this.view.flxFeesTypeRadio3.onClick = this.OnRadioBtnClickFeesPaidBy.bind(this,3);
      this.view.CommonFormActionsNew.btnCancel.isVisible = false;
      this.view.CommonFormActionsNew.btnOption.isVisible = false;
      this.view.lblSelect.onTouchEnd = this.onCheckBoxClick.bind(this);
      this.view.tbxRecipAccNumber.onKeyUp = this.enableOrDisableAddOnValidRecipientDetails.bind(this);
      this.view.tbxRecipAccNumberExt.onKeyUp = this.enableOrDisableAddOnValidRecipientDetails.bind(this);
      this.view.tbxSwift.onKeyUp = this.enableOrDisableAddOnValidRecipientDetails.bind(this);
      this.view.tbxRecipientBankName.onKeyUp = this.enableOrDisableAddOnValidRecipientDetails.bind(this);
      this.view.tbxAmount.onKeyUp = this.enableOrDisableAddOnValidRecipientDetails.bind(this);
      this.view.tbxRecipientName.onKeyUp = this.enableOrDisableAddOnValidRecipientDetails.bind(this);				 
      this.adjustScreen(10);
      this.view.forceLayout();
    },
   
    addNewRecipientsPaymentOrder : function() {

      var feesPaid="";
      var accNum="";
      var bankName="";
      var paymentMethod="";

      if (this.view.imgFees1Type2.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) 
      {
        feesPaid= this.view.lblFeesOpt1.text;
      }
      else if (this.view.imgFees2Type2.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO)
      { 
        feesPaid= this.view.lblFeesOpt2.text;
      }
      else
      {
        feesPaid= this.view.lblFeesOpt3.text;
      }

      if(this.view.tbxRecipAccNumber.text==="")
      {
        accNum=this.view.tbxRecipAccNumberExt.text;
      }
      else
      {
        accNum=this.view.tbxRecipAccNumber.text;         
      }

      if(this.view.tbxRecipientBankName.text==="")
      {
        bankName=this.view.lblRadioOpt1.text;
      }
      else
      {
        bankName=this.view.tbxRecipientBankName.text;
      }
      
      if (this.view.imgRadioBtnRecipientType1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) {
        paymentMethod= BBConstants.INTERNAL;
      }
      else{
        paymentMethod = BBConstants.DOMESTIC;
      }


      var requestParams={
        "recordId":this.bulkPaymentRecordData.recordId,
        "recipientName":this.view.tbxRecipientName.text,
        "accountNumber":accNum,
        "bankName":bankName,
        "swift":  this.view.tbxSwift.text,
        "currency": this.bulkPaymentRecordData.currency,
        "amount":this.view.tbxAmount.text,
        "feesPaidBy":feesPaid,
        "paymentReference": this.view.tbxPaymentRef.text,
        "paymentMethod": paymentMethod
      }

      this.bulkPaymentsModule.presentationController.addPaymentOrder(requestParams);
    },
    

    /* validate all recipient details */
    enableOrDisableAddOnValidRecipientDetails: function() {

      var RadioBtn1 = this.view.imgRadioBtnRecipientType1;
      
      if (RadioBtn1.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO)
      {
        if ((CommonUtilities.isEmptyString(this.view.tbxRecipAccNumber.text)) || 
            (CommonUtilities.isEmptyString( this.view.tbxAmount.text)) || 
            (CommonUtilities.isEmptyString(  this.view.tbxRecipientName.text)))
        {
          FormControllerUtility.disableButton(this.view.CommonFormActionsNew.btnNext);
        } else {
          FormControllerUtility.enableButton(this.view.CommonFormActionsNew.btnNext);
        }
      }

      else{
        
        if ((CommonUtilities.isEmptyString(this.view.tbxRecipAccNumberExt.text)) || 
            (CommonUtilities.isEmptyString(this.view.tbxSwift.text)) || 
            (CommonUtilities.isEmptyString(this.view.tbxRecipientBankName.text)) || 
            (CommonUtilities.isEmptyString(this.view.tbxAmount.text)) || 
            (CommonUtilities.isEmptyString(this.view.tbxRecipientName.text)))
        {
          FormControllerUtility.disableButton(this.view.CommonFormActionsNew.btnNext);
        } else {
          FormControllerUtility.enableButton(this.view.CommonFormActionsNew.btnNext);
        }
      }
    },
    
    onCheckBoxClick: function() {

      if(this.view.lblSelect.text===OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED)
      {
        this.view.lblSelect.text=OLBConstants.FONT_ICONS.CHECBOX_SELECTED;
      }
      else
      {
        this.view.lblSelect.text=OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED;
      }
    },
    
    onPaymentOrdersSearchDone : function() {
      var requestData ={};
      requestData.recordId=this.bulkPaymentRecordData.recordId;
      requestData.searchString =this.view.tbxSearchBox.text ; 
      this.getPaymentOrders(requestData);		
    },

    onPaymentOrdersKeyUp : function() {
      var requestData ={};
      requestData.recordId=this.bulkPaymentRecordData.recordId;
      this.view.imgClearIcon.isVisible =true;
      this.view.imgClearIcon.onTouchStart = function() {
        this.view.tbxSearchBox.text = "";
        this.view.imgClearIcon.isVisible =false;
        this.getPaymentOrders(requestData);		
      }.bind(this);
    },
    
    getRecipientData: function(isEditFlow,BackButtonText) {
      this.resetUI();
      var flag;
      if (isEditFlow === true) flag = false;
      else flag = true;
      this.view.flxAckMessage.isVisible = false;
	  this.view.lblContentHeader.text = kony.i18n.getLocalizedString("kony.i18n.common.bulkPaymentsReview");
      var templateData = {
        "Description": this.bulkPaymentRecordData.description ,
        "Initiated By": this.bulkPaymentRecordData.initiatedBy,
        "Transfer Initiated On": this.bulkPaymentRecordData.scheduledDate,
        "Payment Date": this.bulkPaymentRecordData.paymentDate,
		"Total Amount": this.bulkPaymentRecordData.totalAmount ,
        "From Account": this.bulkPaymentRecordData.fromAccountMasked,
        "Total Transactions" : this.bulkPaymentRecordData.totalTransactions ,
        "Bulk Reference ID": this.bulkPaymentRecordData.recordId,
      };
	  if(this.isMakerHistoryFlow){
		var templateData = {
        "Payment Description": this.bulkPaymentRecordData.description ,
        "Initiated By": this.bulkPaymentRecordData.initiatedBy,
        "Transfer Initiated On": this.bulkPaymentRecordData.scheduledDate,
        "Payment Date": this.bulkPaymentRecordData.paymentDate,
		"Value Date": this.bulkPaymentRecordData.scheduledDate,
        "Total Amount": this.bulkPaymentRecordData.totalAmount ,
        "From Account": this.bulkPaymentRecordData.fromAccountMasked,
        "Number of Transactions" : this.bulkPaymentRecordData.totalTransactions ,
        "Bulk Payment ID": this.bulkPaymentRecordData.recordId,
      };
	  this.view.lblContentHeader.text = kony.i18n.getLocalizedString("kony.i18n.common.bulkPaymentsViewpaymentdetails");
	  }

      this.view.NonEditableBulkPaymentDetails.setData(templateData, true);
      this.view.NonEditableBulkPaymentDetails.isVisible = true;
      this.view.flxPaymentReview.isVisible = true;
      this.view.imgDropDown.text = ViewConstants.FONT_ICONS.CHEVRON_UP;
      
      this.view.lblRecordHeader.text = kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentDetails");
      this.view.TabBodyNew1.segTemplates.rowTemplate = "flxBulkPayementRowTemplate";
      this.view.TabBodyNew1.segTemplates.sectionHeaderTemplate = "flxBulkPayementHeader";
      this.view.TabBodyNew1.segTemplates.bottom = "10dp";
      this.view.flxAddExistingRecipients.onClick = this.showAddRecipientsScreenUI.bind(this);
      this.view.flxAddPayment.onClick = this.showAddRecipientsScreenUI.bind(this);
      this.view.TabBodyNew1.addOnlySectionHeaders(this.getSectionHeadersForNewAccounts());
      this.view.lblAddExistingRecipients.text = "+ " + kony.i18n.getLocalizedString("kony.i18n.common.addExistingRecipients");
      this.view.lblAddPayment.text = kony.i18n.getLocalizedString("kony.i18n.common.newPayment");
      
      var self =this;
      this.view.CommonFormActionsNew.btnOption.onClick =function (){
        self.bulkPaymentsModule.presentationController.submitPaymentOrder({
          "recordId": self.bulkPaymentRecordData.recordId
        }); 
      };

      this.view.flxDropDown.onClick = this.showHidePaymentsSeg.bind(this);
      this.view.flxAcknowledgementNew.flxImgdownload.onTouchEnd= function() {
        var scopeObj = this;
        scopeObj.view.flxAcknowledgementContainer.setVisibility(false);        
      }.bind(this);
      this.view.tbxSearchBox.onKeyUp =this.onPaymentOrdersKeyUp.bind(this);
      this.view.tbxSearchBox.onDone =this.onPaymentOrdersSearchDone.bind(this);
      
      var sectionData = {
        "flxAddRecipients": {
          "isVisible": false
        },
        "flxViewRecipients": {
          "isVisible": true
        },
        "flxTopSeperator":{
          "isVisible": true
        },
        "btnViewRecipientName": {
          "text": kony.i18n.getLocalizedString("i18n.TransfersEur.Beneficiary"),
          "onClick": function(eventobject, context) {
            if (nameSort) {
              this.view.TabBodyNew1.sortData(context.sectionIndex, "", "", "lblAccountName", "String", "Asc");
              this.accountSortType = "AccountNameAsc";
            } else {
              this.view.TabBodyNew1.sortData(context.sectionIndex, "", "", "lblAccountName", "String", "Desc");
              this.accountSortType = "AccountNameDesc";
            }
          }.bind(this),
          "isVisible": true
        },
        "imgSortViewRecipientName": {
          "isVisible": true,
          "src": "sortingfinal.png",
        },
        "btnViewBankName": {
          "text": kony.i18n.getLocalizedString("i18n.konybb.Common.Amount"),
          "onClick": function(eventobject, context) {
            if (numberSort) {
              this.view.TabBodyNew1.sortData(context.sectionIndex, "", "", "lblAccountNumber", "ObjectNumber", "Asc");
              this.accountSortType = "AccountNumberAsc";
            } else {
              this.view.TabBodyNew1.sortData(context.sectionIndex, "", "", "lblAccountNumber", "ObjectNumber", "Desc");
              this.accountSortType = "AccountNumberDesc";
            }
            numberSort = !numberSort;
          }.bind(this),
          "isVisible": true
        },
        "imgSortViewBankName": {
          "isVisible": true,
          "src": "sortingfinal.png",
        },
        "btnViewAmount": {
          "text": kony.i18n.getLocalizedString("i18n.billPay.Status"),
          "onClick": function(eventobject, context) {
            if (typeSort) {
              this.view.TabBodyNew1.sortData(context.sectionIndex, "", "", "lblAccountType", "ObjectText", "Asc");
              this.accountSortType = "AccountTypeAsc";
            } else {
              this.view.TabBodyNew1.sortData(context.sectionIndex, "", "", "lblAccountType", "ObjectText", "Desc");
              this.accountSortType = "AccountTypeDesc";
            }
            typeSort = !typeSort;
          }.bind(this),
          "isVisible": true
        },
        "imgSortViewAmount": {
          "isVisible": true,
          "src": "sortingfinal.png",
        },
        "btnViewAction": {
          "text": kony.i18n.getLocalizedString("i18n.wireTransfers.Actions"),
          "isVisible": flag === true ? true : false,
        },
        "imgFlxBottomSeparator": {
          "text": "-"
        },
        "imgFlxTopSeparator": {
          "text": "-"
        },
        "flxBottomSeperator": {
          "isVisible": true
        },
      };
      var rowDataMap = {
        "lblRecipientsType": "lblRecipientsType",
        "imgRecipTypeDrpdown": "imgRecipTypeDrpdown",
        "imgFlxSeperator": "imgFlxSeperator",
        "imgFlxBottomSeparator": "imgFlxBottomSeparator",
        "imgDropDown": "imgDropDown",
        "lblViewRecipientName": "lblViewRecipientName",
        "lblViewBankName": "lblViewBankName",
        "lblViewAmount": "lblViewAmount",
        "btnViewActions": "btnViewActions",
        "imgFlxTopSeparator": "imgFlxTopSeparator",
        "imgSample": "imgSample",
        "lblAccountNo": "lblAccountNo",
        "lblAccountNoValue": "lblAccountNoValue",
        "lblAccType": "lblAccType",
        "lblAccTypeValue": "lblAccTypeValue",
        "lblSwiftCode": "lblSwiftCode",
        "lblSwiftCodeValue": "lblSwiftCodeValue",
        "lblPayRef": "lblPayRef",
        "lblPayRefValue": "lblPayRefValue",
        "lblFees": "lblFees",
        "lblFeesValue": "lblFeesValue",
        "lblPaymentMethod":"lblPaymentMethod",
        "lblPaymentMethodValue":"lblPaymentMethodValue",
        "btnEdit": "btnEdit",
        "btnViewRecipientName": "btnViewRecipientName",
        "imgSortViewRecipientName": "imgSortViewRecipientName",
        "btnViewBankName": "btnViewBankName",
        "imgSortViewBankName": "imgSortViewBankName",
        "btnViewAmount": "btnViewAmount",
        "imgSortViewAmount": "imgSortViewAmount",
        "btnViewAction": "btnViewAction",
        "flxAddRecipients": "flxAddRecipients",
        "flxViewRecipients": "flxViewRecipients",
        "flxRecipientsType": "flxRecipientsType",
        "flxMain": "flxMain",
        "flxTopSeperator": "flxTopSeperator"
      };
      var defaultValues = {
        flxMain: {
          "height": "51dp"
        },
        "btnViewActions": {
          "text": "Edit",
          "onClick": function(eventobject, context) {
            this.resetUI();
            this.clearRecipientsFields();
            this.view.lblBankNameKey.text = kony.i18n.getLocalizedString("i18n.CheckImages.Bank");
            this.view.lblFeesOpt2.text = kony.i18n.getLocalizedString("i18n.TransfersEur.Beneficiary");
            this.view.lblFeesOpt3.text = kony.i18n.getLocalizedString("i18n.TransfersEur.Both5050");
            this.view.flxAckMessage.isVisible = false;
            this.resetRecipientsUI();						
            this.mapBenificiaryData(true,context);
            var scopeObj = this;
            this.view.tbxAmount.onKeyUp = function() {
              if(!CommonUtilities.isEmptyString(scopeObj.view.tbxAmount.text))
              {
                FormControllerUtility.enableButton(scopeObj.view.CommonFormActionsNew.btnOption);
              }
              else
              {
                FormControllerUtility.disableButton(scopeObj.view.CommonFormActionsNew.btnOption);
              }            
            }.bind(this);
            this.view.flxAddRecipientsManually.isVisible = true;
            this.view.flxAddRecipientsManually.isVisible = true;
            this.view.flxAddType.isVisible = false;
            this.view.flxBankType.isVisible = false;
            this.view.tbxSearchBox.text = "";
            this.view.imgClearIcon.isVisible =false;
            this.view.flxRecipientDetailsInfinity.isVisible = false;
            this.view.flxRecipientDetailsExternal.isVisible = false;
            this.view.flxRecipientName.isVisible = false;
            this.view.flxAddToList.isVisible = false;
            this.view.flxInfo.isVisible = false;
            this.view.lblAddHeader.text = kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentDetails");
            this.view.lblContentHeader.text = kony.i18n.getLocalizedString("kony.i18n.common.editPayment");
            this.view.CommonFormActionsNew.btnNext.isVisible = false;
            this.view.CommonFormActionsNew.btnCancel.isVisible = false;
            this.view.CommonFormActionsNew.btnBack.text = kony.i18n.getLocalizedString("i18n.konybb.common.cancel");
            this.view.CommonFormActionsNew.btnBack.toolTip = kony.i18n.getLocalizedString("i18n.konybb.common.cancel");
            this.view.CommonFormActionsNew.btnBack.onClick = this.fetchBulkPaymentOrders.bind(this, this.bulkPaymentRecordData);
            this.view.CommonFormActionsNew.btnOption.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Update");
            this.view.CommonFormActionsNew.btnOption.toolTip = kony.i18n.getLocalizedString("i18n.PayAPerson.Update");
            this.view.CommonFormActionsNew.btnOption.onClick = this.navBackToPaymentOrders.bind(this,eventobject, context);
            this.view.tbxPaymentRef.onKeyUp = function() {
              if(!CommonUtilities.isEmptyString(scopeObj.view.tbxPaymentRef.text))
              {
                FormControllerUtility.enableButton(scopeObj.view.CommonFormActionsNew.btnOption);
              }
              else
              {
                FormControllerUtility.disableButton(scopeObj.view.CommonFormActionsNew.btnOption);
              }            
            }.bind(this);
            this.view.flxFeesTypeRadio1.onClick = function() {
              FormControllerUtility.enableButton(scopeObj.view.CommonFormActionsNew.btnOption);
              this.OnRadioBtnClickFeesPaidBy(1);
            }.bind(this);
            this.view.flxFeesTypeRadio2.onClick = function() {
              FormControllerUtility.enableButton(scopeObj.view.CommonFormActionsNew.btnOption);
              this.OnRadioBtnClickFeesPaidBy(2);
            }.bind(this);
            this.view.flxFeesTypeRadio3.onClick = function() {
              FormControllerUtility.enableButton(scopeObj.view.CommonFormActionsNew.btnOption);
              this.OnRadioBtnClickFeesPaidBy(3);
            }.bind(this);
            FormControllerUtility.disableButton(scopeObj.view.CommonFormActionsNew.btnOption);                                 
            this.adjustScreen(10); 
          }.bind(this),
          "isVisible": flag === true ? true : false,
        },
        "imgFlxSeperator": {
          "text": "-"
        },
        "flxBulkPayementRowHeader": {
          "skin": "bbSKnFlxffffff"
        },
        "flxBottomSeparator": {
          "isVisible": false
        },
        "imgFlxBottomSeparator": {
          "text": "-"
        },
        "flxSeperator": {
          "isVisible": false
        },
        "imgDropDown": {
          "skin": "sknLblFontTypeIcon1a98ff12pxOther",
          "text": "O",
          "isVisible": true
        },
        "flxTopSeperator": {
          "isVisible": true
        },
        "flxDetailsHighlighter": {
          "isVisible": true
        },
        "lblAccountNo": {
          "text": kony.i18n.getLocalizedString("kony.i18n.common.accountNumber"),
          "isVisible": true,
        },
        "lblSwiftCode": {
          "text": kony.i18n.getLocalizedString("i18n.CheckImages.Bank"),
          "isVisible": true,
        },
        "lblPayRef": {
          "text": kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentReference"),
          "isVisible": true,
        },   
        "lblFees": {
          "text": kony.i18n.getLocalizedString("i18n.TransfersEur.FeesPaidBy"),
          "isVisible": true,
        },
		"lblAccType": {
          "text":  kony.i18n.getLocalizedString("kony.i18n.common.swiftcode"),
          "isVisible": true,
        },
        "lblPaymentMethod": {
          "text": kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentMethod"),
          "isVisible": true,
        },
        "btnEdit": {
          "text": kony.i18n.getLocalizedString("i18n.PayAPerson.payAPersonDeregister"),
          "isVisible": flag === true ? true : false,
          "onClick": function(eventobject, context) {
            this.view.PopupHeaderUM.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.confirm");
            this.showPopUp(kony.i18n.getLocalizedString("kony.i18n.common.removePayments"), this.removePaymentOrder.bind(this, eventobject, context))
          }.bind(this)
        },
        "btnDelete": {
          "isVisible": false
        },
        "btnViewDetails": {
          "isVisible": false
        }
      };      
      this.view.TabBodyNew1.setSectionData([sectionData]);
      this.view.TabBodyNew1.setRowDataMap([rowDataMap]);
      this.view.TabBodyNew1.setDefaultValues([defaultValues]);
      //this.view.TabBodyNew1.addOnlySectionHeaders(this.getSectionHeadersForNewAccounts());
      this.view.TabBodyNew1.addDataForSections([this.paymentOrdersData]);
      if(!kony.sdk.isNullOrUndefined(this.paymentOrdersData) ){
        this.view.TabBodyNew1.addDataForSections([this.paymentOrdersData]);
      }
      this.enableReviewButtons();
      if(flag === false)
      {
        this.disableReviewButtons();
      }
	  if(! kony.sdk.isNullOrUndefined(this.bulkPaymentRecordData.requestId) && this.bulkPaymentRecordData.requestId !== kony.i18n.getLocalizedString("i18n.common.NA")){
		this.fetchRequestHistory(this.bulkPaymentRecordData.requestId);
	  }
	  else{
		this.view.flxApprovalsHistoryInformation.setVisibility(false);		
		FormControllerUtility.hideProgressBar(this.view);
	  } 	  
      this.adjustScreen(10);
      this.view.forceLayout();
    },
    
    navBackToPaymentOrders: function(eventobject, context) {
      
      var data = this.view.TabBodyNew1.getData()[context.sectionIndex][1];
      var feesPaid = "";

      if (this.view.imgFees1Type2.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) {
        feesPaid = this.view.lblFeesOpt1.text;
      } else if (this.view.imgFees2Type2.text === ViewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO) {
        feesPaid = this.view.lblFeesOpt2.text;
      } else {
        feesPaid = this.view.lblFeesOpt3.text;
      }

      var requestParams={
        "recordId":this.bulkPaymentRecordData.recordId,
        "paymentOrderId": data[context.rowIndex].paymentOrderId,
        "currency":data[context.rowIndex].currency,
        "amount":this.view.tbxAmount.text,
        "feesPaidBy":feesPaid,
        "paymentReference":this.view.tbxPaymentRef.text
      }
      this.bulkPaymentsModule.presentationController.editPaymentOrder(requestParams);
      
    },
    
    navToOngoingPayments: function() {
      var BulkPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BulkPayments");
      BulkPaymentsModule.presentationController.noServiceNavigate("frmBulkPaymentsDashboard", BBConstants.VIEW_REQUESTS);
    },
    
    navToPaymentHistory: function() {
      var BulkPaymentsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BulkPayments");
      BulkPaymentsModule.presentationController.noServiceNavigate("frmBulkPaymentsDashboard", BBConstants.VIEW_HISTORY);
    },
    
    removePaymentOrder: function(eventobject, context) {
      this.view.flxCancelPopup.setVisibility(false);
      this.view.tbxSearchBox.text = "";
      this.view.imgClearIcon.isVisible =false;
      var data = this.view.TabBodyNew1.getData()[context.sectionIndex][1];
					
      var requestParams={
        "recordId":this.bulkPaymentRecordData.recordId,						
        "paymentOrderId": data[context.rowIndex].paymentOrderId
      }
      this.bulkPaymentsModule.presentationController.deletePaymentOrder(requestParams);
    },
    
    showPopUp: function(message, onYes) {
      var scope = this;
      scope.view.flxCancelPopup.height = scope.view.customheader.info.frame.height + scope.view.flxMain.info.frame.height + scope.view.flxFooter.info.frame.height;
      FormControllerUtility.scrollToCenterY(scope.view.flxCancelPopup.height);
      function closePopUp() {
        this.view.flxCancelPopup.setVisibility(false);
        this.adjustScreen(0);
      }
      scope.view.PopupHeaderUM.btnNo.text = kony.i18n.getLocalizedString("i18n.common.no");
      scope.view.PopupHeaderUM.btnYes.text = kony.i18n.getLocalizedString("i18n.common.yes");
      scope.view.PopupHeaderUM.lblPopupMessage.text = message;
      //scope.view.PopupHeaderUM.lblHeading.text = kony.i18n.getLocalizedString("i18n.transfers.Cancel");//head
      scope.view.PopupHeaderUM.flxCross.onClick = closePopUp.bind(this);
      scope.view.PopupHeaderUM.btnNo.onClick = closePopUp.bind(this);
      scope.view.PopupHeaderUM.btnYes.onClick = onYes;
      scope.view.flxCancelPopup.setVisibility(true);
      scope.adjustScreen(0);
    },
    
    getWidgetDataMapForRecipients: function() {
      var widgetDataMap = {
        "lblRecipientsType": "lblRecipientsType",
        "imgRecipTypeDrpdown": "imgRecipTypeDrpdown",
        "imgFlxSeperator": "imgFlxSeperator",
        "imgDropDown": "imgDropDown",
        "lblViewRecipientName": "lblViewRecipientName",
        "lblViewBankName": "lblViewBankName",
        "lblViewAmount": "lblViewAmount",
        "btnViewActions": "btnViewActions",
        "imgFlxTopSeparator": "imgFlxTopSeparator",
        "imgSample": "imgSample",
        "lblAccountNo": "lblAccountNo",
        "lblAccountNoValue": "lblAccountNoValue",
        "lblAccType": "lblAccType",
        "lblAccTypeValue": "lblAccTypeValue",
        "lblSwiftCode": "lblSwiftCode",
        "lblSwiftCodeValue": "lblSwiftCodeValue",
        "lblPayRef": "lblPayRef",
        "lblPayRefValue": "lblPayRefValue",
        "imgFlxBottomSeparator": "imgFlxBottomSeparator",
        "btnEdit": "btnEdit",
        "btnDelete": "btnDelete",
        "btnViewDetails": "btnViewDetails",
        "btnViewRecipientName": "btnViewRecipientName",
        "imgSortViewRecipientName": "imgSortViewRecipientName",
        "btnViewBankName": "btnViewBankName",
        "imgSortViewBankName": "imgSortViewBankName",
        "btnViewAmount": "btnViewAmount",
        "imgSortViewAmount": "imgSortViewAmount",
        "btnViewAction": "btnViewAction",
        "flxAddRecipients": "flxAddRecipients",
        "flxViewRecipients": "flxViewRecipients",
        "flxRecipientsType": "flxRecipientsType",
        "flxMain": "flxMain",
      };
      return widgetDataMap;
    },
    getSectionHeadersForNewAccounts: function() {
      var res;
      res = {
        "flxAddRecipients": {
          "isVisible": false
        },
        "flxViewRecipients": {
          "isVisible": true
        },
        "btnViewRecipientName": {
          "text": kony.i18n.getLocalizedString("i18n.transfers.benificiaryName")
        },
        "imgSortViewRecipientName": {
          "isVisible": true
        },
        "btnViewBankName": {
          "text": kony.i18n.getLocalizedString("i18n.transfers.bankName")
        },
        "imgSortViewBankName": {
          "isVisible": true
        },
        "btnViewAmount": {
          "text": kony.i18n.getLocalizedString("i18n.billPay.Status")
        },
        "imgSortViewAmount": {
          "isVisible": true
        },
        "btnViewAction": {
          "text": kony.i18n.getLocalizedString("i18n.transfers.lblAction"),
          "isVisible": flag === true ? true : false,
        },
        "imgFlxBottomSeparator": {
          "text": "-"
        },
        "imgFlxTopSeparator": {
          "text": "-"
        },
      }
      return res;
    },
    
     onSubmitForApproval: function(response) {
      this.getRecipientData(true);
      var templateData = {
         "Description": this.bulkPaymentRecordData.description,
         "Initiated By": this.bulkPaymentRecordData.initiatedby,
         "Transfer Initiated On": this.bulkPaymentRecordData.scheduledDate,
         "Payment Date": this.bulkPaymentRecordData.paymentDate,
         "Value Date": this.bulkPaymentRecordData.scheduledDate,
         "Total Amount": this.bulkPaymentRecordData.totalAmount,
         "From Account": this.bulkPaymentRecordData.fromAccountMasked,
         "Total Transactions": this.bulkPaymentRecordData.totalTransactions,
         "Bulk Reference ID": this.bulkPaymentRecordData.recordId,
       };
	  if(! kony.sdk.isNullOrUndefined(response.requestId) && ! kony.sdk.isNullOrUndefined(response.status) && ! kony.sdk.isNullOrUndefined(response.requiredApprovals) &&
	  	 ! kony.sdk.isNullOrUndefined(response.receivedApprovals) ){
		this.bulkPaymentRecordData.requestId = response.requestId;
		this.bulkPaymentRecordData.status = response.status;
		this.bulkPaymentRecordData.requiredApprovals = response.requiredApprovals;
		this.bulkPaymentRecordData.receivedApprovals = response.receivedApprovals;

		if((response.status === BBConstants.TRANSACTION_STATUS.PENDING)&& (!kony.sdk.isNullOrUndefined(response.receivedApprovals))&& (!kony.sdk.isNullOrUndefined(response.requiredApprovals))) {
			this.bulkPaymentRecordData.Approver = response.receivedApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.of") + " " + response.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approved");
		}
		else if(response.status === BBConstants.TRANSACTION_STATUS.REJECTED) {
			this.bulkPaymentRecordData.Approver = 1 + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Rejection");
		}				
		else if (!kony.sdk.isNullOrUndefined(response.requiredApprovals)) {
			this.bulkPaymentRecordData.Approver = response.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approvals");
		}
		else{
			this.bulkPaymentRecordData.Approver = kony.i18n.getLocalizedString("i18n.common.NA");
		}
	  }	  
      this.view.NonEditableBulkPaymentDetails.setData(templateData, true);
      this.view.flxAddPayment.isVisible=false;
      this.view.NonEditableBulkPaymentDetails.btnEdit.isVisible = false;
      this.view.CommonFormActionsNew.isVisible = false;
      this.view.formActionsNew.isVisible = true;
      this.view.formActionsNew.btnBack.isVisible = false;
      this.view.formActionsNew.btnNext.text = kony.i18n.getLocalizedString("i18n.stopChecks.ViewRequests");
      this.view.formActionsNew.btnNext.toolTip = kony.i18n.getLocalizedString("i18n.stopChecks.ViewRequests");
      this.view.formActionsNew.btnNext.skin = ViewConstants.SKINS.NEXT_BTN;
      this.view.formActionsNew.btnNext.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
      this.view.formActionsNew.btnNext.width = "20%";
      this.view.formActionsNew.btnNext.onClick = this.navToOngoingPayments.bind(this);
      this.view.formActionsNew.btnNext.isVisible = true;
      this.view.formActionsNew.btnOption.isVisible = false;
      this.view.formActionsNew.btnCancel.isVisible = false;
      this.view.flxAckMessage.isVisible = true;
      if(response.status === BBConstants.TRANSACTION_STATUS.PENDING){
        this.view.flxAckMessage.lblSuccessMessage.text = kony.i18n.getLocalizedString("kony.i18n.common.bulkApproval");
      } else{
        this.view.flxAckMessage.lblSuccessMessage.text = kony.i18n.getLocalizedString("kony.i18n.common.bulkSentStatus");
      }
      this.view.flxAckMessage.flxRightContainerInfo.lblReferenceHeader.text = kony.i18n.getLocalizedString("kony.i18n.common.paymentID");
      this.view.flxAckMessage.flxRightContainerInfo.lblReferenceNumber.text = response.paymentId;
      this.view.imgDropDown.text = ViewConstants.FONT_ICONS.CHEVRON_DOWN;
      this.view.TabBodyNew1.isVisible = false;
      this.view.flxSearchRecipients.isVisible = false;
	  if(! kony.sdk.isNullOrUndefined(this.bulkPaymentRecordData.requestId) && this.bulkPaymentRecordData.requestId !== kony.i18n.getLocalizedString("i18n.common.NA")){
	    	this.fetchRequestHistory(this.bulkPaymentRecordData.requestId);
	  }
	  else{
	    	this.view.flxApprovalsHistoryInformation.setVisibility(false);	    	
	  }  
      this.adjustScreen(10);
      this.view.forceLayout();
    },
    
    showRequestHistoryData: function(segRowData) {
      this.view.flxApprovalHistoryContent.isVisible = true;
      this.view.flxApprovalsHistoryErrorMessage = false;
      this.view.lblApprovalStatusValue.text = (kony.sdk.isNullOrUndefined(this.bulkPaymentRecordData.Approver)) ? "N/A" : this.bulkPaymentRecordData.Approver ;
      var breakpoint = kony.application.getCurrentBreakpoint();
      var segHeader = {
        "lblDateAndTimeKey": {
          "text": kony.i18n.getLocalizedString("i18n.konybb.common.dateAndTime")
        },
        "lblUserIDKey": {
          "text": kony.i18n.getLocalizedString("kony.i18n.common.user"),
          "left": (breakpoint === 640 || orientationHandler.isMobile) ? "75%" : "25%",
        },
        "lblActionKey": {
          "text": kony.i18n.getLocalizedString("i18n.transfers.lblAction"),
          "left": (breakpoint === 640 || orientationHandler.isMobile) ? "75%" : "50%"
        },
        "lblCommentsKey": {
          "text": "Comments",
          "left": (breakpoint === 640 || orientationHandler.isMobile) ? "75%" : "75%",
          isVisible: true,
        }
      };
      if (segRowData.length === 0) {
        segRowData = [{
          "lblNoRecords": {
            text: kony.i18n.getLocalizedString("konybb.i18n.requestHistory.NoRecordsFound")
          },
          "flxNoRecords": {
            isVisible: true,
            height: "51dp"
          }
        }];
        this.view.flxApprovalStatus.isVisible = false;
      } else {
        segRowData.forEach(function(record) {
          var skinValue = (breakpoint === 640 || orientationHandler.isMobile) ? "sknLblSSP42424213px" : "bbSknLbl424242SSP15Px";
          var actiontsValue = (kony.sdk.isNullOrUndefined(record.Actionts)) ? "N/A" : CommonUtilities.getDateAndTimeInUTC(record.Actionts);
          var userNameValue = (kony.sdk.isNullOrUndefined(record.userName)) ? "N/A" : record.userName;
          if (record.Action === "Approved") {
            record.Action = kony.i18n.getLocalizedString("i18n.konybb.common.ApprovedRequest");
          } else if (record.Action === "Pending") {
            record.Action = kony.i18n.getLocalizedString("i18n.konybb.common.createdRequest");
          }
          var actionValue = (kony.sdk.isNullOrUndefined(record.Action)) ? "N/A" : record.Action;
          var commentsValue = (kony.sdk.isNullOrUndefined(record.Comments)) ? "N/A" : record.Comments;
          record.Actionts = {
            "text": actiontsValue,
            "skin": skinValue,

          };
          record.Action = {
            "text": actionValue,
            "skin": skinValue,
            "left": (breakpoint === 640 || orientationHandler.isMobile) ? "75%" : "50%"
          };
          record.userName = {
            "text": userNameValue,
            "skin": skinValue,
            "left": (breakpoint === 640 || orientationHandler.isMobile) ? "75%" : "25%"
          };
          record.lblComments = {
            "text": actionValue,
            "skin": skinValue,
            "left": (breakpoint === 640 || orientationHandler.isMobile) ? "75%" : "75%",
            isVisible: true,
          };
        });
        this.view.flxApprovalStatus.isVisible = true;
      }
      var dataMap = {
        "lblDateAndTimeKey": "lblDateAndTimeKey",
        "lblUserIDKey": "lblUserIDKey",
        "lblDateAndTime": "Actionts",
        "lblUserID": "userName",
        "lblAction": "Action",
        "lblActionKey": "lblActionKey",
        "flxNoRecords": "flxNoRecords",
        "lblNoRecords": "lblNoRecords",
        "lblCommentsKey": "lblCommentsKey",
        "lblComments": "lblComments"
      };
      var segDataModel = [
        [segHeader, segRowData]
      ];
      this.view.segApprovalDetails.widgetDataMap = dataMap;
      this.view.segApprovalDetails.setData(segDataModel);
      this.view.flxApprovalsHistoryInformation.setVisibility(true);
      FormControllerUtility.hideProgressBar(this.view);
      this.adjustScreen(0);
    },
    
    showHidePaymentsSeg: function() {
      if (this.view.TabBodyNew1.isVisible === true) this.view.imgDropDown.text = ViewConstants.FONT_ICONS.CHEVRON_DOWN;
      else this.view.imgDropDown.text = ViewConstants.FONT_ICONS.CHEVRON_UP;
      this.view.TabBodyNew1.isVisible = !this.view.TabBodyNew1.isVisible;
      this.view.flxSearchRecipients.isVisible = !this.view.flxSearchRecipients.isVisible;
      var existingData = this.view.TabBodyNew1.segTemplates.data[0][1];     
      this.view.TabBodyNew1.addDataForSections([existingData]);
    },
    
    onEditReviewClick: function() {
      var editAccounts = this.fetchBulkPaymentEditAccounts(); 
      this.view.flxPaymentReview.flxEditableDetails.flxListBox.lstFrmAccount.masterData = this.objectToListBoxArrayFromService(editAccounts);
      if (editAccounts.findIndex(account => account.Name === this.bulkPaymentRecordData.fromAccountMasked) != -1)
      {
        this.view.flxPaymentReview.flxEditableDetails.flxListBox.lstFrmAccount.selectedKey = this.bulkPaymentRecordData.fromAccount;
      }
      else
      {
        this.view.flxPaymentReview.flxEditableDetails.flxListBox.lstFrmAccount.selectedKey = editAccounts[0].Id;
      }
      this.view.flxPaymentReview.flxEditableDetails.flxTextBox.txtDescription.text=this.bulkPaymentRecordData.description;
      this.view.flxPaymentReview.flxEditableDetails.lblACHTitleText.text = kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentDetails");
      this.view.lblContentHeader.text = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.editPaymentDetails");
      this.view.flxPaymentReview.flxEditableDetails.setVisibility(true);
      this.view.flxPaymentReview.NonEditableBulkPaymentDetails.setVisibility(false);
      this.view.flxAckMessage.isVisible = false;
      this.view.flxPaymentReview.flxSearchBar.setVisibility(false);
      this.view.flxPaymentReview.TabBodyNew1.setVisibility(false);
      this.view.CommonFormActionsNew.btnNext.isVisible = true;
      this.view.CommonFormActionsNew.btnNext.text = kony.i18n.getLocalizedString("i18n.konybb.common.cancel");
      this.view.CommonFormActionsNew.btnNext.toolTip = kony.i18n.getLocalizedString("i18n.konybb.common.cancel");
      this.view.CommonFormActionsNew.btnNext.onClick = this.onCancelEditReview.bind(this);
      this.view.CommonFormActionsNew.btnNext.skin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnNext.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnCancel.isVisible = true;
      this.view.CommonFormActionsNew.btnCancel.text = kony.i18n.getLocalizedString("i18n.konybb.common.SaveAndUpdate");
      this.view.CommonFormActionsNew.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.konybb.common.SaveAndUpdate");
      this.view.CommonFormActionsNew.btnCancel.skin = ViewConstants.SKINS.NORMAL;
      this.view.CommonFormActionsNew.btnCancel.hoverSkin = ViewConstants.SKINS.NORMAL;
      this.view.CommonFormActionsNew.btnOption.isVisible = false;
      this.view.CommonFormActionsNew.btnBack.isVisible = false;
      this.view.CommonFormActionsNew.btnCancel.onClick = this.onSaveAndUpdateReviewclick.bind(this);
      this.adjustScreen(10);
      this.view.forceLayout();
    },
    
    onCancelEditReview: function() {
      this.view.flxPaymentReview.NonEditableBulkPaymentDetails.lblACHTitleText.text = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.PaymentSummary");
      this.view.lblContentHeader.text = kony.i18n.getLocalizedString("kony.i18n.common.bulkPaymentsReview");
      this.view.flxPaymentReview.flxEditableDetails.setVisibility(false);
      this.view.flxPaymentReview.NonEditableBulkPaymentDetails.setVisibility(true);
      this.view.flxPaymentReview.flxSearchBar.setVisibility(true);
      this.view.flxPaymentReview.TabBodyNew1.setVisibility(true);
      this.setButtonsForReview();
      this.adjustScreen(10);
      this.view.forceLayout();
    },
    
    onSaveAndUpdateReviewclick: function() {
      this.bulkPaymentRecordData.fromAccountMasked = this.view.flxPaymentReview.flxEditableDetails.flxListBox.lstFrmAccount.selectedKeyValue[1];
      this.bulkPaymentRecordData.fromAccount = this.view.flxPaymentReview.flxEditableDetails.flxListBox.lstFrmAccount.selectedKeyValue[0];
      this.bulkPaymentRecordData.description = this.view.flxPaymentReview.flxEditableDetails.flxTextBox.txtDescription.text;
      this.view.flxPaymentReview.NonEditableBulkPaymentDetails.lblACHTitleText.text = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.PaymentSummary");
      this.view.lblContentHeader.text = kony.i18n.getLocalizedString("kony.i18n.common.bulkPaymentsReview");
      this.view.flxPaymentReview.flxEditableDetails.setVisibility(false);
      this.view.flxPaymentReview.NonEditableBulkPaymentDetails.setVisibility(true);
      this.view.flxPaymentReview.flxSearchBar.setVisibility(true);
      this.view.flxPaymentReview.TabBodyNew1.setVisibility(true);
      this.setButtonsForReview();
      this.updateBulkPaymentRecord(this.bulkPaymentRecordData);
      this.adjustScreen(10);
      this.view.forceLayout();
    }, 
    
    disableReviewButtons: function() {
      this.view.flxAddPayment.isVisible = false;
      this.view.NonEditableBulkPaymentDetails.btnEdit.isVisible = false;
      this.view.CommonFormActionsNew.isVisible = false;
      this.view.formActionsNew.isVisible = true;
      this.view.formActionsNew.btnNext.text = this.btnTextApprovalReq;
      this.view.formActionsNew.btnNext.toolTip = this.btnTextApprovalReq;
      this.view.formActionsNew.btnNext.skin = ViewConstants.SKINS.NEXT_BTN;
      this.view.formActionsNew.btnNext.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
      this.view.formActionsNew.btnNext.width = "20%";
      this.view.formActionsNew.btnNext.onClick = this.navToOngoingPayments.bind(this);
	  if(this.isMakerHistoryFlow){
		  this.view.formActionsNew.btnNext.onClick = this.navToPaymentHistory.bind(this);
	  }
      this.view.formActionsNew.btnNext.isVisible = true;
      this.view.formActionsNew.btnOption.isVisible = false;
      this.view.formActionsNew.btnCancel.isVisible = false;
    },
    
	enableReviewButtons: function() {
	  this.view.flxSearchRecipientsBox.width="82%";
      this.view.flxAddPayment.isVisible = true;
      this.view.NonEditableBulkPaymentDetails.btnEdit.isVisible = this.checkUserPermission("BULK_PAYMENT_REQUEST_EDIT");
	  this.view.CommonFormActionsNew.isVisible = true;
      this.view.formActionsNew.isVisible = false;
      this.view.CommonFormActionsNew.btnBack.isVisible = true;
      this.view.CommonFormActionsNew.btnBack.text = kony.i18n.getLocalizedString("i18n.stopChecks.ViewRequests");
      this.view.CommonFormActionsNew.btnBack.toolTip = kony.i18n.getLocalizedString("i18n.stopChecks.ViewRequests");
      this.view.CommonFormActionsNew.btnBack.skin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnBack.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnBack.width = "20%";
      this.view.CommonFormActionsNew.btnBack.onClick = this.navToOngoingPayments.bind(this);
	  if(this.isMakerHistoryFlow){
		  this.view.CommonFormActionsNew.btnBack.onClick = this.navToPaymentHistory.bind(this);
	  }
      this.view.CommonFormActionsNew.btnNext.isVisible = false;
      this.view.CommonFormActionsNew.btnOption.isVisible = true;
      this.view.CommonFormActionsNew.btnOption.text = kony.i18n.getLocalizedString("i18n.CustomerFeedback.Submit");
      this.view.CommonFormActionsNew.btnOption.toolTip = kony.i18n.getLocalizedString("i18n.CustomerFeedback.Submit");
      this.view.CommonFormActionsNew.btnOption.skin = ViewConstants.SKINS.NORMAL;
      this.view.CommonFormActionsNew.btnOption.hoverSkin = ViewConstants.SKINS.NORMAL;
      this.view.CommonFormActionsNew.btnOption.width = "20%";
      this.view.CommonFormActionsNew.btnCancel.isVisible = true;
      this.view.CommonFormActionsNew.btnCancel.text = kony.i18n.getLocalizedString("kony.i18n.common.cancelBulkPayment");
      this.view.CommonFormActionsNew.btnCancel.toolTip = kony.i18n.getLocalizedString("kony.i18n.common.cancelBulkPayment");
      this.view.CommonFormActionsNew.btnCancel.skin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnCancel.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnCancel.width = "20%";
      this.view.CommonFormActionsNew.btnCancel.onClick = this.onCancelBulkPaymentRecord.bind(this.bulkPaymentRecordData);
	},
	
    setButtonsForReview: function() {
      this.view.formActionsNew.isVisible = false;
      this.view.CommonFormActionsNew.btnBack.isVisible = true;
      this.view.CommonFormActionsNew.btnBack.text = kony.i18n.getLocalizedString("i18n.stopChecks.ViewRequests");
      this.view.CommonFormActionsNew.btnBack.toolTip = kony.i18n.getLocalizedString("i18n.stopChecks.ViewRequests");
      this.view.CommonFormActionsNew.btnBack.skin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnBack.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnBack.width = "20%";
      this.view.CommonFormActionsNew.btnBack.onClick = this.navToOngoingPayments.bind(this);
      this.view.CommonFormActionsNew.btnNext.isVisible = false;
      this.view.CommonFormActionsNew.btnOption.isVisible = true;
      this.view.CommonFormActionsNew.btnOption.text = kony.i18n.getLocalizedString("i18n.CustomerFeedback.Submit");
      this.view.CommonFormActionsNew.btnOption.toolTip = kony.i18n.getLocalizedString("i18n.CustomerFeedback.Submit");
      this.view.CommonFormActionsNew.btnOption.skin = ViewConstants.SKINS.NORMAL;
      this.view.CommonFormActionsNew.btnOption.hoverSkin = ViewConstants.SKINS.NORMAL;
      this.view.CommonFormActionsNew.btnOption.width = "20%";
      this.view.CommonFormActionsNew.btnCancel.isVisible = true;
      this.view.CommonFormActionsNew.btnCancel.text = kony.i18n.getLocalizedString("kony.i18n.common.cancelBulkPayment");
      this.view.CommonFormActionsNew.btnCancel.toolTip = kony.i18n.getLocalizedString("kony.i18n.common.cancelBulkPayment");
      this.view.CommonFormActionsNew.btnCancel.skin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnCancel.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
      this.view.CommonFormActionsNew.btnCancel.width = "20%";
      this.view.CommonFormActionsNew.btnOption.onClick = this.onSubmitForApproval.bind(this);
    },
    
 
    onViewPaymentsScreenForApprovalDashboard: function(isEditFlow,BackButtonText) {

        this.resetUI();
        var flag;
        if (isEditFlow === true) flag = false;
        else flag = true;
        this.view.flxPaymentReview.isVisible = true;
        this.view.flxAddPayment.isVisible = false;
      	this.view.flxSearchBar.isVisible = true;
        this.view.flxSearchRecipientsBox.width="98%";
        this.view.imgDropDown.text = ViewConstants.FONT_ICONS.CHEVRON_UP;
        this.view.lblContentHeader.text =kony.i18n.getLocalizedString("i18n.kony.BulkPayments.approveReviewPayment");
        this.view.NonEditableBulkPaymentDetails.btnEdit.isVisible = false;
        var templateData = {
            "Description": this.bulkPaymentRecordData.description ,
            "Initiated By": this.bulkPaymentRecordData.initiatedby,
            "Transfer Initiated On": this.bulkPaymentRecordData.scheduledDate,
            "Payment Date": this.bulkPaymentRecordData.paymentDate,
            "Total Amount": this.bulkPaymentRecordData.totalAmount ,
            "From Account": this.bulkPaymentRecordData.fromAccount,
            "Total Transactions" : this.bulkPaymentRecordData.totalTransactions ,
            "Bulk Reference ID": this.bulkPaymentRecordData.recordId,
        };
        this.view.NonEditableBulkPaymentDetails.setData(templateData, true);
        this.view.lblRecordHeader.text = kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentDetails");
        this.view.TabBodyNew1.segTemplates.rowTemplate = "flxBulkPayementRowTemplate";
        this.view.TabBodyNew1.segTemplates.sectionHeaderTemplate = "flxBulkPayementHeader";
        this.view.TabBodyNew1.segTemplates.bottom = "10dp";
        this.view.TabBodyNew1.addOnlySectionHeaders(this.getSectionHeadersForNewAccounts());
        this.view.CommonFormActionsNew.isVisible = true;
        this.view.formActionsNew.isVisible = false;
        this.view.CommonFormActionsNew.btnBack.isVisible = true;
     	this.view.CommonFormActionsNew.btnBack.text = BackButtonText;
        this.view.CommonFormActionsNew.btnBack.toolTip = BackButtonText;
        var approveRejectButtonsVisibilty = false;
      	if(BackButtonText === kony.i18n.getLocalizedString("i18n.kony.BulkPayments.backToPendingApprovals"))
        {
             this.view.CommonFormActionsNew.btnBack.onClick = this.navToPendingApprovals.bind(this);
             approveRejectButtonsVisibilty = true;
        }
      	else if(BackButtonText === kony.i18n.getLocalizedString("i18n.kony.BulkPayments.backToPendingRequests"))
        {
             this.view.CommonFormActionsNew.btnBack.onClick = this.navToPendingRequests.bind(this);
             this.view.lblContentHeader.text =kony.i18n.getLocalizedString("i18n.kony.BulkPayments.requestReviewPayment");
        }
        else if(BackButtonText === kony.i18n.getLocalizedString("i18n.kony.BulkPayments.backToApprovalHistory"))
        {
        	this.view.CommonFormActionsNew.btnBack.onClick = this.navToApprovalHistory.bind(this);
        }
        else if(BackButtonText === kony.i18n.getLocalizedString("i18n.kony.BulkPayments.backToRequestHistory"))
        {
        	this.view.CommonFormActionsNew.btnBack.onClick = this.navToRequestHistory.bind(this);
        	this.view.lblContentHeader.text =kony.i18n.getLocalizedString("i18n.kony.BulkPayments.requestReviewPayment");
        }

        this.view.CommonFormActionsNew.btnBack.skin = ViewConstants.SKINS.NEXT_BTN;
        this.view.CommonFormActionsNew.btnBack.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
        this.view.CommonFormActionsNew.btnBack.width = "20%";
        this.view.CommonFormActionsNew.btnNext.isVisible = false;
        this.view.CommonFormActionsNew.btnOption.isVisible = true;
        this.view.CommonFormActionsNew.btnOption.text = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.approveAndInitiatePayment");
        this.view.CommonFormActionsNew.btnOption.toolTip = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.approveAndInitiatePayment");
        this.view.CommonFormActionsNew.btnOption.skin = ViewConstants.SKINS.NORMAL;
        this.view.CommonFormActionsNew.btnOption.hoverSkin = ViewConstants.SKINS.NORMAL;
        this.view.CommonFormActionsNew.btnOption.width = "20%";
        this.view.CommonFormActionsNew.btnCancel.isVisible = true;
        this.view.CommonFormActionsNew.btnCancel.text = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.rejectPayment");
        this.view.CommonFormActionsNew.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.rejectPayment");
        this.view.CommonFormActionsNew.btnCancel.skin = ViewConstants.SKINS.NEXT_BTN;
        this.view.CommonFormActionsNew.btnCancel.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
        this.view.CommonFormActionsNew.btnCancel.width = "20%";
        this.view.CommonFormActionsNew.btnOption.onClick = this.approvePayment.bind(this,this.bulkPaymentRecordData.requestId, BackButtonText);
        this.view.CommonFormActionsNew.btnCancel.onClick = this.showRejectBulkpaymentPopup.bind(this,this.bulkPaymentRecordData.requestId, BackButtonText);        
        if(! approveRejectButtonsVisibilty){
        	this.view.CommonFormActionsNew.btnCancel.isVisible = false;
        	this.view.CommonFormActionsNew.btnOption.isVisible = false;
        }
        this.view.flxDropDown.onClick = this.showHidePaymentsSeg.bind(this);
        this.view.flxAcknowledgementNew.flxImgdownload.onTouchEnd = function() {
            var scopeObj = this;
            scopeObj.view.flxAcknowledgementContainer.setVisibility(false);
        }.bind(this);
      this.view.tbxSearchBox.onKeyUp =this.onPaymentOrdersKeyUp.bind(this);
      this.view.tbxSearchBox.onDone =this.onPaymentOrdersSearchDone.bind(this);
      
        var sectionData = {
            "flxAddRecipients": {
                "isVisible": false
            },
            "flxViewRecipients": {
                "isVisible": true
            },
            "flxTopSeperator": {
                "isVisible": true
            },
            "btnViewRecipientName": {
                "text": kony.i18n.getLocalizedString("i18n.TransfersEur.Beneficiary"),
                "onClick": function(eventobject, context) {
                    if (nameSort) {
                        this.view.TabBodyNew1.sortData(context.sectionIndex, "", "", "lblAccountName", "String", "Asc");
                        this.accountSortType = "AccountNameAsc";
                    } else {
                        this.view.TabBodyNew1.sortData(context.sectionIndex, "", "", "lblAccountName", "String", "Desc");
                        this.accountSortType = "AccountNameDesc";
                    }
                }.bind(this),
                "isVisible": true
            },
            "imgSortViewRecipientName": {
                "isVisible": true,
                "src": "sortingfinal.png",
            },
            "btnViewBankName": {
                "text": kony.i18n.getLocalizedString("i18n.konybb.Common.Amount"),
                "onClick": function(eventobject, context) {
                    if (numberSort) {
                        this.view.TabBodyNew1.sortData(context.sectionIndex, "", "", "lblAccountNumber", "ObjectNumber", "Asc");
                        this.accountSortType = "AccountNumberAsc";
                    } else {
                        this.view.TabBodyNew1.sortData(context.sectionIndex, "", "", "lblAccountNumber", "ObjectNumber", "Desc");
                        this.accountSortType = "AccountNumberDesc";
                    }
                    numberSort = !numberSort;
                }.bind(this),
                "isVisible": true
            },
            "imgSortViewBankName": {
                "isVisible": true,
                "src": "sortingfinal.png",
            },
            "btnViewAmount": {
                "text": kony.i18n.getLocalizedString("i18n.billPay.Status"),
                "onClick": function(eventobject, context) {
                    if (typeSort) {
                        this.view.TabBodyNew1.sortData(context.sectionIndex, "", "", "lblAccountType", "ObjectText", "Asc");
                        this.accountSortType = "AccountTypeAsc";
                    } else {
                        this.view.TabBodyNew1.sortData(context.sectionIndex, "", "", "lblAccountType", "ObjectText", "Desc");
                        this.accountSortType = "AccountTypeDesc";
                    }
                    typeSort = !typeSort;
                }.bind(this),
                "isVisible": true
            },
            "imgSortViewAmount": {
                "isVisible": true,
                "src": "sortingfinal.png",
            },
            "btnViewAction": {
                "text": kony.i18n.getLocalizedString("i18n.wireTransfers.Actions"),
                "isVisible": flag === true ? true : false,
            },
            "imgFlxBottomSeparator": {
                "text": "-"
            },
            "imgFlxTopSeparator": {
                "text": "-"
            },
            "flxBottomSeperator": {
                "isVisible": true
            },
        };
        var rowDataMap = {
            "lblRecipientsType": "lblRecipientsType",
            "imgRecipTypeDrpdown": "imgRecipTypeDrpdown",
            "imgFlxSeperator": "imgFlxSeperator",
            "imgFlxBottomSeparator": "imgFlxBottomSeparator",
            "imgDropDown": "imgDropDown",
            "lblViewRecipientName": "lblViewRecipientName",
            "lblViewBankName": "lblViewBankName",
            "lblViewAmount": "lblViewAmount",
            "btnViewActions": "btnViewActions",
            "imgFlxTopSeparator": "imgFlxTopSeparator",
            "imgSample": "imgSample",
            "lblAccountNo": "lblAccountNo",
            "lblAccountNoValue": "lblAccountNoValue",
            "lblAccType": "lblAccType",
            "lblAccTypeValue": "lblAccTypeValue",
            "lblSwiftCode": "lblSwiftCode",
            "lblSwiftCodeValue": "lblSwiftCodeValue",
            "lblPayRef": "lblPayRef",
            "lblPayRefValue": "lblPayRefValue",
            "lblFees": "lblFees",
            "lblFeesValue": "lblFeesValue",
            "btnEdit": "btnEdit",
            "btnViewRecipientName": "btnViewRecipientName",
            "imgSortViewRecipientName": "imgSortViewRecipientName",
            "btnViewBankName": "btnViewBankName",
            "imgSortViewBankName": "imgSortViewBankName",
            "btnViewAmount": "btnViewAmount",
            "imgSortViewAmount": "imgSortViewAmount",
            "btnViewAction": "btnViewAction",
            "flxAddRecipients": "flxAddRecipients",
            "flxViewRecipients": "flxViewRecipients",
            "flxRecipientsType": "flxRecipientsType",
            "flxMain": "flxMain",
            "flxTopSeperator": "flxTopSeperator"
        };
        var defaultValues = {
            flxMain: {
                "height": "51dp"
            },
            "btnViewActions": {
                "text": "Edit",
                "onClick": function(eventobject, context) {
                    this.view.lblBankNameKey.text = kony.i18n.getLocalizedString("i18n.CheckImages.Bank");
                    this.view.lblFeesOpt2.text = kony.i18n.getLocalizedString("i18n.TransfersEur.Beneficiary");
                    this.view.lblFeesOpt3.text = kony.i18n.getLocalizedString("i18n.TransfersEur.Both5050");
                    this.view.flxAckMessage.isVisible = false;
                    this.onClickContinueAddExistingRecipients();
                    this.view.flxInfo.isVisible = false;
                    this.view.lblAddHeader.text = kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentDetails");
                    this.view.lblContentHeader.text = kony.i18n.getLocalizedString("kony.i18n.common.editPayment");
                    this.view.CommonFormActionsNew.btnNext.isVisible = false;
                    this.view.CommonFormActionsNew.btnCancel.isVisible = false;
                    this.view.CommonFormActionsNew.btnOption.text = kony.i18n.getLocalizedString("i18n.PayAPerson.Update");
                    this.view.CommonFormActionsNew.btnOption.toolTip = kony.i18n.getLocalizedString("i18n.PayAPerson.Update");
                    this.view.CommonFormActionsNew.btnOption.onClick = this.navBackToPaymentOrders.bind(this);
                }.bind(this),
                "isVisible": flag === true ? true : false,
            },
            "imgFlxSeperator": {
                "text": "-"
            },
            "flxBulkPayementRowHeader": {
                "skin": "bbSKnFlxffffff"
            },
            "flxBottomSeparator": {
                "isVisible": false
            },
            "imgFlxBottomSeparator": {
                "text": "-"
            },
            "flxSeperator": {
                "isVisible": false
            },
            "imgDropDown": {
                "skin": "sknLblFontTypeIcon1a98ff12pxOther",
                "text": "O",
                "isVisible": true
            },
            "flxTopSeperator": {
                "isVisible": true
            },
            "flxDetailsHighlighter": {
                "isVisible": true
            },
            "lblAccountNo": {
                "text": kony.i18n.getLocalizedString("kony.i18n.common.accountNumber"),
                "isVisible": true,
            },
            "lblAccType": {
                "text": kony.i18n.getLocalizedString("kony.i18n.common.swiftcode"),
                "isVisible": true,
            },
            "lblSwiftCode": {
                "text": kony.i18n.getLocalizedString("i18n.CheckImages.Bank"),
                "isVisible": true,
            },
            "lblPayRef": {
                "text": kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentReference"),
                "isVisible": true,
            },
            "lblFees": {
                "text": kony.i18n.getLocalizedString("i18n.TransfersEur.FeesPaidBy"),
                "isVisible": true,
            },
            "btnEdit": {
                "text": kony.i18n.getLocalizedString("i18n.PayAPerson.payAPersonDeregister"),
                "isVisible": flag === true ? true : false,
                "onClick": function(eventobject, context) {
                    this.view.PopupHeaderUM.lblHeading.text = kony.i18n.getLocalizedString("i18n.common.confirm");
                    this.showPopUp(kony.i18n.getLocalizedString("kony.i18n.common.removePayments"), this.removePaymentOrder.bind(this, eventobject, context))
                }.bind(this)
            },
            "btnDelete": {
                "isVisible": false
            },
            "btnViewDetails": {
                "isVisible": false
            }
        };        
        this.view.TabBodyNew1.setSectionData([sectionData]);
        this.view.TabBodyNew1.setRowDataMap([rowDataMap]);
        this.view.TabBodyNew1.setDefaultValues([defaultValues]);
        this.view.TabBodyNew1.addDataForSections([this.paymentOrdersData]);
        this.view.imgDropDown.text = ViewConstants.FONT_ICONS.CHEVRON_DOWN;
        this.view.TabBodyNew1.isVisible = false;
        this.view.flxSearchRecipients.isVisible = false;        
        if(! kony.sdk.isNullOrUndefined(this.bulkPaymentRecordData.requestId) && this.bulkPaymentRecordData.requestId !== kony.i18n.getLocalizedString("i18n.common.NA")){
        	this.fetchRequestHistory(this.bulkPaymentRecordData.requestId);
        }
        else{
        	this.view.flxApprovalsHistoryInformation.setVisibility(false);
        	FormControllerUtility.hideProgressBar(this.view);	
        }        
    },

    approvePayment: function(requestId, btnText) {
      var params = { 
      				 "requestId" : requestId
      			   }
      this.bulkPaymentsModule.presentationController.approvePayment(params, btnText);
    },
    
    showPaymentApproveRejectaAcknowledgment: function(data, status) {
     	var BackButtonText = data.btnText;
     	this.bulkPaymentRecordData.status=status;
     	if(!kony.sdk.isNullOrUndefined(this.bulkPaymentRecordData.receivedApprovals) && status === BBConstants.TRANSACTION_STATUS.APPROVED){
     		this.bulkPaymentRecordData.receivedApprovals = Number.parseInt(this.bulkPaymentRecordData.receivedApprovals)+1;
     	}
     	if((status === BBConstants.TRANSACTION_STATUS.APPROVED) && (!kony.sdk.isNullOrUndefined(this.bulkPaymentRecordData.receivedApprovals))&& (!kony.sdk.isNullOrUndefined(this.bulkPaymentRecordData.requiredApprovals))) {
			this.bulkPaymentRecordData.Approver = this.bulkPaymentRecordData.receivedApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.of") + " " + this.bulkPaymentRecordData.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approved");
		}
		else if(status === BBConstants.TRANSACTION_STATUS.REJECTED) {
			this.bulkPaymentRecordData.Approver = 1 + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Rejection");
		}				
		else{
			this.bulkPaymentRecordData.Approver = kony.i18n.getLocalizedString("i18n.common.NA");
		}
        this.view.lblContentHeader.text =kony.i18n.getLocalizedString("i18n.kony.BulkPayments.transactionStatus");
        this.view.flxDisplayErrorMessage.isVisible = false;
        this.view.flxSearchBar.isVisible = false;
        this.view.flxPaymentReview.NonEditableBulkPaymentDetails.lblACHTitleText.text = kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentDetails");
        this.view.flxAddPayment.isVisible = false;
        this.view.CommonFormActionsNew.isVisible = false;
        this.view.formActionsNew.isVisible = true;
        this.view.formActionsNew.btnBack.isVisible = false;
       	this.view.formActionsNew.btnNext.text = BackButtonText;
        this.view.formActionsNew.btnNext.toolTip = BackButtonText;
       	if(BackButtonText === kony.i18n.getLocalizedString("i18n.kony.BulkPayments.backToPendingApprovals"))
        {
          	this.view.formActionsNew.btnNext.onClick = this.navToPendingApprovals.bind(this);
        }
        else if(BackButtonText === kony.i18n.getLocalizedString("i18n.kony.BulkPayments.backToPendingRequests"))
        {
          	this.view.formActionsNew.btnNext.onClick = this.navToPendingRequests.bind(this);
        }

        this.view.formActionsNew.btnNext.skin = ViewConstants.SKINS.NEXT_BTN;
        this.view.formActionsNew.btnNext.hoverSkin = ViewConstants.SKINS.NEXT_BTN;
        this.view.formActionsNew.btnNext.width = "20%";
        this.view.formActionsNew.btnNext.isVisible = true;
        this.view.formActionsNew.btnOption.isVisible = false;
        this.view.formActionsNew.btnCancel.isVisible = false;
        this.view.flxAckMessage.isVisible = true;
        if(status == BBConstants.TRANSACTION_STATUS.APPROVED)
          this.view.flxAckMessage.lblSuccessMessage.text = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.successmessageApprovals");
        else
          this.view.flxAckMessage.lblSuccessMessage.text = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.rejectPaymentOrder");
        this.view.flxAckMessage.flxRightContainerInfo.isVisible = false;  
        var templateData = {
            "Description": this.bulkPaymentRecordData.description ,
            "Initiated By": this.bulkPaymentRecordData.initiatedby,
            "Transfer Initiated On": this.bulkPaymentRecordData.scheduledDate,
            "Payment Date": this.bulkPaymentRecordData.paymentDate,
            "Total Amount": this.bulkPaymentRecordData.totalAmount ,
            "From Account": this.bulkPaymentRecordData.fromAccount,
            "Total Transactions" : this.bulkPaymentRecordData.totalTransactions ,
            "Bulk Reference ID": this.bulkPaymentRecordData.recordId,
        };
        this.view.NonEditableBulkPaymentDetails.setData(templateData, true);
        this.view.imgDropDown.text = ViewConstants.FONT_ICONS.CHEVRON_DOWN;
        this.view.TabBodyNew1.isVisible = false;
        this.view.flxSearchRecipients.isVisible = false;
        if(! kony.sdk.isNullOrUndefined(this.bulkPaymentRecordData.requestId) && this.bulkPaymentRecordData.requestId !== kony.i18n.getLocalizedString("i18n.common.NA")){
			this.fetchRequestHistory(this.bulkPaymentRecordData.requestId);
		}
		else{
			this.view.flxApprovalsHistoryInformation.setVisibility(false);		
			FormControllerUtility.hideProgressBar(this.view);
		} 		
    },

    showRejectBulkpaymentPopup : function(requestId, BackButtonText) {
        var popupConfig = {};
        popupConfig.header = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.rejectBMR");
        popupConfig.msg = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.areYouSureWantToReject");
        popupConfig.commentsHeader = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.whyAreYouRejecting");
        popupConfig.commentsVisibility = true;
        popupConfig.commentsText = "";
        popupConfig.nextText = kony.i18n.getLocalizedString("i18n.common.yes");
        popupConfig.cancelText = kony.i18n.getLocalizedString("i18n.common.no");	
        popupConfig.nextOnClick = this.rejectPayment.bind(this, requestId, BackButtonText);      
       	this.showPopup( popupConfig );
    },
    
    showPopup: function( popupConfig ) {
        this.view.flxPopupConfirmation.height = this.view.customheader.info.frame.height + this.view.flxContentContainer.info.frame.height +this.view.flxFooter.info.frame.height;
        this.view.flxPopupNew.lblHeader.text = popupConfig.header;
        this.view.flxPopupNew.lblPopupMsg.text = popupConfig.msg;
        this.view.flxPopupNew.flxComments.isVisible = popupConfig.commentsVisibility;
        this.view.flxPopupNew.trComments.text = popupConfig.commentsText;
        this.view.flxPopupNew.formActionsNew.btnNext.text = popupConfig.nextText;
        this.view.flxPopupNew.formActionsNew.btnCancel.text = popupConfig.cancelText;
        this.view.flxPopupNew.formActionsNew.btnNext.onClick = popupConfig.nextOnClick;
        this.view.flxPopupNew.formActionsNew.btnCancel.onClick = this.hidePopup;
        this.view.flxPopupNew.flxClose.isVisible = true;
        this.view.flxPopupNew.flxClose.cursorType = "pointer";
        this.view.flxPopupNew.flxClose.onClick = this.hidePopup;
        this.view.flxPopupConfirmation.isVisible = true;
        this.view.flxPopupNew.trComments.onKeyUp = function(){
          if(this.view.flxPopupNew.trComments.text.trim() !== "" && this.view.flxPopupNew.trComments.text !== null){
            CommonUtilities.enableButton(this.view.flxPopupNew.formActionsNew.btnNext);
          }
          else{
            CommonUtilities.disableButton(this.view.flxPopupNew.formActionsNew.btnNext);    
          }
        }.bind(this);
        CommonUtilities.disableButton(this.view.flxPopupNew.formActionsNew.btnNext);
        this.view.flxPopupNew.trComments.placeholder = "";
        if(! kony.sdk.isNullOrUndefined(popupConfig.commentsHeader)){
        	this.view.flxPopupNew.lblCommnets.text = popupConfig.commentsHeader;
        }
        this.view.forceLayout();
    },
    
    hidePopup: function(){
        this.view.flxPopupNew.trComments.text = "";
        this.view.flxPopupConfirmation.isVisible = false;
        this.view.forceLayout();
    },
    
    rejectPayment: function(requestId, BackButtonText) {
        var Comments = this.view.flxPopupNew.trComments.text;
        var params = { 
          "requestId" : requestId,
          "comments" : Comments
        }
        this.bulkPaymentsModule.presentationController.rejectPayment(params, BackButtonText);
        this.hidePopup();
    },
    
    navToPendingApprovals: function() {
        var ApprovalsAndRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
        ApprovalsAndRequestsModule.presentationController.noServiceNavigateToForm("frmBBMyApprovals", BBConstants.MYAPPROVALS_BULK_PAYMENTS);
    },

    navToPendingRequests: function() {
        var ApprovalsAndRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
        ApprovalsAndRequestsModule.presentationController.noServiceNavigateToForm("frmBBMyRequests", BBConstants.MYREQUESTS_BULK_PAYMENTS);
    },
	
	 navToApprovalHistory: function() {
        var ApprovalsAndRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
        ApprovalsAndRequestsModule.presentationController.noServiceNavigateToForm("frmBBApprovalHistory", BBConstants.DASHBOARD_DEFAULT_TAB);
    },

    navToRequestHistory: function() {
        var ApprovalsAndRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
        ApprovalsAndRequestsModule.presentationController.noServiceNavigateToForm("frmBBRequestHistory", BBConstants.DASHBOARD_DEFAULT_TAB);
    },
    
	getPaymentOrders: function(reqData) {        
        this.bulkPaymentsModule.presentationController.fetchPaymentOrders(reqData);
    },

    fetchBulkPaymentOrders: function(bulkPaymentRecordData){
    	this.getPaymentOrders( { 
    			"recordId" : bulkPaymentRecordData.recordId
    	});
    },
    
    fetchBulkPaymentRecordDetailsById: function(){
      
      var params = {
        "recordId": this.bulkPaymentRecordData.recordId               
      }
    	 
      this.bulkPaymentsModule.presentationController.fetchBulkPaymentRecordDetailsById(params);
    },

    fetchRequestHistory : function(requestId){
    	this.bulkPaymentsModule.presentationController.getRequestsHistory({
    		"Request_id" : requestId
    	});	
    },
    
    cancelBulkPaymentRecord: function(bulkPaymentRecordData){
      var Comments = this.view.flxPopupNew.trComments.text;
      this.cancelBMR({
        "recordId": this.bulkPaymentRecordData.recordId,
        "comments": Comments
      });
    },
	
	updateBulkPaymentRecord: function(bulkPaymentRecordData){
    	this.saveAndUpdateBMR( { 
    			"recordId" : bulkPaymentRecordData.recordId,
				"fromAccount" : bulkPaymentRecordData.fromAccount,
				"description" : bulkPaymentRecordData.description
		});
    },
	
    saveAndUpdateBMR: function(reqData) {        
        this.bulkPaymentsModule.presentationController.updateBulkPaymentRecord(reqData);
    },
    
    cancelBMR: function(reqData) {        
        this.bulkPaymentsModule.presentationController.cancelBulkPaymentRecord(reqData);
    },
    
    onupdateBulkPaymentRecordSuccess: function(reqData) {
      this.bulkPaymentRecordData.fromAccountMasked = this.view.flxPaymentReview.flxEditableDetails.flxListBox.lstFrmAccount.selectedKeyValue[1];
      this.bulkPaymentRecordData.description = this.view.flxPaymentReview.flxEditableDetails.flxTextBox.txtDescription.text;
      var templateData = {
        "Description": this.bulkPaymentRecordData.description ,
        "Initiated By": this.bulkPaymentRecordData.initiatedby,
        "Transfer Initiated On": this.bulkPaymentRecordData.scheduledDate,
        "Payment Date": this.bulkPaymentRecordData.paymentDate,
        "Total Amount": this.bulkPaymentRecordData.totalAmount ,
        "From Account": this.bulkPaymentRecordData.fromAccountMasked,
        "Total Transactions" : this.bulkPaymentRecordData.totalTransactions ,
        "Bulk Reference ID": this.bulkPaymentRecordData.recordId,
      };
      this.view.NonEditableBulkPaymentDetails.setData(templateData, true);
      this.view.flxAckMessage.isVisible = true;
      this.view.flxAckMessage.lblSuccessMessage.text = kony.i18n.getLocalizedString("kony.i18n.common.ItemUpdated");
      this.view.flxAckMessage.flxRightContainerInfo.lblReferenceHeader.isVisible = false;
      this.view.flxAckMessage.flxRightContainerInfo.lblReferenceNumber.isVisible = false;
      var self =this;
      this.view.CommonFormActionsNew.btnOption.onClick = function() {
        self.bulkPaymentsModule.presentationController.submitPaymentOrder({
          "recordId": self.bulkPaymentRecordData.recordId
        });
      };
      FormControllerUtility.hideProgressBar(this.view);
    },
    
    onCancelBulkPaymentRecord: function(bulkPaymentRecordData) {
      var popupConfig = {};
      popupConfig.header = kony.i18n.getLocalizedString("i18n.common.confirm");
      popupConfig.msg = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.cancelBMR");
      popupConfig.commentsVisibility = true;
      popupConfig.commentsText = "";
      popupConfig.nextText = kony.i18n.getLocalizedString("i18n.common.yes");
      popupConfig.cancelText = kony.i18n.getLocalizedString("i18n.common.no");
      popupConfig.nextOnClick = this.cancelBulkPaymentRecord.bind(this);
      this.showPopup(popupConfig);
    },

    oncancelBulkPaymentRecordSuccess: function(reqData) {
      this.hidePopup();
      this.navToOngoingPayments();
      FormControllerUtility.hideProgressBar(this.view);
    },
    
    fetchBulkPaymentEditAccounts : function() {
      var editAccounts = [];
      applicationManager.getConfigurationManager().userAccounts.forEach(function (obj) {
        var account = {
          "Id": obj.accountID,
          "Name": CommonUtilities.getMaskedAccount(obj.accountName, obj.accountID)
        };
        if(applicationManager.getConfigurationManager().checkAccountAction(obj.accountID,"BULK_PAYMENT_REQUEST_EDIT")) 
             editAccounts.push(account);
        });
      return editAccounts;
    },
    
	objectToListBoxArrayFromService: function (objArray) {
      var list = [];
      for (var i = 0; i < objArray.length; i ++ ) {
        list.push([objArray[i].Id, objArray[i].Name]);
      }
      return list;
    },
    
    checkUserPermission : function (permission) {
        return applicationManager.getConfigurationManager().checkUserPermission(permission);
    },
  }
});