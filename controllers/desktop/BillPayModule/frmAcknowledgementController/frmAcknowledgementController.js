/**
 * Description of Module representing a Confirm form.
 * @module frmAckowledgementController
 */
define(['commonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility'], function (commonUtilities, OLBConstants, ViewConstants, FormControllerUtility) {
  var orientationHandler = new OrientationHandler();
  return  /** @alias module:frmAckowledgementController */{
    /** updates the present Form based on required function.
       * @param {uiDataMap[]} uiDataMap
    */
    updateFormUI: function (uiDataMap) {
      if (uiDataMap.isLoading) {
        FormControllerUtility.showProgressBar(this.view);
      } else {
          FormControllerUtility.hideProgressBar(this.view);
      }
      if (uiDataMap.sideMenu) {
        this.updateHamburgerMenu(uiDataMap.sideMenu);
      }
      if (uiDataMap.topBar) {
        this.updateTopBar(uiDataMap.topBar);
      }
      if (uiDataMap.bulkPay) {
        this.updateBulkPayUIAcknowledge(uiDataMap.bulkPay);
      }
      if (uiDataMap.transferAcknowledge) {
        this.updateTransferAcknowledgeUI(uiDataMap.transferAcknowledge);
      }
      if (uiDataMap.ackPayABill) {
        this.updateBillPayAcknowledgeUI(uiDataMap.ackPayABill);
      }
      this.AdjustScreen();
    },
    /**
     * used o update the BulkPay Acknoweldgement screen
     * @param {object} bulkPay list of responses
     *
     */
    updateBulkPayUIAcknowledge: function (bulkPay) {
      this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      var self = this;
      var totalSum = 0;
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
      }, {
        text: kony.i18n.getLocalizedString("i18n.billPay.AcknowledgementBillPay")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.AcknowledgementBillPay");
      this.view.flxContainer.isVisible = false;
      this.view.flxMainBulkPay.isVisible = true;
      var bulkPayAckWidgetDataMap = {
        "lblRefernceNumber": "lblRefernceNumber",
        "lblPayee": "lblPayee",
       // "lblPayeeAddress": "lblPayeeAddress",
        "lblPaymentAccount": "lblPaymentAccount",
        "lblEndingBalanceAccount": "lblEndingBalanceAccount",
        "lblSendOn": "lblSendOn",
        "lblDeliverBy": "lblDeliverBy",
        "lblAmount": "lblAmount",
        "imgAcknowledgement": "imgAcknowledgement"
      };
      bulkPay = bulkPay.map(function (dataItem) {
        var ackImage;
        var deliverBy = dataItem.deliverBy;
        if (deliverBy.length > 10) {
          deliverBy = self.getDateFromDateString(dataItem.deliverBy, "YYYY-MM-DDTHH:MM:SS");
        }
        else {
          deliverBy = dataItem.deliverBy;
        }
        if (dataItem.transactionId !== "" || dataItem.transactionId !== null || dataItem.transactionId !== undefined) {
          ackImage = ViewConstants.IMAGES.BULKPAY_SUCCESS;
          totalSum = totalSum + parseFloat(dataItem.amount);
        } else {
          ackImage = ViewConstants.IMAGES.BULKPAY_PENDING;
        }
        return {
          "lblRefernceNumber": dataItem.transactionId,
          "lblPayee": dataItem.payeeName,
          "lblPayeeAddress": "Address: " + dataItem.payeeAddressLine1,
          "lblPaymentAccount": dataItem.fromAccountName,
          "lblEndingBalanceAccount": "Ending Balance: " + commonUtilities.formatCurrencyWithCommas(dataItem.fromAccountBalance),
          "lblSendOn": self.getDateFromDateString(dataItem.transactionDate, "YYYY-MM-DDTHH:MM:SS"),
          "lblDeliverBy": deliverBy,
          "lblAmount": commonUtilities.formatCurrencyWithCommas(dataItem.amount),
          "imgAcknowledgement": ackImage
        };
      });
      commonUtilities.setText(this.view.lblAmountValue, commonUtilities.formatCurrencyWithCommas(totalSum) , commonUtilities.getaccessibilityConfig());
      this.view.segBill.widgetDataMap = bulkPayAckWidgetDataMap;
      this.view.segBill.setData(bulkPay);
      this.view.btnViewPaymentActivity.onClick = this.onClickPaymentActivity;
      this.view.btnMakeAnotherPayment.onClick = this.onClickMakePayment;
      this.view.flxSuccessMessage.setVisibility(false);
      this.view.customheader.customhamburger.activateMenu("Bill Pay");
      this.view.forceLayout();
    },
    /**
     * used to perform UI Activities like fith the UI in Screen.
     */
    AdjustScreen: function () {
      this.view.forceLayout();
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.info.frame.height + this.view.flxMain.info.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.info.frame.height;
        if (diff > 0) {
          this.view.flxFooter.top = mainheight + diff + "dp";
        }
        else {
          this.view.flxFooter.top = mainheight + "dp";
        }
      } else {
        this.view.flxFooter.top = mainheight + "dp";
      }
      this.view.forceLayout();
    },
    /**
     * used to navigate the billPay History screen
     */
    onClickPaymentActivity: function () {
      var self = this;
      self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      self.presenter.showBillPayData(null, 'History', false);
    },
    /**
     * used to navigate the allPayees screen
     */
    onClickMakePayment: function () {
      var self = this;
      self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      self.presenter.showBillPayData(null, {
        show: 'AllPayees',
        sender: 'acknowledgement',
        resetSorting: true
      });
    },
    /**
     * used perform the initialize activities.
     *
    */
    initActions: function () {
      var scopeObj = this;
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      if(kony.application.getCurrentBreakpoint() == 640) {
        this.view.Balance.lblAvailableBalance.skin = "sknSSP42424217Pxop70";
        this.view.Balance.lblSavingsAccount.skin = "sknSSPMediium42424217Pxop70";
      } else {
        this.view.Balance.lblAvailableBalance.skin = "sknLblSSP42424215px";
        this.view.Balance.lblSavingsAccount.skin = "sknLblSSP42424215pxBold";
      }
      this.view.onBreakpointChange = function () {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      };
      this.AdjustScreen();
      this.view.btnSavePayee.onClick = this.onClickSavePayee;
      if (commonUtilities.isPrintEnabled()) {
        if(kony.application.getCurrentBreakpoint()==640 || kony.application.getCurrentBreakpoint()==1024){
          this.view.flxPrintBulkPay.setVisibility(false);
        }else{  this.view.flxPrintBulkPay.setVisibility(true);}
        this.view.imgPrintBulk.setVisibility(true);
        this.view.lblPrintfontIcon.setVisibility(true);
        this.view.imgPrintBulk.onTouchStart = this.onClickPrintBulk;
        this.view.lblPrintfontIcon.onTouchStart = this.onClickPrint;
      } else {
        this.view.imgPrintBulk.setVisibility(false);
        this.view.lblPrintfontIcon.setVisibility(false);
        this.view.flxPrintBulkPay.setVisibility(false);
      }
    },
    preShow: function () {
      FormControllerUtility.updateWidgetsHeightInInfo(this, ['customheader','flxMain','flxFooter','flxAcknowledgementMain','flxTransactionDetails','flxHeader']);
    },
    /**
     * used to perform the post show activities
     *
     */
    postShowFrmAcknowledgement: function () {
      var scopeObj = this;
      this.view.customheader.forceCloseHamburger();
      this.view.confirmDialogAccounts.confirmButtons.setVisibility(false);
      this.customiseHeaderComponentforAcknowledgement();
      this.view.forceLayout();
      this.view.flxDownload.setVisibility(false); // AOLB-997
      applicationManager.getNavigationManager().applyUpdates(this);
      this.AdjustScreen();
    },
    /**
     * used to perform Ui Activities
     */
    customiseHeaderComponentforAcknowledgement: function () {
      this.view.breadcrumb.imgBreadcrumb2.setVisibility(false);
      this.view.breadcrumb.lblBreadcrumb3.setVisibility(false);
    },
    /**used to convert the CalenderFormat Date
     * @param {String} dateString string formated date
     * @param {String} inputFormat input format of the date
     * @return {date} Date object
     */
    getDateFromDateString: function (dateString, inputFormat) {
      var fu = applicationManager.getFormatUtilManager();
      var dateObj = fu.getDateObjectfromString(dateString, inputFormat);
      var outputDate= fu.getFormatedDateString(dateObj, fu.getApplicationDateFormat());
      return outputDate;
    },
    /**Returns Date Object from Date Components
   * @param  {array} dateComponents Date Components returned from Calendar Widget
   * @return {date} date
   */
    getDateObj: function (dateComponents) {
      var date = new Date();
      date.setDate(dateComponents[0]);
      date.setMonth(parseInt(dateComponents[1]) - 1);
      date.setFullYear(dateComponents[2]);
      date.setHours(0, 0, 0, 0)
      return date;
    },
    /** Compares Date with todays and tell is its future or not
   * @param  {object} dateComponents object
   * @returns {boolean} True for future else false
   */
    isFutureDate: function (dateComponents) {
      var dateObj = this.getDateObj(dateComponents)
      var endTimeToday = commonUtilities.getServerDateObject();
      var minutes = ViewConstants.MAGIC_NUMBERS.MAX_MINUTES;
      endTimeToday.setHours(ViewConstants.MAGIC_NUMBERS.MAX_HOUR, minutes, minutes, minutes);
      if (dateObj.getTime() > endTimeToday.getTime()) {
        return true;
      }
      return false;
    },
    /**Formats the Currency
       * @param  {Array} amount Array of transactions model
       * @param  {function} currencySymbolNotRequired Needs to be called when cancel button is called
       * @return {function} function to put comma
       */
    formatCurrency: function (amount, currencySymbolNotRequired,currencyCode) {
      return commonUtilities.formatCurrencyWithCommas(amount, currencySymbolNotRequired,currencyCode);
    },
    /**Entry Point for Transfer Acknowledgement
    * @param {object} viewModel Transfer Data
    */
    updateTransferAcknowledgeUI: function (viewModel) {
      var current_breakpoint = kony.application.getCurrentBreakpoint();
      var transactionCurrency = applicationManager.getFormatUtilManager().getCurrencySymbol(viewModel.transferData.transactionCurrency)
      this.presenter = applicationManager.getModulesPresentationController("TransferModule");
      this.customizeUIForTransferAcknowledege();
      if (this.isFutureDate(viewModel.transferData.sendOnDateComponents) || viewModel.transferData.frequencyKey !== "Once") {
        this.showTransferAcknowledgeForScheduledTransaction(viewModel);
      }
      else {
        this.showTransferAcknowledgeForRecentTransaction(viewModel);
      }
      if(current_breakpoint ===640){
      this.acknowledgementUI();
      }
       if (current_breakpoint == 1024) {
        this.view.flxTransactionDetails.top = this.view.flxAcknowledgementMain.info.frame.height+ this.view.flxAcknowledgementMain.info.frame.y + 40 + "dp";
        this.view.btnMakeTransfer.top = 995 + "dp";
        this.view.btnSavePayee.top = 995 + "dp";
      }
      this.view.confirmDialogAccounts.keyValueBenificiaryName.setVisibility(true);
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueBankName.lblValue, commonUtilities.getAccountDisplayName(viewModel.transferData.accountFrom) , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueCountryName.lblValue, viewModel.transferData.accountTo.beneficiaryName ? viewModel.transferData.accountTo.nickName : (commonUtilities.getAccountDisplayName(viewModel.transferData.accountTo)) , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountType.lblKey, kony.i18n.getLocalizedString("i18n.transfers.lblAmount")+"("+transactionCurrency+")" , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountType.lblValue, this.formatCurrency(viewModel.transferData.amount, true) , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey, kony.i18n.getLocalizedString('i18n.transfers.send_on') , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue, viewModel.transferData.sendOnDate , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue, viewModel.transferData.frequencyType , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue, viewModel.transferData.notes , commonUtilities.getaccessibilityConfig());
      this.view.btnMakeTransfer.onClick = function () {
        this.presenter.showTransferScreen();
      }.bind(this);
      this.view.btnAddAnotherAccount.onClick = function () {
        this.presenter.showTransferScreen({ initialView: 'recent' });
      }.bind(this);
      this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString('i18n.transfers.viewTransferActivity');
      this.view.customheader.customhamburger.activateMenu("Transfers")
    },
    /**UI Logic for Transfers
       */
    customizeUIForTransferAcknowledege: function () {
      this.view.flxMainBulkPay.setVisibility(false);
      this.view.confirmDialogAccounts.flxDileveryBy.setVisibility(false);
      this.view.flxContainer.setVisibility(true);
      this.view.btnSavePayee.setVisibility(false);
      commonUtilities.setText(this.view.lblBillPayAcknowledgement, kony.i18n.getLocalizedString("i18n.transfers.Acknowledgementlbl") , commonUtilities.getaccessibilityConfig());
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
      }, {
        text: kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.Acknowledgement");
      commonUtilities.setText(this.view.btnMakeTransfer, kony.i18n.getLocalizedString("i18n.transfers.makeanothertransfereuro") , commonUtilities.getaccessibilityConfig());
      this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString("i18n.transfers.makeanothertransfereuro");
      commonUtilities.setText(this.view.btnAddAnotherAccount, kony.i18n.getLocalizedString("i18n.transfers.viewTransferActivity") , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.lblBillPayAcknowledgement, kony.i18n.getLocalizedString("i18n.transfers.Acknowledgementlbl") , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.acknowledgment.lblTransactionMessage, kony.i18n.getLocalizedString("i18n.transfers.AcknowledgementMessage") , commonUtilities.getaccessibilityConfig());
    },
    /**Entry Point for Transfer Acknowledgement for Scheduled Transaction
       * @param {object} viewModel Transfer Data
       */

    getScheduledMessage: function (viewModel) {
      if (viewModel.transferData.frequencyKey !== "Once" && (viewModel.transferData.howLongKey === "ON_SPECIFIC_DATE" || viewModel.transferData.howLongKey === "NO_OF_RECURRENCES")) {
        return kony.i18n.getLocalizedString("i18n.Transfers.YourRecurringTransferScheduled")
      }
      else {
        return kony.i18n.getLocalizedString("i18n.Transfers.YourTransactionHasBeenScheduledfor")
      }
    },
    showTransferAcknowledgeForScheduledTransaction: function (viewModel) {
      function getDateFromDateComponents(dateComponents) {
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return [dateComponents[0], monthNames[dateComponents[1] - 1], dateComponents[2]].join(" ");
      }
      this.view.acknowledgmentModify.setVisibility(true);
      this.view.acknowledgment.setVisibility(false);
      this.view.Balance.setVisibility(false);
      commonUtilities.setText(this.view.acknowledgmentModify.btnModifyTransfer, kony.i18n.getLocalizedString("i18n.common.modifyTransaction") , commonUtilities.getaccessibilityConfig());
      this.view.acknowledgmentModify.btnModifyTransfer.onClick = this.modifyTransfer.bind(this, viewModel.transferData);
      this.view.acknowledgmentModify.btnModifyTransfer.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyTransaction');
      commonUtilities.setText(this.view.acknowledgmentModify.lblRefrenceNumberValue, viewModel.transferData.referenceId , commonUtilities.getaccessibilityConfig());
      if(viewModel.transferData.status != "Pending"){
        commonUtilities.setText(this.view.lblTransactionMessage, this.getScheduledMessage(viewModel) , commonUtilities.getaccessibilityConfig());
      }else{
        commonUtilities.setText(this.view.lblTransactionMessage, kony.i18n.getLocalizedString("i18n.transfers.approvalAck") , commonUtilities.getaccessibilityConfig());  
      }
      commonUtilities.setText(this.view.acknowledgmentModify.lblTransactionDate, getDateFromDateComponents(viewModel.transferData.sendOnDateComponents) , commonUtilities.getaccessibilityConfig());
      this.view.acknowledgmentModify.lblTransactionDate.setVisibility(true);
      if (viewModel.transferData.frequencyKey !== "Once" && viewModel.transferData.howLongKey === "ON_SPECIFIC_DATE") {
        this.view.acknowledgmentModify.lblTransactionDate.setVisibility(false);
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey, kony.i18n.getLocalizedString("i18n.transfers.endby") , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue, viewModel.transferData.endOnDate , commonUtilities.getaccessibilityConfig());
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);
      }
      else if (viewModel.transferData.frequencyKey !== "Once" && viewModel.transferData.howLongKey === "NO_OF_RECURRENCES") {
        this.view.acknowledgmentModify.lblTransactionDate.setVisibility(false);
	commonUtilities.setText(this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey, kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences") , commonUtilities.getaccessibilityConfig());
	commonUtilities.setText(this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue, viewModel.transferData.numberOfRecurrences , commonUtilities.getaccessibilityConfig());
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);
      }
      else {
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
      }
    },
    /**Modify A Scheduled Transfer
       * @param {object} transferData Transfer Data
       */
    modifyTransfer: function (transferData) {
      this.presenter = applicationManager.getModulesPresentationController("TransferModule");
      var record =  {transactionType: this.presenter.getTransferType(transferData.accountTo),
      toAccountNumber: transferData.accountTo.accountID,
      fromAccountNumber: transferData.accountFrom.accountID,
      ExternalAccountNumber: transferData.accountTo.accountNumber,
      amount: transferData.amount,
      frequencyType: transferData.frequencyKey,
      numberOfRecurrences: transferData.numberOfRecurrences,
      transactionsNotes: transferData.notes,
      scheduledDate: transferData.sendOnDate,
      frequencyEndDate: transferData.endOnDate,
      frequencyStartDate: transferData.sendOnDate,
      isScheduled: "1",
      transactionDate: transferData.sendOnDate,
      transactionId: transferData.referenceId,
      isModifiedTransferFlow: true,
      serviceName: transferData.serviceName
      }
      this.presenter.showTransferScreen({
       editTransactionObject: record
      });
    },
    /**Entry Point for Transfer Acknowledgement for Recent Transaction
       * @param {object} viewModel Transfer Data
       */
    showTransferAcknowledgeForRecentTransaction: function (viewModel) {
      this.view.acknowledgment.setVisibility(true);
      this.view.Balance.setVisibility(true);
      this.view.acknowledgmentModify.setVisibility(false);
      commonUtilities.setText(this.view.acknowledgment.lblRefrenceNumberValue, viewModel.transferData.referenceId , commonUtilities.getaccessibilityConfig());
      this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
      commonUtilities.setText(this.view.Balance.lblSavingsAccount, viewModel.transferData.accountFrom.accountName , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.Balance.lblBalanceValue, this.formatCurrency(viewModel.transferData.accountFrom.availableBalance,false,viewModel.transferData.accountFrom.currencyCode) , commonUtilities.getaccessibilityConfig());
    },
    /**
     * used to set single Bill Pay scrren
     * @param {object} data
     */
    updateBillPayAcknowledgeUI: function (data) {
      var self = this;
       var current_breakpoint = kony.application.getCurrentBreakpoint();
       var transactionCurrency = applicationManager.getFormatUtilManager().getCurrencySymbol(data.savedData.transactionCurrency);
      if (data.savedData.gettingFromOneTimePayment) {
          commonUtilities.setText(this.view.btnSavePayee, kony.i18n.getLocalizedString("i18n.billpay.savePayee") , commonUtilities.getaccessibilityConfig());
        this.view.btnSavePayee.toolTip = kony.i18n.getLocalizedString("i18n.billpay.savePayee");
        this.view.btnSavePayee.setVisibility(true);
      } else {
        this.view.btnSavePayee.setVisibility(false);
      }
      this.customizeUIForBillPayAcknowledege();
      this.registerActionsBillPay(data.savedData);
      this.view.confirmDialogAccounts.keyValueBenificiaryName.setVisibility(false);
      commonUtilities.setText(this.view.acknowledgment.lblRefrenceNumberValue, data.referenceId , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.Balance.lblSavingsAccount, data.accountData.accountName , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.Balance.lblBalanceValue, commonUtilities.formatCurrencyWithCommas(data.accountData.availableBalance, false,data.accountData.currencyCode) , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueBankName.lblValue, data.savedData.payFrom , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueCountryName.lblValue, data.savedData.payeeName , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountType.lblKey, kony.i18n.getLocalizedString("i18n.transfers.lblAmount")+"("+ transactionCurrency+")" , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountType.lblValue, data.savedData.languageAmount , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue, data.savedData.sendOn , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue, data.savedData.categories , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue, data.savedData.notes , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountNickName.lblKey, kony.i18n.getLocalizedString("i18n.accounts.Note") , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey, kony.i18n.getLocalizedString("i18n.billPay.PaymentDate") , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueBenificiaryName.lblKey, kony.i18n.getLocalizedString("i18n.billPay.category") , commonUtilities.getaccessibilityConfig());
      this.view.confirmDialogAccounts.flxDileveryBy.setVisibility(true);
      commonUtilities.setText(this.view.confirmDialogAccounts.flxDileveryBy.lblKey, kony.i18n.getLocalizedString("i18n.billPay.DeliverBy") , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.flxDileveryBy.lblValue, data.savedData.deliveryDate , commonUtilities.getaccessibilityConfig());
      if (data.savedData.gettingFromOneTimePayment) {
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(false);
      }
      else {
        this.view.confirmDialogAccounts.keyValueRoutingNumber.setVisibility(true);
      }
       commonUtilities.setText(this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey, kony.i18n.getLocalizedString("i18n.transfers.lblFrequency") , commonUtilities.getaccessibilityConfig());
       commonUtilities.setText(this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue, data.savedData.frequencyType , commonUtilities.getaccessibilityConfig());
      if (data.savedData.frequencyType !== "Once" && data.savedData.hasHowLong === "ON_SPECIFIC_DATE") {
      commonUtilities.setText(this.view.acknowledgment.lblTransactionMessage, kony.i18n.getLocalizedString("i18n.transfers.recurringAcknowledgementMessage")+data.savedData.payeeName , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey, kony.i18n.getLocalizedString("i18n.ConfirmEur.SendOn") , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue, data.savedData.frequencyStartDate , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialogAccounts.flxDileveryBy.lblKey, kony.i18n.getLocalizedString("i18n.transfers.endby") , commonUtilities.getaccessibilityConfig());
       commonUtilities.setText(this.view.confirmDialogAccounts.flxDileveryBy.lblValue, data.savedData.frequencyEndDate , commonUtilities.getaccessibilityConfig());
      }
      else if (data.savedData.frequencyType !== "Once" && data.savedData.hasHowLong === "NO_OF_RECURRENCES") {
	commonUtilities.setText(this.view.acknowledgment.lblTransactionMessage, kony.i18n.getLocalizedString("i18n.transfers.recurringAcknowledgementMessage")+data.savedData.payeeName , commonUtilities.getaccessibilityConfig());
	commonUtilities.setText(this.view.confirmDialogAccounts.flxDileveryBy.lblKey, kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences") , commonUtilities.getaccessibilityConfig());
	commonUtilities.setText(this.view.confirmDialogAccounts.flxDileveryBy.lblValue, data.savedData.numberOfRecurrences , commonUtilities.getaccessibilityConfig());
      }
       if(current_breakpoint ===640){
      this.acknowledgementUI();
      }
       if (current_breakpoint == 1024) {
        this.view.flxTransactionDetails.top = this.view.flxAcknowledgementMain.info.frame.height + this.view.flxAcknowledgementMain.info.frame.y + 40 + "dp";
        this.view.btnMakeTransfer.top = 995 + "dp";
        this.view.btnSavePayee.top = 995 + "dp";
      }
      this.view.customheader.customhamburger.activateMenu("Bill Pay");
    },
    /**
     * used to set single BillPay Acknowledge Page
     */
    customizeUIForBillPayAcknowledege: function () {
      var current_breakpoint = kony.application.getCurrentBreakpoint();
      this.view.flxMainBulkPay.setVisibility(false);
      this.view.flxContainer.setVisibility(true);
      this.view.acknowledgmentModify.setVisibility(false);
      this.view.acknowledgment.setVisibility(true);
      this.view.Balance.setVisibility(true);
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
      }, {
        text: kony.i18n.getLocalizedString("i18n.billPay.AcknowledgementBillPay")
      }]);
      if(current_breakpoint ===640){
      this.acknowledgementUI();
      }
       if (current_breakpoint == 1024) {
        this.view.flxTransactionDetails.top = this.view.flxAcknowledgementMain.info.frame.height +this.view.flxAcknowledgementMain.info.frame.y+ 40 + "dp";
        this.view.btnMakeTransfer.top = 995 + "dp";
        this.view.btnSavePayee.top = 995 + "dp";
      }
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.AcknowledgementBillPay");
      commonUtilities.setText(this.view.btnMakeTransfer, kony.i18n.getLocalizedString("i18n.billPay.MakeAnotherPayment") , commonUtilities.getaccessibilityConfig());
      this.view.btnMakeTransfer.toolTip = kony.i18n.getLocalizedString("i18n.billPay.MakeAnotherPayment");
      commonUtilities.setText(this.view.btnAddAnotherAccount, kony.i18n.getLocalizedString("i18n.billPay.ViewPaymentActivity") , commonUtilities.getaccessibilityConfig());
      this.view.btnAddAnotherAccount.toolTip = kony.i18n.getLocalizedString("i18n.billPay.ViewPaymentActivity");
      commonUtilities.setText(this.view.lblBillPayAcknowledgement, kony.i18n.getLocalizedString("i18n.billPay.AcknowledgementBillPay") , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.acknowledgment.lblTransactionMessage, kony.i18n.getLocalizedString("i18n.transfers.AcknowledgementMessage") , commonUtilities.getaccessibilityConfig());
      this.view.forceLayout();
    },
    /**
     * used to set actions to billPay Acknowledge Form
     * @param {object} data
     */
    registerActionsBillPay: function (data) {
      var self = this;
      self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      if (data.gettingFromOneTimePayment === true) {
        this.view.btnMakeTransfer.onClick = function () {
          self.presenter.showOneTimePaymentPage('acknowledgement', data);
        };
      } else {
        this.view.btnMakeTransfer.onClick = function () {
          data.categories = '';
          data.amount = '';
          data.sendOn = '';
          data.notes = '';
          data.deliveryDate = '';
          data.frequencyType = '';
          data.numberOfRecurrences = '';
          data.frequencyStartDate = '';
          data.frequencyEndDate = '';
          data.billCategory = '';
          self.presenter.showBillPayData('acknowledgement', 'PayABill', true, data);
        };
      }
      this.view.btnAddAnotherAccount.onClick = function () {
        FormControllerUtility.showProgressBar(this.view);
        self.presenter.showBillPayData('acknowledgement', 'History', true);
      };
    },
    /**
     * used to save the payee
     */
    onClickSavePayee: function () {
      var self = this;
      self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      self.presenter.showUpdateBillerPageWithTransaction({
        transactionId: this.view.acknowledgment.lblRefrenceNumberValue.text
      });
    },
    acknowledgementUI: function(){
         this.view.flxMain.top=50 +"dp";
        if (!this.view.Balance.isVisible) {
			   this.view.flxAcknowledgementMain.height=450+"dp";
			   this.view.flxTransactionDetails.top=490+"dp";
			   this.view.flxTransactionDetails.height=650+"px";
			}
			else{
				this.view.flxAcknowledgementMain.height=530+"dp";
				this.view.flxAcknowledgementMain.info.frame.height = 530;
			   this.view.flxTransactionDetails.top = this.view.flxAcknowledgementMain.info.frame.height + 40 + "dp";
			   this.view.flxTransactionDetails.height=563+"px";
			   this.view.flxTransactionDetails.info.frame.height=563;
			}
        this.view.btnMakeTransfer.top = 590 + this.view.flxTransactionDetails.info.frame.height + "dp";
        this.view.btnAddAnotherAccount.top = 590 + this.view.flxTransactionDetails.info.frame.height + 60 + "dp";
        this.view.btnSavePayee.top = 590 + this.view.flxTransactionDetails.info.frame.height + 120 + "dp";
        this.view.btnAddAnotherAccount.isVisible = true;
        this.view.flxTransactionDetails.left="10dp";
        this.view.flxTransactionDetails.width = this.view.flxAcknowledgementMain.width;
       this.AdjustScreen();
    },
    //UI Code
    /**
    * onBreakpointChange : Handles ui changes on .
    * @member of {frmAcknowledgementController}
    * @param {integer} width - current browser width
    * @return {}
    * @throws {}
    */
    onBreakpointChange: function (width) {
      kony.print('on breakpoint change');
      var scope = this;
	  this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup,width);
      orientationHandler.onOrientationChange(this.onBreakpointChange);
      this.view.customheader.onBreakpointChangeComponent(width);
      this.setupFormOnTouchEnd(width);
      this.AdjustScreen();
      if (width == 640) {
        this.view.acknowledgmentModify.lblTransactionMessage.skin="sknSSPLight42424218Px";
        this.view.acknowledgmentModify.lblTransactionDate.skin="sknSSPLight42424218Px";
        this.view.acknowledgmentModify.btnModifyTransfer.skin="sknBtnSSP0273e315px";
        this.view.btnTransferActivity.isVisible = false;
        this.view.confirmDialogAccounts.flxMain.skin = "sknFlexRoundedBorderFFFFFF3Pxshadowd9d9d9noshadow";
        commonUtilities.setText(this.view.customheader.lblHeaderMobile, "Acknowledgement" , commonUtilities.getaccessibilityConfig());
        this.view.flxMain.top=50 +"dp";
        this.view.flxTransactionDetails.top = 470 + "dp";
        this.view.btnMakeTransfer.top = 590 + this.view.flxTransactionDetails.info.frame.height + "dp";
        this.view.btnAddAnotherAccount.top = 590 + this.view.flxTransactionDetails.info.frame.height + 60 + "dp";
        this.view.btnSavePayee.top = 590 + this.view.flxTransactionDetails.info.frame.height + 120 + "dp";
        this.view.btnAddAnotherAccount.isVisible = true;
        this.view.flxTransactionDetails.left="10dp";
        this.view.flxTransactionDetails.width = this.view.flxAcknowledgementMain.width;
      } else {
        this.view.acknowledgmentModify.lblTransactionMessage.skin="sknLabel4SSPLoight2424224Px";
        this.view.acknowledgmentModify.lblTransactionDate.skin="sknLabel4SSPLoight2424224Px";
        this.view.acknowledgmentModify.btnModifyTransfer.skin="sknBtnSSP0273e317px";
        this.view.flxMain.top = 120 + "dp";
        this.view.btnTransferActivity.isVisible = false;
        //this.view.btnMakeTransfer.top = 525 + "dp";
        this.view.btnAddAnotherAccount.skin = "sknBtnSSPBg0273e3Border0273e3";
         commonUtilities.setText(this.view.customheader.lblHeaderMobile, "" , commonUtilities.getaccessibilityConfig());
      }
      if (width == 1024) {
        this.view.btnMakeTransfer.top = 995 + "dp";
        this.view.btnSavePayee.top = 995 + "dp";
      }
      this.AdjustScreen();
    },
      setupFormOnTouchEnd: function(width){
        if(width==640){
          this.view.onTouchEnd = function(){}
          this.nullifyPopupOnTouchStart();
        }else{
          if(width==1024){
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }else{
              this.view.onTouchEnd = function(){
                hidePopups();
              }
            }
            var userAgent = kony.os.deviceInfo().userAgent;
            if (userAgent.indexOf("iPad") != -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }
        }
      },
      nullifyPopupOnTouchStart: function(){
      },
    onClickPrintBulk: function () {
      this.presenter.showPrintPage({ transactionList: { "details": this.view.segBill.data, "module": this.view.lblBulkBillPayAcknowledgement.text}});
    },
    onClickPrint: function () {
      var printData = [];
      printData.push({
        key: kony.i18n.getLocalizedString("i18n.common.status"),
        value: this.view.acknowledgment.lblTransactionMessage.text
      });
      printData.push({
        key: this.view.acknowledgment.lblRefrenceNumber.text,
        value: this.view.acknowledgment.lblRefrenceNumberValue.text
      });
      printData.push({
        key: kony.i18n.getLocalizedString("i18n.transfers.accountName"),
        value: this.view.Balance.lblSavingsAccount.text
      });
      printData.push({
        key: kony.i18n.getLocalizedString("i18n.accounts.availableBalance"),
        value: this.view.Balance.lblBalanceValue.text
      });
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueBankName.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueBankName.lblValue.text
      });
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueCountryName.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueCountryName.lblValue.text
      });
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueAccountType.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueAccountType.lblValue.text
      });
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueAccountNumber.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueAccountNumber.lblValue.text
      });
      if(this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text){
        printData.push({
          key: this.view.confirmDialogAccounts.keyValueBenificiaryName.lblKey.text,
          value: this.view.confirmDialogAccounts.keyValueBenificiaryName.lblValue.text
        });
      }
      if(this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text){
        printData.push({
          key: this.view.confirmDialogAccounts.keyValueAccountNickName.lblKey.text,
          value: this.view.confirmDialogAccounts.keyValueAccountNickName.lblValue.text
        });
      }
      printData.push({
        key: this.view.confirmDialogAccounts.keyValueRoutingNumber.lblKey.text,
        value: this.view.confirmDialogAccounts.keyValueRoutingNumber.lblValue.text
      });
      if (this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text !== "") {
        printData.push({
          key: this.view.confirmDialogAccounts.flxDileveryBy.lblKey.text,
          value: this.view.confirmDialogAccounts.flxDileveryBy.lblValue.text
        });
      }
      var printCallback = function(){
            var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
            billPayModule.presentationController.navigateToAcknowledgementForm();
      }
      var viewModel = {
        moduleHeader: this.view.lblBillPayAcknowledgement.text,
        tableList: [{
            tableHeader: this.view.confirmDialogAccounts.confirmHeaders.lblHeading.text,
            tableRows: printData
        }],
        printCallback: printCallback
      }
      this.presenter.showPrintPage({
        printKeyValueGroupModel: viewModel
      });
    }
  }
});
