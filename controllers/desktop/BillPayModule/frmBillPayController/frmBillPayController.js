/**
 * Description of Module representing a BillPay form.
 * @module frmBillPayController
 */
define(['CommonUtilities', 'OLBConstants', 'FormControllerUtility', 'ViewConstants'], function (CommonUtilities, OLBConstants, FormControllerUtility, ViewConstants) {
    var orientationHandler = new OrientationHandler();
    return /** @alias module:frmBillPayController */ {
        /** updates the present Form based on required function.
        * @param {uiDataMap[]} uiDataMap
        */
        updateFormUI: function (uiDataMap) {
            if (uiDataMap.ProgressBar) {
                if (uiDataMap.ProgressBar.show) {
                    FormControllerUtility.showProgressBar(this.view);
                }
                else {
                    FormControllerUtility.hideProgressBar(this.view);
                }
            }
            if (uiDataMap.showNotEligibleView) {
                this.showBillPayActivationNotEligibileScreen();
            }
            if (uiDataMap.showDeactivatedView) {
                this.showBillPayActivationScreen(uiDataMap.billPayAcccounts);
            }
            if (uiDataMap.TnCcontent) {
                this.bindTnCData(uiDataMap.TnCcontent);
            }
            if (uiDataMap.showBillPayActivationAck) {
                this.showBillPayActivationAcknowledgementScreen();
            }
            if (uiDataMap.allPayees) {
                this.setAllPayeesSegmentData(uiDataMap.allPayees, uiDataMap.billPayAccounts);
                FormControllerUtility.updateSortFlex(this.allPayeesSortMap, uiDataMap.sortingInputs);
            }
            if (uiDataMap.paymentDueBills) {
                this.bindPaymentDueSegment(uiDataMap.paymentDueBills, uiDataMap.billPayAccounts);
                FormControllerUtility.updateSortFlex(this.paymentDueSortMap, uiDataMap.noOfRecords);
            } else if (uiDataMap.billDueData) {
                this.bindTotalEbillAmountDue(uiDataMap.billDueData);
            }
            if (uiDataMap.scheduledBills && uiDataMap.sortingInputs) {
                this.bindScheduleBillsSegment(uiDataMap.scheduledBills);
                FormControllerUtility.updateSortFlex(this.scheduledSortMap, uiDataMap.sortingInputs);
            }
            if (uiDataMap.serverError) {
                this.setServerError(true, uiDataMap.serverError);
            } else {
                this.setServerError(false);
            }
            if (uiDataMap.billpayHistory) {
                this.setHistoryUI();
                this.binduserBillPayHistory(uiDataMap.billpayHistory);
                FormControllerUtility.updateSortFlex(this.historySortMap, uiDataMap.billpayHistory.noOfRecords);
            }
            if (uiDataMap.intialView) {
                this.showIntialBillPayUI(uiDataMap.intialView);
            }
            if (uiDataMap.managePayee && uiDataMap.managePayee.searchvisibility) {
                this.bindManagePayeeData(uiDataMap.managePayee.managePayee, null, uiDataMap.managePayee.searchvisibility);
            } else if (uiDataMap.managePayee) {
                this.bindManagePayeeData(uiDataMap.managePayee);
            }
            if (uiDataMap.payeeActivities) {
                this.showPayeeViewActivities(uiDataMap.payeeActivities);
            }
            if (uiDataMap.navigateToManagePayeeFromViewActivity) {
                this.changeSelectedTemplate(uiDataMap.navigateToManagePayeeFromViewActivity);
            }
            if (uiDataMap.singleBillPayData) {
                this.setDataForPayABill(uiDataMap.singleBillPayData, uiDataMap.context);
            }
            if (uiDataMap.deleteSchedule) {
                this.setScheduledFlex();
            }
            if (uiDataMap.showOneTimePayment) {
                this.setDataForShowOneTimePayment(uiDataMap.showOneTimePayment.data, uiDataMap.showOneTimePayment.sender);
            }
            if (uiDataMap.payees) {
                this.loadAllPayees(uiDataMap.payees);
            }
            if (uiDataMap.resetBillPayForm) {
                this.resetBillPayUI();
            }
            if (uiDataMap.modifyPayment) {
                this.modifyPayment();
            }
            if(uiDataMap.TnCcontentTransfer){
                if(uiDataMap.isBulkBillPay){
                    this.showConfirmBulkPay(uiDataMap.TnCcontentTransfer);
                }
                else{
                    this.sendSinglePayABillPayData(uiDataMap.data, uiDataMap.context, uiDataMap.TnCcontentTransfer);
                }
            }
            if(uiDataMap.TnCcontentOneTimeTransfer){
                this.sendMakeOneTimePayABill(uiDataMap.data,uiDataMap.TnCcontentOneTimeTransfer);
            }
            this.updateHamburgerMenu();
            this.AdjustScreen();
        },
        /**
         * used to load the payees in dropDown
         * @param {object} payees payees list
         */
      loadAllPayees: function (payees) {
        var scopeObj = this;
        scopeObj.allPayees = payees.filter(function (payee) {
          return payee.ebillStatus !== "0";
        });
        this.view.lbxPayee.masterData = FormControllerUtility.getListBoxDataFromObjects(scopeObj.allPayees, "payeeId", "payeeNickName");
        this.view.payABill.lbxpayee.masterData = FormControllerUtility.getListBoxDataFromObjects(scopeObj.allPayees, "payeeId", "payeeNickName");
        this.view.payABill.lbxpayee.onSelection = this.onPayeeSelect.bind(scopeObj);
        this.AdjustScreen();
      },
        /**
          * selecting payee
         */
        onPayeeSelect: function () {
            var selectedKey = this.view.payABill.lbxpayee.selectedKey;
            var selectedPayee = this.allPayees.filter(function (payee) {
                return payee.payeeId === selectedKey;
            })[0];
            this.populateValuesFromPayee(selectedPayee);
        },
        /**
            * populateValuesFromPayee: get value from payee
            * @param {object} payee list of payee
         */
        populateValuesFromPayee: function (payee) {
            var nonValue = kony.i18n.getLocalizedString("i18n.common.none");
            payee.billDueDate =  payee.billDueDate ? CommonUtilities.getFrontendDateString(payee.billDueDate) : nonValue;
            payee.dueAmount = payee.dueAmount ? CommonUtilities.formatCurrencyWithCommas(payee.dueAmount) : nonValue;
            CommonUtilities.setText(this.view.payABill.lblDueDateValue, payee.billDueDate , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.payABill.lblBillValue, payee.dueAmount , CommonUtilities.getaccessibilityConfig());
            this.eBillForPayABill(payee);
         },
        /** used to diplay the billPay Not Eligible screen.
        */
        showBillPayActivationNotEligibileScreen: function () {
            var self = this;
            self.view.Activatebillpays.flxActivateMain.setVisibility(false);
            self.view.Activatebillpays.flxActivateAcknowledgement.setVisibility(false);
            self.view.Activatebillpays.flxActiveBillPayNotEligible.setVisibility(true);
            CommonUtilities.setText(self.view.Activatebillpays.lblHeaderNotEligible, kony.i18n.getLocalizedString("i18n.billPay.activateBillPay") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(self.view.Activatebillpays.lblWarningNotEligible, kony.i18n.getLocalizedString("i18n.billPay.activateBillPayNotEligibleMsg") , CommonUtilities.getaccessibilityConfig());
            self.resetUIDuringActivation();
        },
        /**
       * used to reset Ui for billpay activation screen
       *
       */
        resetUIDuringActivation: function () {
            var self = this;
            var configurationManager = applicationManager.getConfigurationManager();
            self.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
            }, {
                text: kony.i18n.getLocalizedString("i18n.billPay.activateBillPay")
            }]);
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.activateBillPay");
            CommonUtilities.setText(self.view.Activatebillpays.lblHeader, kony.i18n.getLocalizedString("i18n.billPay.activateBillPay") , CommonUtilities.getaccessibilityConfig());
            if(kony.application.getCurrentBreakpoint()!=640 && !orientationHandler.isMobile){
                self.view.flxActivatebillpays.width = "88%";
            }
            self.view.flxActivatebillpays.setVisibility(true);
            self.view.flxBillPay.setVisibility(false);
            self.view.flxPagination.setVisibility(false);
            self.view.flxActivateBiller.setVisibility(false);
            self.view.flxViewEbill.setVisibility(false);
            self.view.flxTotalEbillAmountDue.setVisibility(false);
            self.view.flxBillPayManagePayeeActivity.setVisibility(false);
            self.view.flxaddpayeemakeonetimepayment.setVisibility(false);
            self.view.flxActivateBiller.setVisibility(false);
            self.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            self.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            if(configurationManager.isFastTransferEnabled == "false")
            self.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
            else
              {
            this.view.customheader.topmenu.flxMyBills.skin = "sknFlxFFFFFbrdr3343a8Pointer";
                this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknHoverTopmenu7f7f7pointer";
              }
            self.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
            self.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            self.resetSingeBillPay();
            self.view.forceLayout();
        },
        /**
         * reset the single BillPay Screen
         */
        resetSingeBillPay: function () {
            this.view.flxBillPay.payABill.CalDeliverBy.setVisibility(false);
            this.view.flxBillPay.payABill.lblDelieverBy.setVisibility(false);
        },
        /**
         * initlize the respective studio Actions and UI
         */
        initActions: function () {
            var scopeObj = this;
            this.view.onBreakpointChange = function () {
                scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
            }
            FormControllerUtility.setRequestUrlConfig(this.view.brwBodyTnC);
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            this.view.tableView.tableTabs.btnWithdraws.onClick = this.managePayeesOnClick.bind(this);
            this.view.tableView.tableTabs.btnTransfers.onClick = this.setPaymentDueSegmentData.bind(this);
            this.view.tableView.tableTabs.BtnAll.onClick = this.setAllPayessOnClick.bind(this);
            this.view.tableView.tableTabs.btnDeposits.onClick = this.setScheduledFlex.bind(this);
            this.view.tableView.tableTabs.btnChecks.onClick = this.setDataForHistory.bind(this);
            scopeObj.settingConfigurationsToBillPay();
            scopeObj.settingSortingToBillPay();
            this.view.tableView.Search.btnConfirm.onClick = this.onSearchBtnClick.bind(this);
            this.view.tableView.flxSearch.onClick = this.toggleSearchBox.bind(this);
            this.view.tableView.Search.txtSearch.onKeyUp = this.onTxtSearchKeyUp.bind(this);
            this.view.tableView.Search.txtSearch.onDone = this.onSearchBtnClick.bind(this);
            this.view.tableView.Search.flxClearBtn.onClick = this.onSearchClearBtnClick.bind(this);
            this.view.payABill.lbxFrequency.masterData = this.presenter.getFrequencies();
            this.view.payABill.lbxForHowLong.masterData = this.presenter.getHowLongValues();
            this.view.btnMakeonetimepayment.onClick = this.onClickOneTimePayement.bind(this);
            this.view.Activatebillpays.btnProceedNotEligible.onClick = this.onClickOpenNewAccount.bind(this);
            this.view.Activatebillpays.btnCancelNotEligible.onClick = this.onClickCancelEligibile.bind(this);
            this.view.btnAddPayee.onClick = this.onClickAddPayee;
            this.view.flxActivatebillpays.setVisibility(false);
            this.view.AllForms.setVisibility(false);
            this.view.flxBillPay.setVisibility(true);
            this.view.payABill.setVisibility(false);
            this.view.tableView.Search.setVisibility(true);
            this.view.flxBillPayManagePayeeActivity.setVisibility(false);
            this.view.flxViewEbill.setVisibility(false);
            this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            this.view.tableView.flxBillPayAllPayees.setVisibility(false);
            this.view.tableView.flxBillPayScheduled.setVisibility(false);
            this.view.tableView.tableSubHeaderHistory.setVisibility(false);
            this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
            scopeObj.view.tableView.flxNoPayment.setVisibility(false);
            this.view.ViewEBill.flxImgCancel.onClick = function () {
                scopeObj.view.flxViewEbill.setVisibility(false);
            }
            this.view.CustomPopup.btnNo.onClick = function () {
                scopeObj.view.flxLogout.left = "-100%";
            }
            this.view.CustomPopup.flxCross.onClick = function () {
                scopeObj.view.flxLogout.left = "-100%";
            }
        },
        /**
         * used to navigate the account page
         */
        onClickCancelEligibile: function () {
            var scopeObj = this;
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.presenter.cancelEligibile();
        },
        /**
         * used to navigate the newAccount screen
         */
        onClickOpenNewAccount: function () {
            var scopeObj = this;
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.presenter.openNewBillPayAccount();
        },
        /**
         * sorting configurations to billPay
         */
        settingSortingToBillPay: function () {
            var scopeObj = this;
            scopeObj.managePayeeSortMap = [
                { name: 'payeeNickName', imageFlx: scopeObj.view.tableView.imgBillerSort, clickContainer: scopeObj.view.tableView.flxBillerName }
            ];
            FormControllerUtility.setSortingHandlers(scopeObj.managePayeeSortMap, scopeObj.onManagePayeeBillerNameClickHandler, scopeObj);
            scopeObj.scheduledSortMap = [
                { name: 'scheduledDate', imageFlx: scopeObj.view.tableView.CopyimgSortPayee0h18cd98de27745, clickContainer: scopeObj.view.tableView.CopyflxPayee0g1766db8a5a746 },
                { name: 'nickName', imageFlx: scopeObj.view.tableView.CopyimgSortPayee0b9e84f45c64f4c, clickContainer: scopeObj.view.tableView.CopyflxPayee0geacd45d25de4f },
                { name: 'billDueAmount', imageFlx: scopeObj.view.tableView.CopyimgSortDatee0i6778bd50dbf47, clickContainer: scopeObj.view.tableView.CopyflxDate0e9f719bff5034e },
                { name: 'amount', imageFlx: scopeObj.view.tableView.CopyimgBill0idfbb778171e48, clickContainer: scopeObj.view.tableView.CopyflxBill0a0a4340e41b14e },
            ];
            scopeObj.view.tableView.CopyimgSortDatee0i6778bd50dbf47.setVisibility(true);
            scopeObj.view.tableView.CopyimgBill0idfbb778171e48.setVisibility(true);
            FormControllerUtility.setSortingHandlers(scopeObj.scheduledSortMap, scopeObj.onScheduledSortClickHandler, scopeObj);
            scopeObj.historySortMap = [
                { name: 'transactionDate', imageFlx: scopeObj.view.tableView.tableSubHeaderHistory.imgSortDate, clickContainer: scopeObj.view.tableView.tableSubHeaderHistory.flxSortDate },
                { name: 'nickName', imageFlx: scopeObj.view.tableView.tableSubHeaderHistory.imgSortDescription, clickContainer: scopeObj.view.tableView.tableSubHeaderHistory.flxSendTo },
                { name: 'amount', imageFlx: scopeObj.view.tableView.tableSubHeaderHistory.imgSortAmount, clickContainer: scopeObj.view.tableView.tableSubHeaderHistory.flxAmount },
                { name: 'statusDesc', imageFlx: scopeObj.view.tableView.tableSubHeaderHistory.imgSortCategory, clickContainer: scopeObj.view.tableView.tableSubHeaderHistory.flxStatus },
            ];
            scopeObj.view.tableView.tableSubHeaderHistory.imgSortType.setVisibility(false);
            FormControllerUtility.setSortingHandlers(scopeObj.historySortMap, scopeObj.onHistorySortClickHandler, scopeObj);
            scopeObj.paymentDueSortMap = [{
                name: 'nickName',
                imageFlx: scopeObj.view.tableView.imgSortPayee,
                clickContainer: scopeObj.view.tableView.flxPayee
            },
            { name: 'billDueDate', imageFlx: this.view.tableView.imgSortDatee, clickContainer: this.view.tableView.flxDate }
            ];
            FormControllerUtility.setSortingHandlers(scopeObj.paymentDueSortMap, scopeObj.onPaymentDueSortClickHandler, scopeObj);
            scopeObj.allPayeesSortMap = [{
                name: 'payeeNickName',
                imageFlx: scopeObj.view.tableView.imgSortPayee,
                clickContainer: scopeObj.view.tableView.flxPayee
            }];
            FormControllerUtility.setSortingHandlers(scopeObj.allPayeesSortMap, scopeObj.onAllPayeeSortClickHanlder, scopeObj);
        },
        /**
         * setting Configurations to billPay screen
         */
        settingConfigurationsToBillPay: function () {
            var scopeObj = this;
            if (applicationManager.getConfigurationManager().billPaySearch === "true") {
//                 scopeObj.view.tableView.flxSearch.setVisibility(true);
            } else {
                scopeObj.view.tableView.flxSearch.setVisibility(false);
            }
            if (applicationManager.getConfigurationManager().billPayOneTimePayment === "true") {
                scopeObj.view.flxmakeonetimepayment.setVisibility(true);
            }
            else {
                scopeObj.view.flxmakeonetimepayment.setVisibility(false);
            }
            if (applicationManager.getConfigurationManager().defaultBillPayAccountSelection === "true") {
                scopeObj.view.Activatebillpays.flxDefaultAccount.setVisibility(false);
            }
            else {
                scopeObj.view.Activatebillpays.flxDefaultAccount.setVisibility(true);
            }
        },
        /**
         * executes the form pre show methods
         */
        frmBillPayPreShow: function () {
            var configurationManager = applicationManager.getConfigurationManager();
            this.setServerError(false);
            this.view.btnConfirm.setVisibility(false);
            CommonUtilities.setText(this.view.tableView.lblBill, "Bill" , CommonUtilities.getaccessibilityConfig());
            this.view.payABill.lblCategory.setVisibility(false);
            this.view.payABill.lbxCategoryValue.setVisibility(false);
          	this.view.flxrightcontainerbillpay.top = "90dp";
            applicationManager.getLoggerManager().setCustomMetrics(this, false, "Bill Pay");
            this.view.tableView.Search.txtSearch.width = "100%";
            this.view.tableView.imgSearch.src = ViewConstants.IMAGES.SEARCH_IMAGE;
            this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
            if(configurationManager.isFastTransferEnabled == "false")
            this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
            else
              {
            this.view.customheader.topmenu.flxMyBills.skin = "sknFlxFFFFFbrdr3343a8Pointer";
                this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknHoverTopmenu7f7f7pointer";
              }
            this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
            CommonUtilities.setText(this.view.tableView.lblDueDate, kony.i18n.getLocalizedString("i18n.billPay.DueDate") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.tableView.tableTabs.btnTransfers, kony.i18n.getLocalizedString("i18n.billPay.PaymentDue") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.tableView.tableTabs.btnDeposits, kony.i18n.getLocalizedString("i18n.billPay.scheduled") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.tableView.tableTabs.btnChecks, kony.i18n.getLocalizedString("i18n.billPay.History") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.tableView.tableTabs.btnWithdraws, kony.i18n.getLocalizedString("i18n.billPay.ManagePayee") , CommonUtilities.getaccessibilityConfig());
            this.view.tableView.tableTabs.BtnAll.text = kony.i18n.getLocalizedString("i18n.billPay.AllPayees");
            this.view.payABill.calSendOn.hidePreviousNextMonthDates = true;
            this.view.oneTimePayment.flxCalSendOn.calSendOn.hidePreviousNextMonthDates = true;
            this.view.payABill.CalDeliverBy.hidePreviousNextMonthDates = true;
            this.view.payABill.calEndingOnRec.hidePreviousNextMonthDates = true;
            this.view.oneTimePayment.flxCalEndingOn.calEndingOn.hidePreviousNextMonthDates = true;
            CommonUtilities.setText(this.view.transferActivity.lblAmount, kony.i18n.getLocalizedString("i18n.transfers.lblAmount")+"("+applicationManager.getConfigurationManager().getCurrencyCode()+")" , CommonUtilities.getaccessibilityConfig());            
            FormControllerUtility.updateWidgetsHeightInInfo(this, ['customheader','flxContainer','flxFooter','flxHeader','Activatebillpays.flxDefaultAccount','Activatebillpays.flxinfo']);
        },
        /**
         * executes and displays the error flex in landing page.
         * @param {boolean} isError used to display the flex
         * @param {object} erroObj  get the exact error mesage
         */
            /**
         * executes and displays the error flex in landing page.
         * @param {boolean} isError used to display the flex
         * @param {object} erroObj  get the exact error mesage
         */
       setServerError: function (isError, erroObj) {
        var scopeObj = this;
        scopeObj.view.flxDowntimeWarning.setVisibility(isError);
        if (isError) {
            scopeObj.view.rtxDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
 		 if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
                scopeObj.view.flxBillPay.top = '100dp';
         }
                else{
                    scopeObj.view.flxBillPay.top = '25dp';
                }           
          if (erroObj.gettingFromOneTimePayment) {
                scopeObj.view.flxOneTImePayment.oneTimePayment.flxDowntimeWarning.setVisibility(isError);
                scopeObj.view.flxOneTImePayment.oneTimePayment.flxDowntimeWarning.rtxDowntimeWarningDomestic.text = erroObj.errorMessage;
            } else if (erroObj.gettingFromSingleBillPay) {
               // if (erroObj.isServerUnreachable === true && erroObj.errorMessage) {
                    if (erroObj.errorMessage) {
                        scopeObj.view.payABill.flxError.setVisibility(isError);
                        scopeObj.view.payABill.flxError.rtxErrorPayBill.text = erroObj.errorMessage;
                        scopeObj.view.flxDowntimeWarning.setVisibility(false);
                    } else {
                        scopeObj.view.flxDowntimeWarning.setVisibility(true);
                    }
            } else if (erroObj.bulkBillPayement) {
                scopeObj.view.rtxDowntimeWarning.text = erroObj.errorMessage;
            }else if(erroObj.errorMessage)
            {
                scopeObj.view.rtxDowntimeWarning.text = erroObj.errorMessage;
            }
            FormControllerUtility.hideProgressBar(scopeObj.view);
        } else {
          if(kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile){
            if(scopeObj.view.flxBillHeaderMobile.isVisible === true)
              if(scopeObj.view.lbxPayee.isVisible === true){scopeObj.view.flxBillPay.top = "150dp";}
              else{scopeObj.view.flxBillPay.top = "110dp";}
            else
              scopeObj.view.flxBillPay.top="0dp";
          }
          else{
            scopeObj.view.flxBillPay.top = "25dp";
          }
            scopeObj.view.flxTotalEbillAmountDue.top = '0dp';
            // scopeObj.view.flxaddpayeemakeonetimepayment.top = '20dp';
        }
        scopeObj.AdjustScreen();
    },
        /**
     * intigrate and update  the HamburgerMenu
     * @param {object}  sideMenuModel side menu model
      */
        updateHamburgerMenu: function () {
            this.view.customheader.customhamburger.activateMenu("Bill Pay", "Pay A Bill");
            if (this.selectedTab === "managePayees") {
                this.view.customheader.customhamburger.activateMenu(kony.i18n.getLocalizedString("i18n.billPay.BillPay"), "My Payee List");
            }
            if (this.selectedTab === "history") {
                this.view.customheader.customhamburger.activateMenu(kony.i18n.getLocalizedString("i18n.billPay.BillPay"), "Bill Pay History");
            }
            if (this.selectedTab === "allPayees") {
                this.view.customheader.customhamburger.activateMenu(kony.i18n.getLocalizedString("i18n.billPay.BillPay"), "Pay a Bill");
            }
        },
        /**
         * exexcutes the BillPay Post Show Form actions
         */
        frmBillPayPostShow: function () {
            // this.view.flxAddPayee.skin = ViewConstants.SKINS.SKNFLEXROUNDEDBORDERFFFFFF3PX;
            this.setCalendarContexts();
            this.AdjustScreen();
          	var scopeObj = this;
          	this.view.Activatebillpays.flxinfo.onClick = function(){
            	scopeObj.view.AllForms.setVisibility(true);
            };
          	this.view.AllForms.flxCross.onClick = function(){
                scopeObj.view.AllForms.setVisibility(false);
            };
            applicationManager.getNavigationManager().applyUpdates(this);
            this.view.customheader.forceCloseHamburger();
            this.onBreakpointChange(kony.application.getCurrentBreakpoint());
            this.accessibilityFocusSetup();
        },
        /**
         * Set foucs handlers for skin of parent flex on input focus 
         */
        accessibilityFocusSetup: function(){
            let widgets = [
                [this.view.txtSearch, this.view.flxtxtSearchandClearbtn],
            ]
            for(let i=0; i<widgets.length; i++){
                CommonUtilities.setA11yFoucsHandlers(widgets[i][0], widgets[i][1], this)
            }
        },
        /**   Method to handle the UI Screen Activities.
        *    @param {object} data data
        */
        AdjustScreen: function (data) {
            if (!data) {
                data = 0;
            }
            this.view.forceLayout();
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.customheader.info.frame.height + this.view.flxContainer.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + data + "dp";
                }
                else {
                    this.view.flxFooter.top = mainheight + data + "dp";
                }
            } else {
                this.view.flxFooter.top = mainheight + data + "dp";
            }
            this.view.forceLayout();
            this.initializeResponsiveViews();
        },
        /**
         * used to bind Terms and condition data on Activation screen
         * @param {object} TnCcontent bill pay supported sccounts
         */
        bindTnCData: function(TnCcontent){
            if(TnCcontent.alreadySigned){
                this.view.Activatebillpays.flxAgree.setVisibility(false);
            }
            else{
                CommonUtilities.disableButton(this.view.Activatebillpays.btnProceed);
                this.view.Activatebillpays.lblFavoriteEmailCheckBox.text = OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED;
                this.view.Activatebillpays.flxAgree.setVisibility(true);
                if(TnCcontent.contentTypeId===OLBConstants.TERMS_AND_CONDITIONS_URL){
                    this.view.Activatebillpays.btnTandC.onClick = function(){ 
                        window.open(TnCcontent.termsAndConditionsContent);
                    }
                }else{
                     this.view.Activatebillpays.btnTandC.onClick = this.showTermsAndConditionPopUp;
                     this.setTnCDATASection(TnCcontent.termsAndConditionsContent);
                     FormControllerUtility.setHtmlToBrowserWidget(this, this.view.brwBodyTnC, TnCcontent.termsAndConditionsContent);
                }
                this.view.flxClose.onClick = this.hideTermsAndConditionPopUp;
                this.view.Activatebillpays.lblFavoriteEmailCheckBox.onClick = this.toggleTnC.bind(this,this.view.Activatebillpays.lblFavoriteEmailCheckBox);
            }
        },
        showTermsAndConditionPopUp:function(){
            var height = this.view.flxHeader.info.frame.height + this.view.flxContainer.info.frame.height + this.view.flxFooter.info.frame.height;
            this.view.flxTermsAndConditionsPopUp.height=height+"dp";
            this.view.flxTermsAndConditionsPopUp.setVisibility(true);
            this.view.forceLayout();
        },
        hideTermsAndConditionPopUp:function(){
            this.view.flxTermsAndConditionsPopUp.setVisibility(false);
        },
        setTnCDATASection:function(content){
            this.view.rtxTC.text = content;
        },
        toggleTnC: function(widget) {
            CommonUtilities.toggleFontCheckbox(widget);
            if(widget.text === OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED)
                CommonUtilities.disableButton(this.view.Activatebillpays.btnProceed);
            else
                CommonUtilities.enableButton(this.view.Activatebillpays.btnProceed);
        },
        /**
         * used to show billpay activation screen
         * @param {object} billPayAcccounts bill pay supported sccounts
         */
        showBillPayActivationScreen: function (billPayAcccounts) {
            var self = this;
            self.view.AllForms.isVisible = false;
            self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            self.resetUIDuringActivation();
            self.setAccountsForActivationScreen(billPayAcccounts);
            self.view.Activatebillpays.flxActivateMain.setVisibility(true);
            self.view.Activatebillpays.flxActivateAcknowledgement.setVisibility(false);
            self.view.Activatebillpays.flxActiveBillPayNotEligible.setVisibility(false);
            CommonUtilities.setText(self.view.Activatebillpays.lblFavoriteEmailCheckBox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED , CommonUtilities.getaccessibilityConfig());
          	self.view.Activatebillpays.lblFavoriteEmailCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
            CommonUtilities.setText(self.view.Activatebillpays.lblWarning, kony.i18n.getLocalizedString("i18n.billPay.activateBillPayMsg") , CommonUtilities.getaccessibilityConfig());
            if (CommonUtilities.isCSRMode()) {
                self.view.Activatebillpays.btnProceed.onClick = CommonUtilities.disableButtonActionForCSRMode();
                self.view.Activatebillpays.btnProceed.skin = CommonUtilities.disableButtonSkinForCSRMode();
                self.view.Activatebillpays.btnProceed.focusSkin = CommonUtilities.disableButtonSkinForCSRMode();
                self.view.Activatebillpays.btnProceed.hoverSkin = CommonUtilities.disableButtonSkinForCSRMode();
            }
            else {
                self.view.Activatebillpays.btnProceed.onClick = function () {
                    if (self.view.Activatebillpays.lblFavoriteEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED) {
                        self.view.Activatebillpays.flxWarning.setVisibility(true);
                        CommonUtilities.setText(self.view.Activatebillpays.lblWarning, kony.i18n.getLocalizedString("i18n.billPay.activateBillPayProceedError") , CommonUtilities.getaccessibilityConfig());
                    } else {
                        self.view.Activatebillpays.flxWarning.setVisibility(false);
                        var accountNumber = self.view.Activatebillpays.listbxAccountType.selectedKey;
                        self.presenter.activateBillPay(accountNumber);
                    }
                };
            }
            self.view.Activatebillpays.lblFavoriteEmailCheckBox.onClick = function () {
                if (self.view.Activatebillpays.lblFavoriteEmailCheckBox.text === ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED) {
                    CommonUtilities.setText(self.view.Activatebillpays.lblFavoriteEmailCheckBox, ViewConstants.FONT_ICONS.CHECBOX_SELECTED , CommonUtilities.getaccessibilityConfig());
          			self.view.Activatebillpays.lblFavoriteEmailCheckBox.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                } else {
                    CommonUtilities.setText(self.view.Activatebillpays.lblFavoriteEmailCheckBox, ViewConstants.FONT_ICONS.CHECBOX_UNSELECTED , CommonUtilities.getaccessibilityConfig());
          			self.view.Activatebillpays.lblFavoriteEmailCheckBox.skin = ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                }
            };
            FormControllerUtility.hideProgressBar(this.view);
            self.view.forceLayout();
        },
        /**
        * used to set data for accounts in BillPay Actvation Screen
        * @param {object} billPayAccounts bill pay suported accounts
        */
        setAccountsForActivationScreen: function (billPayAccounts) {
            var listBoxData = FormControllerUtility.getListBoxDataFromObjects(billPayAccounts, "accountID", CommonUtilities.getAccountDisplayNameWithBalance);
            this.view.Activatebillpays.listbxAccountType.masterData = listBoxData;
            this.view.AllForms.isVisible = false;
            this.view.forceLayout();
        },
        /**
         * used to show  billpay activation screen
        */
        showBillPayActivationAcknowledgementScreen: function () {
            var self = this;
            self.resetUIDuringActivation();
            self.view.Activatebillpays.flxActivateMain.setVisibility(false);
            self.view.Activatebillpays.flxActivateAcknowledgement.setVisibility(true);
            self.view.Activatebillpays.flxActiveBillPayNotEligible.setVisibility(false);
            if(kony.application.getCurrentBreakpoint()!=640 && !orientationHandler.isMobile){
                self.view.flxActivatebillpays.width = "58.2%";
            }
            CommonUtilities.setText(self.view.Activatebillpays.CopylblHeader0h02f9026a70b47, kony.i18n.getLocalizedString("i18n.billPay.activateBillPay") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(self.view.Activatebillpays.lblTransactionMessage, kony.i18n.getLocalizedString("i18n.billPay.activateBillPaySuccessMsg") , CommonUtilities.getaccessibilityConfig());
            self.view.flxaddpayeemakeonetimepayment.setVisibility(true);
            self.view.Activatebillpays.btnAddPayee.onClick = self.onClickAddPayee.bind(self);
            self.view.Activatebillpays.btnMakeOneTimePayment.onClick = self.onClickOneTimePayement.bind(self);
            self.view.AllForms.isVisible = false;
            self.view.forceLayout();
        },
        /**
         * click on All Payess
         */
        setAllPayessOnClick: function () {
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            this.presenter.showAllPayees(null, "AllPayees", true);
        },
        /**
         * handleing on manage payee schenario
         */
        managePayeesOnClick: function () {
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            this.presenter.showBillPayData(null, "ManagePayees", true);
        },
         /**
        * onClickPayBill:    on Click action of btnPayBill
        * @param {object} payeeData details of the payee
        * */
        onClickPayBill : function (payeeData) {
            function getDateFromDateString(dateStr) {
                if (dateStr) {
                    return CommonUtilities.getFrontendDateString(dateStr);
                }
                else {
                    return "";
                }
            }
            var payBillData = {
                "payeeNickname": payeeData.payeeNickName||payeeData.payeeName,
                "lastPaidAmount": payeeData.lastPaidAmount,
                "lastPaidDate": getDateFromDateString(payeeData.lastPaidDate),
                "dueAmount": CommonUtilities.formatCurrencyWithCommas(payeeData.dueAmount),
                "billDueDate": getDateFromDateString(payeeData.billDueDate),
                "eBillSupport": payeeData.eBillSupport,
                "eBillStatus": payeeData.ebillStatus,
                "billid": payeeData.billid,
                "payeeId": payeeData.payeeId,
                "accountNumber": payeeData.accountNumber,
                "billGeneratedDate": getDateFromDateString(payeeData.billGeneratedDate),
                "ebillURL": payeeData.ebillURL
            };
            this.setDataForPayABill(payBillData);
        },
        /**
        * setAllPayeesSegmentData:    used to set all payees segment data.
        * @param {object} allPayees all payees object
        * @param {object} billPayAccounts bill pay accounts
        * */
        setAllPayeesSegmentData: function (allPayees, billPayAccounts) {
            FormControllerUtility.showProgressBar(this.view);
            self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            this.setAllPayeesUI();
            this.setBottomDefaultValues();
            var scopeObj = this;
            if (allPayees) {
                if (allPayees.length > 0) {
                    var preferredAccountNumber = self.presenter.getBillPayPreferedAccountNumber();
                    this.view.btnConfirm.onClick = this.validateBulkPayData.bind(this);
                    if(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile){
                        this.view.flxConfirmButton.setVisibility(false);
                    }else{
                        this.view.flxConfirmButton.setVisibility(true);
                    }
                    this.view.btnConfirm.setVisibility(true);
                    var billPayData = {
                        "allPayees": allPayees
                    };
                    this.getNewBulkPayData(billPayData, billPayAccounts, preferredAccountNumber);
                    var dataMap = {
                        "lblLineOne": "lblLineOne",
                        "lblLineTwo": "lblLineTwo",
                        "lblIdentifier": "lblIdentifier",
                        "imgDropdown": "imgDropdown",
                        "lblPayee": "lblPayee",
                        "lblPayeeDate": "lblPayeeDate",
                        "btnEbill": "btnEbill",
                        "lblDueDate": "lblDueDate",
                        "lblBill": "lblBill",
                        "txtAmount": "txtAmount",
                        "lblSeparator": "lblSeparator",
                        "lblPayFrom": "lblPayFrom",
                        "lblSendOn": "lblSendOn",
                        "lblDeliverBy": "lblDeliverBy",
                        "imgListBoxDropdown": "imgListBoxDropdown",
                        "calSendOn": "calSendOn",
                        "calDeliverBy": "calDeliverBy",
                        "lblNotes": "lblNotes",
                        "lblCategory": "lblCategory",
                        "txtNotes": "txtNotes",
                        "lblCategoryList": "lblCategoryList",
                        "imgCategoryListDropdown": "imgCategoryListDropdown",
                        "btnViewEBill": "btnViewEBill",
                        "lblDollar": "lblDollar",
                        "lblSeparatorBottom": "lblSeparatorBottom",
                        "btnActivateEbill": "btnActivateEbill",
                        "lstPayFrom": "lstPayFrom",
                        "btnPayBill": "btnPayBill"
                    };
                    this.view.tableView.segmentBillpay.widgetDataMap = dataMap;
                    var segmentData = billPayData.formattedBulkBillPayRecords;
                    for (var i = 0; i < segmentData.length; i++) {
                        var e = billPayData.formattedBulkBillPayRecords[i];
                        segmentData[i].btnPayBill = {
                            "text": kony.i18n.getLocalizedString("i18n.Pay.PayBill"),
                            "onClick":  scopeObj.onClickPayBill.bind(this, e)
                           }
                    }
                    this.view.tableView.segmentBillpay.setData(segmentData);
                    this.view.tableView.segmentBillpay.setVisibility(true);
                    scopeObj.view.tableView.imgSortDatee.setVisibility(false);
                    this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
                    if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
                      this.view.tableView.flxBillPayAllPayees.setVisibility(false);
                    }
                    else{
                      this.view.tableView.flxBillPayAllPayees.setVisibility(true);
                    }
                    this.view.tableView.lblPayee.setFocus(true);
                    scopeObj.view.tableView.flxDate.onClick = null;
                }
                else {
                    this.view.tableView.segmentBillpay.setVisibility(false);
                    this.view.tableView.flxBillPayAllPayees.setVisibility(false);
                    scopeObj.showNoPayementDetails({
                        noPaymentMessageI18Key: "i18n.billPay.noPayeesMessage"
                    });
                    scopeObj.view.flxConfirmButton.setVisibility(false);
                    scopeObj.view.btnConfirm.setVisibility(false);
                }
            }
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * used to show the no payees flow.
         * @param {message} message used to show the no message message on the page
         */
        showNoPayementDetails: function (message) {
            var scopeObj = this;
            if (message) {
                scopeObj.view.tableView.flxNoPayment.setVisibility(true);
                scopeObj.view.tableView.segmentBillpay.setVisibility(false);
                scopeObj.setPagination({ show: false });
                scopeObj.view.tableView.rtxNoPaymentMessage.text = kony.i18n.getLocalizedString(message.noPaymentMessageI18Key);
                if (message.showActionMessageI18Key) {
                    scopeObj.view.tableView.lblScheduleAPayment.setVisibility(false);
                    CommonUtilities.setText(scopeObj.view.tableView.lblScheduleAPayment, kony.i18n.getLocalizedString(message.showActionMessageI18Key) , CommonUtilities.getaccessibilityConfig());
                } else {
                    scopeObj.view.tableView.lblScheduleAPayment.setVisibility(false);
                }
            }
        },
        /**
               * setAllPayeesUI:   Method to set UI for All Payees Tab, on Navigate to allPayees tab UI
            */
        setAllPayeesUI: function () {
            this.selectedTab = "allPayees";
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
            }, {
                text: kony.i18n.getLocalizedString("i18n.billPay.AllPayees")
            }]);
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
            }, {
                text: kony.i18n.getLocalizedString("i18n.billPay.AllPayees")
            }]);
            this.setSkinActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
            this.setSkinInActive(this.view.tableView.tableTabs.btnTransfers);
            this.setSkinInActive(this.view.tableView.tableTabs.btnDeposits);
            this.setSkinInActive(this.view.tableView.tableTabs.btnChecks);
            this.setSkinInActive(this.view.tableView.tableTabs.btnWithdraws);
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.AllPayees");
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.AllPayees");
            this.view.tableView.isVisible = true;
            this.view.payABill.setVisibility(false);
            this.view.flxBillPay.setVisibility(true);
            this.view.flxBillPayManagePayeeActivitymodified.setVisibility(false);
            this.view.transferActivitymodified.setVisibility(false);
            this.view.transferActivitymodified.flxSegmentBillPay.setVisibility(false);
            if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
              this.view.tableView.flxBillPayAllPayees.setVisibility(false);
            }
            else{
              this.view.tableView.flxBillPayAllPayees.setVisibility(true);
            }
            this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
            this.view.tableView.flxBillPayScheduled.setVisibility(false);
            this.view.flxOneTImePayment.setVisibility(false);
            this.view.flxActivatebillpays.setVisibility(false);
            this.view.AllForms.setVisibility(false);
            if(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile){
                this.view.flxConfirmButton.setVisibility(false);
            }else{
                this.view.flxConfirmButton.setVisibility(true);
            }
            this.view.btnConfirm.setVisibility(true);
            this.view.tableView.Search.setVisibility(true);
            this.view.tableView.flxNoPayment.setVisibility(false);
            this.view.tableView.tableSubHeaderHistory.setVisibility(false);
            this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
            CommonUtilities.setText(this.view.tableView.lblDueDate, kony.i18n.getLocalizedString("i18n.billPay.DueDate") , CommonUtilities.getaccessibilityConfig());
            FormControllerUtility.setSortingHandlers(this.allPayeesSortMap, this.onAllPayeeSortClickHanlder, this);
            this.view.tableView.Search.setVisibility(false);
           this.view.flxBillHeaderMobile.isVisible =false;
            this.view.forceLayout();
        },
        /**
         * used to handle the active skin
         * @param {object} obj selected skin
         */
        setSkinActive: function (obj) {
            obj.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
           obj.hoverSkin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED;
        },
        /**
         * used to reset the BillPay UI
         */
        resetBillPayUI: function () {
            this.view.flxActivatebillpays.setVisibility(false);
            this.view.AllForms.setVisibility(false);
            this.view.flxBillPay.setVisibility(true);
            this.view.tableView.segmentBillpay.setVisibility(false);
            this.view.tableView.Search.setVisibility(true);
            this.view.flxBillPayManagePayeeActivity.setVisibility(false);
            this.view.flxViewEbill.setVisibility(false);
            this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
            this.view.tableView.flxBillPayAllPayees.setVisibility(false);
            this.view.tableView.flxBillPayScheduled.setVisibility(false);
            this.view.tableView.tableSubHeaderHistory.setVisibility(false);
            this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
            this.view.tableView.flxNoPayment.setVisibility(false);
            this.view.flxPagination.setVisibility(false);
        },
        /**
         * used to handle the active skin for all Payees
         * @param {object} obj modified skin
         */
        setSkinActiveAllPayees: function (obj) {
            obj.skin = ViewConstants.SKINS.ACCOUNT_SUMMARY_SELETED_MODIFIED;
        },
        /**
         * used to handle in-active skin
         * @param {object} obj object
         */
        setSkinInActive: function (obj) {
            obj.skin =  ViewConstants.SKINS.TAB_INACTIVE;
            //obj.hoverSkin = ViewConstants.SKINS.ACCOUNT_DETAILS_SUMMARY_UNSELECTED_HOVER;
        },
        /**
         * used to set page bottom values
         */
        setBottomDefaultValues: function () {
            this.view.flxPagination.setVisibility(false);
            this.view.flxConfirmButton.setVisibility(false);
            this.view.btnConfirm.setVisibility(false); //TODO: for R2
            // this.view.flxBottom.height = "220dp";
            this.view.flxTermsAndConditions.top = "30dp";
            this.view.flxBottom.forceLayout();
        },
        /**
           * getNewBulkPayData:     Method to handle bulk pay
           * @param {object} data data
           * @param {object} billPayAccounts billPay accounts
           * @param {string} preferredAccountNumber prefferd account number
       */
        getNewBulkPayData: function (data, billPayAccounts, preferredAccountNumber) {
            data.formattedBulkBillPayRecords = data.allPayees.map(this.createNewBulkPayData.bind(this, billPayAccounts, preferredAccountNumber));
        },
        /**
         * Method to create the bulk Payement UI
        * @param {object} billPayAccounts billPay accounts
        * @param {string}  preferredAccountNumber prefferd account number
        * @param {object} data data
        * @returns {object} payee list
        */
       createNewBulkPayData: function(billPayAccounts, preferredAccountNumber, data) {
            var lblPayeeDate;
            var lblDueDate;
            var lblBill;
            var btnEbill;
            var btnViewEbill;
            var lblPayee;
            var btnActivateEBill;
            var txtAmount;
            var scopeObj = this;
            var self = this;
            lblPayee = data.payeeNickName ? data.payeeNickName : data.payeeName;
            var nonValue = kony.i18n.getLocalizedString("i18n.common.none");
            if (data.billid === "0") {
                lblPayeeDate = kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ": " + kony.i18n.getLocalizedString("i18n.billPay.noPaymentActivity");
                lblDueDate = nonValue;
                lblBill = nonValue;
                txtAmount = {
                    "placeholder": kony.i18n.getLocalizedString("i18n.common.EnterAmount"),
                    "text": ""
                };
            } else {
                lblPayeeDate = kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ": " + (data.lastPaidAmount ? ((scopeObj.formatAmount(data.lastPaidAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(data.transactionCurrency)) + " " + kony.i18n.getLocalizedString("i18n.common.on") + " " + scopeObj.getDateFromDateString(data.lastPaidDate, "YYYY-MM-DDTHH:MM:SS"))) : ("No Payment Activity"));
                lblDueDate = data.billDueDate ? this.getDateFromDateString(data.billDueDate, "YYYY-MM-DD") : nonValue;
                lblBill = data.dueAmount ? this.formatAmount(data.dueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(data.transactionCurrency)): nonValue;
                txtAmount = {
                    "placeholder": kony.i18n.getLocalizedString("i18n.common.EnterAmount"),
                    "text": data.dueAmount ? this.formatAmount(data.dueAmount, true) : nonValue
                };
            }
            var lblNotes = data.notes ? data.notes : '';
            var calSendOn = {
                "dateComponents": data.sendOn ? this.getDateComponents(data.sendOn) : CommonUtilities.getServerDateComponent(),
                "dateFormat": applicationManager.getFormatUtilManager().getDateFormat(),
                "validStartDate": self.getvalidStartDate(),
                "hidePreviousNextMonthDates": true,
               "accessibilityconfig": {
                            "a11yLabel": data.sendOn
                }
            };
            var calDeliverBy = {
                "dateComponents": lblDueDate == nonValue ? CommonUtilities.getServerDateComponent() : this.getDateComponents(data.billDueDate),
                "dateFormat": applicationManager.getFormatUtilManager().getDateFormat(),
                "validStartDate": self.getvalidStartDate(),
                "hidePreviousNextMonthDates": true
            };
            if (data.eBillSupport == "false" || data.isManuallyAdded == "true") {
                btnEbill = {
                    "text": "SOME TEXT",
                    "skin": ViewConstants.SKINS.SKNBTNEBILLACTIVE,
                    "isVisible": false
                };
            } else {
                if (data.eBillStatus == 1) {
                    btnEbill = {
                        "text": "SOME TEXT",
                        "skin": ViewConstants.SKINS.SKNBTNEBILLACTIVE,
                        "onClick": (data.billid === undefined || data.billid === null || data.billid == "0") ? null : scopeObj.viewEBill.bind(scopeObj, {
                            "billGeneratedDate": scopeObj.getDateFromDateString(data.billGeneratedDate, "YYYY-MM-DD"),
                            "amount": scopeObj.formatAmount(data.dueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(data.transactionCurrency)),
                            "ebillURL": data.ebillURL
                        })
                    };
                    lblDueDate = {
                        "text": lblDueDate,
                        "isVisible": true
                    };
                    btnActivateEBill = {
                        "isVisible": false,
                        "text": kony.i18n.getLocalizedString("i18n.WireTransfer.ACTIVATE"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.WireTransfer.ACTIVATE"),
                        "onClick": null,
                    };
                } else {
                    btnEbill = {
                        "text": "SOME TEXT",
                        "skin": ViewConstants.SKINS.SKNBTNIMGINACTIVEEBILL,
                        "onClick": null
                    };
                    lblDueDate = {
                        "text": lblDueDate,
                        "isVisible": false,
                         "accessibilityconfig": {
                            "a11yLabel": lblDueDate
                    }
                    };
                    btnActivateEBill = {
                        "isVisible": true,
                        "text": kony.i18n.getLocalizedString("i18n.WireTransfer.ACTIVATE"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.WireTransfer.ACTIVATE"),
                        "onClick": self.activateEbillUI.bind(this, data, "AllPayees")
                    };
                }
            }
            return {
                "payeeId": data.payeeId,
                "billid": data.billid,
                "payeeAccountNumber": data.payeeAccountNumber,
                "lblSeparatorBottom": {
                    "text": " "
                },
                "lblIdentifier": {
                    "text": " ",
                    "skin": "skin Name"
                },
                "imgDropdown": {
                    "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                    "accessibilityconfig": {
                        "a11yLabel": "View Transaction details"
                    }
                },
                "lblPayee": {
                    "text": lblPayee,
                    "accessibilityconfig": {
                        "a11yLabel": lblPayee
                    }
                },
                "lblPayeeDate":{
                    "text" : lblPayeeDate,
                     "accessibilityconfig": {
                        "a11yLabel": lblPayeeDate
                    }
                },
                "btnEbill": btnEbill,
                "lblDueDate": lblDueDate,
                "lblBill": {
                    "text" : lblBill,
                    "accessibilityconfig": {
                        "a11yLabel": lblBill
                    }
                },
                "txtAmount": txtAmount,
                "lblSeparator": "A",
                "lblPayFrom": {
                    "text" :  kony.i18n.getLocalizedString("i18n.billPay.PayFrom"),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.PayFrom")
                    }
                },
                "lblSendOn": {
                    "text" :  kony.i18n.getLocalizedString("i18n.transfers.send_on"),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.send_on")
                    }
                },
                "lblDeliverBy":{
                    "text" : kony.i18n.getLocalizedString("i18n.billPay.DeliverBy"),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.DeliverBy")
                    }
                },
                "imgListBoxDropdown": ViewConstants.IMAGES.ARRAOW_DOWN,
                "calSendOn": calSendOn,
                "calDeliverBy": calDeliverBy,
                "lblNotes": {
                    "text" : kony.i18n.getLocalizedString("i18n.transfers.Description"),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.Description")
                    }
                },
                "lblCategory": {
                    "text" : kony.i18n.getLocalizedString("i18n.billPay.category"),
                    "accessibilityconfig": {
                        "a11yLabel": kony.i18n.getLocalizedString("i18n.billPay.category")
                    }
                },
                "txtNotes": {
                    "placeholder": kony.i18n.getLocalizedString("i18n.transfers.optional"),
                    "text": ""
                },
                "lblCategoryList": {
                    "text" : "Phone Bill",
                    "accessibilityconfig": {
                        "a11yLabel": "Phone Bill"
                    }
                },
                "imgCategoryListDropdown": ViewConstants.IMAGES.ARRAOW_DOWN,
                "btnViewEBill": btnViewEbill,
                "lblLineOne": "1",
                "lblLineTwo": "2",
                "accountNumber": {
                    "text" : data.accountNumber,
                    "accessibilityconfig": {
                        "a11yLabel": data.accountNumber
                    }
                },
                "payeeNickName":  {
                    "text" : data.payeeNickName,
                    "accessibilityconfig": {
                        "a11yLabel": data.payeeNickName
                    }
                },
                "payeeName": {
                    "text" : data.payeeName,
                    "accessibilityconfig": {
                        "a11yLabel": data.payeeName
                    }
                },
                "lstPayFrom": {
                    "masterData": FormControllerUtility.getListBoxDataFromObjects(billPayAccounts, "accountID", CommonUtilities.getAccountDisplayNameWithBalance),
                    "selectedKey": preferredAccountNumber ? preferredAccountNumber : billPayAccounts[0].accountID,
                },
                "template": orientationHandler.isMobile ? "flxBillPayAllPayeesMobile" : "flxBillPayAllPayees",
                "btnActivateEbill": btnActivateEBill,
                "lblDollar": applicationManager.getConfigurationManager().getCurrencyCode(),
                "preferredAccountNumber": preferredAccountNumber
            }
        },
        /**
      * used to convert the CalenderFormat Date
      * @param {String} dateString string formated date
      * @param {string} inputFormat input format
      * @returns {string} outputDate output date
      */
        getDateFromDateString: function (dateString, inputFormat) {
            var fu = applicationManager.getFormatUtilManager();
            var dateObj = fu.getDateObjectfromString(dateString, inputFormat);
            var outputDate = fu.getFormatedDateString(dateObj, fu.getApplicationDateFormat());
            return outputDate;
        },
        /**
         * Method to get the date
         * @return {array[]} startdate
         */
        getvalidStartDate: function () {
            var date = CommonUtilities.getServerDateObject();
            var dd = date.getDate();
            var mm = date.getMonth() + 1;
            var yy = date.getFullYear();
            return [dd, mm, yy, 0, 0, 0];
        },
        /**
         * viewEBill:    method to show view the ebill.
        * @param {object} viewModel ebill information
         */
        viewEBill: function (viewModel) {
            if (viewModel) {
                var scopeObj = this;
                var nonValue = scopeObj.nonValue;
                var heightToSet = 140 + scopeObj.view.flxContainer.info.frame.height + scopeObj.view.flxFooter.info.frame.height;
                scopeObj.view.flxViewEbill.height = heightToSet + "dp";
                CommonUtilities.setText(scopeObj.view.ViewEBill.lblPostDateValue, viewModel.billGeneratedDate || nonValue , CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(scopeObj.view.ViewEBill.lblAmountValue, viewModel.amount || nonValue , CommonUtilities.getaccessibilityConfig());
                scopeObj.view.ViewEBill.flxMemo.setVisibility(false); //TODO: Not required for R2.
                scopeObj.view.ViewEBill.imgEBill.src = viewModel.ebillURL;
                if (viewModel.ebillURL) {
                    scopeObj.view.ViewEBill.flxDownload.onClick = scopeObj.downloadFile.bind(scopeObj, {
                        'url': viewModel.ebillURL
                    });
                    scopeObj.view.ViewEBill.flxDownload.setVisibility(true);
                } else {
                    scopeObj.view.ViewEBill.flxDownload.setVisibility(false);
                    scopeObj.view.ViewEBill.flxDownload.onClick = null;
                }
				this.view.ViewEBill.imgZoom.setVisibility(false)
				this.view.ViewEBill.imgFlip.setVisibility(false)
                scopeObj.view.flxViewEbill.setVisibility(true);
                scopeObj.view.ViewEBill.lblTransactions.setFocus(true);
                scopeObj.view.forceLayout();
            }
        },
        /**
* downloadFile:    method to download the e-bill file.
* @param {object} data information
*/
        downloadFile: function (data) {
            if (data) {
                CommonUtilities.downloadFile({
                    'url': data.url,
                    'filename': kony.i18n.getLocalizedString('i18n.billPay.Bill')
                })
            }
        },
        /**
       * bindPaymentDueSegment:    method used to bind due bills to segment.
       * @param {billsData} billsData list of bills
       * @param {object} billPayAccounts sorting configuration
       */
       bindPaymentDueSegment: function(billsData, billPayAccounts) {
            var scopeObj = this;
            this.setPaymentDueUI();
            if (billsData.length === 0) {
                scopeObj.showNoPayementDetails({
                    noPaymentMessageI18Key: "i18n.billPay.noPaymentDueMessage"
                });
                scopeObj.view.tableView.flxBillPayAllPayees.setVisibility(false);
                scopeObj.view.flxConfirmButton.setVisibility(false);
                scopeObj.view.btnConfirm.setVisibility(false);
            } else {
                self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
                var billDueData = {
                    count: billsData.length,
                    totalDueAmount: 0
                };
                var preferredAccountNumber = self.presenter.getBillPayPreferedAccountNumber();
                var dataMap = {
                    "lblIdentifier": "lblIdentifier",
                    "imgDropdown": "imgDropdown",
                    "lblPayee": "lblPayee",
                    "lblPayeeDate": "lblPayeeDate",
                    "btnEbill": "btnEbill",
                    "lblDueDate": "lblDueDate",
                    "lblBill": "lblBill",
                    "txtAmount": "txtAmount",
                    "lblSeparator": "lblSeparator",
                    "lblPayFrom": "lblPayFrom",
                    "lblSendOn": "lblSendOn",
                    "lblDeliverBy": "lblDeliverBy",
                    "lstPayFrom": "lstPayFrom",
                    "imgListBoxDropdown": "imgListBoxDropdown",
                    "calSendOn": "calSendOn",
                    "calDeliverBy": "calDeliverBy",
                    "lblNotes": "lblNotes",
                    "lblCategory": "lblCategory",
                    "txtNotes": "txtNotes",
                    "lblCategoryList": "lblCategoryList",
                    "imgCategoryListDropdown": "imgCategoryListDropdown",
                    "btnViewEBill": "btnViewEBill",
                    "btnPayBill": "btnPayBill",
                    "lblfromAccountNumber": "lblfromAccountNumber",
                    "lblBillId": "lblBillId",
                    "lblPayeeId": "lblPayeeId",
                    "paymentDue": "paymentDue",
                    "lblDollar": "lblDollar",
                };
                var dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
                billsData = billsData.map(function(dataItem) {
                    billDueData.totalDueAmount += parseFloat(dataItem.dueAmount);
                    return {
                        "preferredAccountName": "",
                        "preferredAccountNumber": preferredAccountNumber,
                        "lblSeparatorBottom": {
                            "text": " "
                        },
                        "lblIdentifier": {
                            "text": " ",
                            "skin": "skin Name"
                        },
                        "imgDropdown": {
                            "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                            "accessibilityconfig": {
                                "a11yLabel": "View Transaction details"
                            }
                        },
                        "lblPayee":{
                            "text" : dataItem.payeeName,
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.payeeName
                            }
                        },
                        "lblPayeeDate": {
                            "text" : kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ": " + (dataItem.paidAmount ? ((scopeObj.formatAmount(dataItem.paidAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency))  + " " + kony.i18n.getLocalizedString("i18n.common.on") + " " + scopeObj.getDateFromDateString(dataItem.paidDate, "YYYY-MM-DDTHH:MM:SS"))) : ("No Payment Activity")),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ": " + (dataItem.paidAmount ? ((scopeObj.formatAmount(dataItem.paidAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) + " " + kony.i18n.getLocalizedString("i18n.common.on") + " " + scopeObj.getDateFromDateString(dataItem.paidDate, "YYYY-MM-DDTHH:MM:SS"))) : ("No Payment Activity")),
                            }
                        },
                        "btnEbill": {
                            "text": "SOME TEXT",
                            "skin": dataItem.ebillStatus == 1 ? ViewConstants.SKINS.SKNBTNEBILLACTIVE : ViewConstants.SKINS.SKNBTNIMGINACTIVEEBILL,
                            "onClick": dataItem.ebillStatus == 1 ? scopeObj.viewEBill.bind(scopeObj, {
                                "billGeneratedDate": scopeObj.getDateFromDateString(dataItem.billGeneratedDate, "YYYY-MM-DDTHH:MM:SS"),
                                "amount": scopeObj.formatAmount(dataItem.dueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)),
                                "ebillURL": dataItem.ebillURL
                            }) : null
                        },
                        "lblDueDate": {
                            "text" : scopeObj.getDateFromDateString(dataItem.billDueDate, "YYYY-MM-DDTHH:MM:SS"),
                            "accessibilityconfig" : {
                                "a11yLabel" : scopeObj.getDateFromDateString(dataItem.billDueDate, "YYYY-MM-DDTHH:MM:SS")
                            }
                        },
                        "lblBill":  {
                            "text" : dataItem.dueAmount ? scopeObj.formatAmount(dataItem.dueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) : dataItem.dueAmount,
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.dueAmount ?scopeObj.formatAmount(dataItem.dueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) : dataItem.dueAmount
                            }
                        },
                        "txtAmount": {
                            "placeholder": kony.i18n.getLocalizedString("i18n.common.EnterAmount"),
                            "text": scopeObj.formatAmount(dataItem.dueAmount, true),
                            "accessibilityconfig" : {
                                "a11yLabel" : scopeObj.formatAmount(dataItem.dueAmount, true)
                            }
                        },
                        "lblSeparator": "A",
                        "lblPayFrom": {
                            "text" : kony.i18n.getLocalizedString("i18n.billPay.PayFrom"),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.billPay.PayFrom")
                            }
                        },
                        "lblSendOn": {
                            "text" : kony.i18n.getLocalizedString("i18n.transfers.send_on"),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.transfers.send_on")
                            }
                        },
                        "lblDeliverBy": {
                            "text" : kony.i18n.getLocalizedString("i18n.billPay.DeliverBy"),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.billPay.DeliverBy")
                            }
                        },
                        "lstPayFrom": {
                            "masterData": FormControllerUtility.getListBoxDataFromObjects(billPayAccounts, "accountID", CommonUtilities.getAccountDisplayNameWithBalance),
                            "selectedKey": preferredAccountNumber ? preferredAccountNumber : billPayAccounts[0].accountID
                        },
                        "imgListBoxDropdown": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "calSendOn": {
                            "dateComponents": CommonUtilities.getServerDateComponent(),
                            "dateFormat": dateFormat,
                            "validStartDate": scopeObj.getvalidStartDate(),
                            "hidePreviousNextMonthDates": true
                        },
                        "calDeliverBy": {
                            "dateComponents": CommonUtilities.getServerDateComponent(),
                            "dateFormat": dateFormat,
                            "validStartDate": scopeObj.getvalidStartDate(),
                            "hidePreviousNextMonthDates": true
                        },
                        "lblNotes": {
                            "text" : kony.i18n.getLocalizedString("i18n.transfers.Description"),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.transfers.Description")
                            }
                        },
                        "lblCategory": {
                            "text" : kony.i18n.getLocalizedString("i18n.billPay.category"),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.billPay.category")
                            }
                        },
                        "txtNotes": {
                            "placeholder": kony.i18n.getLocalizedString("i18n.transfers.optional"),
                            "text": ""
                        },
                        "lblCategoryList": {
                            "text" : dataItem.billerCategoryName,
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.billerCategoryName
                            }
                        },
                        "imgCategoryListDropdown": ViewConstants.IMAGES.ARRAOW_DOWN,
                        "btnViewEBill": {
                            "text": kony.i18n.getLocalizedString("i18n.billPay.viewEBill"),
                            "onClick": dataItem.ebillStatus == 1 ? scopeObj.viewEBill.bind(scopeObj, {
                                "billGeneratedDate": scopeObj.getDateFromDateString(dataItem.billGeneratedDate, "YYYY-MM-DDTHH:MM:SS"),
                                "amount": scopeObj.formatAmount(dataItem.dueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)),
                                "ebillURL": dataItem.ebillURL
                            }) : null
                        },
                        "lblLineOne": "1",
                        "lblLineTwo": "2",
                        "template": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "flxBillPayAllPayeesMobile" : "flxBillPayAllPayees",
                        "lblBillId": {
                            "text" : dataItem.id,
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.id
                        }
                    },
                        "lblfromAccountNumber": {
                            "text" : dataItem.fromAccountNumber,
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.fromAccountNumber
                        }
                    },
                        "lblPayeeId": {
                            "text" : dataItem.payeeid,
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.payeeid,
                        }
                    },
                        "lblDollar": applicationManager.getConfigurationManager().getCurrencyCode(),
                        "paymentDue": {
                            "text" : kony.i18n.getLocalizedString("i18n.billPay.PaymentDue"),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.billPay.PaymentDue")
                        }
                    },
                    };
                });
                if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
                    var segmentData = billsData;
                    for (var i = 0; i < billsData.length; i++) {
                        var e = billsData[i];
                        segmentData[i].btnPayBill = {
                            "text": kony.i18n.getLocalizedString("i18n.Pay.PayBill"),
                            "onClick": scopeObj.onClickPayDueBill.bind(this, e)
                        }
                    }
                }
                scopeObj.view.btnConfirm.onClick = scopeObj.validateBulkPayData.bind(scopeObj);
                scopeObj.view.tableView.segmentBillpay.widgetDataMap = dataMap;
                scopeObj.view.tableView.segmentBillpay.setData(billsData);
                scopeObj.bindTotalEbillAmountDue(billDueData);
            }
            FormControllerUtility.hideProgressBar(this.view);
            scopeObj.view.forceLayout();
        },
        /**
        * onClickPayDueBill:    on Click action of btnPayBill
        * @param {object} payeeData details of the payee
        * */
        onClickPayDueBill : function (payeeData) {
            function getDateFromDateString(dateStr) {
                if (dateStr) {
                    return CommonUtilities.getFrontendDateString(dateStr);
                }
                else {
                    return "";
                }
            }
            var payBillData = {
                "payeeNickname": payeeData.lblPayee||payeeData.payeeName,
                "lastPaidAmount": payeeData.lastPaidAmount,
                "lastPaidDate": getDateFromDateString(payeeData.lastPaidDate),
                "dueAmount": CommonUtilities.formatCurrencyWithCommas(payeeData.lblBill),
                "billDueDate": getDateFromDateString(payeeData.lblDueDate),
                "eBillSupport": payeeData.eBillSupport,
                "eBillStatus": payeeData.eillStatus,
                "billid": payeeData.lblBillId,
                "payeeId": payeeData.payeeId,
                "accountNumber": payeeData.accountNumber,
                "billGeneratedDate": getDateFromDateString(payeeData.billGeneratedDate),
                "ebillURL": payeeData.ebillURL
            };
            this.setDataForPayABill(payBillData);
        },
        /**
        * setPaymentDueUI:   Method to set UI for Bill due Payments Tab, on Navigate to due payments tab UI
        */
        setPaymentDueUI: function () {
            this.selectedTab = "paymentDue";
            this.setBottomDefaultValues();
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
            }, {
                text: kony.i18n.getLocalizedString("i18n.billPay.PaymentDue")
            }]);
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.PaymentDue");
            this.view.tableView.lblPayee.setFocus(true);
            this.view.tableView.flxBillPayScheduled.setVisibility(false);
            this.view.tableView.tableSubHeaderHistory.setVisibility(false);
            this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
            CommonUtilities.setText(this.view.tableView.lblDueDate, kony.i18n.getLocalizedString("i18n.billPay.DueDate") , CommonUtilities.getaccessibilityConfig());
            this.setSkinInActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
            this.setSkinActive(this.view.tableView.tableTabs.btnTransfers);
            this.setSkinInActive(this.view.tableView.tableTabs.btnDeposits);
            this.setSkinInActive(this.view.tableView.tableTabs.btnChecks);
            this.setSkinInActive(this.view.tableView.tableTabs.btnWithdraws);
            this.view.tableView.flxNoPayment.setVisibility(false);
            this.view.tableView.segmentBillpay.setVisibility(true);
            this.view.tableView.imgSortDatee.setVisibility(false);
            if(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile){
                this.view.tableView.flxBillPayAllPayees.setVisibility(false);
                this.view.flxConfirmButton.setVisibility(false);
            }else{
                this.view.tableView.flxBillPayAllPayees.setVisibility(true);
                this.view.flxConfirmButton.setVisibility(true);
            }
            this.view.btnConfirm.setVisibility(true);
            this.setSearchFlexVisibility(false);
            this.view.tableView.Search.setVisibility(false);
            this.setServerError(false);
            this.setPagination({ show: false });
            FormControllerUtility.setSortingHandlers(this.paymentDueSortMap, this.onPaymentDueSortClickHandler, this);
        },
        /**
         * used to set inActive Skin For AllPayees
         * @param {object} obj skin
         */
        setSkinInActiveAllPayees: function (obj) {
            obj.skin = ViewConstants.SKINS.SKNBTNACCOUNTSUMMARYUNSELECTEDMODIFIED;
        },
        /**
         * used to show the dueBills count and totalDueBills Amount
         * @param {object} dueBills contains the no of bills and toatalBillamount
         */
        bindTotalEbillAmountDue: function (dueBills) {
            var scopeObj = this;
            if (dueBills && dueBills.count === 0) {
                this.view.flxTotalEbillAmountDue.setVisibility(false);
            }
            else {
                this.view.flxTotalEbillAmountDue.setVisibility(true);
                CommonUtilities.setText(scopeObj.view.lblBills, dueBills.count + " " + kony.i18n.getLocalizedString("i18n.billPay.eBills") , CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(scopeObj.view.lblEbillAmountDueValue, scopeObj.formatAmount(String(dueBills.totalDueAmount)) , CommonUtilities.getaccessibilityConfig());
            }
        },
        /**
         * setPaymentDueSegmentData:    used to load  due bills.
       */
        setPaymentDueSegmentData: function () {
            this.loadPaymentDueBills();
        },
        /**
        * used to fetch due bills.
        */
        loadPaymentDueBills: function () {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(this.view);
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.presenter.showBillPayData(null, "DueBills", true);
        },
        /**
        * setScheduledFlex:    method used to set scheduled tab activities
        */
        setScheduledFlex: function () {
            this.selectedTab = "scheduled";
            var scopeObj = this;
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            FormControllerUtility.showProgressBar(this.view);
            scopeObj.presenter.showBillPayData(null, "ScheduleBills", true);
        },
        /**
         * used to bind schedule Bills
         * @param {object} scheduledBills scheduled bills
         */
     bindScheduleBillsSegment: function(scheduledBills) {
            var scopeObj = this;
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.setScheduleUI();
            if (scheduledBills.length === 0) {
                scopeObj.showNoPayementDetails({
                    noPaymentMessageI18Key: "i18n.billPay.noPaymentScheduleMessage",
                    showActionMessageI18Key: "i18n.billPay.ScheduleAPayment"
                });
                scopeObj.view.tableView.flxBillPayScheduled.setVisibility(false);
            } else {
                var dataMap = {
                    "lblIdentifier": "lblIdentifier",
                    "lblSeperatorone": "lblSeperatorone",
                    "lblSeparator": "lblSeparator",
                    "imgDropdown": "imgDropdown",
                    "lblDate": "lblDate",
                    "lblPayeeName": "lblPayeeName",
                    "lblBillDueAmount": "lblBillDueAmount",
                    "lblPaidAmount": "lblPaidAmount",
                    "btnEdit": "btnEdit",
                    "lblRefrenceNumber": "lblRefrenceNumber",
                    "lblRefrenceNumberValue": "lblRefrenceNumberValue",
                    "lblSentFrom": "lblSentFrom",
                    "lblSentFromValue": "lblSentFromValue",
                    "lblNotes": "lblNotes",
                    "lblNotesValue": "lblNotesValue",
                    "btnCancel": "btnCancel",
                    "btnEbill": "btnEbill",
                    "flxRecurrenceNumber": "flxRecurrenceNumber",
                    "lblRecurrenceNo": "lblRecurrenceNo",
                    "lblRecurrenceNoValue": "lblRecurrenceNoValue",
                    "flxFrequency": "flxFrequency",
                    "lblFrequencyTitle": "lblFrequencyTitle",
                    "lblFrequencyValue": "lblFrequencyValue",
                    "flxCancelSeries": "flxCancelSeries",
                    "btnCancelSeries": "btnCancelSeries"
                };
                scheduledBills = scheduledBills.map(function(dataItem) {
                    dataItem.payeeName = dataItem.payeeNickName || dataItem.payeeName;
                    dataItem.paidAmount = dataItem.amount;
                    dataItem.notes = dataItem.transactionsNotes || "";
                    dataItem.referenceNumber = dataItem.referenceId;
                    dataItem.lastPaidAmount = dataItem.billPaidAmount;
                    dataItem.lastPaidDate = dataItem.billPaidDate;
                    dataItem.eBillStatus = dataItem.eBillEnable;
                    dataItem.billDueDate = dataItem.billDueDate;
                    dataItem.dueAmount = scopeObj.formatAmount(dataItem.billDueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency));
                    dataItem.payeeNickname = dataItem.payeeName;
                    dataItem.sendOn = dataItem.scheduledDate;
                    dataItem.isScheduleEditFlow = true;
                    var dataObject = {
                        "lblSeparatorBottom": {
                            "text": " "
                        },
                        "lblIdentifier": {
                            "text": " ",
                            "skin": "skin Name"
                        },
                        "imgDropdown": {
                            "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                            "accessibilityconfig": {
                                "a11yLabel": "View Transaction details"
                            }
                        },
                        "lblDate": {
                            "text" : scopeObj.getDateFromDateString(dataItem.scheduledDate, "YYYY-MM-DDTHH:MM:SS"),
                            "accessibilityconfig" : {
                                "a11yLabel" : scopeObj.getDateFromDateString(dataItem.scheduledDate, "YYYY-MM-DDTHH:MM:SS")
                            }
                        },
                        "lblPayeeName": {
                            "text" : dataItem.payeeName,
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.payeeName
                            }
                        },
                        "lblBillDueAmount": {
                            "text" : scopeObj.formatAmount(dataItem.billDueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)),
                            "accessibilityconfig" : {
                                "a11yLabel" : scopeObj.formatAmount(dataItem.billDueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency))
                            }
                        },
                        "lblPaidAmount": {
                            "text" : scopeObj.formatAmount(dataItem.paidAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)),
                            "accessibilityconfig" : {
                                "a11yLabel" : scopeObj.formatAmount(dataItem.paidAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency))
                            }
                        },
                        "btnEdit": {
                            "text": kony.i18n.getLocalizedString("i18n.billPay.Edit"),
                            "onClick": scopeObj.presenter.showBillPayData.bind(scopeObj.presenter, null, "PayABill", true, dataItem)
                        },
                        "lblSeparator": "A",
                        "lblSeperatorone": "A",
                        "lblRefrenceNumber":  {
                            "text" : kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber"),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber")
                            }
                        },
                        "lblRefrenceNumberValue": {
                            "text" : dataItem.referenceNumber,
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.referenceNumber
                            }
                        },
                        "lblSentFrom": kony.i18n.getLocalizedString("i18n.billPay.sentFrom"),
                        "lblSentFromValue":{
                            "text" :  CommonUtilities.getAccountDisplayName({
                                name: dataItem.fromAccountName,
                                accountID: dataItem.fromAccountNumber,
                                nickName: dataItem.fromNickName,
                                Account_id : dataItem.fromAccountNumber
                            }),
                            "accessibilityconfig" : {
                                "a11yLabel" :  CommonUtilities.getAccountDisplayName({
                                    name: dataItem.fromAccountName,
                                    accountID: dataItem.fromAccountNumber,
                                    nickName: dataItem.fromNickName,
                                    Account_id : dataItem.fromAccountNumber
                                })
                            }
                        },
                        "lblNotes": {
                            "text" :  kony.i18n.getLocalizedString("i18n.transfers.Description"),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.transfers.Description")
                            }
                        },
                        "lblNotesValue": {
                            "text" :  dataItem.notes ? dataItem.notes : kony.i18n.getLocalizedString("i18n.common.none"),
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.notes ? dataItem.notes : kony.i18n.getLocalizedString("i18n.common.none")
                            }
                        },
                        "lblFrequencyTitle":{
                            "text" :   kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
                            "accessibilityconfig" : {
                                "a11yLabel" :  kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
                            }
                        },
                        "lblFrequencyValue":{
                            "text" :    dataItem.frequencyType ? dataItem.frequencyType : kony.i18n.getLocalizedString("i18n.common.none"),
                            "accessibilityconfig" : {
                                "a11yLabel" :   dataItem.frequencyType ? dataItem.frequencyType : kony.i18n.getLocalizedString("i18n.common.none")
                            }
                        },
                        "flxRecurrenceNumber": {
                            "isVisible": dataItem.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false : true
                        },
                        "lblRecurrenceNo": {
                            "text" :  kony.i18n.getLocalizedString("i18n.accounts.recurrence"),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.accounts.recurrence"),
                            }
                        },
                        "lblRecurrenceNoValue": {
                            "text" : dataItem.recurrenceDesc ? dataItem.recurrenceDesc : "-",
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.recurrenceDesc ? dataItem.recurrenceDesc : "-"
                            }
                        },
                        "btnCancel": {
                            "text": dataItem.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? kony.i18n.getLocalizedString("i18n.transfers.Cancel") : kony.i18n.getLocalizedString("i18n.common.cancelThisOccurrence"),
                            "toolTip": dataItem.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? kony.i18n.getLocalizedString("i18n.transfers.Cancel") : kony.i18n.getLocalizedString("i18n.common.cancelThisOccurrence"),
                            "onClick": CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : dataItem.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? scopeObj.onScheduledCancelBtnClick.bind(scopeObj, dataItem) : scopeObj.onCancelOccurrence.bind(scopeObj, dataItem)
                        },
                        "btnCancelSeries": {
                            "isVisible": dataItem.frequencyType === OLBConstants.TRANSACTION_RECURRENCE.ONCE ? false : true,
                            "text": kony.i18n.getLocalizedString("i18n.common.cancelSeries"),
                            "toolTip": kony.i18n.getLocalizedString("i18n.common.cancelSeries"),
                            "onClick": CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : scopeObj.onScheduledCancelBtnClick.bind(scopeObj, dataItem)
                        },
                        "btnEbill": {
                            "text": dataItem.eBillStatus == 1 ? kony.i18n.getLocalizedString("i18n.billPay.viewEBill") : '',
                            "onClick": dataItem.eBillStatus == 1 ? scopeObj.viewEBill.bind(scopeObj, {
                                "billGeneratedDate": scopeObj.getDateFromDateString(dataItem.billGeneratedDate, "YYYY-MM-DDTHH:MM:SS"),
                                "amount": scopeObj.formatAmount(dataItem.billDueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)),
                                "ebillURL": dataItem.ebillURL
                            }) : null
                        },
                        "flxDropdown": {
                            "onClick": scopeObj.onScheduledSegmentRowClick
                        },
                        "template": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "flxBillPayScheduledMobile" : "flxBillPayScheduled"
                    };
                    if (CommonUtilities.isCSRMode()) {
                        dataObject.btnCancel.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(15);
                    }
                    return dataObject;
                });
                scopeObj.view.tableView.segmentBillpay.widgetDataMap = dataMap;
                scopeObj.view.tableView.segmentBillpay.setData(scheduledBills);
                // scopeObj.view.tableView.segmentBillpay.onClick = scopeObj.onScheduledSegmentRowClick;
            }
            scopeObj.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * used to set the scheduled UI
         */
        setScheduleUI: function () {
            var scopeObj = this;
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
            }, {
                text: kony.i18n.getLocalizedString("i18n.billPay.scheduled")
            }]);
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.scheduled");
            this.setSkinInActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
            this.setSkinInActive(this.view.tableView.tableTabs.btnTransfers);
            this.setSkinActive(this.view.tableView.tableTabs.btnDeposits);
            this.setSkinInActive(this.view.tableView.tableTabs.btnChecks);
            this.setSkinInActive(this.view.tableView.tableTabs.btnWithdraws);
            if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
              this.view.tableView.flxBillPayScheduled.setVisibility(false);
            }
            else{
              this.view.tableView.flxBillPayScheduled.setVisibility(true);
            }
            this.view.tableView.CopylblPayee0bfdbf2c8c16a4d.setFocus(true);
            this.view.tableView.flxBillPayAllPayees.setVisibility(false);
            this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
            this.view.tableView.tableSubHeaderHistory.setVisibility(false);
            CommonUtilities.setText(this.view.tableView.lblDueDate, kony.i18n.getLocalizedString("i18n.billpay.scheduledDate") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.tableView.lblAmount, kony.i18n.getLocalizedString("i18n.transfers.PaymentAmount") , CommonUtilities.getaccessibilityConfig());
            this.setBottomDefaultValues();
            scopeObj.view.tableView.flxNoPayment.setVisibility(false);
            scopeObj.view.tableView.segmentBillpay.setVisibility(true);
            scopeObj.setPagination({ show: false });
            this.setSearchFlexVisibility(false);
            this.view.tableView.Search.setVisibility(false);
        },
        /**
        *method used to show  bill history activities.
        */
        setDataForHistory: function () {
            var scopeObj = this;
            scopeObj.offset = 0;
            FormControllerUtility.showProgressBar(this.view);
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.presenter.showBillPayData(null, "History", true);
        },
        /**
         * setHistoryUI:  Method to set UI for History Tab, on Navigate to history UI
        */
      setHistoryUI: function () {
        this.selectedTab = "history";
        this.view.breadcrumb.setBreadcrumbData([{
          text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
        }, {
          text: kony.i18n.getLocalizedString("i18n.billPay.History")
        }]);
        this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
        this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.History");
        this.view.tableView.isVisible = true;
        this.view.tableView.flxBillPayScheduled.setVisibility(false);
        this.view.flxBillPayManagePayeeActivitymodified.setVisibility(false);
        this.view.payABill.setVisibility(false);
        this.view.flxOneTImePayment.setVisibility(false);
        this.view.flxActivatebillpays.setVisibility(false);
        this.view.AllForms.setVisibility(false);
        this.view.flxConfirmButton.setVisibility(false);
        this.view.btnConfirm.setVisibility(false);
        if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
          this.view.tableView.tableSubHeaderHistory.setVisibility(false);
        } else {
          this.view.tableView.tableSubHeaderHistory.setVisibility(true);
        }
        this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
        this.view.tableView.Search.setVisibility(true);
        this.view.flxBillHeaderMobile.isVisible =false;
        this.view.forceLayout();
      },
      /**
        * method to bind  billPay history data.
        * @param {object} data list of history records.
        */
     binduserBillPayHistory: function(data) {
            var scopeObj = this;
            scopeObj.showHistoryUI();
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            if (data.noMoreRecords) {
                this.view.imgPaginationNext.src = "pagination_next_inactive.png";
                this.view.flxPaginationNext.onClick = null;
                kony.ui.Alert(kony.i18n.getLocalizedString("i18n.navigation.norecordsfound"));
                return;
            }
            if (data.noHistory === true) {
                scopeObj.showNoPayementDetails({
                    noPaymentMessageI18Key: "i18n.billpay.noTransactionHistory"
                });
                scopeObj.view.tableView.tableSubHeaderHistory.setVisibility(false);
            } else {
                var dataMap = {
                    "lblIdentifier": "lblIdentifier",
                    "imgDropdown": "imgDropdown",
                    "lblDate": "lblDate",
                    "lblSendTo": "lblSendTo",
                    "lblSortAmount": "lblSortAmount",
                    "lblSortBalance": "lblSortBalance",
                    "btnRepeat": "btnRepeat",
                    "lblRefrenceNumber": "lblRefrenceNumber",
                    "lblRefrenceNumberValue": "lblRefrenceNumberValue",
                    "lblSentFrom": "lblSentFrom",
                    "lblSentFromValue": "lblSentFromValue",
                    "lblNotes": "lblNotes",
                    "lblNotesValue": "lblNotesValue",
                    "btnEdit": "btnEdit",
                    "lblSeparator": "lblSeparator",
                    "lblSeperatorone": "lblSeperatorone"
                }
                var userbillPayHistory = data.billpayHistory.map(function(dataItem) {
                    dataItem.lastPaidDate = dataItem.transactionDate;
                    dataItem.RefrenceNumber = dataItem.referenceId;
                    dataItem.SentFrom = dataItem.fromAccountName;
                    dataItem.lastPaidAmount = scopeObj.formatAmount(dataItem.amount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency));
                    dataItem.Status = dataItem.statusDescription;
                    dataItem.Notes = dataItem.transactionsNotes || '';
                    dataItem.eBillStatus = dataItem.eBillEnable;
                    dataItem.billGeneratedDate = dataItem.billGeneratedDate;
                    dataItem.billDueDate = dataItem.billDueDate;
                    dataItem.dueAmount = scopeObj.formatAmount(dataItem.billDueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency));
                    dataItem.payeeNickname = dataItem.payeeNickName;
                    return {
                        "lblIdentifier": "a",
                        "imgDropdown": {
                            "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                            "accessibilityconfig": {
                                "a11yLabel": "View Transaction details"
                            }
                        },
                        "lblSeparator": "A",
                        "lblSeperatorone": "A",
                        "lblDate": {
                            "text" : scopeObj.getDateFromDateString(dataItem.transactionDate, "YYYY-MM-DDTHH:MM:SS"),
                            "accessibilityconfig" : {
                                "a11yLabel" : scopeObj.getDateFromDateString(dataItem.transactionDate, "YYYY-MM-DDTHH:MM:SS")
                            }
                        },
                        "lblSendTo": {
                            "text" : dataItem.payeeNickname,
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.payeeNickname
                            }
                        },
                        "lblSortAmount": {
                            "text" : dataItem.lastPaidAmount,
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.lastPaidAmount
                            }
                        },
                        "lblSortBalance": {
                            "text" : dataItem.Status,
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.Status
                            }
                        },
                        "btnRepeat": {
                            "text": kony.i18n.getLocalizedString("i18n.accounts.repeat"),
                            "onClick": scopeObj.presenter.showBillPayData.bind(scopeObj.presenter, null, "PayABill", true, dataItem)
                        },
                        "lblRefrenceNumber": {
                            "text" : kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber"),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber")
                            }
                        },
                        "lblRefrenceNumberValue": {
                            "text" : dataItem.RefrenceNumber,
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.RefrenceNumber
                            }
                        },
                        "lblSentFrom": {
                            "text" : kony.i18n.getLocalizedString("i18n.billPay.sentFrom"),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.billPay.sentFrom")
                            }
                        },
                        "lblSentFromValue": {
                            "text" :  CommonUtilities.getAccountDisplayName({
                                name: dataItem.fromAccountName,
                                accountID: dataItem.fromAccountNumber,
                                nickName: dataItem.fromNickName,
                                Account_id : dataItem.fromAccountNumber
                            }),
                            "accessibilityconfig" : {
                                "a11yLabel" :  CommonUtilities.getAccountDisplayName({
                                    name: dataItem.fromAccountName,
                                    accountID: dataItem.fromAccountNumber,
                                    nickName: dataItem.fromNickName,
                                    Account_id : dataItem.fromAccountNumber
                                }),
                            }
                        },
                        "lblNotes": {
                            "text" : kony.i18n.getLocalizedString("i18n.accounts.Note"),
                            "accessibilityconfig" : {
                                "a11yLabel" : kony.i18n.getLocalizedString("i18n.accounts.Note")
                            }
                        },
                        "lblNotesValue": {
                            "text" : dataItem.Notes ? dataItem.Notes : kony.i18n.getLocalizedString("i18n.common.none"),
                            "accessibilityconfig" : {
                                "a11yLabel" : dataItem.Notes ? dataItem.Notes : kony.i18n.getLocalizedString("i18n.common.none")
                            }
                        },
                        "btnEdit": {
                            "text": "Download Report",
                            "onClick": function(){
											var index = scopeObj.view.tableView.segmentBillpay.selectedRowIndex[1];
											var data = scopeObj.view.tableView.segmentBillpay.data;
											var selectedData = data[index];
											var transactionsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Transactions");
											var transactionObject = new transactionsModel({
											"transactionId": selectedData.lblRefrenceNumberValue.text
											});
											var billpayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
											billpayModule.presentationController.downloadTransactionReport(transactionObject);
									}
                        },
                        "template": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "flxBillPayHistoryMobile" : "flxMain"
                    }
                });
                this.view.tableView.segmentBillpay.widgetDataMap = dataMap;
                CommonUtilities.setText(this.view.lblPagination, (data.noOfRecords.offset + 1) + " - " + (data.noOfRecords.offset + data.noOfRecords.limit) + " " + kony.i18n.getLocalizedString("i18n.common.transactions"), CommonUtilities.getaccessibilityConfig());
                this.view.tableView.segmentBillpay.setData(userbillPayHistory);
                this.view.tableView.segmentBillpay.setVisibility(true);
                if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
                    this.view.tableView.tableSubHeaderHistory.setVisibility(false);
                } else {
                    this.view.tableView.tableSubHeaderHistory.setVisibility(true);
                }
                this.setPagination({
                    'show': true,
                    'offset': data.noOfRecords.offset,
                    'limit': data.noOfRecords.limit,
                    'recordsLength': data.billpayHistory.length,
                    'text': kony.i18n.getLocalizedString("i18n.common.transactions")
                }, this.prevHistory, this.nextHistory);
            }
            FormControllerUtility.hideProgressBar(this.view);
            scopeObj.view.forceLayout();
        },
        /**
        * method to set billPay history activities.
        */
        showHistoryUI: function () {
            var scopeObj = this;
            this.view.tableView.isVisible = true;
            this.view.payABill.setVisibility(false);
            this.view.flxOneTImePayment.setVisibility(false);
            scopeObj.setServerError(false);
            this.setBottomforManagePayees();
            this.selectedTab = "history";
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
            }, {
                text: kony.i18n.getLocalizedString("i18n.billPay.History")
            }]);
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.History");
            this.view.tableView.flxBillPayAllPayees.setVisibility(false);
            if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
          this.view.tableView.tableSubHeaderHistory.setVisibility(false);
        } else {
          this.view.tableView.tableSubHeaderHistory.setVisibility(true);
        }
            this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
            this.view.tableView.flxNoPayment.setVisibility(false);
            this.setSkinInActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
            this.setSkinInActive(this.view.tableView.tableTabs.btnTransfers);
            this.setSkinInActive(this.view.tableView.tableTabs.btnDeposits);
            this.setSkinActive(this.view.tableView.tableTabs.btnChecks);
            this.setSkinInActive(this.view.tableView.tableTabs.btnWithdraws);
            this.view.tableView.flxBillPayScheduled.setVisibility(false);
            this.view.tableView.Search.setVisibility(false);
        },
        /**
         * used to set manage payee ui
         */
        setBottomforManagePayees: function () {
            this.view.flxPagination.setVisibility(true);
            // this.view.flxBottom.height = "190dp";
            this.view.flxTermsAndConditions.top = "30dp";
            this.view.flxConfirmButton.setVisibility(false);
            this.view.btnConfirm.setVisibility(false);
            this.view.flxBottom.forceLayout();
        },
        /**
            Method to set UI for Manage Payee Tab, on Navigate to managePayees tab UI
        */
        setManagePayeeUI: function () {
            var configurationManager = applicationManager.getConfigurationManager();
            if(configurationManager.isFastTransferEnabled == "false")
            this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU;
            else
              {
            this.view.customheader.topmenu.flxMyBills.skin = "sknFlxFFFFFbrdr3343a8Pointer";
                this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknHoverTopmenu7f7f7pointer";
              }
            this.setBottomforManagePayees();
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            this.selectedTab = "managePayees";
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
            }, {
                text: kony.i18n.getLocalizedString("i18n.billPay.ManagePayee")
            }]);
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.ManagePayee");
            this.view.tableView.setVisibility(true);
            this.view.flxBillPay.setVisibility(true);
            this.view.flxBillPayManagePayeeActivitymodified.setVisibility(false);
            this.view.transferActivitymodified.setVisibility(false);
            this.view.tableView.flxBillPayScheduled.setVisibility(false);
            this.view.payABill.setVisibility(false);
            this.view.flxPagination.setVisibility(true);
            this.view.flxOneTImePayment.setVisibility(false);
            this.view.flxActivatebillpays.setVisibility(false);
            this.view.AllForms.setVisibility(false);
            this.view.tableView.Search.setVisibility(true);
            this.view.flxConfirmButton.setVisibility(false);
            this.view.btnConfirm.setVisibility(false);
           this.view.flxBillHeaderMobile.isVisible =false;
            this.view.forceLayout();
        },
        /**
         * used to bind the manage payees data
         * @param {object} data data
         * @param {object}  noofRecords no of records
         * @param {boolean} searchvisibility search visibulity
         */
        bindManagePayeeData: function (data, noofRecords, searchvisibility) {
            this.setManagePayeesSegmentData({ "managePayee": data.managePayee ? data.managePayee : data, "noofRecords": data.noOfRecords, "searchView": searchvisibility });
            FormControllerUtility.updateSortFlex(this.managePayeeSortMap, data.noOfRecords);
        },
        /**
         *  Method to set data for Manage Payee Segment
         * @param {object}  data list of payees
         */
       setManagePayeesSegmentData: function(data) {
            var self = this;
            this.setBottomforManagePayees();
            var scopeObj = this;
            this.view.tableView.segmentBillpay.setVisibility(true);
            this.view.tableView.flxNoPayment.setVisibility(false);
            this.view.tableView.flxBillPayAllPayees.setVisibility(false);
            if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
                this.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
            } else {
                this.view.tableView.FlxBillpayeeManagePayees.setVisibility(true);
            }
            this.view.tableView.lblBillerName.setFocus(true);
            this.view.tableView.tableSubHeaderHistory.setVisibility(false);
            this.setSkinInActiveAllPayees(this.view.tableView.tableTabs.BtnAll);
            this.setSkinInActive(this.view.tableView.tableTabs.btnTransfers);
            this.setSkinInActive(this.view.tableView.tableTabs.btnDeposits);
            this.setSkinInActive(this.view.tableView.tableTabs.btnChecks);
            this.setSkinActive(this.view.tableView.tableTabs.btnWithdraws);
            if (data.managePayee.noMoreRecords) {
                this.view.imgPaginationNext.src = "pagination_next_inactive.png";
                this.view.flxPaginationNext.onClick = null;
                kony.ui.Alert(kony.i18n.getLocalizedString("i18n.navigation.norecordsfound"));
                return;
            }
            if (data.managePayee.noHistory === true || data.managePayee.length === 0) {
                if (data.managePayee.noHistory === true) {
                    scopeObj.showNoPayementDetails({
                        noPaymentMessageI18Key: "i18n.billPay.noPayeesMessage"
                    });
                    scopeObj.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
                }
                if (data.searchView && data.managePayee.length === 0) {
                    scopeObj.showNoPayementDetails({
                        noPaymentMessageI18Key: "i18n.PayAPerson.NoRecords"
                    });
                }
            } else {
                if (data.noofRecords) {
                    scopeObj.setPagination({
                        'show': true,
                        'offset': data.noofRecords.offset,
                        'limit': data.noofRecords.limit,
                        'recordsLength': data.managePayee.length,
                        'text': kony.i18n.getLocalizedString("i18n.billpay.payees")
                    }, scopeObj.prevManagePayees, scopeObj.nextManagePayees);
                }
                var searchView = data.searchView;
                data = data.managePayee;
                var dataMap = {
                    "lblIdentifier": "lblIdentifier",
                    "imgDropdown": "imgDropdown",
                    "lblPayee": "lblPayee",
                    "btnEbill": "btnEbill",
                    "lblLastPayment": "lblLastPayment",
                    "lblLastPaymentDate": "lblLastPaymentDate",
                    "lblNextBill": "lblNextBill",
                    "lblNextBillDate": "lblNextBillDate",
                    "btnPayBill": "btnPayBill",
                    "lblSeparator": "lblSeparator",
                    "lblAccountNumberTitle": "lblAccountNumberTitle",
                    "lblAccountNumberValue": "lblAccountNumberValue",
                    "lblBankAddressOne": "lblBankAddressOne",
                    "lblBankAddressTwo": "lblBankAddressTwo",
                    "btnViewActivity": "btnViewActivity",
                    "btnViewEbill": "btnViewEbill",
                    "btnEditBiller": "btnEditBiller",
                    "btnDeleteBiller": "btnDeleteBiller",
                    "lblSeparatorBottom": "lblSeparatorBottom",
                    "lblError": "lblError",
                    "txtPayee": "txtPayee",
                    "txtBankName": "txtBankName",
                    "txtAddress": "txtAddress",
                    "txtCity": "txtCity",
                    "tbxState": "tbxState",
                    "tbxPinCode": "tbxPinCode",
                    "btnSave": "btnSave",
                    "btnCancel": "btnCancel",
                    "lblBillerName": "lblBillerName",
                    "lblBillerAddressTitle": "lblBillerAddressTitle",
                    "lblLastPaymentTitle": "lblLastPaymentTitle",
                    "lblPaymentDateTitle": "lblPaymentDateTitle",
                    "lblNextBillAmountTitle": "lblNextBillAmountTitle",
                    "lblDueDateTitle": "lblDueDateTitle"
                };
                if (data.length > 0) {
                    var managePayees = data.map(function(dataItem) {
                        var managePayee = {
                            "payeeId": {
                                "text" : dataItem.payeeId,
                                "accessibilityconfig" : {
                                    "a11yLabel" : dataItem.payeeId
                                }
                            },
                            "lblSeparatorTwo": {
                                "text": ""
                            },
                            "lblSeparatorBottom": {
                                "text": " "
                            },
                            "lblIdentifier": {
                                "text": " ",
                                "skin": "skin Name"
                            },
                            "imgDropdown": {
                                "src": ViewConstants.IMAGES.ARRAOW_DOWN,
                                "accessibilityconfig": {
                                    "a11yLabel": "View Transaction details"
                                }
                            },
                            "lblPayee": {
                                "text" : dataItem.payeeNickName ? dataItem.payeeNickName : '',
                                "accessibilityconfig" : {
                                    "a11yLabel" : dataItem.payeeNickName ? dataItem.payeeNickName : ''
                                }
                            },
                            "lblSeparator": {
                                "text": " "
                            },
                            "lblAccountNumberTitle": {
                                "text" : kony.i18n.getLocalizedString("i18n.ProfileManagement.AccountNumber"),
                                "accessibilityconfig" : {
                                    "a11yLabel" : kony.i18n.getLocalizedString("i18n.ProfileManagement.AccountNumber")
                                }
                            },
                            "lblAccountNumberValue": {
                                "text" : dataItem.accountNumber ? dataItem.accountNumber : '',
                                "accessibilityconfig" : {
                                    "a11yLabel" : dataItem.accountNumber ? dataItem.accountNumber : ''
                                }
                            },
                            "lblBankAddressOne": {
                                "text" : dataItem.payeeName ? dataItem.payeeName : '',
                                "accessibilityconfig" : {
                                    "a11yLabel" : dataItem.payeeName ? dataItem.payeeName : ''
                                }
                            },
                            "lblBankAddressTwo": {
                                "text" : dataItem.addressLine1 ? dataItem.addressLine1 : '',
                                "accessibilityconfig" : {
                                    "a11yLabel" :dataItem.addressLine1 ? dataItem.addressLine1 : ''
                                }
                            },
                            "btnViewActivity": {
                                "text": kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
                                "toolTip": kony.i18n.getLocalizedString("i18n.transfers.viewActivity"),
                                "onClick": scopeObj.viewBillPayActivity.bind(scopeObj, dataItem)
                            },
                            "btnEditBiller": {
                                "text": kony.i18n.getLocalizedString("i18n.billPay.editBiller"),
                                "toolTip": kony.i18n.getLocalizedString("i18n.billPay.editBiller"),
                                "onClick": function() {
                                    scopeObj.changeToEditBiller();
                                }
                            },
                            "btnDeleteBiller": {
                                "text": kony.i18n.getLocalizedString("i18n.billPay.deleteBiller"),
                                "toolTip": kony.i18n.getLocalizedString("i18n.billPay.deleteBiller"),
                                "onClick": CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : function() {
                                    scopeObj.editDeleteOnClick(self.offset);
                                }
                            },
                            "template": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "flxBillPayManagePayeesMobile" : "flxBillPayManagePayees",
                            "txtPayee": {
                                "text" : dataItem.payeeNickName,
                                "accessibilityconfig" : {
                                    "a11yLabel" : dataItem.payeeNickName
                                }
                            },
                            "lblBillerName": {
                                "text" : dataItem.payeeName,
                                "accessibilityconfig" : {
                                    "a11yLabel" : dataItem.payeeName
                                }
                            },
                            "lblError": " ",
                            "txtBankName": {
                                "text": dataItem.addressLine1 ? dataItem.addressLine1 : '',
                                "placeholder": kony.i18n.getLocalizedString("i18n.transfers.bankName")
                            },
                            "txtAddress": {
                                "text": dataItem.addressLine2 ? dataItem.addressLine2 : '',
                                "placeholder": kony.i18n.getLocalizedString("i18n.ProfileManagement.Address")
                            },
                            "txtCity": {
                                "text": dataItem.cityName ? dataItem.cityName : '',
                                "placeholder": kony.i18n.getLocalizedString("i18n.common.city")
                            },
                            "tbxState": {
                                "text": dataItem.state ? dataItem.state : '',
                                "placeholder": kony.i18n.getLocalizedString("i18n.common.state")
                            },
                            "tbxPinCode": {
                                "text": dataItem.zipCode ? dataItem.zipCode : ' ',
                                "placeholder": kony.i18n.getLocalizedString("i18n.common.zipcode")
                            },
                            "btnSave": {
                                "text": kony.i18n.getLocalizedString("i18n.ProfileManagement.Save"),
                                "toolTip": kony.i18n.getLocalizedString("i18n.ProfileManagement.Save"),
                                "onClick": CommonUtilities.isCSRMode() ? CommonUtilities.disableButtonActionForCSRMode() : function() {
                                    scopeObj.editSaveOnClick(self.offset);
                                }
                            },
                            "btnCancel": {
                                "text": kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                                "toolTip": kony.i18n.getLocalizedString("i18n.transfers.Cancel"),
                                "onClick": function() {
                                    scopeObj.editCancelOnClick(dataItem);
                                }
                            }
                        };
                        var ebillRelatedPayeeDetails = scopeObj.setManagePayeeInfoByEBill(managePayee, dataItem);
                        var ebillRelatedPayeeDetailskeys = Object.keys(ebillRelatedPayeeDetails[0]);
                        for (var i in ebillRelatedPayeeDetailskeys) {
                            managePayee[ebillRelatedPayeeDetailskeys[i]] = ebillRelatedPayeeDetails[0][ebillRelatedPayeeDetailskeys[i]];
                        }
                        var notActivatedEbillRelatedPayeeDetailskeys = Object.keys(ebillRelatedPayeeDetails[1]);
                        for (var j in notActivatedEbillRelatedPayeeDetailskeys) {
                            managePayee[notActivatedEbillRelatedPayeeDetailskeys[j]] = ebillRelatedPayeeDetails[1][notActivatedEbillRelatedPayeeDetailskeys[j]];
                        }
                        return managePayee;
                    });
                    this.view.tableView.segmentBillpay.widgetDataMap = dataMap;
                    this.view.tableView.segmentBillpay.setData(managePayees);
                    scopeObj.view.tableView.flxNoPayment.setVisibility(false);
                    if (searchView) {
                        this.view.flxPagination.setVisibility(false);
                        scopeObj.view.tableView.Search.setVisibility(true);
                        scopeObj.view.tableView.FlxBillpayeeManagePayees.setVisibility(false);
                    }
                }
            }
            FormControllerUtility.hideProgressBar(this.view);
            this.view.forceLayout();
        },
        /**
         * set Payee Information based on eBill Flag
         * @param {object}  managePayees manage payees
         * @param {object} dataItem data
         * @returns {object} manage payee object
         */
        setManagePayeeInfoByEBill: function(managePayees, dataItem) {
            var scopeObj = this;
            var notEnableEBillPayee = {};
            var billGeneratedPayee = {};
            if (CommonUtilities.isCSRMode()) {
                managePayees.btnDeleteBiller.skin = CommonUtilities.disableSegmentButtonSkinForCSRMode(13);
                managePayees.btnSave.skin = CommonUtilities.disableButtonSkinForCSRMode();
            }
            if (dataItem.billid === "0") {
                var lblPayment;
                if (kony.application.getCurrentBreakpoint() === 640 || orientationHandler.isMobile) {
                    lblPayment = kony.i18n.getLocalizedString("i18n.billPay.noPaymentActivity");
                } else {
                    lblPayment = {
                        "text": kony.i18n.getLocalizedString("i18n.billPay.noPaymentActivity"),
                        "skin": "sknLblSSP42424215px",
                        "top": "10px",
                        "accessibilityconfig" : {
                            "a11yLabel" : kony.i18n.getLocalizedString("i18n.billPay.noPaymentActivity")
                        }
                    };
                }
                billGeneratedPayee = {
                    "lblLastPayment": lblPayment,
                    "lblLastPaymentDate": "",
                    "lblNextBill": lblPayment,
                    "lblNextBillDate": ""
                };
            } else {
                billGeneratedPayee = {
                    "lblLastPayment": {
                        "text" : dataItem.lastPaidAmount ? scopeObj.formatAmount(dataItem.lastPaidAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) : '',
                        "accessibilityconfig" : {
                            "a11yLabel" : dataItem.lastPaidAmount ? scopeObj.formatAmount(dataItem.lastPaidAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) : ''
                        }
                    },
                    "lblLastPaymentDate": {
                        "text" : scopeObj.getDateFromDateString(dataItem.lastPaidDate, "YYYY-MM-DDTHH:MM:SS"),
                        "accessibilityconfig" : {
                            "a11yLabel" : scopeObj.getDateFromDateString(dataItem.lastPaidDate, "YYYY-MM-DDTHH:MM:SS")
                        }
                    },
                    "lblNextBill": {
                        "text" : dataItem.dueAmount ? scopeObj.formatAmount(dataItem.dueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) : '',
                        "accessibilityconfig" : {
                            "a11yLabel" : dataItem.dueAmount ? scopeObj.formatAmount(dataItem.dueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) : ''
                        }
                    },
                    "lblNextBillDate": {
                        "text" : kony.i18n.getLocalizedString("i18n.billPay.DueDate") + ": " + scopeObj.getDateFromDateString(dataItem.billDueDate, "YYYY-MM-DDTHH:MM:SS"),
                        "accessibilityconfig" : {
                            "a11yLabel" : kony.i18n.getLocalizedString("i18n.billPay.DueDate") + ": " + scopeObj.getDateFromDateString(dataItem.billDueDate, "YYYY-MM-DDTHH:MM:SS")
                        }
                    },
                };
            }
            if (dataItem.eBillSupport === "false" || dataItem.isManuallyAdded === "true") {
                notEnableEBillPayee = {
                    "lblLastPayment": " ",
                    "lblLastPaymentDate": " ",
                    "lblNextBill": " ",
                    "lblNextBillDate": " ",
                    "btnPayBill": {
                        "text": kony.i18n.getLocalizedString("i18n.Pay.PayBill"),
                        "onClick": scopeObj.singleBillPayFromPayee.bind(scopeObj, dataItem)
                    },
                    "lblAccountNumberTitle": "Acc Num : ",
                    "lblAccountNumberValue":  {
                        "text" : dataItem.accountNumber ? dataItem.accountNumber : '',
                        "accessibilityconfig" : {
                            "a11yLabel" : dataItem.accountNumber ? dataItem.accountNumber : ''
                        }
                    },
                    "lblBankAddressOne":{
                        "text" :  dataItem.payeeName ? dataItem.payeeName : '',
                        "accessibilityconfig" : {
                            "a11yLabel" :  dataItem.payeeName ? dataItem.payeeName : ''
                        }
                    },
                    "lblBankAddressTwo": {
                        "text" :  dataItem.addressLine2 ? dataItem.addressLine2 : '',
                        "accessibilityconfig" : {
                            "a11yLabel" : dataItem.addressLine2 ? dataItem.addressLine2 : ''
                        }
                    },
                }
            } else if (dataItem.eBillStatus === "1") {
                notEnableEBillPayee = {
                    "btnEbill": {
                        "text": "SOME TEXT",
                        "skin": ViewConstants.SKINS.SKNBTNEBILLACTIVE,
                        "onClick": dataItem.billid === "0" ? null : scopeObj.viewEBill.bind(scopeObj, {
                            "billGeneratedDate": scopeObj.getDateFromDateString(dataItem.billGeneratedDate, "YYYY-MM-DDTHH:MM:SS"),
                            "amount": scopeObj.formatAmount(dataItem.dueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)),
                            "ebillURL": dataItem.ebillURL
                        }),
                        "accessibilityconfig" : {
                            "a11yLabel" : "ebill"
                        }
                    },
                    "btnPayBill": {
                        "text": kony.i18n.getLocalizedString("i18n.Pay.PayBill"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.Pay.PayBill"),
                        "onClick": scopeObj.singleBillPayFromPayee.bind(scopeObj, dataItem),
                         "accessibilityconfig" : {
                            "a11yLabel" : kony.i18n.getLocalizedString("i18n.Pay.PayBill")
                        }
                    },
                    "btnViewEbill": {
                        "text": kony.i18n.getLocalizedString("i18n.billpay.deactivate"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.billpay.deactivate"),
                        "onClick": function() {
                            scopeObj.editDeactivateOnClick();
                        },
                       "accessibilityconfig" : {
                            "a11yLabel" : kony.i18n.getLocalizedString("i18n.billpay.deactivate")
                        }
                    }
                };
            } else {
                notEnableEBillPayee = {
                    "btnEbill": {
                        "text": "SOME TEXT",
                        "skin": ViewConstants.SKINS.SKNBTNIMGINACTIVEEBILL,
                        "onClick": null,
                       "accessibilityconfig" : {
                            "a11yLabel" : "ebill"
                        }
                    },
                    "btnPayBill": {
                        "text": kony.i18n.getLocalizedString("i18n.WireTransfer.ACTIVATE"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.WireTransfer.ACTIVATE"),
                        "onClick": function() {
                            scopeObj.activateEbillUI(dataItem, "managePayees");
                        },
                        "accessibilityconfig" : {
                            "a11yLabel" : kony.i18n.getLocalizedString("i18n.WireTransfer.ACTIVATE")
                        }
                    },
                    "btnViewEbill": {
                        /*"text": kony.i18n.getLocalizedString("i18n.billpay.deactivate"),
                        "toolTip": kony.i18n.getLocalizedString("i18n.billpay.deactivate"),
                        "onClick": null,
                        "skin": kony.application.getCurrentBreakpoint()===640 ? "sknBtnSSP3343A813PxBg0CSR": "sknBtnSSP0273e315px"*/
                        "isVisible": false
                    }
                };
            }
            return [notEnableEBillPayee, billGeneratedPayee];
        },
        /**
         * constract a pay A Bill object and send to single Bill Pay
         * @param {object} dataItem payee Payment object
        */
        singleBillPayFromPayee: function (dataItem) {
            var scopeObj = this;
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            var payABillData = {
                "payeeNickname": dataItem.payeeNickName,
                "lastPaidAmount": dataItem.lastPaidAmount,
                "lastPaidDate": dataItem.lastPaidDate,
                "dueAmount": scopeObj.formatAmount(dataItem.dueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)),
                "billDueDate": dataItem.billDueDate,
                "eBillSupport": dataItem.eBillSupport,
                "eBillStatus": dataItem.ebillStatus,
                "billid": dataItem.billid,
                "payeeId": dataItem.payeeId,
                "accountNumber": dataItem.accountNumber,
                "billGeneratedDate": dataItem.billGeneratedDate,
                "ebillURL": dataItem.ebillURL,
                "searchView": scopeObj.searchView
            };
            scopeObj.presenter.showBillPayData(null, "PayABill", false, payABillData);
        },
        /**
         * constract a pay A Bill object and send to single Bill Pay
         * @param {object} data payee Payment object
         * @param {string} context context
        */
        setDataForPayABill: function (data, context) {
            this.setViewPayABill();
            this.setSinglePayementValue(data, context);
            if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
                this.setMobileHeaderDetailsData({
                    "title1": this.view.payABill.lblPayee.text + ":",
                    "value1": this.view.payABill.lblPayeeName.text,
                    "title2": this.view.payABill.lblBill.text + ":" + this.view.payABill.lblBillValue.text,
                    "value2": this.view.payABill.lblDueDate.text + ":" + this.view.payABill.lblDueDateValue.text,
                    "textBtn1": "",
                    "actionBtn1": "",
                    "textBtn2": this.view.payABill.btnViewEbill.onClick==null? "" :this.view.payABill.btnViewEbill.text,
                    "actionBtn2": this.view.payABill.btnViewEbill.onClick
                });
            } else {
                this.view.flxBillHeaderMobile.isVisible = false;
            }
            this.AdjustScreen();
        },
        /**
         * constract a pay A Bill object and send to single Bill Pay
         * @param {object} data payee Payment object
         * @param {string} context  context
        */
        setSinglePayementValue: function (data, context) {
            var self = this;
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            if (context === "acknowledgement") {
                self.view.payABill.txtSearch.text = "";
            };
            this.view.payABill.txtNotes.text = data.notes ? data.notes : '';
            var preferredAccNum = this.presenter.getBillPayPreferedAccountNumber();
            this.view.payABill.lbxpayFromValue.selectedKey = data.fromAccountNumber ? data.fromAccountNumber : (preferredAccNum ? preferredAccNum : this.view.payABill.lbxpayFromValue.masterData[0][0]);
            this.view.payABill.calSendOn.dateComponents = data.sendOn ? self.getDateComponents(data.sendOn,applicationManager.getFormatUtilManager().getDateFormat().toUpperCase()) : CommonUtilities.getServerDateComponent();
            this.view.payABill.CalDeliverBy.dateComponents = data.deliveryDate ? self.getDateComponents(data.deliveryDate,applicationManager.getFormatUtilManager().getDateFormat().toUpperCase()) : CommonUtilities.getServerDateComponent();
            self.setDefaultBillDetails(data);
            self.quickActionsFromBillPay(data, context);
            self.setFrequencyForPayABill(data);
            self.eBillForPayABill(data);
            self.setAmounForPayABilll(data, context);
            if (data.searchView === true) {
                this.view.payABill.btnCancel.onClick = this.showCurrentFlex;
            } else {
                this.view.payABill.btnCancel.onClick = data.onCancel ? function(){
                    self.view.flxBillPay.top = "0dp";
                  self.view.flxBillHeaderMobile.isVisible=false;
                    data.onCancel();} : this.cancelCurrentFlex;
            }
            self.view.payABill.btnConfirm.onClick = this.getTnC.bind(this, false, data, context);
            FormControllerUtility.hideProgressBar(this.view);
        },
        getTnC:function( isBulkBillPay, data, context){
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController.getTnCBillPayTransfer(isBulkBillPay, data, context);
        },
        getTnCOneTimeBillPay:function(data){
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController.getTnCOneTimeBillPay(data);
        },
        /**
         * used to diplay tableView flex
         */
        showCurrentFlex: function () {
            this.view.tableView.isVisible = true;
            this.view.tableView.segmentBillpay.isVisible = true;
            this.view.payABill.isVisible = false;
          this.view.flxBillHeaderMobile.isVisible=false;
            this.view.flxBillPay.top = "0dp";
        },
        /**
        * method used to handle the Bill Pay ui activities
        */
        cancelCurrentFlex: function () {
            this.view.payABill.setVisibility(false);
            this.view.flxConfirmButton.setVisibility(false);
            this.view.btnConfirm.setVisibility(false);
            this.view.tableView.setVisibility(true);
            this.view.tableView.segmentBillpay.setVisibility(true);
            this.view.flxPagination.setVisibility(true);
          this.view.flxBillHeaderMobile.isVisible=false;
            this.view.flxBillPay.top = "0dp";
            this.setUIBasedOnTab();
            this.AdjustScreen();
        },
        /**
        * common Method for to set UI based on tab click
        */
        setUIBasedOnTab: function () {
            if (this.selectedTab === "managePayees") { this.setManagePayeeUI(); }
            else if (this.selectedTab === "history") { this.setHistoryUI(); }
            else if (this.selectedTab === "allPayees") { this.setAllPayeesSegmentData(); }
            else if (this.selectedTab === "paymentDue") { this.setPaymentDueSegmentData(); }
            else if (this.selectedTab === "scheduled") { this.setScheduleUI(); }
        },
        /**
         * setting amount value
         * @param {object} data data
         * @param {object} context context
         */
        setAmounForPayABilll: function (data, context) {
            var self = this;
            if (data.amount) {
                if (context === "acknowledgement") {
                    this.view.payABill.txtSearch.text = '';
                    FormControllerUtility.disableButton(self.view.payABill.btnConfirm);
                } else {
                    this.view.payABill.txtSearch.text = data.amount ? CommonUtilities.formatCurrencyWithCommas(data.amount, true) : '';
                    FormControllerUtility.enableButton(self.view.payABill.btnConfirm);
                }
            } else {
                this.view.payABill.txtSearch.text = '';
                FormControllerUtility.disableButton(self.view.payABill.btnConfirm);
            }
        },
        /**
         * call to pay a bil
         * @param {object} data data
         * @param {object} context context
         */
        sendSinglePayABillPayData: function (data, context, TnCcontentTransfer) {
            var self = this;
            var message;
            var currTime = CommonUtilities.getServerDateObject();
			currTime.setHours(0,0,0,0); // Sets to midnight.
            var formattedPayABill = self.constructSingleBillPayObj(data, context, TnCcontentTransfer);
            var payFromAccount = formattedPayABill.payFrom;
            var index = payFromAccount.indexOf("(");
            var accountPaid = payFromAccount.substring(0, index);
          	var sendOnDate = this.getDateObj(this.view.payABill.calSendOn.dateComponents);
          	var endOnDate = this.getDateObj(this.view.payABill.calEndingOnRec.dateComponents);
            var deliverByDate = this.getDateObj(this.view.payABill.CalDeliverBy.dateComponents);
            this.view.flxBillPay.top = "0dp";
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            var deFormatAmount  = self.deformatAmount(self.view.payABill.txtSearch.text);
            var result = self.presenter.validateBillPayAmount(parseFloat(deFormatAmount));
            CommonUtilities.setText(self.view.ConfirmDefaultAccount.lblPopupMessage, kony.i18n.getLocalizedString("i18n.billPay.setDefaultPopUpBillPay") + " " + accountPaid + kony.i18n.getLocalizedString("i18n.billPay.setDefaultPopUpBillPayee") , CommonUtilities.getaccessibilityConfig());
            if (result.isAmountValid) {
                if (sendOnDate.getTime() < currTime.getTime()) {
                    message = kony.i18n.getLocalizedString("i18n.transfers.error.invalidSendOnDate");
                    this.showErrorFlexForSinglePayABill(message);
                    return;
                }
              if(self.view.payABill.lbxFrequency.selectedKey !== 'Once') {
        		if (self.view.payABill.lbxForHowLong.selectedKey === 'ON_SPECIFIC_DATE') {
          			if(endOnDate.getTime() < sendOnDate.getTime()) {
                      	 message = kony.i18n.getLocalizedString("i18n.transfers.errors.invalidEndOnDate");
            			self.showErrorFlexForSinglePayABill(message);
          			} else if(endOnDate.getTime() ===  sendOnDate.getTime()) {
                      	 message = kony.i18n.getLocalizedString("i18n.transfers.errors.sameEndDate");
            			self.showErrorFlexForSinglePayABill(message);
          			} else if(deliverByDate.getTime() < sendOnDate.getTime()) {
                      	 message = kony.i18n.getLocalizedString("i18n.transfers.errors.invalidDeliverByDate");
            			self.showErrorFlexForSinglePayABill(message);
          			} else{
                  		if (self.presenter.getBillPayPreferedAccountNumber() === "") {
                    		if (this.presenter.getDefaultBillPayPopUp() === true) {
                        		self.setBillPayDefaultAccountWithSingleBillPayConfirm(formattedPayABill);
                    		} else {
                        		self.view.flxConfirmDefaultAccount.isVisible = false;
                        		self.view.payABill.flxError.setVisibility(false);
                        		self.presenter.setDataForConfirm(formattedPayABill);
                    		}
                      	} else {
                    		self.view.flxConfirmDefaultAccount.setVisibility(false);
                    		self.view.payABill.flxError.setVisibility(false);
                    		self.presenter.setDataForConfirm(formattedPayABill);
                	  }
                    }
        		}
        		if (self.view.payABill.lbxForHowLong.selectedKey === 'NO_OF_RECURRENCES') {
          			if(deliverByDate.getTime() < sendOnDate.getTime()){
                  		message = kony.i18n.getLocalizedString("i18n.transfers.errors.invalidDeliverByDate");
            			self.showErrorFlexForSinglePayABill(message);
                	} else {
                      if (self.presenter.getBillPayPreferedAccountNumber() === "") {
                    	if (this.presenter.getDefaultBillPayPopUp() === true) {
                        	self.setBillPayDefaultAccountWithSingleBillPayConfirm(formattedPayABill);
                    	} else {
                        	self.view.flxConfirmDefaultAccount.isVisible = false;
                        	self.view.payABill.flxError.setVisibility(false);
                        	self.presenter.setDataForConfirm(formattedPayABill);
                    	}
                      } else {
                    		self.view.flxConfirmDefaultAccount.setVisibility(false);
                    		self.view.payABill.flxError.setVisibility(false);
                    		self.presenter.setDataForConfirm(formattedPayABill);
                	  }
                    }
            	}
              } else{
                	if(deliverByDate.getTime() < sendOnDate.getTime()) {
            			message = kony.i18n.getLocalizedString("i18n.transfers.errors.invalidDeliverByDate");
            			self.showErrorFlexForSinglePayABill(message);
          			}
                	else{
                      if (self.presenter.getBillPayPreferedAccountNumber() === "") {
                    	if (this.presenter.getDefaultBillPayPopUp() === true) {
                        	self.setBillPayDefaultAccountWithSingleBillPayConfirm(formattedPayABill);
                    	} else {
                        	self.view.flxConfirmDefaultAccount.isVisible = false;
                        	self.view.payABill.flxError.setVisibility(false);
                        	self.presenter.setDataForConfirm(formattedPayABill);
                    	}
                      } else {
                    		self.view.flxConfirmDefaultAccount.setVisibility(false);
                    		self.view.payABill.flxError.setVisibility(false);
                    		self.presenter.setDataForConfirm(formattedPayABill);
                	  }
                    }
        		}
            }else{
                self.view.payABill.rtxErrorPayBill.text = result.errMsg;
                self.view.payABill.flxError.setVisibility(true);
              if (kony.application.getCurrentBreakpoint() == 640){
                if(this.view.lbxPayee.isVisible === true){this.view.flxBillPay.top = "150dp";}
              else{this.view.flxBillPay.top = "110dp";}
              }else {
                this.view.flxBillPay.top="25dp";
              }
              this.AdjustScreen();
              return;
            }
        },
      	/**
        * showErrorFlexForSinglePayABill:    used to show error flex.
        * @param {string} message error information
        */
      	showErrorFlexForSinglePayABill: function (message) {
            this.view.payABill.rtxErrorPayBill.text = message;
            this.view.payABill.flxError.setVisibility(true);
            this.view.payABill.flxError.setFocus(true);
          if (kony.application.getCurrentBreakpoint() == 640){
            this.view.flxBillPay.top="140dp";
          }else {
            this.view.flxBillPay.top="25dp";
          }
            this.view.forceLayout();
          this.AdjustScreen();
        },
      	/**
        * getDateObj : used to format the date and return as object
        * @param {dateComponents} date
        */
      	getDateObj: function(dateComponents) {
            var date = new Date();
            date.setDate(dateComponents[0]);
            date.setMonth(parseInt(dateComponents[1]) - 1);
            date.setFullYear(dateComponents[2]);
            date.setHours(0, 0, 0, 0)
            return date;
        },
        /**
         * used to set BillPay Default Account and navigate to single Bill Pay confirm screen
         * @param {object} formattedPayABill constructed Single BillPay object
         */
        setBillPayDefaultAccountWithSingleBillPayConfirm: function (formattedPayABill) {
            var self = this;
            var payFromAccount = formattedPayABill.payFrom;
            var index = payFromAccount.indexOf("(");
            var accountPaid = payFromAccount.substring(0, index);
				self.view.ConfirmDefaultAccount.lblCheckBox.text='D';
            self.view.flxConfirmDefaultAccount.height = self.view.flxHeader.info.frame.height + self.view.flxContainer.info.frame.height + self.view.flxFooter.info.frame.height + "dp";
            self.view.flxConfirmDefaultAccount.setVisibility(true);
            self.view.ConfirmDefaultAccount.lblPopupMessage.setFocus(true);
            self.view.ConfirmDefaultAccount.imgCross.onTouchEnd = function () {
                self.view.flxConfirmDefaultAccount.setVisibility(false);
            }
            self.view.ConfirmDefaultAccount.btnNo.onClick = function () {
                self.view.flxConfirmDefaultAccount.setVisibility(false);
                self.view.oneTimePayment.flxDowntimeWarning.setVisibility(false);
                self.view.payABill.flxError.setVisibility(false);
                self.presenter.setDataForConfirm(formattedPayABill);
            }
            self.view.ConfirmDefaultAccount.flxCheckBox.onClick = function () {
                if (self.view.ConfirmDefaultAccount.lblCheckBox.text === 'D') {
				self.view.ConfirmDefaultAccount.lblCheckBox.text='C';
                  	self.view.ConfirmDefaultAccount.lblCheckBox.skin = ViewConstants.SKINS.CHECKBOX_SELECTED_SKIN;
                } else {
				self.view.ConfirmDefaultAccount.lblCheckBox.text='D';
                  	self.view.ConfirmDefaultAccount.lblCheckBox.skin=ViewConstants.SKINS.CHECKBOX_UNSELECTED_SKIN;
                }
            }
            self.view.ConfirmDefaultAccount.btnYes.onClick = function () {
                if (self.view.ConfirmDefaultAccount.lblCheckBox.text === 'D') {
                    self.presenter.updateBillPayPreferedAccount(formattedPayABill.fromAccountNumber);
                    formattedPayABill.statusOfDefaultAccountSetUp = true;
                    formattedPayABill.defaultAccountBillPay = accountPaid;
                    self.view.flxConfirmDefaultAccount.setVisibility(false);
                    self.view.oneTimePayment.flxDowntimeWarning.setVisibility(false);
                    self.view.payABill.flxError.setVisibility(false);
                    self.presenter.setDataForConfirm(formattedPayABill);
                } else {
                    self.presenter.updateShowBillPayFromAccPop();
                    self.presenter.updateBillPayPreferedAccount(formattedPayABill.fromAccountNumber);
                    formattedPayABill.statusOfDefaultAccountSetUp = true;
                    formattedPayABill.defaultAccountBillPay = accountPaid;
                    self.view.flxConfirmDefaultAccount.setVisibility(false);
                    self.view.oneTimePayment.flxDowntimeWarning.setVisibility(false);
                    self.presenter.setDataForConfirm(formattedPayABill);
                }
            }
        },
        /**
         * used to get the amount
         * @param {number} amount amount
         * @returns {number} amount
         */
        deformatAmount: function(amount)
        {
            return applicationManager.getFormatUtilManager().deFormatAmount(amount);
        },
        /**
         * used to construct the single bill payement object
         * @param {object} data data
         * @param {string} context  context
         * @returns {object} object
         */
        constructSingleBillPayObj: function (data, context, TnCcontentTransfer) {
            var self = this;
            var formattedPayABill = {
                "payeeId": data.payeeId,
                "payFrom": self.view.payABill.lbxpayFromValue.selectedKeyValue[1],
                "fromAccountNumber": self.view.payABill.lbxpayFromValue.selectedKeyValue[0],
                "payeeName": self.view.payABill.lblPayeeName.text,
                "amount": self.view.payABill.txtSearch.text,
                "sendOn": self.view.payABill.calSendOn.formattedDate,
                "notes": self.view.payABill.txtNotes.text,
                "payeeNickname": data.payeeNickname,
                "billid": data.billid,
                "lastPaidAmount": data.lastPaidAmount,
                "lastPaidDate": data.lastPaidDate,
                "dueAmount": data.dueAmount,
                "billDueDate": data.billDueDate,
                "eBillSupport": data.eBillSupport,
                "eBillStatus": data.eBillStatus,
                "referenceNumber": data.referenceNumber,
                "accountNumber": data.accountNumber,
                "deliveryDate": self.view.payABill.CalDeliverBy.formattedDate,
                "isScheduleEditFlow": data.isScheduleEditFlow,
                "frequencyType": self.view.payABill.lbxFrequency.selectedKeyValue[0],
                "numberOfRecurrences": self.view.payABill.txtEndingOn.text,
                "hasHowLong": self.view.payABill.lbxForHowLong.selectedKey,
                "frequencyStartDate": self.view.payABill.calSendOn.formattedDate,
                "frequencyEndDate": self.view.payABill.calEndingOnRec.formattedDate,
                "isScheduled": data.isScheduled,
                "statusOfDefaultAccountSetUp": false,
                "defaultAccountBillPay": data.payFrom,
                "onCancel" : data.onCancel,
                "transactionCurrency" : self.returnCurrencyCode(self.view.payABill.CopylblBillValue0ba5a858ec5504b.text),
                "TnCcontentTransfer": TnCcontentTransfer
            };
            if (context === "quickAction") {
              formattedPayABill.context = "quickAction";
              if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
                formattedPayABill.payeeName = self.view.lbxPayee.selectedKeyValue[1];
                formattedPayABill.payeeId = self.view.lbxPayee.selectedKeyValue[0];
              } else {
                formattedPayABill.payeeName = self.view.payABill.lbxpayee.selectedKeyValue[1];
                formattedPayABill.payeeId = self.view.payABill.lbxpayee.selectedKeyValue[0];
            }
        }
		return formattedPayABill;
    },
    returnCurrencyCode : function(currencySymbol){
        return applicationManager.getFormatUtilManager().getCurrencySymbolCode(currencySymbol);
     },
        /**
         * used to set BillId
         * @param {object} data data
         */
        setDefaultBillDetails: function (data) {
            var nonValue = kony.i18n.getLocalizedString("i18n.common.none");
            if (data.billid === "0" || data.billid === undefined || data.billid === null) {
                CommonUtilities.setText(this.view.payABill.lblDueDateValue, nonValue , CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(this.view.payABill.lblBillValue, nonValue , CommonUtilities.getaccessibilityConfig());
            } else {
                CommonUtilities.setText(this.view.payABill.lblDueDateValue, data.billDueDate ? CommonUtilities.getFrontendDateString(data.billDueDate) : nonValue , CommonUtilities.getaccessibilityConfig());
                CommonUtilities.setText(this.view.payABill.lblBillValue, data.dueAmount ? CommonUtilities.formatCurrencyWithCommas(data.dueAmount) : nonValue , CommonUtilities.getaccessibilityConfig());
            }
            if (data.lastPaidAmount) {
                    CommonUtilities.setText(this.view.payABill.lblLastPayment, kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ":" +  (data.lastPaidAmount ? CommonUtilities.formatCurrencyWithCommas(data.lastPaidAmount) : nonValue) +  " " + kony.i18n.getLocalizedString("i18n.common.on") + " " + CommonUtilities.getFrontendDateString(data.lastPaidDate , CommonUtilities.getaccessibilityConfig()));
            } else {
                CommonUtilities.setText(this.view.payABill.lblLastPayment, kony.i18n.getLocalizedString("i18n.billPay.lastPayment") + ": " + kony.i18n.getLocalizedString("i18n.billPay.noPaymentActivity") , CommonUtilities.getaccessibilityConfig());
            }
        },
        /**
         * used data for paya A Bill
         * @param {object} data
         * @param {string} context
         */
        quickActionsFromBillPay: function (data, context) {
          if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
        //   this.view.flxBillHeaderMobile.height="110dp";
        //   this.view.flxBillPay.top="120dp";
        //   this.view.label3.top="70dp";
        //   this.view.label4.top="85dp";
        //   this.view.lbxPayee.left = "10dp";
         // this.view.lbxPayee.width = "385dp";
          this.view.flxBillHeaderMobile.isVisible = true;
        } else {
          this.view.flxBillHeaderMobile.isVisible = false;
        }
            if (context === "quickAction" || data.context === "quickAction") {
                this.view.payABill.lbxpayee.selectedKey = data.payeeId ? data.payeeId : this.view.payABill.lbxpayee.masterData[0][0];
                this.view.payABill.lbxpayee.setVisibility(true);
                if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
                  this.view.flxBillHeaderMobile.height ="140dp";
                  this.view.payABill.lbxpayee.setVisibility(false);
                }
                this.view.lbxPayee.setVisibility(true);
                this.view.label2.setVisibility(false);
                this.view.payABill.lblLastPayment.setVisibility(false);
                this.fromAccountNumber = data.fromAccountNumber;
            } else {
                this.view.payABill.lbxpayee.setVisibility(false);
                this.view.payABill.lblLastPayment.setVisibility(true);
                this.view.lbxPayee.setVisibility(false);
                if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
                  this.view.flxBillHeaderMobile.height ="100dp";
                }
                this.view.label2.setVisibility(true);
                this.view.label2.top = "30dp";
                this.view.label2.left = "10dp";
                CommonUtilities.setText(this.view.payABill.lblPayeeName, data.payeeNickname , CommonUtilities.getaccessibilityConfig());
                this.fromAccountNumber = null;
            }
          this.view.forceLayout();
        },
        /**
         * Frequency For Pay A Bill
         * @param {object} data
         */
        setFrequencyForPayABill: function (data) {
            var self = this;
            this.view.payABill.lbxFrequency.selectedKey = data.frequencyType ? data.frequencyType : this.view.payABill.lbxFrequency.masterData[0][0];
            if (data.frequencyStartDate !== null && data.frequencyStartDate !== "" && data.frequencyEndDate !== null && data.frequencyEndDate !== "") {
                this.view.payABill.lbxForHowLong.selectedKey = "ON_SPECIFIC_DATE";
                this.view.payABill.calEndingOnRec.dateComponents = data.frequencyEndDate ? self.getDateComponents(data.frequencyEndDate,applicationManager.getFormatUtilManager().getDateFormat().toUpperCase()) : CommonUtilities.getServerDateComponent();
            } else if (data.numberOfRecurrences !== null && data.numberOfRecurrences !== "" && data.numberOfRecurrences !== "0") {
                this.view.payABill.lbxForHowLong.selectedKey = "NO_OF_RECURRENCES";
                this.view.payABill.txtEndingOn.text = data.numberOfRecurrences;
            }
            self.getFrequencyAndFormLayout(this.view.payABill.lbxFrequency.selectedKey, this.view.payABill.lbxForHowLong.selectedKey);
        },
        /**
         * EBill For Pay A Bill
         * @param {object} data
         */
        eBillForPayABill: function (data) {
            if (data.eBillSupport === "true") {
                this.view.payABill.btnEbill.setVisibility(true);
                if (data.eBillStatus === "1") {
                    var eBillViewModel = {
                        billGeneratedDate: CommonUtilities.getFrontendDateString(data.billGeneratedDate),
                        amount: CommonUtilities.formatCurrencyWithCommas(data.dueAmount),
                        ebillURL: data.ebillURL
                    };
                    this.view.payABill.btnEbill.skin = ViewConstants.SKINS.SKNBTNEBILLACTIVE;
                  if (kony.application.getCurrentBreakpoint() == 640){
                    this.view.payABill.btnViewEbill.setVisibility(false);
                  }else {
                    this.view.payABill.btnViewEbill.setVisibility(true);
                  }
                    this.view.payABill.btnEbill.onClick = this.viewEBill.bind(this, eBillViewModel);
                    this.view.payABill.btnViewEbill.setVisibility(true);
                    this.view.payABill.btnViewEbill.onClick = this.viewEBill.bind(this, eBillViewModel);
                } else {
                    this.view.payABill.btnEbill.skin = ViewConstants.SKINS.SKNBTNEBILLACTIVE;
                    this.view.payABill.btnEbill.onClick = null;
                    this.view.payABill.btnViewEbill.setVisibility(false);
                    this.view.payABill.btnViewEbill.onClick = null;
                }
            } else {
                this.view.payABill.btnEbill.setVisibility(false);
                this.view.payABill.btnEbill.skin = ViewConstants.SKINS.SKNBTNIMGINACTIVEEBILL;
                this.view.payABill.btnEbill.onClick = null;
                this.view.payABill.btnViewEbill.setVisibility(false);
                this.view.payABill.btnViewEbill.onClick = null;
            }
        },
        setFromAccountCurrency: function(billPayAccounts) {
            var fromAccount = this.getFromAccount(billPayAccounts, this.view.payABill.lbxpayFromValue.selectedKey);
            this.view.payABill.CopylblBillValue0ba5a858ec5504b.text = applicationManager.getFormatUtilManager().getCurrencySymbol(fromAccount.currencyCode);
        },
        getFromAccount: function(data, value) {
            data = data.filter(function(item) {
                return item.Account_id === value
            });
            return data[0];
        },
        /**
         * used to show pay a bill screen
        */
        setViewPayABill: function () {
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            var scopeObj = this;
            CommonUtilities.setText(this.view.payABill.CopylblBillValue0ba5a858ec5504b, applicationManager.getConfigurationManager().getCurrencyCode() , CommonUtilities.getaccessibilityConfig());
            var billPayAccounts = this.presenter.getBillPaySupportedAccounts();
            this.view.payABill.lbxpayFromValue.masterData = FormControllerUtility.getListBoxDataFromObjects(billPayAccounts, "accountID", CommonUtilities.getAccountDisplayNameWithBalance);
            this.setFromAccountCurrency(billPayAccounts);
            this.view.payABill.lbxpayFromValue.onSelection = function() {
                scopeObj.setFromAccountCurrency(billPayAccounts);
            };
            this.view.payABill.lbxFrequency.masterData = this.presenter.getFrequencies();
            this.view.payABill.lbxForHowLong.masterData = this.presenter.getHowLongValues();
            CommonUtilities.disableOldDaySelection(this.view.payABill.calSendOn);
            CommonUtilities.disableOldDaySelection(this.view.payABill.CalDeliverBy);
            CommonUtilities.disableOldDaySelection(this.view.payABill.calEndingOnRec);
            var dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
            var today = CommonUtilities.getServerDateObject();
            var date = [today.getDate(), today.getMonth()+1, today.getFullYear()];
            this.view.payABill.calSendOn.dateFormat = dateFormat;
            this.view.payABill.CalDeliverBy.dateFormat = dateFormat;
            this.view.payABill.calEndingOnRec.dateFormat = dateFormat;
            this.view.payABill.calSendOn.dateComponents = date;
            this.view.payABill.CalDeliverBy.dateComponents = date;
            this.view.payABill.calEndingOnRec.dateComponents = date;
            this.singleBillPayAmountField = FormControllerUtility.wrapAmountField(scopeObj.view.payABill.txtSearch)
                .onKeyUp(this.checkValidityBillPay.bind(this));
            this.view.payABill.flxError.setVisibility(false);
          if (kony.application.getCurrentBreakpoint() == 640){
            this.view.payABill.top="10dp";
            this.view.payABill.btnViewEbill.isVisible=false;
          }else {
            this.view.payABill.top="0dp";
            this.view.payABill.btnViewEbill.isVisible=true;
          }
            this.view.flxOneTImePayment.setVisibility(false);
            this.view.flxPagination.setVisibility(false);
            this.view.flxConfirmButton.setVisibility(false);
            this.view.btnConfirm.setVisibility(false);
            this.view.flxTermsAndConditions.top = "30dp";
            this.view.payABill.isVisible = true;
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
            }, {
                text: kony.i18n.getLocalizedString("i18n.hamburger.payABill")
            }]);
            this.view.breadcrumb.setFocus(true);
            this.view.tableView.isVisible = false;
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.payABill");
            this.view.payABill.txtNotes.maxTextLength = OLBConstants.NOTES_LENGTH;
            this.view.payABill.lbxFrequency.onSelection = this.onFrequencyChanged.bind(this);
            this.view.payABill.lbxForHowLong.onSelection = this.onHowLongChange.bind(this);
            this.view.payABill.txtEndingOn.onKeyUp = this.checkValidityBillPay.bind(this);
            if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
                this.view.payABill.flxHeader.isVisible = false;
                this.view.payABill.lblLastPayment.isVisible = false;
                if(this.view.lbxPayee.isVisible === true){this.view.flxBillPay.top = "150dp";}
              else{this.view.flxBillPay.top = "110dp";}
            }else{
                this.view.payABill.flxHeader.isVisible = true;
                this.view.payABill.lblLastPayment.isVisible = true;
            }
            this.AdjustScreen();
        },
        /**
        * viewBillPayActivity: view the bill pay view activity.
        * @param {object} dataItem data
        */
        viewBillPayActivity: function (dataItem) {
            FormControllerUtility.showProgressBar(this.view);
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            this.presenter.fetchPayeeBills({
                "payeeId": dataItem.payeeId
            });
        },
        /**
         * previous history
         */
        prevHistory: function () {
            FormControllerUtility.showProgressBar(this.view);
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            this.presenter.fetchPreviousUserBillPayHistory();
        },
        /**
         * next history
         */
        nextHistory: function () {
            FormControllerUtility.showProgressBar(this.view);
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            this.presenter.fetchNextUserBillPayHistory();
        },
        /**
        * previous manage Payees
        */
        prevManagePayees: function () {
            FormControllerUtility.showProgressBar(this.view);
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            this.presenter.fetchPreviousManagePayees();
        },
        /**
         * next Manage Payees
         */
        nextManagePayees: function () {
            FormControllerUtility.showProgressBar(this.view);
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            this.presenter.fetchNextManagePayees();
        },
        /**
        * setPagination:   used to set pagination.
        * @param {obejct} data list of records
        * @param {function} previousCallBack -- previous button handler
        * @param {function}  nextCallBack -- next button handler
        */
        setPagination: function (data, previousCallBack, nextCallBack) {
            var scopeObj = this;
            if (data && data.show === true) {
                this.view.flxPagination.setVisibility(true);
                var offset = data.offset;
                var limit = data.limit || OLBConstants.PAGING_ROWS_LIMIT;
                var recordsLength = data.recordsLength;
                CommonUtilities.setText(this.view.lblPagination, (offset + 1) + " - " + (offset + recordsLength) + " " + data.text , CommonUtilities.getaccessibilityConfig());
                if (data.offset > 0) {
                    scopeObj.view.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
                    scopeObj.view.flxPaginationPrevious.onClick = previousCallBack;
                }
                else {
                    scopeObj.view.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
                    scopeObj.view.flxPaginationPrevious.onClick = null;
                }
                if (recordsLength >= OLBConstants.PAGING_ROWS_LIMIT) {
                    scopeObj.view.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
                    scopeObj.view.flxPaginationNext.onClick = nextCallBack;
                } else {
                    scopeObj.view.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
                    scopeObj.view.flxPaginationNext.onClick = null;
                }
            }
            else {
                scopeObj.view.flxPagination.setVisibility(false);
                scopeObj.view.flxPaginationPrevious.onClick = null;
                scopeObj.view.flxPaginationNext.onClick = null;
            }
        },
        /**
         * validateBulkPayData:    used to validate the bulk payment.
       */
        validateBulkPayData: function () {
            var data = this.view.tableView.segmentBillpay.data;
            var self = this;
            var isInputValid = false;
            var isValidRecordFound = false;
            var errMsg = kony.i18n.getLocalizedString("i18n.billPay.EnterAValidInput");
            self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            for (var record in data) {
                if (data[record].txtAmount.text) {
                    var amount = parseFloat(self.deformatAmount(data[record].txtAmount.text));
                    if (isNaN(amount)) {
                        isInputValid = false;
                        break;
                    } else {
                        var result = self.presenter.validateBillPayAmount(amount);
                        if (!result.isAmountValid) {
                            isInputValid = false;
                            errMsg = result.errMsg;
                            break;
                        } else {
                            isInputValid = true;
                            isValidRecordFound = true;
                        }
                    }
                }
            }
            if (isInputValid) {
                self.hideErrorFlex();
                this.getTnC(true);
            } else {
                self.showErrorFlex(errMsg);
            }
        },
        /**
        * hideErrorFlex:    used to hide error flex.
        */
        hideErrorFlex: function () {
            this.view.flxDowntimeWarning.setVisibility(false);
            this.view.forceLayout();
        },
        /**
         * showErrorFlex:    used to show error flex.
        * @param {string} message error information
        */
        showErrorFlex: function (message) {
            this.view.rtxDowntimeWarning.text = message;
            this.view.flxDowntimeWarning.setVisibility(true);
            this.view.flxDowntimeWarning.setFocus(true);
            this.AdjustScreen();
        },
        /**
        * showConfirmBulkPay:   showing bulk billpay confirmation screen.
        */
        showConfirmBulkPay: function(TnCcontentTransfer) {
            var self = this;
            var bulkPayRecords = this.view.tableView.segmentBillpay.data;
            self.view.payABill.flxError.setVisibility(false);
            self.view.flxOneTImePayment.setVisibility(false);
            var records = [];
            var totalSum = 0;
            var index;
            function getDateFromConfiguration(dateComponents) {
                var dateObj = new Date(dateComponents[2] /* Year */ , dateComponents[1] - 1 /* Month*/ , dateComponents[0] /* Day*/ );
                return applicationManager.getFormatUtilManager().getFormatedDateString(dateObj, applicationManager.getFormatUtilManager().getApplicationDateFormat());
            }
            for (index = 0; index < bulkPayRecords.length; index++) {
                var txtAmount = bulkPayRecords[index].txtAmount.text;
                if (txtAmount !== '' && txtAmount !== null && (parseFloat(self.deformatAmount(txtAmount)) > 0)) {
                    totalSum = totalSum + parseFloat(self.deformatAmount(bulkPayRecords[index].txtAmount.text));
                    records.push({
                        "lblPayee": bulkPayRecords[index].lblPayee,
                        "lblSendOn": getDateFromConfiguration(bulkPayRecords[index].calSendOn.dateComponents),
                        "lblDeliverBy": getDateFromConfiguration(bulkPayRecords[index].calDeliverBy.dateComponents),
                        "lblAmount": {
                            "text" : self.formatAmount(self.deformatAmount(bulkPayRecords[index].txtAmount.text)),
                            "accessibilityconfig" : {
                                "a11yLabel" : self.formatAmount(self.deformatAmount(bulkPayRecords[index].txtAmount.text))
                            }
                        },
                        "lblPaymentAccount": {
                            "text" : self.setSelectedKeyValueForListbox(bulkPayRecords[index].lstPayFrom.masterData, bulkPayRecords[index].lstPayFrom.selectedKey),
                            "accessibilityconfig" : {
                                "a11yLabel" : self.setSelectedKeyValueForListbox(bulkPayRecords[index].lstPayFrom.masterData, bulkPayRecords[index].lstPayFrom.selectedKey)
                            }
                        },
                        "payeeId": bulkPayRecords[index].payeeId !== undefined ? bulkPayRecords[index].payeeId : bulkPayRecords[index].lblPayeeId,
                        "billid": bulkPayRecords[index].billid !== undefined ? bulkPayRecords[index].billid : bulkPayRecords[index].lblBillId,
                        "payeeAccountNumber": bulkPayRecords[index].payeeAccountNumber,
                        "accountNumber": bulkPayRecords[index].lstPayFrom.selectedKey,
                        "transactionsNotes": bulkPayRecords[index].txtNotes.text,
                    });
                }
            }
            var billPayData = {};
            billPayData.records = records;
            billPayData.totalSum = self.formatAmount(totalSum);
            billPayData.bulkPayRecords = bulkPayRecords;
            billPayData.TnCcontentTransfer = TnCcontentTransfer;
            self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            self.presenter.setDataForBulkBillPayConfirm(billPayData);
            self.view.payABill.flxError.setVisibility(false);
            this.view.forceLayout();
        },
        /**
        * getDateComponents:  Method to get the date
        * @param {object} dateString date string
        * @returns {object} date components
        */
        getDateComponents: function (dateString) {
            var dateObj = applicationManager.getFormatUtilManager().getDateObjectfromString(dateString, applicationManager.getFormatUtilManager().getDateFormat().toUpperCase());
            return [dateObj.getDate(), dateObj.getMonth()+1, dateObj.getFullYear()];
        },
        /**
         * Method to return value for selected key value for listbox
         * @param {object} masterData has master data of listbox
         * @param {string} selectedKey has selected key for listbox
         * @return {string} selectedKeyValue
         */
        setSelectedKeyValueForListbox: function (masterData, selectedKey) {
            var selectedKeyValue;
            masterData.map(function (item) {
                if (item[0] === selectedKey) {
                    selectedKeyValue = item;
                }
            });
             var result = selectedKeyValue ? selectedKeyValue[1]:masterData[0][1];
            return result;
        },
        /**
         * used to set initialView
         * @param {object} data data
         */
        showIntialBillPayUI: function (data) {
            var scopeObj = this;
            this.view.tableView.segmentBillpay.setVisibility(false);
            if (data === "History") {
                scopeObj.showHistoryUI();
            }
            else if (data === "PayABill") {
                scopeObj.view.payABill.flxError.setVisibility(false);
            } else if (data === "AllPayees") {
                scopeObj.setAllPayeesUI();
            } else if (data === "ManagePayees") {
                scopeObj.setManagePayeeUI();
            }
        },
        /**
        * Display View Activities
        * @param {object} response  list of payee bills
        */
        showPayeeViewActivities: function (response) {
            var scopeObj = this;
            var searchView = this.view.flxPagination.isVisible;
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
            }, {
                text: kony.i18n.getLocalizedString("i18n.transfers.viewActivity")
            }]);
            this.view.transferActivitymodified.btnbacktopayeelist.onClick = scopeObj.onViewActiviyBackToPayeeList.bind(scopeObj, searchView);
            if (response) {
                if (response.length > 0) {
                    this.view.flxBillPayManagePayeeActivity.setVisibility(true);
                    this.view.transferActivitymodified.setVisibility(true);
                    this.view.transferActivitymodified.flxSegmentBillPay.setVisibility(true);
                    this.view.transferActivitymodified.flxNoRecords.setVisibility(false);
                    scopeObj.bindViewActivityData(response);
                    this.view.flxPagination.setVisibility(false);
                    this.view.flxBillPay.setVisibility(false);
                    this.view.flxBillPayManagePayeeActivitymodified.setVisibility(true);
                    this.view.flxBillHeaderMobile.setVisibility(false);
                    this.view.flxEditBillerMobile.setVisibility(false);
                    this.view.forceLayout();
                }
                else {
                    this.view.flxBillPayManagePayeeActivity.setVisibility(false);
                    this.view.transferActivitymodified.setVisibility(true);
                    this.view.flxBillPay.setVisibility(false);
                    this.view.flxPagination.setVisibility(false);
                    CommonUtilities.setText(this.view.transferActivitymodified.lblAmountDeducted, CommonUtilities.formatCurrencyWithCommas("0") , CommonUtilities.getaccessibilityConfig());
                    this.view.transferActivitymodified.flxSegmentBillPay.setVisibility(false);
                    this.view.transferActivitymodified.flxNoRecords.setVisibility(true);
                    this.view.flxBillPayManagePayeeActivitymodified.setVisibility(true);
                    this.view.flxBillHeaderMobile.setVisibility(false);
                    this.view.flxEditBillerMobile.setVisibility(false);
                    FormControllerUtility.hideProgressBar(this.view);
                    this.view.forceLayout();
                }
                CommonUtilities.setText(this.view.customheader.lblHeaderMobile, kony.i18n.getLocalizedString("i18n.billPay.PaymentActivity") , CommonUtilities.getaccessibilityConfig());
            }
            else {
                scopeObj.onError("showPayeeViewActivities : Invalid ViewModel");
            }
        },
        /**
         * bindViewActivityData:   Method to handle view activity data.
         * @param {object} response response
         */
        bindViewActivityData: function (response) {
            function formatBillString(payeeNickName, fromAccountName, billDate) {
                var monthString = kony.i18n.getLocalizedString("i18n.common.none");
                var year = kony.i18n.getLocalizedString("i18n.common.none");
                if (billDate !== "") {
                    var dateObj = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(billDate, applicationManager.getFormatUtilManager().getDateFormat());
                    monthString = dateObj.getMonth()+1;
                    year = dateObj.getFullYear();
                }
                return payeeNickName.split(' ')[0] + '-' + fromAccountName.split(' ')[0] + '-' + monthString + '-' + year;
            }
            var self = this;
            if (response.length === 0) {
                return;
            }
            var payeeName = response[0].payeeName ? response[0].payeeName : response[0].payeeNickName;
            var payeeAccountNumber = response[0].payeeAccountNumber ? response[0].payeeAccountNumber : kony.i18n.getLocalizedString("i18n.common.none");
            var totalAmount = response[0].billPaidAmount ? self.formatAmount(response[0].billPaidAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(response[0].transactionCurrency)) : kony.i18n.getLocalizedString("i18n.common.none")
            var dataMap = {
                "lblDate": "lblDate",
                "flxbills": "flxbills",
                "btnBills": "btnBills",
                "lblpaiddate": "lblpaiddate",
                "lblFrom": "lblFrom",
                "lblAmount": "lblAmount",
                "lblStatus": "lblStatus",
                "lblFromHeader": "lblFromHeader",
                "lblAmountHeader": "lblAmountHeader",
                "lblAmount1": "lblAmount1",
                "lblSeperator": "lblSeperator",
                "template": (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) ? "flxtransferactivitymobile" : "flxSort",
            };
            response = response.map(function (dataItem) {
                var payeeNickName = dataItem.payeeNickName ? dataItem.payeeNickName : kony.i18n.getLocalizedString("i18n.common.none");
                var fromAccountName = dataItem.fromAccountName ? dataItem.fromAccountName : kony.i18n.getLocalizedString("i18n.common.none");
                var billGeneratedDate = dataItem.billGeneratedDate ? dataItem.billGeneratedDate : kony.i18n.getLocalizedString("i18n.common.none");
                return {
                    "lblDate": dataItem.billGeneratedDate ? self.getDateFromDateString(dataItem.billGeneratedDate, "YYYY-MM-DDTHH:MM:SS") : kony.i18n.getLocalizedString("i18n.common.none"),
                    "flxbills": {
                        "onClick": applicationManager.getConfigurationManager().canViewPastEBills === 'true' && dataItem.eBillStatus == 1 ? self.viewEBill.bind(self, {
                            "billGeneratedDate": dataItem.billGeneratedDate ? self.getDateFromDateString(dataItem.billGeneratedDate, "YYYY-MM-DDTHH:MM:SS") : kony.i18n.getLocalizedString("i18n.common.none"),
                            "amount": dataItem.billDueAmount ? self.formatAmount(dataItem.billDueAmount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) : kony.i18n.getLocalizedString("i18n.common.none"),
                            "ebillURL": dataItem.ebillURL
                        }) : null
                    },
                    "btnBills": {
                        "text": formatBillString(payeeNickName, fromAccountName, self.getDateFromDateString(billGeneratedDate, "YYYY-MM-DDTHH:MM:SS")) //TO-DO: Formatted string with Account name, Month, year
                    },
                    "lblpaiddate": dataItem.billPaidDate ? self.getDateFromDateString(dataItem.billPaidDate, "YYYY-MM-DDTHH:MM:SS") : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblFrom": dataItem.fromAccountName ? dataItem.fromAccountName : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblAmount": dataItem.amount ? self.formatAmount(dataItem.amount,false,applicationManager.getFormatUtilManager().getCurrencySymbol(dataItem.transactionCurrency)) : kony.i18n.getLocalizedString("i18n.common.none"),
                    "lblStatus": dataItem.statusDescription ? dataItem.statusDescription : kony.i18n.getLocalizedString("i18n.common.none"),
                    "template": "flxSort"
                };
            });
            CommonUtilities.setText(this.view.transferActivitymodified.lblHeader, kony.i18n.getLocalizedString("i18n.billPay.PaymentActivity") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.transferActivitymodified.lblFromTitle, kony.i18n.getLocalizedString("i18n.BillPay.BillerName") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.transferActivitymodified.lblAccountName, payeeName , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.transferActivitymodified.lblAccountHolder, payeeAccountNumber ? payeeAccountNumber : " " , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.transferActivitymodified.lblAmountDeductedTitle, kony.i18n.getLocalizedString("i18n.billpay.amountPaidTillDate") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.transferActivitymodified.lblAmountDeducted, totalAmount , CommonUtilities.getaccessibilityConfig());
            this.view.transferActivity.setVisibility(false);
            this.view.flxBillPayManagePayeeActivity.setVisibility(false);
            this.view.transferActivitymodified.segBillPayActivity.setVisibility(true);
            this.view.transferActivitymodified.tablePaginationBillpay.setVisibility(false);
            this.view.transferActivitymodified.segBillPayActivity.widgetDataMap = dataMap;
            this.view.transferActivitymodified.segBillPayActivity.setData(response);
            this.onBreakpointChange(kony.application.getCurrentBreakpoint());
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * used to navigate the payees page
         * @param {boolean} searchView seacrh status
         */
        onViewActiviyBackToPayeeList: function (searchView) {
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
            }, {
                text: kony.i18n.getLocalizedString("i18n.billPay.ManagePayee")
            }]);
            this.view.transferActivitymodified.flxSegmentBillPay.setVisibility(false);
            this.view.transferActivitymodified.flxNoRecords.setVisibility(false);
            if (searchView) {
                this.view.flxPagination.setVisibility(true);
            }
            else {
                this.view.flxPagination.setVisibility(false);
            }
            this.view.flxBillPay.setVisibility(true);
            CommonUtilities.setText(this.view.customheader.lblHeaderMobile, kony.i18n.getLocalizedString("i18n.billPay.BillPay") , CommonUtilities.getaccessibilityConfig());
            this.view.flxBillPayManagePayeeActivitymodified.setVisibility(false);
            this.view.flxBillPayManagePayeeActivity.setVisibility(false);
            this.view.transferActivitymodified.setVisibility(false);
            this.view.forceLayout();
            this.AdjustScreen();
        },
        /**
         * used to change the segment templete
         * @param {object} dataModel
         */
        changeSelectedTemplate: function (dataModel) {
            var scopeObj = this;
            var data = dataModel.data;
            var index = dataModel.index;
            if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
                data[index].template = "flxBillPayManagePayeesSelectedMobile";
            } else {
                data[index].template = "flxBillPayManagePayeesSelected";
            }
            scopeObj.view.tableView.segmentBillpay.setDataAt(data[index], index);
            scopeObj.view.forceLayout();
        },
        /**
        * onScheduledSegmentRowClick:    method used to view  respective scheduled bill
        */
        onScheduledSegmentRowClick: function () {
            var scopeObj = this;
            var index = scopeObj.view.tableView.segmentBillpay.selectedRowIndex[1];
            var data = scopeObj.view.tableView.segmentBillpay.data;
            for (var i = 0; i < data.length; i++) {
                if (i === index) {
                    if (data[index].template == "flxBillPayScheduled" || data[index].template == "flxBillPayScheduledMobile") {
                        data[index].imgDropdown = ViewConstants.IMAGES.CHEVRON_UP;
                        data[index].template = (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) ? "flxBillPayScheduledSelectedMobile" : "flxBillPayScheduledSelected";
                    } else {
                        data[index].imgDropdown = ViewConstants.IMAGES.ARRAOW_DOWN;
                        data[index].template = (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) ? "flxBillPayScheduledMobile" : "flxBillPayScheduled";
                    }
                } else {
                    data[i].imgDropdown = ViewConstants.IMAGES.ARRAOW_DOWN;
                    data[i].template = (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) ? "flxBillPayScheduledMobile" : "flxBillPayScheduled";
                }
            }
            scopeObj.view.tableView.segmentBillpay.setData(data);
        },
        /**
        * editDeleteOnClick: Method to delete biller in Edit ManagePayee on click of Delete Biller button.
        * @param {number} offsetVal offset value
        * */
        editDeleteOnClick: function (offsetVal) {
            var scopeObj = this;
            var data = this.view.tableView.segmentBillpay.data;
            var index = this.view.tableView.segmentBillpay.selectedRowIndex[1];
            var deleteData = {
                "payeeId": data[index].payeeId.text
            };
            CommonUtilities.setText(scopeObj.view.deletePopup.lblHeading, kony.i18n.getLocalizedString("i18n.transfers.deleteExternalAccount") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.deletePopup.lblPopupMessage, kony.i18n.getLocalizedString("i18n.billpay.deleteBillerAlert") , CommonUtilities.getaccessibilityConfig());
            var height = scopeObj.view.flxHeader.info.frame.height + scopeObj.view.flxContainer.info.frame.height;
            scopeObj.view.flxDelete.height = height + "dp";
            scopeObj.view.flxDelete.left = "0%";
            scopeObj.view.flxHeader.setFocus(true);
            this.view.deletePopup.btnYes.onClick = function () { //onClick for YES button in popup
                scopeObj.view.flxDelete.left = "-100%";
              	scopeObj.view.flxDelete.setVisibility(false);
                if (deleteData !== null || deleteData.payeeId !== null || deleteData.payeeId !== "") {
                    scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
                    scopeObj.presenter.deleteManagePayee(deleteData, offsetVal);
                }
            };
            this.view.deletePopup.btnNo.onClick = function () { //onClick for NO button in popup
                scopeObj.view.flxDelete.left = "-100%";
              	scopeObj.view.flxDelete.setVisibility(false);
            };
            this.view.deletePopup.flxCross.onClick = function () { //onClick for CLOSE button in popup
                scopeObj.view.flxDelete.left = "-100%";
              	scopeObj.view.flxDelete.setVisibility(false);
            };
          	this.view.flxDelete.setVisibility(true);
            this.AdjustScreen();
        },
        /**
        *  used to deactivate the payee.
        * @param {number} offsetVal
        */
        editDeactivateOnClick: function (offsetVal) {
            var scopeObj = this;
            var data = this.view.tableView.segmentBillpay.data;
            var index = this.view.tableView.segmentBillpay.selectedRowIndex[1];
            if (data !== null && data[index].payeeId !== null && data[index].payeeId !== "") {
                FormControllerUtility.showProgressBar(this.view);
                scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
                scopeObj.presenter.deactivateEbill(data[index].payeeId, offsetVal);
            }
        },
        /**
        * Activates the E-Bill
        * @param {object} data data
        * @param {string} selectedTab selected tab
        */
        activateEbillUI: function (data, selectedTab) {
            var self = this;
            var height = self.view.flxHeader.info.frame.height + self.view.flxContainer.info.frame.height + self.view.flxFooter.info.frame.height;
            self.view.flxActivateBiller.height = height + "dp";
            CommonUtilities.setText(self.view.ActivateBiller.lblAccountNumberValue, data.accountNumber ? data.accountNumber : " " , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(self.view.ActivateBiller.lblBillerNameValue, data.payeeNickName ? data.payeeNickName : data.payeeName , CommonUtilities.getaccessibilityConfig());
            self.view.flxActivateBiller.setVisibility(true);
            self.view.ActivateBiller.lblHeader.setFocus(true);
            self.view.ActivateBiller.btnProceed.onClick = function () {
                FormControllerUtility.showProgressBar(this.view);
                self.view.flxActivateBiller.setVisibility(false);
                self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
                self.presenter.modifyEbillStatus(data, selectedTab);
            };
            self.view.ActivateBiller.btnCancel.onClick = function () {
                self.view.flxActivateBiller.setVisibility(false);
            };
            self.view.ActivateBiller.flxCross.onClick = function () {
                self.view.flxActivateBiller.setVisibility(false);
            };
            self.view.ActivateBiller.flxHeader.setFocus(true);
        },
        /**
        * editSaveOnClick: Method to Save Edited data in Edit ManagePayee on click of Save button.
        */
        editSaveOnClick: function () {
            var scopeObj = this;
            var data = this.view.tableView.segmentBillpay.data;
            var index = this.view.tableView.segmentBillpay.selectedRowIndex[1];
            if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
                data = this.view.segEditBillerMobile.data;
                index = 0;
            }
            var updateData = {
                "payeeId": data[index].payeeId.text,
                "payeeNickName": data[index].txtPayee.text,
                "addressLine1": data[index].txtBankName.text,
                "addressLine2": data[index].txtAddress.text,
                "state": data[index].tbxState.text,
                "zipCode": data[index].tbxPinCode.text,
                "cityName": data[index].txtCity.text
            };
            var isValid;
            if (data[index] !== undefined || data[index] !== null || data[index] !== "" ||
                updateData.payeeId !== null || updateData.payeeId !== "" || updateData.payeeId !== undefined) {
                isValid = true;
            } else {
                isValid = false;
            }
            if (isValid) {
                if (this.validateEditManagePayees(updateData)) {
                    FormControllerUtility.showProgressBar(this.view);
                    scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
                    scopeObj.presenter.updateManagePayee(updateData);
                    data[index].lblError = " ";
                    if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
                        data[index].template = "flxBillPayManagePayeesSelectedMobile";
                    } else {
                        data[index].template = "flxBillPayManagePayeesSelected";
                    }
                    this.view.tableView.setVisibility(true);
                    this.view.flxEditBillerMobile.setVisibility(false);
                    this.view.flxBillHeaderMobile.setVisibility(false);
                    this.view.tableView.segmentBillpay.setDataAt(data[index], index);
                }
                else {
                    data[index].template = kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile ? "flxBillPayManagePayeesEditMobile" : "flxBillPayManagePayeesEdit";
                    data[index].txtPayee = updateData.payeeNickName;
                    data[index].txtBankName.text = updateData.addressLine1 ? updateData.addressLine1 : '';
                    data[index].txtBankName.placeholder = kony.i18n.getLocalizedString("i18n.transfers.bankName");
                    data[index].txtAddress.text = updateData.addressLine2 ? updateData.addressLine2 : '';
                    data[index].txtAddress.placeholder = kony.i18n.getLocalizedString("i18n.ProfileManagement.Address");
                    data[index].txtCity.text = updateData.cityName ? updateData.cityName : '';
                    data[index].txtCity.placeholder = kony.i18n.getLocalizedString("i18n.common.city");
                    data[index].tbxState.text = updateData.state ? updateData.state : '';
                    data[index].tbxState.placeholder = kony.i18n.getLocalizedString("i18n.common.state");
                    data[index].tbxPinCode.text = updateData.zipCode ? updateData.zipCode : '';
                    data[index].tbxPinCode.placeholder = kony.i18n.getLocalizedString("i18n.common.zipcode");
                    data[index].lblError = kony.i18n.getLocalizedString("i18n.common.errorEditData");
                    if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
                        this.view.segEditBillerMobile.setDataAt(data[index], index);
                    }
                    else {
                        this.view.tableView.segmentBillpay.setDataAt(data[index], index);
                    }
                }
            }
            this.view.forceLayout();
        },
        /**
         * changeToEditBiller:  Method to show Edit column on click of EDIT in manage Bill Payee
       */
        changeToEditBiller: function () {
            var data = this.view.tableView.segmentBillpay.data;
            var index = this.view.tableView.segmentBillpay.selectedRowIndex[1];
            for (var i = 0; i < data.length; i++) {
                if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
                    data[i].template = "flxBillPayManagePayeesMobile";
                } else {
                    data[i].template = "flxBillPayManagePayees";
                }
            }
            if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
                data[index].template = "flxBillPayManagePayeesSelectedMobile";
                this.setEditBillerMobileUI(data[index]);
               this.view.flxBillHeaderMobile.height = "100dp";
                this.view.flxEditBillerMobile.top = "110dp";
                this.view.btnEbill.setVisibility(true);
            } else {
                data[index].template = "flxBillPayManagePayeesEdit";
            }
            this.view.tableView.segmentBillpay.setDataAt(data[index], index);
            this.AdjustScreen(100);
        },
        /**
        * Method for Cancel Button in Edit ManagePayee
        * @param {object} dataItem data item
        */
        editCancelOnClick: function (dataItem) {
            var data = this.view.tableView.segmentBillpay.data;
            var index = this.view.tableView.segmentBillpay.selectedRowIndex[1];
            if (kony.application.getCurrentBreakpoint() == 640|| orientationHandler.isMobile) {
                data[index].template = "flxBillPayManagePayeesSelectedMobile";
            } else {
                data[index].template = "flxBillPayManagePayeesSelected";
            }
            data[index].txtPayee = dataItem.payeeNickName;
            data[index].txtBankName = dataItem.addressLine1 ? dataItem.addressLine1 : ' ';
            data[index].txtAddress = dataItem.addressLine2 ? dataItem.addressLine2 : ' ';
            data[index].txtCity = dataItem.cityName ? dataItem.cityName : ' ';
            data[index].tbxState = dataItem.state ? dataItem.state : ' ';
            data[index].tbxPinCode = dataItem.zipCode ? dataItem.zipCode : ' ';
            data[index].lblError = " ";
            this.view.tableView.segmentBillpay.setDataAt(data[index], index);
            this.view.forceLayout();
        },
        /**
        * method used to handle the search functionality
        */
        onSearchBtnClick: function () {
            var scopeObj = this;
            var searchKeyword = scopeObj.view.tableView.Search.txtSearch.text.trim();
            FormControllerUtility.showProgressBar(this.view);
            scopeObj.presenter.searchBillPayPayees({
                'searchKeyword': searchKeyword
            });
            scopeObj.searchView = true;
        },
        /**
        * toggleSearchBox:    used for toggleing search flex.
        * @param {boolean} searchView search view
        */
        toggleSearchBox: function (searchView) {
            this.view.tableView.Search.txtSearch.setFocus(true);
            this.setSearchFlexVisibility(!this.view.tableView.Search.isVisible);
            var scopeObj = this;
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            if (this.view.tableView.tableTabs.btnWithdraws.skin === ViewConstants.SKINS.ACCOUNT_SUMMARY_UNSELECTED || this.searchView === true) {
                FormControllerUtility.showProgressBar(this.view);
                if (searchView === false) {
                    scopeObj.setManagePayeeUI();
                    scopeObj.presenter.showManagePayees();
                } else {
                    scopeObj.setManagePayeeUI();
                    scopeObj.presenter.searchBillPayPayees();
                }
            }
            if (!this.view.tableView.Search.isVisible) {
                this.searchView = false;
                this.prevSearchText = '';
                scopeObj.presenter.showManagePayees();
            } else {
                scopeObj.presenter.searchBillPayPayees();
            }
            this.AdjustScreen();
        },
        /**
        *method used to enable or disable the search button.
        * @param {object} event event
        */
        onTxtSearchKeyUp: function (event) {
            var scopeObj = this;
            var searchKeyword = scopeObj.view.tableView.Search.txtSearch.text.trim();
            if (searchKeyword.length > 0) {
                scopeObj.enableSearch();
            } else {
                scopeObj.disableSearch();
            }
        },
        /** used to disable the search button.
         */
        disableSearch: function () {
            // FormControllerUtility.disableButton(this.view.tableView.Search.btnConfirm);
            this.view.tableView.Search.flxClearBtn.setVisibility(false);
        },
        /**
         * used to enable the search button.
        */
        enableSearch: function () {
            // FormControllerUtility.enableButton(this.view.tableView.Search.btnConfirm);
            this.view.tableView.Search.flxClearBtn.setVisibility(true);
        },
        /**
        *  method used to call the service.
       */
        onSearchClearBtnClick: function () {
            var scopeObj = this;
            scopeObj.view.tableView.Search.txtSearch.text = "";
          scopeObj.view.tableView.Search.flxClearBtn.setVisibility(false);
            if (this.searchView === true) {
                FormControllerUtility.showProgressBar(this.view);
                scopeObj.managePayeesOnClick();
            }
            this.searchView = false;
        },
        /**
         * used to display search flex visibility.
         * @param {boolean} flag status
         */
        setSearchFlexVisibility: function (flag) {
            if (typeof flag === "boolean") {
                this.view.tableView.Search.txtSearch.placeholder = kony.i18n.getLocalizedString("i18n.transfers.searchTransferPayees");
                this.view.tableView.imgSearch.src = flag ? ViewConstants.IMAGES.SELECTED_SEARCH : ViewConstants.IMAGES.SEARCH_BLUE;
                this.view.tableView.Search.setVisibility(true);
                this.view.tableView.Search.txtSearch.text = '';
                this.view.tableView.Search.txtSearch.setFocus(true);
            }
            this.AdjustScreen();
        },
        /**
         * onFrequencyChanged:   Method for handling On Frequeny Change
        */
        onFrequencyChanged: function () {
            var self = this;
            self.getFrequencyAndFormLayout(this.view.payABill.lbxFrequency.selectedKey,
                this.view.payABill.lbxForHowLong.selectedKey);
            self.checkValidityBillPay();
        },
        /**
       * onHowLongChange:  Method for handling On onHowLongChange Change
       */
        onHowLongChange: function () {
            var self = this;
            self.getForHowLongandFormLayout(this.view.payABill.lbxForHowLong.selectedKey);
            self.checkValidityBillPay();
        },
        /**
        * checkValidityBillPay:  Method for handling On onHowLongChange Change
        */
        checkValidityBillPay: function () {
            var self = this;
            var disableConfirmButton = function () {
                FormControllerUtility.disableButton(self.view.payABill.btnConfirm);
            }.bind(this);
            if (!this.singleBillPayAmountField.isValidAmount()) {
                disableConfirmButton();
                return;
            }
            if (this.view.payABill.lbxFrequency.selectedKey !== "Once" && this.view.payABill.lbxForHowLong.selectedKey === "NO_OF_RECURRENCES" && this.view.payABill.txtEndingOn.text === "") {
                disableConfirmButton();
                return;
            }
            if (this.view.payABill.txtEndingOn.text && (isNaN(this.view.payABill.txtEndingOn.text) || parseInt(this.view.payABill.txtEndingOn.text) <= 0)) {
                disableConfirmButton();
                return;
            }
            FormControllerUtility.enableButton(self.view.payABill.btnConfirm);
        },
        /**
        * getFrequencyAndFormLayout:  Method for handling On onHowLongChange Change
        * @param {String} frequencyValue frequency value
        * @param {String} howLangValue has long value
        */
        getFrequencyAndFormLayout: function (frequencyValue, howLangValue) {
            if (frequencyValue !== "Once" && howLangValue !== 'NO_OF_RECURRENCES') {
                this.makeLayoutfrequencyWeeklyDate();
            } else if (frequencyValue !== "Once" && howLangValue === 'NO_OF_RECURRENCES') {
                this.makeLayoutfrequencyWeeklyRecurrences();
            } else {
                this.makeLayoutfrequencyOnce();
            }
        },
        /**
         * getForHowLongandFormLayout:  Method for handling On onHowLongChange Change
         * @param {object} value value
         */
        getForHowLongandFormLayout: function (value) {
            if (value === "ON_SPECIFIC_DATE") {
                this.makeLayoutfrequencyWeeklyDate();
            } else if (value === "NO_OF_RECURRENCES") {
                this.makeLayoutfrequencyWeeklyRecurrences();
            } else if (value === "CONTINUE_UNTIL_CANCEL") {
                this.makeLayoutfrequencyWeeklyCancel();
            }
        },
        /**
        * makeLayoutfrequencyWeeklyDate:  Method for handling On onHowLongChange Change
        */
        makeLayoutfrequencyWeeklyDate: function () {
            this.view.payABill.lblForHowLong.setVisibility(true);
            this.view.payABill.lbxForHowLong.setVisibility(true);
            CommonUtilities.setText(this.view.payABill.lblSendOn, kony.i18n.getLocalizedString("i18n.transfers.start_date") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.payABill.lblEndingOnRec, kony.i18n.getLocalizedString("i18n.transfers.end_date") , CommonUtilities.getaccessibilityConfig());
            this.view.payABill.calEndingOnRec.setVisibility(true);
            this.view.payABill.lblEndingOnRec.setVisibility(true);
            this.view.payABill.txtEndingOn.setVisibility(false);
            this.view.payABill.CalDeliverBy.setVisibility(false);
            this.view.payABill.lblDelieverBy.setVisibility(false);
            this.view.payABill.txtEndingOn.text = "";
            this.AdjustScreen();
            this.view.forceLayout();
        },
        /**
         * used to show the frequency weekly recuurences
         */
        makeLayoutfrequencyWeeklyRecurrences: function () {
            this.view.payABill.lblForHowLong.setVisibility(true);
            this.view.payABill.lbxForHowLong.setVisibility(true);
            this.view.payABill.calEndingOnRec.setVisibility(false);
            this.view.payABill.lblEndingOnRec.setVisibility(true);
            this.view.payABill.txtEndingOn.setVisibility(true);
            this.view.payABill.CalDeliverBy.setVisibility(false);
            this.view.payABill.lblDelieverBy.setVisibility(false);
            CommonUtilities.setText(this.view.payABill.lblSendOn, kony.i18n.getLocalizedString("i18n.transfers.send_on") , CommonUtilities.getaccessibilityConfig());
           CommonUtilities.setText(this.view.payABill.lblEndingOnRec, kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences") , CommonUtilities.getaccessibilityConfig());
          this.AdjustScreen();
        },
        /**
         * makeLayoutfrequencyOnce:   used to diplay tableView flex
         */
        makeLayoutfrequencyOnce: function () {
            CommonUtilities.setText(this.view.payABill.lblSendOn, kony.i18n.getLocalizedString("i18n.transfers.send_on") , CommonUtilities.getaccessibilityConfig());
            this.view.payABill.lblForHowLong.setVisibility(false);
            this.view.payABill.lbxForHowLong.setVisibility(false);
            this.view.payABill.lblEndingOnRec.setVisibility(false);
            this.view.payABill.txtEndingOn.setVisibility(false);
            this.view.payABill.calEndingOnRec.setVisibility(false);
            this.view.payABill.CalDeliverBy.setVisibility(true);
            this.view.payABill.lblDelieverBy.setVisibility(true);
          this.AdjustScreen();
            this.view.forceLayout();
        },
        /**
         * method to handle the cancel the schedule transaction actvity.
         * @param {object} dataItem dataItem
         */
        onScheduledCancelBtnClick: function (dataItem) {
            var scopeObj = this;
            var params = {
                transactionId: dataItem.transactionId,
                transactionType: dataItem.transactionType
            };
            scopeObj.showDeletePopup();
            scopeObj.view.deletePopup.btnYes.onClick = scopeObj.deleteScheduledTransaction.bind(scopeObj, params);
            scopeObj.view.deletePopup.btnNo.onClick = function () {
                scopeObj.view.flxDelete.left = "-100%";
            };
            scopeObj.view.deletePopup.flxCross.onClick = function () {
                scopeObj.view.flxDelete.left = "-100%";
            };
        },
        /**
         * showDeletePopup ui
         */
        showDeletePopup: function () {
            var scopeObj = this;
            CommonUtilities.setText(scopeObj.view.deletePopup.lblHeading, kony.i18n.getLocalizedString("i18n.transfers.Cancel") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.deletePopup.lblPopupMessage, kony.i18n.getLocalizedString("I18n.billPay.QuitTransactionMsg") , CommonUtilities.getaccessibilityConfig());
            scopeObj.view.flxDelete.left = "0%";
            scopeObj.view.deletePopup.lblHeading.setFocus(true);
            scopeObj.view.flxDelete.height = scopeObj.view.flxHeader.info.frame.height + scopeObj.view.flxContainer.info.frame.height + scopeObj.view.flxFooter.info.frame.height + "dp";
        },
        /**
         *  method to handle the delete the schedule transaction.
         *  @param {object} params params
         */
        deleteScheduledTransaction: function (params) {
            var scopeObj = this;
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.view.flxDelete.left = "-100%";
            scopeObj.presenter.deleteScheduledTransaction(params);
        },
        /**
         * Method to handle transaction cancel occurrence action
         * @param {object} data object
         */
        onCancelOccurrence: function (data) {
            var self = this;
            self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            this.showDeletePopup();
            CommonUtilities.setText(this.view.deletePopup.lblPopupMessage, kony.i18n.getLocalizedString("i18n.common.cancelOccurrenceMessage") , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.deletePopup.lblHeading, kony.i18n.getLocalizedString("i18n.transfers.Cancel") , CommonUtilities.getaccessibilityConfig());
            this.view.deletePopup.btnYes.onClick = function () {
                self.view.flxDelete.left = "-100%";
                FormControllerUtility.showProgressBar(self.view);
                self.presenter.cancelScheduledTransactionOccurrence({ transactionId: data.referenceNumber });
            };
            this.view.deletePopup.btnNo.onClick = function () {
                self.view.flxDelete.left = "-100%";
            };
            this.view.deletePopup.imgCross.onTouchEnd = function () {
                self.view.flxDelete.left = "-100%";
            };
        },
        /**
         * Manage payee biller name sorting handler
         * @param {object} event
         * @param {object} data
         */
        onManagePayeeBillerNameClickHandler: function (event, data) {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(this.view);
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.presenter.managePayeePagination(data);
            scopeObj.setSearchFlexVisibility(false);
        },
        /**
        * On Scheduled sort Click handler
        * @param {object} event
        * @param {object} data
        */
        onScheduledSortClickHandler: function (event, data) {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(this.view);
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.presenter.fetchScheduledBills(data);
            scopeObj.setSearchFlexVisibility(false);
        },
        /**
         * On History Sort Click handler
         * @param {object} event
         * @param {object} data
         */
        onHistorySortClickHandler: function (event, data) {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(this.view);
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.presenter.fetchUserBillPayHistory(data);
            scopeObj.setSearchFlexVisibility(false);
        },
        /**
         * On Payment Due Sort Click handler
         *  @param {object} event event
         *  @param {object} data data
         */
        onPaymentDueSortClickHandler: function (event, data) {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(this.view);
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.presenter.getPayementBills(data, true);
            scopeObj.setSearchFlexVisibility(false);
        },
        /**
         * On All payees Sort Click handler
         * @param {object} event event
         * @param {object} data data
         */
        onAllPayeeSortClickHanlder: function (event, data) {
            var scopeObj = this;
            FormControllerUtility.showProgressBar(this.view);
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.presenter.showAllPayees(data);
        },
        /**
        * used to navigate the add Payee screen
        */
        onClickAddPayee: function () {
            var scopeObj = this;
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.presenter.showBillPayData(null, "AddPayee", true);
        },
        /**
         * Method for handling one time payment activity and set data for that flow.
         * @param {object} data onetime object
         * @param {object}  sender sender object
         */
        setDataForShowOneTimePayment: function (data, sender) {
            var self = this;
            self.showOneTimePayement();
            self.setOneTimePayementVaules(data, sender);
          	self.view.flxrightcontainerbillpay.top = "30dp";
            this.view.oneTimePayment.flxAddOrCancelKA.btnNext.onClick =  this.getTnCOneTimeBillPay.bind(this, data);
            this.view.forceLayout();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * make a one time payement
         * @param {object} data transaction object
         */
        sendMakeOneTimePayABill: function (data,TnCcontentOneTimeTransfer) {
            var self = this;
            var formattedMakeOneTimePayABill = self.constructOneTimeBillPayObj(data,TnCcontentOneTimeTransfer);
            self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            var deFormatAmount  = self.deformatAmount(self.view.oneTimePayment.flxDetails.txtPaymentAmount.text);
            var result = self.presenter.validateBillPayAmount(parseFloat(deFormatAmount));
            var resultDate = self.validateBillPayDate();
          	var payFromAccount = formattedMakeOneTimePayABill.payFrom;
            var index = payFromAccount.indexOf("(");
            var accountPaid = payFromAccount.substring(0, index);
            CommonUtilities.setText(self.view.ConfirmDefaultAccount.lblPopupMessage, kony.i18n.getLocalizedString("i18n.billPay.setDefaultPopUpBillPay") + " " + accountPaid + kony.i18n.getLocalizedString("i18n.billPay.setDefaultPopUpBillPayee") , CommonUtilities.getaccessibilityConfig());
            if (result.isAmountValid && resultDate.isDateValid) {
                if (this.presenter.getBillPayPreferedAccountNumber() === "") {
                    if (this.presenter.getDefaultBillPayPopUp() === true) {
                        self.setBillPayDefaultAccountWithSingleBillPayConfirm(formattedMakeOneTimePayABill);
                    } else {
                        self.view.flxConfirmDefaultAccount.isVisible = false;
                        self.view.oneTimePayment.flxDowntimeWarning.setVisibility(false);
                        self.presenter.setDataForConfirm(formattedMakeOneTimePayABill);
                    }
                } else {
                    self.view.flxConfirmDefaultAccount.setVisibility(false);
                    self.view.oneTimePayment.flxDowntimeWarning.setVisibility(false);
                    self.presenter.setDataForConfirm(formattedMakeOneTimePayABill);
                }
            } else if(!resultDate.isDateValid){
              	var message = kony.i18n.getLocalizedString("i18n.transfers.errors.invalidDeliverByDate");
                this.showErrorFlex(message);
            } else {
                self.view.oneTimePayment.rtxDowntimeWarningDomestic.text = result.errMsg;
                self.view.oneTimePayment.flxDowntimeWarning.setVisibility(true);
                self.AdjustScreen();
                return;
            }
        },
      	/**
        * used to check Date validations for OneTime Payment.
        */
      	validateBillPayDate : function(){
        	var resultDate = {
            	isDateValid: false
        	};
        	var sendOnDate = this.getDateObj(this.view.oneTimePayment.flxCalSendOn.calSendOn.dateComponents);
        	var deliverByDate = this.getDateObj(this.view.oneTimePayment.flxCalEndingOn.calEndingOn.dateComponents);
        	if(sendOnDate.getTime() <= deliverByDate.getTime()){
          		resultDate.isDateValid = true;
        	}
        	return resultDate;
      	},
        /**
         * constructing OneTime BillPay Object
         * @param {object} data data
         * @returns {object} formattedOneTimePayment
         */
        constructOneTimeBillPayObj: function (data,TnCcontentOneTimeTransfer) {
            var self = this;
            var formattedOneTimePayment = {
                "payeeId": data.payeeId,
                "payFrom": self.view.oneTimePayment.flxDetails.lbxPayFrom.selectedKeyValue[1],
                "fromAccountNumber": self.view.oneTimePayment.flxDetails.lbxPayFrom.selectedKeyValue[0],
                "payeeName": self.view.oneTimePayment.lblToOne.text,
                "amount": self.view.oneTimePayment.flxDetails.txtPaymentAmount.text,
                "sendOn": self.view.oneTimePayment.flxDetails.flxCalSendOn.calSendOn.formattedDate,
                "deliveryDate": self.view.oneTimePayment.flxDetails.flxCalEndingOn.calEndingOn.formattedDate,
                "notes": self.view.oneTimePayment.flxDetails.txtNotes.text,
                "payeeNickname": data.payeeNickname,
                "referenceNumber": data.referenceNumber,
                "accountNumber": data.accountNumber,
                "billerName": data.billerName,
                "mobileNumber": data.mobileNumber,
                "gettingFromOneTimePayment": true,
                "statusOfDefaultAccountSetUp": false,
                "defaultAccountBillPay": data.payFrom,
                "zipCode" : data.zipCode,
                "billerId" : data.billerId,
                "TnCcontentTransfer":TnCcontentOneTimeTransfer
            };
            return formattedOneTimePayment;
        },
        /**
        * onClickOneTimePayement:   Used to naviage the oneTimePayment Flow.
        * @param {object} isCancel is cancel
        */
        onClickOneTimePayement: function (isCancel) {
            var scopeObj = this;
            scopeObj.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            scopeObj.presenter.navigateToOneTimePayement(isCancel);
            scopeObj.view.flxBillPay.top = "0dp";
        },
        /**
        *  Method for handling one time payment activity Ui activities
        */
        showOneTimePayement: function () {
            this.setSkinInActive(this.view.tableView.tableTabs.btnTransfers);
            this.setSkinInActive(this.view.tableView.tableTabs.BtnAll);
            this.view.Activatebillpays.flxActivateAcknowledgement.setVisibility(false);
            this.view.oneTimePayment.flxDowntimeWarning.setVisibility(false);
            this.view.flxOneTImePayment.setVisibility(true);
           this.view.flxBillHeaderMobile.setVisibility(false);
          	this.view.flxEditBillerMobile.setVisibility(false);
          this.view.flxOneTImePayment.top="10dp";
            FormControllerUtility.disableButton(this.view.oneTimePayment.btnNext);
            this.view.oneTimePayment.flxDetails.txtPaymentAmount.text = "";
            this.view.oneTimePayment.flxSearchPayee.setVisibility(false);
            this.view.oneTimePayment.flxDetails.setVisibility(true);
            this.view.flxConfirmButton.setVisibility(false);
            this.view.btnConfirm.setVisibility(false);
            this.view.flxPagination.setVisibility(false);
            this.view.oneTimePayment.lblCategoryTitle.setVisibility(false);
            this.view.oneTimePayment.lbxCategory.setVisibility(false);
            // this.view.flxBottom.height = "140dp";
            this.view.flxTermsAndConditions.top = "30dp";
            this.view.payABill.isVisible = false;
            this.view.breadcrumb.setBreadcrumbData([{
                text: kony.i18n.getLocalizedString("i18n.billPay.BillPay")
            }, {
                text: kony.i18n.getLocalizedString("i18n.BillPay.ONETIMEPAYMENT")
            }]);
            this.view.tableView.isVisible = false;
            this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
            this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.BillPay.ONETIMEPAYMENT");
            this.view.oneTimePayment.flxAddOrCancelKA.setVisibility(true);
            CommonUtilities.disableOldDaySelection(this.view.oneTimePayment.flxDetails.flxCalSendOn.calSendOn);
            CommonUtilities.disableOldDaySelection(this.view.oneTimePayment.flxDetails.flxCalEndingOn.calEndingOn);
            var dateFormat = applicationManager.getFormatUtilManager().getDateFormat();
            var today = CommonUtilities.getServerDateObject();
            var date = [today.getDate(), today.getMonth()+1, today.getFullYear()];
            this.view.oneTimePayment.flxDetails.flxCalSendOn.calSendOn.dateFormat = dateFormat;
			this.view.oneTimePayment.flxDetails.flxCalEndingOn.calEndingOn.dateFormat = dateFormat;
            this.view.oneTimePayment.flxDetails.flxCalSendOn.calSendOn.dateComponents = date;
			this.view.oneTimePayment.flxDetails.flxCalEndingOn.calEndingOn.dateComponents = date;
            this.oneTimeBillPayAmountField = FormControllerUtility.wrapAmountField(this.view.oneTimePayment.flxDetails.txtPaymentAmount)
                .onKeyUp(this.checkValidOneTimeAmount.bind(this));
            this.view.oneTimePayment.btnCancel.onClick = this.onClickOneTimePayement.bind(this, true);
        },
        /**
        * used to check all field validations for OneTime Payment.
        */
        checkValidOneTimeAmount: function () {
            var self = this;
            if (self.view.oneTimePayment.flxDowntimeWarning.isVisible) {
                self.view.oneTimePayment.flxDowntimeWarning.setVisibility(false);
                self.view.forceLayout();
            }
            if (!this.oneTimeBillPayAmountField.isValidAmount()) {
                FormControllerUtility.disableButton(this.view.oneTimePayment.btnNext);
            } else {
                FormControllerUtility.enableButton(this.view.oneTimePayment.btnNext);
            }
        },
        /**
         * used to set the oneTime payement values
         * @param {object} data payement object
         * @param {object} sender sender
         */
        setOneTimePayementVaules: function (data, sender) {
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
            this.view.oneTimePayment.flxDetails.lbxPayFrom.selectedKey = data.fromAccountNumber ? data.fromAccountNumber : this.view.oneTimePayment.flxDetails.lbxPayFrom.masterData[0][0];
            this.view.oneTimePayment.flxDetails.txtNotes.text = data.notes ? data.notes : '';
            CommonUtilities.setText(this.view.oneTimePayment.lblAccountNumberValue, data.accountNumber ? data.accountNumber : '' , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.oneTimePayment.lblToOne, data.billerName ? data.billerName : '' , CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.oneTimePayment.lbloTwo, data.mobileNumber ? data.mobileNumber : '' , CommonUtilities.getaccessibilityConfig());
            var billPayAccounts = this.presenter.getBillPaySupportedAccounts();
            this.view.oneTimePayment.flxDetails.lbxPayFrom.masterData = FormControllerUtility.getListBoxDataFromObjects(billPayAccounts, "accountID", CommonUtilities.getAccountDisplayNameWithBalance);
            this.setAmounForOneTimeAmount(data, sender);
        },
        /**
         * used to format the amount
         * @param {string} amount amount
         * @param {boolean} currencySymbolNotRequired currency symbol required
         * @returns {string} formated amount
        */
        formatAmount: function (amount, currencySymbolNotRequired,currencySymbol) {
             this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
             return this.presenter.formatAmount(amount, currencySymbolNotRequired);
        },
        /**
    * setting amount value
    * @param {object} data data
    * @param {object} context context
    */
        setAmounForOneTimeAmount: function (data, context) {
            var self = this;
            if (data.amount) {
                if (context === "acknowledgement") {
                    this.view.oneTimePayment.flxDetails.txtPaymentAmount.text = '';
                    FormControllerUtility.disableButton(self.view.payABill.btnConfirm);
                } else {
                    this.view.oneTimePayment.flxDetails.txtPaymentAmount.text = data.amount ? data.amount : '';
                    FormControllerUtility.enableButton(self.view.payABill.btnConfirm);
                }
            } else {
                this.view.oneTimePayment.flxDetails.txtPaymentAmount.text = '';
                FormControllerUtility.disableButton(self.view.payABill.btnConfirm);
            }
        },
        /**
        * Method to validate data entered by user while editing manage payees
        * @param {object} updateData update data
        * @returns {boolean} status
        */
        validateEditManagePayees: function (updateData) {
            var isValid = false;
            if (updateData.payeeNickName) {
                isValid = true;
            }
            if (updateData.addressLine1) {
                isValid = true;
            }
            if (updateData.state) {
                if (/^[a-zA-Z]+$/.test(updateData.state))
                    isValid = true;
                else
                    isValid = false;
            }
            if (updateData.cityName) {
                if (/^[a-zA-Z]+$/.test(updateData.cityName))
                    isValid = true;
                else
                    isValid = false;
            }
            if (updateData.zipCode) {
                isValid = true;
            } else {
                isValid = false;
            }
            return isValid;
        },
        responsiveViews: {},
        initializeResponsiveViews: function () {
            this.responsiveViews["flxDowntimeWarning"] = this.isViewVisible("flxDowntimeWarning");
            this.responsiveViews["flxBillPay"] = this.isViewVisible("flxBillPay");
            this.responsiveViews["tableView"] = this.isViewVisible("tableView");
            this.responsiveViews["payABill"] = this.isViewVisible("payABill");
            this.responsiveViews["flxBillPayManagePayeeActivitymodified"] = this.isViewVisible("flxBillPayManagePayeeActivitymodified");
            this.responsiveViews["flxOneTImePayment"] = this.isViewVisible("flxOneTImePayment");
            this.responsiveViews["flxActivatebillpays"] = this.isViewVisible("flxActivatebillpays");
            this.responsiveViews["flxViewEbill"] = this.isViewVisible("flxViewEbill");
            this.responsiveViews["flxConfirmDefaultAccount"] = this.isViewVisible("flxConfirmDefaultAccount");
            this.responsiveViews["flxDelete"] = this.isViewVisible("flxDelete");
            this.responsiveViews["flxActivateBiller"] = this.isViewVisible("flxActivateBiller");
            this.responsiveViews["flxBillPayManagePayeeActivity"] = this.isViewVisible("flxBillPayManagePayeeActivity");
            this.responsiveViews["flxrightcontainerbillpay"] = this.isViewVisible("flxrightcontainerbillpay");
            this.responsiveViews["flxTotalEbillAmountDue"] = this.isViewVisible("flxTotalEbillAmountDue");
            this.responsiveViews["flxaddpayeemakeonetimepayment"] = this.isViewVisible("flxaddpayeemakeonetimepayment");
            this.responsiveViews["flxConfirmButton"] = this.isViewVisible("flxConfirmButton");
        },
        isViewVisible: function (container) {
            if (this.view[container].isVisible) {
                return true;
            } else {
                return false;
            }
        },
        setCalendarContexts: function () {
            var contextPayABillCalSendOn = {
                "widget": this.view.payABill.calSendOn,
                "anchor": "bottom"
            };
            this.view.payABill.calSendOn.setContext(contextPayABillCalSendOn);
            var contextPayABillCalEndingOn = {
                "widget": this.view.payABill.calEndingOnRec,
                "anchor": "bottom"
            };
            this.view.payABill.calEndingOnRec.setContext(contextPayABillCalEndingOn);
            var contextPayABillCalDeliverBy = {
                "widget": this.view.payABill.CalDeliverBy,
                "anchor": "bottom"
            };
            this.view.payABill.CalDeliverBy.setContext(contextPayABillCalDeliverBy);
            var contextPayABillCalSendOnMod = {
                "widget": this.view.payABill.calSendOnmod,
                "anchor": "bottom"
            };
            this.view.payABill.calSendOnmod.setContext(contextPayABillCalSendOnMod);
            var contextPayABillCalDeliverByMod = {
                "widget": this.view.payABill.CalDeliverBymod,
                "anchor": "bottom"
            };
            this.view.payABill.CalDeliverBymod.setContext(contextPayABillCalDeliverByMod);
            var contextOTPCalSendOn = {
                "widget": this.view.oneTimePayment.calSendOn,
                "anchor": "bottom"
            };
            this.view.oneTimePayment.calSendOn.setContext(contextOTPCalSendOn);
            var contextOTPCalDeliverBy = {
                "widget": this.view.oneTimePayment.calEndingOn,
                "anchor": "bottom"
            };
            this.view.oneTimePayment.calEndingOn.setContext(contextOTPCalDeliverBy);
        },
        /**
        * onBreakpointChange : Handles ui changes on .
        * @member of {frmBillPayController}
        * @param {integer} width - current browser width
        * @return {}
        * @throws {}
        */
        onBreakpointChange: function (width) {
            kony.print('on breakpoint change');
            orientationHandler.onOrientationChange(this.onBreakpointChange);
            this.view.customheader.onBreakpointChangeComponent(width);
            this.setupFormOnTouchEnd(width);
            var scope = this;
            var views;
            var data;
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup,width);
            this.view.deletePopup.onBreakpointChangeComponent(scope.view.deletePopup,width);
            this.view.CustomPopupCancel.onBreakpointChangeComponent(scope.view.CustomPopupCancel,width);
            this.view.tableView.tableSubHeaderHistory.skin = "sknFlexF9F9F9";
            this.view.tableView.tableSubHeaderHistory.flxSortSixColumn.skin = "sknFlexF9F9F9";
            this.view.payABill.skin = "slFbox";
            this.view.tableView.Search.txtSearch.skin = "skntbxSSP42424215pxnoborder";
            var responsiveFonts = new ResponsiveFonts();
            var mobileTemplates = {
                "flxBillPayAllPayees": "flxBillPayAllPayeesMobile",
                "flxBillPayAllPayeesSelected": "flxBillPayAllPayeesSelectedMobile",
                "flxBillPayScheduled": "flxBillPayScheduledMobile",
                "flxBillPayScheduledSelected": "flxBillPayScheduledSelectedMobile",
                "flxMain": "flxBillPayHistoryMobile",
                "flxContainer": "flxBillPayHistoryDetailsMobile",
                "flxBillPayManagePayees": "flxBillPayManagePayeesMobile",
                "flxBillPayManagePayeesEdit": "flxBillPayManagePayeesMobile",
                "flxBillPayManagePayeesSelected": "flxBillPayManagePayeesSelectedMobile",
                "flxSort": "flxtransferactivitymobile"
            };
            var desktopTemplates = {
                "flxBillPayAllPayeesMobile": "flxBillPayAllPayees",
                "flxBillPayScheduledMobile": "flxBillPayScheduled",
                "flxBillPayScheduledSelectedMobile": "flxBillPayScheduledSelected",
                "flxBillPayHistoryMobile": "flxMain",
                "flxBillPayHistoryDetailsMobile": "flxContainer",
                "flxBillPayManagePayeesMobile": "flxBillPayManagePayees",
                "flxBillPayManagePayeesSelectedMobile": "flxBillPayManagePayeesSelected",
                "flxBillPayManagePayeesEditMobile": "flxBillPayManagePayees",
                "flxtransferactivitymobile": "flxSort"
            };
            if (width === 640 || orientationHandler.isMobile) {
                responsiveFonts.setMobileFonts();
                views = Object.keys(this.responsiveViews);
                views.forEach(function (e) {
                    scope.view[e].isVisible = scope.responsiveViews[e];
                });
                this.view.payABill.calSendOn.padding=[3,0,0,0];
                this.view.payABill.CalDeliverBy.padding=[3,0,0,0];
                this.view.payABill.calSendOn.contentAlignment= constants.CONTENT_ALIGN_MIDDLE_LEFT;
                this.view.payABill.CalDeliverBy.contentAlignment= constants.CONTENT_ALIGN_MIDDLE_LEFT;
              	this.view.AllForms.top = this.view.Activatebillpays.flxDefaultAccount.info.frame.y + 30 + "dp";
                this.view.AllForms.left = this.view.Activatebillpays.flxinfo.info.frame.x - 130 + "dp";
                this.view.payABill.lblNotes.left="10dp";
                this.view.payABill.lblNotes.width="";
                this.view.payABill.txtNotes.left="10dp";
                this.view.payABill.txtNotes.width="";
                this.view.payABill.txtNotes.right="10dp";
                CommonUtilities.setText(this.view.customheader.lblHeaderMobile, "Bill Pay" , CommonUtilities.getaccessibilityConfig());
                this.view.tableView.flxHeader.isVisible = false;
                this.view.tableView.Search.skin = "sknFlexF9F9F9";
                this.view.tableView.Search.isVisible = false;
                this.view.flxrightcontainerbillpay.isVisible = false;
                this.view.payABill.btnViewEbill.isVisible = false;
                //this.view.tableView.segmentBillpay.top = "-40dp";
                this.view.flxConfirmButton.isVisible = false;
                this.view.payABill.btnConfirm.width = "95%";
                this.view.payABill.btnConfirm.left = "2.5%";
                this.view.payABill.btnConfirm.centerY = "";
                this.view.payABill.btnCancel.width = "95%";
                this.view.payABill.btnCancel.left = "2.5%";
                this.view.payABill.btnCancel.top = "80dp";
                this.view.payABill.btnCancel.centerY = "";
                this.view.flxOneTImePayment.top = "120dp";
              this.view.payABill.lblPayFrom.left="10dp";
                this.view.payABill.lblSendOn.left="10dp";
               this.view.oneTimePayment.txtPaymentAmount.padding = [7,0,0,0];
              this.view.flxContainer.top="50dp";
				this.view.transferActivitymodified.flxSort.isVisible = false;
                data = this.view.tableView.segmentBillpay.data;
                if (data == undefined) return;
                data.map(function (e) {
                    if (mobileTemplates[e.template] == undefined)
                        return;
                    e.template = mobileTemplates[e.template];
                });
                this.view.tableView.segmentBillpay.setData(data);
              	this.view.tableView.Search.txtSearch.placeholder = kony.i18n.getLocalizedString("i18n.billpay.SearchMessageMobile")
            } else {
                this.view.oneTimePayment.txtPaymentAmount.padding = [3,0,0,0];
                this.view.payABill.calSendOn.padding=[0,0,0,0];
                this.view.payABill.CalDeliverBy.padding=[0,0,0,0];
                this.view.payABill.calSendOn.contentAlignment= constants.CONTENT_ALIGN_CENTER;
                this.view.payABill.CalDeliverBy.contentAlignment= constants.CONTENT_ALIGN_CENTER;
                this.view.flxBillHeaderMobile.isVisible = false;
                responsiveFonts.setDesktopFonts();
                views = Object.keys(this.responsiveViews);
                views.forEach(function (e) {
                    scope.view[e].isVisible = scope.responsiveViews[e];
                });
                this.view.transferActivitymodified.flxSort.isVisible = true;
                CommonUtilities.setText(this.view.customheader.lblHeaderMobile, "" , CommonUtilities.getaccessibilityConfig());
                this.view.tableView.flxHeader.isVisible = true;
                this.view.tableView.Search.skin = "slFbox";
                this.view.tableView.Search.isVisible = false;
                this.view.flxrightcontainerbillpay.isVisible = true;
                this.view.flxBillHeaderMobile.isVisible = false;
                this.view.tableView.segmentBillpay.top = "0dp";
                this.view.flxBillPay.top = "0dp";
                if (this.view.tableView.isVisible === true && (this.view.tableView.tableTabs.BtnAll.skin === ViewConstants.SKINS.TAB_ACTIVE ||
                    this.view.tableView.tableTabs.btnTransfers.skin === ViewConstants.SKINS.TAB_ACTIVE)) {
                  if(this.view.flxActivatebillpays.isVisible===true){
                    this.view.flxConfirmButton.isVisible = false;
                    this.view.btnConfirm.setVisibility(false);
                  }else{
                    this.view.flxConfirmButton.isVisible = true;
                    this.view.btnConfirm.setVisibility(true);
                  }
                }
                else {
                    this.view.flxConfirmButton.isVisible = false;
                    this.view.btnConfirm.setVisibility(false);
                }
                this.view.payABill.btnConfirm.width = "32%";
                this.view.payABill.btnConfirm.centerY = "50%";
                this.view.payABill.btnConfirm.right = "20dp";
                this.view.payABill.btnConfirm.left = "";
				this.view.flxContainer.top="120dp";
                this.view.payABill.btnCancel.width = "32%";
                this.view.payABill.btnCancel.centerY = "50%";
                this.view.payABill.btnCancel.right = "36%";
                this.view.payABill.btnCancel.left = "";
                this.view.flxOneTImePayment.top = "30dp";
                data = this.view.tableView.segmentBillpay.data;
                if (data == undefined) return;
                data.map(function (e) {
                    if (desktopTemplates[e.template] == undefined)
                        return;
                    e.template = desktopTemplates[e.template];
                });
                this.view.tableView.segmentBillpay.setData(data);
            }
            if (width === 640 || orientationHandler.isMobile) {
                data1 = this.view.transferActivitymodified.segBillPayActivity.data;
                if (data1 == undefined) return;
                data1.map(function(e) {
                    if (mobileTemplates[e.template] == undefined) return;
                    e.template = mobileTemplates[e.template];
                });
                this.view.transferActivitymodified.segBillPayActivity.setData(data1);
            } else {
                data1 = this.view.transferActivitymodified.segBillPayActivity.data;
                if (data1 == undefined) return;
                data1.map(function(e) {
                    if (desktopTemplates[e.template] == undefined) return;
                    e.template = desktopTemplates[e.template];
                });
                this.view.transferActivitymodified.segBillPayActivity.setData(data1);
            }
            CommonUtilities.setText(this.view.TermsAndConditions.lblTermsAndConditions, kony.i18n.getLocalizedString("i18n.BillPay.TermsAndConditions") , CommonUtilities.getaccessibilityConfig());
            this.AdjustScreen();
        },
      setupFormOnTouchEnd: function(width){
        if(width==640){
          this.view.onTouchEnd = function(){}
          this.nullifyPopupOnTouchStart();
        }else{
          if(width==1024){
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }else{
              this.view.onTouchEnd = function(){
                hidePopups();
              }
            }
            var userAgent = kony.os.deviceInfo().userAgent;
            if (userAgent.indexOf("iPad") != -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
              this.view.onTouchEnd = function(){}
              this.nullifyPopupOnTouchStart();
            }
        }
      },
      nullifyPopupOnTouchStart: function(){
      },
      setMobileHeaderDetailsData: function (data) {
        this.view.label1.text = data.title1;
        this.view.label2.text = data.value1;
        this.view.label3.text = data.title2;
        this.view.label4.text = data.value2;
        CommonUtilities.setText(this.view.btnEbill, data.textBtn1 , CommonUtilities.getaccessibilityConfig());
        this.view.btnEbill.onClick = data.actionBtn1;
        this.view.btnEbill.skin = data.skinBtn1;
        CommonUtilities.setText(this.view.btnAction, data.textBtn2 , CommonUtilities.getaccessibilityConfig());
        this.view.btnAction.isVisible = data.textBtn2===""?false:true;
        this.view.btnAction.onClick = data.actionBtn2;
        this.view.flxBillHeaderMobile.isVisible = true;
        this.view.flxBillPay.top="110dp";
       this.view.payABill.lbxpayee.setVisibility(false);
        this.view.flxPagination.isVisible = false;
        this.AdjustScreen();
        this.view.forceLayout();
    },
      setEditBillerMobileUI: function(data) {
        var scope = this;
        this.view.tableView.isVisible = false;
        data.btnCancel = {
          "text": "Cancel",
          "onClick": function() {
            scope.view.flxBillHeaderMobile.isVisible = false;
            scope.view.flxEditBillerMobile.isVisible = false;
            scope.view.tableView.isVisible = true;
            var data = scope.view.tableView.segmentBillpay.data;
            for (var i = 0; i < data.length; i++) {
              data[i].template = "flxBillPayManagePayeesMobile";
            }
            scope.view.tableView.segmentBillpay.setData(data);
            scope.view.flxBillPay.top = "0px";
            scope.view.forceLayout();
          }
        };
        data.lblHeader = "Bill Payee Details";
        data.lblSeparatorOne = "DummyText";
        data.lblBillerName = "Biller Name:";
        data.lblAddress = "Address:";
        data.lblCity = "City:";
        data.lblState = "State:";
        data.lblCountry = "Country:";
        data.lblZipCode = "Zip Code:";
        data.lblSeparatorTwo = "Dummy Text";
        data.template = "flxBillPayManagePayeesEditMobile";
        var segData = [];
        segData.push(data);
        this.view.segEditBillerMobile.height = "680dp";
        this.view.segEditBillerMobile.setData(segData);
        this.view.flxEditBillerMobile.isVisible = true;
        this.view.flxPagination.isVisible = false;
        this.view.flxBillHeaderMobile.setVisibility(false);

        this.setMobileHeaderDetailsDataEdit({
          "title1": data.lblAccountNumberTitle,
          "value1": data.lblAccountNumberValue,
          "title2": "Biller Address:",
          "value2": data.lblBankAddressOne,
          "textBtn1": data.btnEbill != undefined ? data.btnEbill.text : "",
          "actionBtn1": data.btnEbill != undefined ? data.btnEbill.onClick : "",
          "skinBtn1": data.btnEbill != undefined ? data.btnEbill.skin : "",
          "textBtn2": "",
        });
      },
        setMobileHeaderDetailsDataEdit: function(data) {
            this.view.label1.text = data.title1.text;
            this.view.label2.setVisibility(true);
            this.view.label2.text = data.value1.text;
            this.view.lbxPayee.setVisibility(false);
            this.view.label3.text = data.title2 +":";
             this.view.label4.text=  data.value2.text;
            CommonUtilities.setText(this.view.btnEbill, data.textBtn1, CommonUtilities.getaccessibilityConfig());
            this.view.btnEbill.onClick = data.actionBtn1;
            var index = this.view.tableView.segmentBillpay.selectedRowIndex[1];
            var skin =  this.view.tableView.segmentBillpay.data[index].btnEbill.skin;
            this.view.btnEbill.skin = skin;
            this.view.btnEbill.setVisibility(true);
            CommonUtilities.setText(this.view.btnAction, data.textBtn2, CommonUtilities.getaccessibilityConfig());
            this.view.btnAction.isVisible = data.textBtn2 === "" ? false : true;
            this.view.btnAction.onClick = data.actionBtn2;
            this.view.flxBillHeaderMobile.isVisible = true;
            this.view.flxBillPay.top = "130dp";
            this.view.flxBillHeaderMobile.height="130dp";
            this.view.payABill.lbxpayee.setVisibility(false);
            this.view.flxPagination.isVisible = false;
            this.view.btnEbill.setVisibility(true);
            this.AdjustScreen();
            this.view.forceLayout();
        },
        modifyPayment:function(){
            if(kony.application.getCurrentBreakpoint()===640|| orientationHandler.isMobile){
               if(this.view.flxBillHeaderMobile.isVisible === true){
                if(this.view.lbxPayee.isVisible === true){this.view.flxBillPay.top = "150dp";}
                else{this.view.flxBillPay.top = "110dp";}
               this.view.flxOneTImePayment.top="110dp";
               }
              else{
                 this.view.flxBillPay.top="0dp";
                this.view.flxOneTImePayment.top="10dp";
              }
            }
            else{
                this.view.flxBillPay.top="0dp";
            }
        }
    }
});
