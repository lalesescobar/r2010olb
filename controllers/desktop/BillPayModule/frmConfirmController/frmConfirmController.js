/**
 * Description of Module representing a Confirm form.
 * @module frmConfirmController
 */
define(['commonUtilities','OLBConstants','ViewConstants','FormControllerUtility'], function (commonUtilities,OLBConstants,ViewConstants,FormControllerUtility) {
  var orientationHandler = new OrientationHandler();
  return /** @alias module:frmConfirmController */ {
    /** updates the present Form based on required function.
       * @param {object} viewModel model on which new pathe should decide
       */
    updateFormUI: function (viewModel) {
      if (viewModel.isLoading) {
        FormControllerUtility.showProgressBar(this.view);
      } else {
        FormControllerUtility.hideProgressBar(this.view);
      }
      if (viewModel.bulkPayRecords) {
        this.preShowFrmConfirmBulkPay();
        this.setDataForConfirmBulkPay(viewModel.bulkPayRecords);
        this.bindTnCData(viewModel.bulkPayRecords.TnCcontentTransfer,true);
      }
	    if(viewModel.transferConfirm){
        this.updateTransferConfirmDetails(viewModel.transferConfirm);
      }
      if (viewModel.payABill) {
        this.updateBillPayConfirmForm(viewModel.payABill);
        this.bindSingleBillPayData(viewModel.payABill);
        this.bindTnCData(viewModel.payABill.TnCcontentTransfer,false);
      }
      if (viewModel.ProgressBar) {
        if (viewModel.ProgressBar.show) {
            FormControllerUtility.showProgressBar(this.view);
        }
        else {
            FormControllerUtility.hideProgressBar(this.view);
        }
      }
    },
    /**
     * used perform the initialize activities.
     *
     */
    initActions: function () {
      FormControllerUtility.setRequestUrlConfig(this.view.brwBodyTnC);
      this.view.customheader.topmenu.flxMenu.skin = "slFbox";
      this.view.customheader.topmenu.flxaccounts.skin = "slFbox";
      this.view.customheader.topmenu.flxTransfersAndPay.skin = "sknFlxFFFFFbrdr3343a8";
      this.view.customheader.topmenu.flxContextualMenu.setVisibility(false);
      this.view.customheader.topmenu.flxaccounts.skin = "sknHoverTopmenu7f7f7pointer";
      var scopeObj = this;
      this.view.onBreakpointChange = function () {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      }
      applicationManager.getNavigationManager().applyUpdates(this);
      FormControllerUtility.updateWidgetsHeightInInfo(this, ['customheader','flxMainContainer','flxFooter','confirmDialog.keyValueFrom.flxValue','confirmDialog.keyValueFrom.lblValue','flxHeader']);
    },
          /**
         * used to get the amount
         * @param {number} amount amount
         * @returns {number} amount
         */
        deformatAmount: function(amount)
        {
            return applicationManager.getFormatUtilManager().deFormatAmount(amount);
        },
    /**
     * used to perform the postShow Activiities.
     *
     */
    postShowFrmConfirm: function () {
      var self = this;
      //this.view.confirmButtons.skin = "sknFlxe9ebeeop100";
      this.customiseHeaderComponentforAcknowledgement();
      this.AdjustScreen();
      this.view.confirmDialog.keyValueFrom.flxInfo.onClick = function () {
        self.view.AllForms.left = self.view.confirmDialog.keyValueFrom.flxValue.info.frame.x + self.view.confirmDialog.keyValueFrom.lblValue.info.frame.width - 106 + "dp";
        if (self.view.AllForms.isVisible === true) {
          self.view.AllForms.isVisible = false;
        }
        else {
          self.view.AllForms.isVisible = true;
        }
      };
      this.view.AllForms.flxCross.onClick = function () {
        self.view.AllForms.isVisible = false;
      };
       this.view.customheader.forceCloseHamburger();
    },
    /**
     * breadcrumb hiding logic
     */
    customiseHeaderComponentforAcknowledgement: function () {
      this.view.breadcrumb.imgBreadcrumb2.setVisibility(false);
      this.view.breadcrumb.lblBreadcrumb3.setVisibility(false);
    },
    /**
     * used to perform UI Activities like fith the UI in Screen.
     */
    AdjustScreen: function () {
      this.view.forceLayout();
      var mainheight = 0;
      var screenheight = kony.os.deviceInfo().screenHeight;
      mainheight = this.view.customheader.info.frame.height + this.view.flxMainContainer.info.frame.height;
      var diff = screenheight - mainheight;
      if (mainheight < screenheight) {
        diff = diff - this.view.flxFooter.info.frame.height;
        if (diff > 0) {
          this.view.flxFooter.top = mainheight + diff + "dp";
        }
        else {
          this.view.flxFooter.top = mainheight + "dp";
        }
      } else {
        this.view.flxFooter.top = mainheight + "dp";
      }
      this.view.forceLayout();
    },
    /**
     * used to hide or view the bulkPay screen
     */
    preShowFrmConfirmBulkPay: function () {
      this.view.confirmDialog.confirmButtons.skin="slFbox";
      this.view.breadcrumb.setBreadcrumbData([{
        text: kony.i18n.getLocalizedString("i18n.billPay.BillPay"), callback: this.showBulkPayCancelPopUp
      }, {
        text: kony.i18n.getLocalizedString("i18n.billPay.ConfirmBillPay")
      }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.ConfirmBillPay");
      this.view.flxMainSinglePay.setVisibility(false);
      this.view.flxMainBulkPay.setVisibility(true);
      this.view.flxPopup.setVisibility(false);
      this.view.confirmButtons.skin = "slFbox";
      this.AdjustScreen();
    },
    /**
     * setting data in bulk pay screen
     * @param {object}   bulkPayRecords list of transactions
     */
    setDataForConfirmBulkPay: function (bulkPayRecords) {
      var self = this;
      var billPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
      this.view.confirmDialog.keyValueFrequencyType.setVisibility(false);
      var bulkPayWidgetDataMap = {
        "lblPaymentAccount": "lblPaymentAccount",
        "lblPayee": "lblPayee",
        "lblSendOn": "lblSendOn",
        "lblDeliverBy": "lblDeliverBy",
        "lblAmount": "lblAmount"
      };
      this.view.confirmButtons.btnConfirm.onClick = function () {
          billPayModule.presentationController.createBulkPayments.call(billPayModule.presentationController, bulkPayRecords.records);
      };
      commonUtilities.setText(this.view.CustomPopup.lblHeading, kony.i18n.getLocalizedString('i18n.billPay.QuitBillPay') , commonUtilities.getaccessibilityConfig());
      this.view.confirmButtons.btnCancel.onClick = function () {
        var currBreakpoint = kony.application.getCurrentBreakpoint();
        if(currBreakpoint === 640){
          self.view.CustomPopup.width = "80%";
          self.view.CustomPopupLogout.width="80%";
        }else{
          self.view.CustomPopup.width = "43%";
          self.view.CustomPopupLogout.width="43%";
        }
        var height_to_set =  self.view.flxFooter.info.frame.y + self.view.flxFooter.info.frame.height;
        self.view.flxPopup.height = height_to_set + "dp";
        self.view.flxPopup.setVisibility(true);
        self.view.CustomPopup.lblHeading.setFocus(true);
      };
      this.view.CustomPopup.btnYes.onClick = function () {
        self.view.flxPopup.setVisibility(false);
        billPayModule.presentationController.cancelBulkPay(billPayModule.presentationController);
      };
      this.view.CustomPopup.btnNo.onClick = function () {
        self.view.flxPopup.setVisibility(false);
        self.view.forceLayout();
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        self.view.flxPopup.setVisibility(false);
        self.view.forceLayout();
      };
      this.view.confirmButtons.btnModify.onClick = billPayModule.presentationController.modifyPayement.bind(billPayModule.presentationController);
      commonUtilities.setText(this.view.lblAmountValue, bulkPayRecords.totalSum , commonUtilities.getaccessibilityConfig());
      this.view.segBill.widgetDataMap = bulkPayWidgetDataMap;
      this.view.segBill.setData(bulkPayRecords.records);
	    FormControllerUtility.hideProgressBar(this.view);
      this.AdjustScreen();
      this.view.forceLayout();
    },
	/** Shows Transfer Confirmation
     * @param {object} transferConfirm Transfer Details
     */
  updateTransferConfirmDetails: function (transferConfirm) {
    var viewModel = transferConfirm.transferData;
    var scopeObj = this;
    var transactionCurrency = applicationManager.getFormatUtilManager().getCurrencySymbol(viewModel.transactionCurrency);
    if(commonUtilities.isCSRMode() && transferConfirm.makeTransferViewModel.isEdit){
      this.view.confirmDialog.confirmButtons.btnConfirm.skin = FormControllerUtility.disableButtonSkinForCSRMode();
      this.view.confirmDialog.confirmButtons.btnConfirm.hoverSkin = FormControllerUtility.disableButtonSkinForCSRMode();
      this.view.confirmDialog.confirmButtons.btnConfirm.focusSkin = FormControllerUtility.disableButtonSkinForCSRMode();
    }
    this.preShowFrmConfirmSinglePay();
    this.view.flxPopup.setVisibility(false);
    this.view.confirmDialog.flxdeliverby.setVisibility(false);
    commonUtilities.setText(this.view.confirmDialog.keyValueFrequency.lblKey, kony.i18n.getLocalizedString("i18n.transfers.lblFrequency") , commonUtilities.getaccessibilityConfig());
    this.view.breadcrumb.setBreadcrumbData([{
      text: kony.i18n.getLocalizedString("i18n.hamburger.transfers")
    }, {
      text: kony.i18n.getLocalizedString("i18n.transfers.confirmTransfer")
    }]);
    this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.hamburger.transfers");
    this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.transfers.confirmTransfer");
    commonUtilities.setText(this.view.CustomPopup.lblHeading, kony.i18n.getLocalizedString('i18n.transfer.QuitTransfer') , commonUtilities.getaccessibilityConfig());
    commonUtilities.setText(this.view.lblConfirmBillPay, kony.i18n.getLocalizedString('i18n.transfers.confirmTransfer') , commonUtilities.getaccessibilityConfig());
    commonUtilities.setText(this.view.confirmDialog.keyValueFrom.lblValue, commonUtilities.getAccountDisplayName(viewModel.accountFrom) , commonUtilities.getaccessibilityConfig());
    commonUtilities.setText(this.view.confirmDialog.keyValueTo.lblValue, viewModel.accountTo.beneficiaryName ? viewModel.accountTo.nickName : (commonUtilities.getAccountDisplayName(viewModel.accountTo)) , commonUtilities.getaccessibilityConfig());
    commonUtilities.setText(this.view.confirmDialog.keyValueAmount.lblValue, this.formatCurrency(viewModel.amount, true) , commonUtilities.getaccessibilityConfig());
    commonUtilities.setText(this.view.confirmDialog.keyValueAmount.lblKey, kony.i18n.getLocalizedString("i18n.transfers.lblAmount")+"("+ transactionCurrency+")" , commonUtilities.getaccessibilityConfig());
    commonUtilities.setText(this.view.confirmDialog.keyValuePaymentDate.lblValue, viewModel.sendOnDate , commonUtilities.getaccessibilityConfig());
    commonUtilities.setText(this.view.confirmDialog.keyValueFrequency.lblValue, viewModel.frequencyKey , commonUtilities.getaccessibilityConfig());
    this.view.confirmDialog.keyValueFrequency.setVisibility(true);
     if(viewModel.notes === "")
            this.view.confirmDialog.keyValueNote.lblValue.text = "-";
     else
    commonUtilities.setText(this.view.confirmDialog.keyValueNote.lblValue, viewModel.notes , commonUtilities.getaccessibilityConfig());
    if (viewModel.frequencyKey !== kony.i18n.getLocalizedString("i18n.transfers.frequency.once") && viewModel.howLongKey === "ON_SPECIFIC_DATE") {
      this.view.confirmDialog.keyValueFrequencyType.setVisibility(true);
      commonUtilities.setText(this.view.confirmDialog.keyValueFrequencyType.lblKey, kony.i18n.getLocalizedString("i18n.transfers.endby") , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValueFrequencyType.lblValue, viewModel.endOnDate , commonUtilities.getaccessibilityConfig());
    } else if (viewModel.frequencyKey !== kony.i18n.getLocalizedString("i18n.transfers.frequency.once") && viewModel.howLongKey === "NO_OF_RECURRENCES") {
      this.view.confirmDialog.keyValueFrequencyType.setVisibility(true);
      commonUtilities.setText(this.view.confirmDialog.keyValueFrequencyType.lblKey, kony.i18n.getLocalizedString("i18n.transfers.lblNumberOfRecurrences") , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValueFrequencyType.lblValue, viewModel.noOfRecurrences , commonUtilities.getaccessibilityConfig());
    } else {
      this.view.confirmDialog.keyValueFrequencyType.setVisibility(false);
    }
    this.view.confirmDialog.confirmButtons.btnConfirm.toolTip = kony.i18n.getLocalizedString('i18n.common.confirmTransaction');
    if(commonUtilities.isCSRMode() && transferConfirm.makeTransferViewModel.isEdit){
      this.view.confirmDialog.confirmButtons.btnConfirm.onClick = FormControllerUtility.disableButtonActionForCSRMode();
    }
    else{
    this.view.confirmDialog.confirmButtons.btnConfirm.onClick = this.saveTransfer.bind(this, transferConfirm);
    }
    this.view.confirmDialog.confirmButtons.btnCancel.toolTip = kony.i18n.getLocalizedString("i18n.transfers.deleteTransfer");
    this.view.confirmDialog.confirmButtons.btnCancel.onClick = this.showTransferCancelPopup.bind(this, transferConfirm.makeTransferViewModel.onCancelCreateTransfer || applicationManager.getModulesPresentationController("TransferModule").showTransferScreen.bind(applicationManager.getModulesPresentationController("TransferModule")))
    this.view.confirmDialog.confirmButtons.btnModify.toolTip = kony.i18n.getLocalizedString('i18n.common.modifyTransaction');
    this.view.confirmDialog.confirmButtons.btnModify.onClick = function () {
      applicationManager.getModulesPresentationController("TransferModule").modifyTransaction(transferConfirm);
    }.bind(this);
    FormControllerUtility.hideProgressBar(this.view);
    this.view.customheader.customhamburger.activateMenu("Transfers")
     this.AdjustScreen();
  },
  /**Formats the Currency
   * @param  {Array} amount Array of transactions model
   * @param  {function} onCancelCreateTransfer Needs to be called when cancel button is called
   */
  formatCurrency : function(amount,currencySymbolNotRequired,currencySymbol){
    return commonUtilities.formatCurrencyWithCommas(amount, currencySymbolNotRequired,currencySymbol);
  },
  /**pre show of confirm single pay screen
   */
  preShowFrmConfirmSinglePay: function () {
    this.view.flxMainSinglePay.setVisibility(true);
    this.view.flxMainBulkPay.setVisibility(false);
    this.view.customheader.forceCloseHamburger();
    this.AdjustScreen();
  },
  /**pre show of confirm screen
   */
  postShowFrmConfirm: function () {
    var self = this;
    //this.view.confirmButtons.skin = "sknFlxe9ebeeop100";
    this.customiseHeaderComponentforAcknowledgement();
    this.view.confirmDialog.keyValueFrom.flxInfo.onClick = function() {
       self.view.AllForms.left = self.view.confirmDialog.keyValueFrom.flxValue.info.frame.x + self.view.confirmDialog.keyValueFrom.lblValue.info.frame.width - 106 +ViewConstants.POSITIONAL_VALUES.DP;
       if(self.view.AllForms.isVisible === true)
         {self.view.AllForms.isVisible = false;}
      else
         {self.view.AllForms.isVisible = true;}
    };
    this.view.AllForms.flxCross.onClick = function() {
       self.view.AllForms.isVisible = false;
    };
     this.view.customheader.forceCloseHamburger();
    this.AdjustScreen();
  },
  /** Callback for confirm transfer button
     * @param {object} transferConfirm Transfer Details
     */
  saveTransfer: function (transferConfirm) {
    FormControllerUtility.showProgressBar();
    if (transferConfirm.makeTransferViewModel.editTransactionObject) {
      applicationManager.getModulesPresentationController("TransferModule").editTransfer(this.updateTransactionObject(transferConfirm.makeTransferViewModel.editTransactionObject, transferConfirm.transferData));
    }
    else {
      if(transferConfirm.makeTransferViewModel && transferConfirm.makeTransferViewModel.repeatTransactionObject){
        this.filterTransferData(transferConfirm.transferData,transferConfirm.makeTransferViewModel.type, transferConfirm.makeTransferViewModel.repeatTransactionObject.serviceName);
      }else{
        this.filterTransferData(transferConfirm.transferData, transferConfirm.makeTransferViewModel.type);
      }
    }
  },
  /** Filter the Data
     * @param {object} transferData Transfer Details
     */
  filterTransferData: function (transferData,type, serviceName){
    var accountsModel  =  kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts");
            var transactionType = transferData.accountTo instanceof accountsModel ? 'InternalTransfer' : 'ExternalTransfer';
            var toAccountNumber = transferData.accountTo instanceof accountsModel ? transferData.accountTo.accountID : transferData.accountTo.accountNumber;
            var externalAccountNumber = transactionType === "ExternalTransfer" ? transferData.accountTo.accountNumber : null;
            var commandData = {
                fromAccountNumber:  transferData.accountFrom,
                accountFrom: transferData.accountFrom,
                accountTo: transferData.accountTo,
                amount:transferData.amount,
                notes: transferData.notes,
                ExternalAccountNumber: externalAccountNumber,
                isScheduled: this.isFutureDate(transferData.sendOnDateComponents) || transferData.frequencyKey !== 'Once' ? "1" : "0",
                transactionType: transactionType,
                toAccountNumber: toAccountNumber,
                frequencyType: transferData.frequencyKey,
                numberOfRecurrences: transferData.howLongKey === 'NO_OF_RECURRENCES' ? transferData.noOfRecurrences : null,
                frequencyEndDate: transferData.howLongKey === 'ON_SPECIFIC_DATE' ? transferData.endOnDate : null,
                scheduledDate: transferData.sendOnDate,
                sendOnDateComponents:transferData.sendOnDateComponents,
                endOnDateComponents:transferData.endOnDateComponents,
                sendOnDate: transferData.sendOnDate,
                endOnDate: transferData.endOnDate,
                frequencyKey: transferData.frequencyKey,
                howLongKey: transferData.howLongKey,
                transactionCurrency: transferData.transactionCurrency,
                toAccountCurrency: transferData.toAccountCurrency,
                beneficiaryId : transferData.beneficiaryId,
                type:type
            }
            if(serviceName){
                commandData.serviceName = serviceName;
            }
    applicationManager.getModulesPresentationController("TransferModule").createTransfer(commandData);
  },
  /** Compares Date with todays and tell is its future or not
     * @param  {object} date object
     * @returns {boolean} True for future else false
     */
  isFutureDate : function (dateComponents) {
    var dateObj = this.getDateObj(dateComponents)
    var endTimeToday = commonUtilities.getServerDateObject();
    var minutes = ViewConstants.MAGIC_NUMBERS.MAX_MINUTES;
    endTimeToday.setHours(ViewConstants.MAGIC_NUMBERS.MAX_HOUR,minutes,minutes,minutes);
    if(dateObj.getTime() > endTimeToday.getTime()) {
        return true;
    }
    return false;
},
  /** Show Transfer Cancel popup for Cancel Button
     * @param {function} onCancelListener function call on click of cancel button
     */
  showTransferCancelPopup: function (onCancelListener) {
    var self = this;
    var currBreakpoint = kony.application.getCurrentBreakpoint();
    if(currBreakpoint === 640){
      self.view.CustomPopup.width = "80%";
      self.view.CustomPopupLogout.width="80%";
    }else{
      self.view.CustomPopup.width = "43%";
      self.view.CustomPopupLogout.width="43%";
    }
    commonUtilities.setText(this.view.CustomPopup.lblHeading, kony.i18n.getLocalizedString("i18n.transfer.QuitTransfer") , commonUtilities.getaccessibilityConfig());
    this.view.flxPopup.setVisibility(true);
    this.view.CustomPopup.lblHeading.setFocus(true);
    var height = self.view.flxFooter.info.frame.height + self.view.flxFooter.info.frame.y;
    self.view.flxPopup.height = height + ViewConstants.POSITIONAL_VALUES.DP;
    this.view.CustomPopup.btnYes.onClick = onCancelListener;
    this.view.CustomPopup.btnYes.toolTip = kony.i18n.getLocalizedString("i18n.transfers.deleteTransfer");
    this.view.CustomPopup.flxCross.onClick = function() {
      self.view.flxPopup.setVisibility(false);
    };
    this.view.CustomPopup.btnNo.onClick =function(){
      self.view.flxPopup.setVisibility(false);
    }
    this.view.CustomPopup.btnNo.toolTip = kony.i18n.getLocalizedString('i18n.common.dontCancelTransaction');
    this.view.forceLayout();
  },
  customiseHeaderComponentforAcknowledgement: function () {
    this.view.breadcrumb.imgBreadcrumb2.setVisibility(false);
    this.view.breadcrumb.lblBreadcrumb3.setVisibility(false);
  },
  /** If editing a transaction, updates transaction object
     * @param {object} transaction Existing transaction
     * @param {object} viewModel Transfer Data
     */
  updateTransactionObject: function (transaction, viewModel) {
    transaction.fromAccountNumber = viewModel.accountFrom.accountID;
    transaction.amount = viewModel.amount;
    transaction.transactionsNotes = viewModel.notes;
    transaction.frequencyType = viewModel.frequencyKey;
    transaction.scheduledDate = viewModel.sendOnDate;
    transaction.numberOfRecurrences = viewModel.noOfRecurrences === "" && viewModel.howLongKey === 'NO_OF_RECURRENCES' ? null : viewModel.noOfRecurrences;
    transaction.frequencyStartDate =  viewModel.sendOnDate;
    transaction.frequencyEndDate = viewModel.endOnDate;
    transaction.beneficiaryId = viewModel.beneficiaryId;
    return transaction;
  },
  /** Get date from date components
     * @param {object} dateComponents Date Components from calendar object
     */
  getDateObj: function (dateComponents) {
    var date =  new Date();
    date.setDate(dateComponents[0]);
    date.setMonth(parseInt(dateComponents[1])-1);
    date.setFullYear(dateComponents[2]);
    date.setHours(0,0,0,0)
    return date;
  },
  /**
     * used to set the BillPay Confirmation Screen
     * @param {object} viewData
     */
    updateBillPayConfirmForm: function (viewData) {
      var self = this;
      self.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule").presentationController;
      if ((viewData.gettingFromOneTimePayment && commonUtilities.isCSRMode()) || (viewData.isScheduleEditFlow && commonUtilities.isCSRMode())) {
        self.view.confirmDialog.confirmButtons.btnConfirm.skin = commonUtilities.disableButtonSkinForCSRMode();
        self.view.confirmDialog.confirmButtons.btnConfirm.hoverSkin = commonUtilities.disableButtonSkinForCSRMode();
        self.view.confirmDialog.confirmButtons.btnConfirm.focusSkin = commonUtilities.disableButtonSkinForCSRMode();
      }
      this.view.confirmDialog.keyValueFrequencyType.setVisibility(false);
      this.view.flxMainSinglePay.setVisibility(true);
      this.view.confirmDialog.flxdeliverby.setVisibility(true);
      this.view.flxMainBulkPay.setVisibility(false);
      this.view.breadcrumb.setBreadcrumbData([{ text: kony.i18n.getLocalizedString('i18n.billPay.BillPay') }, { text: kony.i18n.getLocalizedString("i18n.billPay.ConfirmBillPay") }]);
      this.view.breadcrumb.btnBreadcrumb1.toolTip = kony.i18n.getLocalizedString("i18n.billPay.BillPay");
      this.view.breadcrumb.lblBreadcrumb2.toolTip = kony.i18n.getLocalizedString("i18n.billPay.ConfirmBillPay");
      commonUtilities.setText(this.view.CustomPopup.lblHeading, kony.i18n.getLocalizedString('i18n.billPay.QuitBillPay') , commonUtilities.getaccessibilityConfig());
      this.view.confirmDialog.confirmButtons.btnCancel.onClick = function () {
        var currBreakpoint = kony.application.getCurrentBreakpoint();
        if(currBreakpoint === 640){
          self.view.CustomPopup.width = "80%";
          self.view.CustomPopupLogout.width="80%";
        }else{
          self.view.CustomPopup.width = "43%";
          self.view.CustomPopupLogout.width="43%";
        }
        var height_to_set =  self.view.flxFooter.info.frame.y + self.view.flxFooter.info.frame.height;
        self.view.flxPopup.height = height_to_set + "dp";
        self.view.flxPopup.setVisibility(true);
        self.view.CustomPopup.lblHeading.setFocus(true);
      };
      this.view.confirmDialog.confirmButtons.btnModify.onClick = function () {
         self.presenter.modifyPayement(self.presenter);
      };
      this.view.confirmDialog.confirmButtons.btnConfirm.onClick = function () {
        if ((viewData.gettingFromOneTimePayment && commonUtilities.isCSRMode()) || (viewData.isScheduleEditFlow && commonUtilities.isCSRMode())) {
          self.view.confirmDialog.confirmButtons.btnConfirm.skin = commonUtilities.disableButtonSkinForCSRMode();
          self.view.confirmDialog.confirmButtons.btnConfirm.hoverSkin = commonUtilities.disableButtonSkinForCSRMode();
          self.view.confirmDialog.confirmButtons.btnConfirm.focusSkin = commonUtilities.disableButtonSkinForCSRMode();
        } else {
          var obj1 = {
            "showFlex": "singlepay"
          };
          FormControllerUtility.showProgressBar(this.view);
          viewData.languageAmount = viewData.amount;
          var deformatedAmount = this.deformatAmount(viewData.amount);
          viewData.amount = deformatedAmount;
          self.presenter.checkMFASingleBillPay(viewData);
        }
      }.bind(this);
      this.view.CustomPopup.btnYes.onClick = function () {
        self.view.flxPopup.setVisibility(false);
        self.presenter.showBillPayData();
      };
      this.view.CustomPopup.btnNo.onClick = function () {
        self.view.flxPopup.setVisibility(false);
        self.view.forceLayout();
      };
      this.view.CustomPopup.flxCross.onClick = function () {
        self.view.flxPopup.setVisibility(false);
        self.view.forceLayout();
      };
    },
    /**
     * bind single billPay Data values
     * @param {object} data
     */
    bindSingleBillPayData: function (data) {
      var self = this;      
      var transactionCurrency = applicationManager.getFormatUtilManager().getCurrencySymbol(data.transactionCurrency);
      if (data.statusOfDefaultAccountSetUp === true) {
        this.view.flxWarning.setVisibility(true);
        commonUtilities.setText(this.view.lblWarning, data.defaultAccountBillPay + kony.i18n.getLocalizedString("i18n.billPay.setDefaultPopUpConfirmBillPayee") , commonUtilities.getaccessibilityConfig());
      } else {
        this.view.flxWarning.setVisibility(false);
      }
      this.view.imgCloseWarning.onTouchEnd = function () {
        self.view.flxWarning.setVisibility(false);
        self.view.forceLayout();
      };
      commonUtilities.setText(this.view.confirmDialog.keyValueFrom.lblKey, "From" , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValueTo.lblKey, "To" , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValuePaymentDate.lblKey, "Payment Date" , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValueNote.lblKey, "Notes" , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValueFrom.lblValue, data.payFrom , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValueTo.lblValue, data.payeeName , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValueAmount.lblValue, data.amount , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValueAmount.lblKey, kony.i18n.getLocalizedString("i18n.transfers.lblAmount")+"("+transactionCurrency+")" , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValuePaymentDate.lblValue, data.sendOn , commonUtilities.getaccessibilityConfig());
      this.view.confirmDialog.keyValueFrequency.setVisibility(false);
      commonUtilities.setText(this.view.confirmDialog.keyValueFrequency.lblValue, data.categories , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValueFrequency.lblKey, kony.i18n.getLocalizedString("i18n.billPay.category") , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValueNote.lblValue, data.notes , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.flxdeliverby.lblKey, kony.i18n.getLocalizedString("i18n.billPay.DeliverBy") , commonUtilities.getaccessibilityConfig());
      if (data.gettingFromOneTimePayment){
          this.view.confirmDialog.keyValueFrequencyType.setVisibility(false);
      }
      else{
        this.view.confirmDialog.keyValueFrequencyType.setVisibility(true);
      }
      commonUtilities.setText(this.view.confirmDialog.keyValueFrequencyType.lblKey, kony.i18n.getLocalizedString("i18n.transfers.lblFrequency") , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.keyValueFrequencyType.lblValue, data.frequencyType , commonUtilities.getaccessibilityConfig());
      commonUtilities.setText(this.view.confirmDialog.flxdeliverby.lblValue, data.deliveryDate , commonUtilities.getaccessibilityConfig());
      FormControllerUtility.hideProgressBar(this.view);
      this.view.forceLayout();
      this.AdjustScreen();
    },
    //UI Code
    /**
    * onBreakpointChange : Handles ui changes on .
    * @member of {frmConfirmController}
    * @param {integer} width - current browser width
    * @return {}
    * @throws {}
    */
    onBreakpointChange: function (width) {
      var scope = this;
      this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup,width);
      this.view.CustomPopupLogout.onBreakpointChangeComponent(scope.view.CustomPopupLogout,width);
      orientationHandler.onOrientationChange(this.onBreakpointChange);
      this.view.customheader.onBreakpointChangeComponent(width);
      this.setupFormOnTouchEnd(width);
      this.view.confirmDialog.confirmButtons.skin="slFbox";
      this.view.flxConfirmContainer.skin="slFbox";
      this.view.confirmDialog.flxMain.skin="sknFlxffffffShadowdddcdc";
      this.view.confirmButtons.skin = "slFbox";
      if (width === 640) {
        commonUtilities.setText(this.view.customheader.lblHeaderMobile, "Confirm" , commonUtilities.getaccessibilityConfig());
        this.view.CustomPopupLogout.width="75%";
      }
      else {
        commonUtilities.setText(this.view.customheader.lblHeaderMobile, "" , commonUtilities.getaccessibilityConfig());
        if(width==1024){
          this.view.lblAmount.left="";
          this.view.lblAmount.right="1dp";
          this.view.confirmButtons.btnConfirm.left = "";
          this.view.confirmButtons.btnConfirm.right = "0dp";
          this.view.confirmButtons.btnConfirm.width = "150dp";
          this.view.confirmButtons.btnModify.left = "";
          this.view.confirmButtons.btnModify.right = "170dp";
          this.view.confirmButtons.btnCancel.left = "";
          this.view.confirmButtons.btnCancel.right = "340dp";
          this.view.confirmButtons.btnModify.width = "150dp";
       }
      }
      this.AdjustScreen();
    },
    setupFormOnTouchEnd: function(width){
      if(width==640){
        this.view.onTouchEnd = function(){}
        this.nullifyPopupOnTouchStart();
      }else{
        if(width==1024){
            this.view.onTouchEnd = function(){}
            this.nullifyPopupOnTouchStart();
          }else{
            this.view.onTouchEnd = function(){
              hidePopups();
            }
          }
          var userAgent = kony.os.deviceInfo().userAgent;
          if (userAgent.indexOf("iPad") != -1) {
            this.view.onTouchEnd = function(){}
            this.nullifyPopupOnTouchStart();
          } else if (userAgent.indexOf("Android") != -1 && userAgent.indexOf("Mobile") == -1) {
            this.view.onTouchEnd = function(){}
            this.nullifyPopupOnTouchStart();
          }
        }
    },
    nullifyPopupOnTouchStart: function(){
    },
    /**
     * used to bind Terms and condition data on Activation screen
     * @param {object} TnCcontent bill pay supported sccounts
     */
    bindTnCData: function(TnCcontent, isBulkBillPay){
      if(isBulkBillPay)
      {
        this.bindTnC(TnCcontent,this.view.flxCheckBox,this.view.lblCheckBoxIcon,this.view.btnTermsAndConditions,this.view.confirmButtons.btnConfirm);
      }
      else
      {
        this.bindTnC(TnCcontent,this.view.confirmDialog.flxCheckBoxTnC,this.view.confirmDialog.lblCheckBoxIcon,this.view.confirmDialog.btnTermsAndConditions,this.view.confirmDialog.confirmButtons.btnConfirm);
      }
      this.view.forceLayout();
    },
    bindTnC:function(TnCcontent,checkboxFlex,checkboxIcon,btnTnC,confirmButton){
      if(TnCcontent.alreadySigned){
          checkboxFlex.setVisibility(false);
      }
      else{
          commonUtilities.disableButton(confirmButton);
          checkboxIcon.text = OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED;
          checkboxIcon.onTouchEnd = this.toggleTnC.bind(this,checkboxIcon,confirmButton);
          checkboxFlex.setVisibility(true);
          if(TnCcontent.contentTypeId===OLBConstants.TERMS_AND_CONDITIONS_URL){
              btnTnC.onClick = function(){ 
                  window.open(TnCcontent.termsAndConditionsContent);
              }
          }else{
                btnTnC.onClick = this.showTermsAndConditionPopUp;
                this.setTnCDATASection(TnCcontent.termsAndConditionsContent);
                FormControllerUtility.setHtmlToBrowserWidget(this, this.view.brwBodyTnC, TnCcontent.termsAndConditionsContent);
          }
          this.view.flxClose.onClick = this.hideTermsAndConditionPopUp;
        }
      },
      showTermsAndConditionPopUp:function(){
          var height = this.view.flxFooter.info.frame.y + this.view.flxFooter.info.frame.height;
          this.view.flxTermsAndConditionsPopUp.height=height+"dp";
          this.view.flxTermsAndConditionsPopUp.setVisibility(true);
          this.view.forceLayout();
      },
      hideTermsAndConditionPopUp:function(){
          this.view.flxTermsAndConditionsPopUp.setVisibility(false);
      },
      setTnCDATASection:function(content){
          this.view.rtxTC.text = content;
      },
      toggleTnC: function(widget,confirmButton) {
          commonUtilities.toggleFontCheckbox(widget,confirmButton);
          if(widget.text === OLBConstants.FONT_ICONS.CHECBOX_UNSELECTED)
              commonUtilities.disableButton(confirmButton);
          else
              commonUtilities.enableButton(confirmButton);
      }
  }
});