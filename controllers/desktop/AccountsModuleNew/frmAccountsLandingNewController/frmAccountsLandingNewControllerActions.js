define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onKeyUp defined for tbxNewUsername **/
    AS_TextField_cf2826b931594d0fa5c4944344d9dc18: function AS_TextField_cf2826b931594d0fa5c4944344d9dc18(eventobject) {
        var self = this;
        this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text, this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text);
    },
    /** onKeyUp defined for tbxEnterpassword **/
    AS_TextField_f2030e8e14ef414facdb8bf21ac978f9: function AS_TextField_f2030e8e14ef414facdb8bf21ac978f9(eventobject) {
        var self = this;
        this.enableOrDisableExternalLogin(this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxNewUsername.text, this.view.AddExternalAccounts.LoginUsingSelectedBank.tbxEnterpassword.text);
    },
    /** onKeyUp defined for tbxName **/
    AS_TextField_f21640467d6440dcbb0abd7833eb9542: function AS_TextField_f21640467d6440dcbb0abd7833eb9542(eventobject) {
        var self = this;
        this.onTextChangeOfExternalBankSearch();
    },
});