define(['FormControllerUtility', 'CommonUtilities', 'ViewConstants', 'OLBConstants'], function (FormControllerUtility, CommonUtilities, ViewConstants, OLBConstants) {
    var responsiveUtils = new ResponsiveUtils();
    var orientationHandler = new OrientationHandler();
    var pageNumber;
    var totalNoOfRecords;
    var recordsPerPage = 10;
    var records = [];
    var searchView;
    return {
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
            var scopeObj = this;
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferEurModule").presentationController;
            scopeObj.pastPaymentSortMap = [{
                name: 'transactionDate',
                imageFlx: scopeObj.view.imgSortDate,
                clickContainer: scopeObj.view.flxSortDate
            }, {
                name: 'toaccountname',
                imageFlx: scopeObj.view.imgSortDescription,
                clickContainer: scopeObj.view.flxSortTo
            }, {
                name: 'amount',
                imageFlx: scopeObj.view.imgSortType,
                clickContainer: scopeObj.view.flxSortAmount
            }];
            FormControllerUtility.setSortingHandlers(scopeObj.pastPaymentSortMap, scopeObj.onPastPaymentSortClickHandler, scopeObj);
            this.view.btnScheduledPayments.onClick = function() {
                scopeObj.presenter.showTransferScreen({
                    context: "ScheduledPayments"
                })
            };
            this.view.btnPastPayments.onClick = function() {
                scopeObj.presenter.showTransferScreen({
                    context: "PastPayments"
                })
            };
            this.view.flxNewPayment.onClick = function() {
                scopeObj.presenter.showTransferScreen({
                    context: "MakePayment"
                })
            };
            this.view.flxPaymentActivities.onClick = function() {
                scopeObj.presenter.showTransferScreen({
                    context: ""
                })
            };
            this.view.flxManageBeneficiaries.onClick = function() {
                scopeObj.presenter.showTransferScreen({
                    context: "ManageBeneficiaries"
                })
            };
            this.view.flxMakePayment.onClick = function() {
                scopeObj.presenter.showTransferScreen({
                    context: "MakePayment"
                })
            };
            this.view.flxPaginationPrevious.onClick = this.prevPagination.bind(this);
            this.view.flxPaginationNext.onClick = this.nextPagination.bind(this);
            this.view.txtSearch.onKeyUp = this.onTxtSearchKeyUp.bind(this);
            this.view.txtSearch.onDone = this.onSearchBtnClick.bind(this);
            this.view.flxClearBtn.onClick = this.onSearchClearBtnClick.bind(this);
        },
        onBreakpointChange: function(form, width) {
            var scope = this;
            this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup, width);
            this.view.DeletePopup.onBreakpointChangeComponent(scope.view.DeletePopup, width);
            FormControllerUtility.setupFormOnTouchEnd(width);
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        },
        preShow: function() {
            this.view.customheadernew.activateMenu("EUROTRANSFERS", "Manage Payments");
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.frame.height - this.view.flxFooter.frame.height + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
            applicationManager.executeAuthorizationFramework(this);
            this.accessibilityFocusSetup();
        },
      	showManageBeneficiaryFlx: function(){
        	this.view.flxManageBeneficiaries.setVisibility(true);
      	},
      	hideManageBeneficiaryFlx: function(){
        	this.view.flxManageBeneficiaries.setVisibility(false);
      	},
      	hideNewPayment: function () {
        	this.view.flxNewPayment.setVisibility(false)
      	},
      	showNewPayment: function () {
        	this.view.flxNewPayment.setVisibility(true);
      	},
        /**
         * Set foucs handlers for skin of parent flex on input focus 
         */
        accessibilityFocusSetup: function() {
            let widgets = [
                [this.view.txtSearch, this.view.flxtxtSearchandClearbtn]
            ]
            for (let i = 0; i < widgets.length; i++) {
                CommonUtilities.setA11yFoucsHandlers(widgets[i][0], widgets[i][1], this)
            }
        },
        /**
         * updateFormUI - the entry point method for the form controller.
         * @param {Object} viewModel - it contains the set of view properties and keys.
         */
        updateFormUI: function(viewModel) {
            if (viewModel.isLoading === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewModel.isLoading === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewModel.pastPayments) {
                this.showPastPaymentsData(viewModel.pastPayments);
            }
            if (viewModel.noPastPayment) {
                this.showNoPayment();
            }
            if (viewModel.transactionDownloadFile) {
                this.downloadAttachmentsFile(viewModel.transactionDownloadFile);
            }
            if (viewModel.serverError) {
                this.showServerError(viewModel.serverError);
            } else {
                this.showServerError(false);
            }
        },
        /** 
         * Method for past payments click sorting handler
         * @param {object} event object
         * @param {object} data Sorting Data
         */
        onPastPaymentSortClickHandler: function(event, data) {
            var scopeObj = this;
            scopeObj.presenter.getPastPayments(data);
        },
        /**
         * Method to show past payments records
         * @param {Array} data - Array of all Past Payments
         */
        showPastPaymentsData: function(data) {
            var scopeObj = this;
            scopeObj.view.segPastPaymentsEur.setVisibility(true);
            scopeObj.view.flxNoPayment.setVisibility(false);
            if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
                scopeObj.view.flxSortHeader.setVisibility(false);
            } else {
                scopeObj.view.flxSortHeader.setVisibility(true);
            }
            scopeObj.sortByTransactionDate(data);
            pageNumber = 1;
            records = data;
            totalNoOfRecords = data.length;
            searchView = false;
            scopeObj.setSegmentData(scopeObj.getDataOfPage());
        },
        /**
         * Method to Sort Past Payment records in Descending Order of Transaction Date
         * @param {Array} data Past Payment records array
         */
        sortByTransactionDate: function(data) {
            data.sort(function(a, b) {
                var keyA = new Date(a.transactionDate);
                var keyB = new Date(b.transactionDate);
                if (keyA < keyB) return 1;
                if (keyA > keyB) return -1;
                return 0;
            });
        },
        /**
         * Method to update Pagination Values
         */
        updatePaginationValues: function(limit) {
            var scopeObj = this;
            if (pageNumber === 1) {
                scopeObj.view.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_INACTIVE;
            } else {
                scopeObj.view.imgPaginationPrevious.src = ViewConstants.IMAGES.PAGINATION_BACK_ACTIVE;
            }
            if (pageNumber === Math.ceil(totalNoOfRecords / recordsPerPage)) {
                scopeObj.view.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_INACTIVE;
            } else {
                scopeObj.view.imgPaginationNext.src = ViewConstants.IMAGES.PAGINATION_NEXT_ACTIVE;
            }
            var from = (pageNumber - 1) * recordsPerPage + 1;
            var to = (pageNumber - 1) * recordsPerPage + limit;
            CommonUtilities.setText(this.view.lblPagination, from + " - " + to + " " + kony.i18n.getLocalizedString("i18n.common.transactions"), CommonUtilities.getaccessibilityConfig());
        },
        /**
         * Method to get Past Payment records of a particular page
         * @return {Array} Past Payment records of a particular page
         */
        getDataOfPage: function() {
            return records.slice((pageNumber - 1) * recordsPerPage, pageNumber * recordsPerPage);
        },
        /**
         * Method to set records in segment
         * @param {Array} data - Past Payment Records Array of a particular page
         */
        setSegmentData: function(data) {
            var scopeObj = this;
            if (!searchView) {
                scopeObj.view.flxPagination.setVisibility(true);
                scopeObj.updatePaginationValues(data.length);
            } else {
                scopeObj.view.flxPagination.setVisibility(false);
            }
            if (data.length === 0) {
                scopeObj.showNoPayment();
                return;
            }
            var dataMap = {
                "btnRepeat": "btnRepeat",
                "btnAttachments":"btnAttachments",
                "flxIdentifier": "flxIdentifier",
                "lblIdentifier": "lblIdentifier",
                "lblDropdown": "lblDropdown",
                "lblDate": "lblDate",
                "lblSendTo": "lblSendTo",
                "lblAmount": "lblAmount",
                "lblSeparator": "lblSeparator",
                "lblSeparatorBottom": "lblSeparatorBottom",
                "lblReferenceNumberTItle": "lblReferenceNumberTItle",
                "lblReferenceNumberValue": "lblReferenceNumberValue",
                "lblFrequencyTitle": "lblFrequencyTitle",
                "lblFrequencyValue": "lblFrequencyValue",
                "lblPaymentReferenceTitle": "lblPaymentReferenceTitle",
                "lblPaymentReferenceValue": "lblPaymentReferenceValue",
                "lblFromAccountTitle": "lblFromAccountTitle",
                "lblFromAccountValue": "lblFromAccountValue",
                "lblFromAccount": "lblFromAccount",
                "lblRecurrencesTitle": "lblRecurrencesTitle",
                "lblRecurrencesValue": "lblRecurrencesValue",
                "flxStatusTitle": "flxStatusTitle",
                "lblStatusTitle": "lblStatusTitle",
                "lblStatusValue": "lblStatusValue",
                "flxPastPaymentsEurSelected": "flxPastPaymentsEurSelected",
                "flxPastPaymentsEurSelectedMobile": "flxPastPaymentsEurSelectedMobile",
                "flxReferenceNumberTitle": "flxReferenceNumberTitle",
                "flxRecurrenceTitle": "flxRecurrenceTitle",
                "flxPaymentReferenceTitle": "flxPaymentReferenceTitle",
              	"lblPaymentMethodTitle": "lblPaymentMethodTitle",
        		"lblPaymentMethodValue": "lblPaymentMethodValue"
            };
          	var  payment_method;
            var pastPaymentSegmentData = data.map(function(dataItem) {
              if(dataItem.serviceName === "INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE"){ payment_method = "International";}
              else if(dataItem.serviceName === "INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE"){payment_method = "Domestic";}
              else{payment_method = "Within Bank";}
                var dataObject = {
                  "btnRepeat": {
                    "text": kony.i18n.getLocalizedString("i18n.accounts.repeat"),
                    "toolTip": kony.i18n.getLocalizedString("i18n.accounts.repeat"),
                    "onClick": function () {
                    	if (dataItem.transactionType === "InternalTransfer") {
                    		scopeObj.presenter.showTransferScreen({
                    			"context": "MakePaymentOwnAccounts",
                    			"editTransaction": dataItem
                  			});
                		}
                		else {
                  			scopeObj.presenter.showTransferScreen({
                    			"context": "MakePayment",
                    			"editTransaction": dataItem
                  			});
                		}
            		}
  					 },
                    "btnAttachments": {
                        "isVisible": (dataItem.fileNames) ? (dataItem.fileNames.length > 0 ? true : false) : false,
                        "text": (dataItem.fileNames) ? kony.i18n.getLocalizedString("i18n.common.Attachments") + "(" + dataItem.fileNames.length + ")" : "",
                        "toolTip": (dataItem.fileNames) ? kony.i18n.getLocalizedString("i18n.common.Attachments") + "(" + dataItem.fileNames.length + ")" :"",
                        "onClick": function() {
                            scopeObj.viewAttachments(dataItem);
                        }
                    },
                    "flxIdentifier": {
                        "skin": "sknFlxIdentifier",
                    },
                    "lblIdentifier": {
                        "skin": "sknffffff15pxolbfonticons"
                    },
                    "lblDropdown": {
                        "text": "O"
                    },
                    "lblDate": {
                        "text": CommonUtilities.getFrontendDateString(dataItem.scheduledDate),
                        "accessibilityconfig": {
                            "a11yLabel": CommonUtilities.getFrontendDateString(dataItem.scheduledDate)
                        }
                    },
                    "lblSendTo": {
                        "text": dataItem.toAccountName + "..." + CommonUtilities.getLastFourDigit(dataItem.toAccountNumber),
                        "accessibilityconfig": {
                            "a11yLabel": dataItem.toAccountName + "..." + CommonUtilities.getLastFourDigit(dataItem.toAccountNumber)
                        }
                    },
                    "lblAmount": {
                        "text": CommonUtilities.formatCurrencyWithCommas(dataItem.amount, false, dataItem.paymentCurrencyId),
                        "accessibilityconfig": {
                            "a11yLabel": CommonUtilities.formatCurrencyWithCommas(dataItem.amount, false, dataItem.paymentCurrencyId)
                        }
                    },
                    "lblSeparator": "lblSeparator",
                    "lblSeparatorBottom": "lblSeparatorBottom",
                    "lblReferenceNumberTItle": {
                        "text": kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber")
                        }
                    },
                    "lblReferenceNumberValue": {
                        "text": dataItem.transactionId,
                        "accessibilityconfig": {
                            "a11yLabel": dataItem.transactionId
                        }
                    },
                    "lblFrequencyTitle": {
                        "text": kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.lblFrequency"),
                        }
                    },
                    "lblFrequencyValue": {
                        "text": dataItem.frequencyType || "-"
                    },
                    "lblPaymentReferenceTitle": {
                        "text": kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentReference"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentReference"),
                        }
                    },
                    "lblPaymentReferenceValue": {
                        "text": dataItem.transactionsNotes || dataItem.description || "-",
                        "accessibilityconfig": {
                            "a11yLabel": dataItem.transactionsNotes || dataItem.description || "-"
                        }
                    },
                    "lblFromAccountTitle": {
                        "text": kony.i18n.getLocalizedString("i18n.transfers.fromAccount"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.fromAccount"),
                        }
                    },
                    "lblFromAccountValue": {
                        "text": dataItem.fromAccountName + "..." + CommonUtilities.getLastFourDigit(dataItem.fromAccountNumber),
                        "accessibilityconfig": {
                            "a11yLabel": dataItem.fromAccountName + "..." + CommonUtilities.getLastFourDigit(dataItem.fromAccountNumber)
                        }
                    },
                    "lblRecurrencesTitle": {
                        "text": kony.i18n.getLocalizedString("i18n.transfers.lblRecurrences"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.transfers.lblRecurrences"),
                        }
                    },
                    "lblRecurrencesValue": {
                        "text": dataItem.numberOfRecurrences
                    },
                    "lblStatusTitle": {
                        "text": kony.i18n.getLocalizedString("i18n.common.status"),
                        "accessibilityconfig": {
                            "a11yLabel": kony.i18n.getLocalizedString("i18n.common.status"),
                        }
                    },
                    "lblStatusValue": {
                        "text": dataItem.currentStatus
                    },
                  	"lblPaymentMethodTitle": {
                    	"text": kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentMethod"),
                    	"accessibilityconfig": {
                      		"a11yLabel": kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentMethod"),
                    	}
                  	},
                  	"lblPaymentMethodValue": {
                    	"text": payment_method,
                    	"accessibilityconfig": {
                      		"a11yLabel": payment_method
                    	}
                  	},
                    "flxPastPaymentsEurSelected": {
                        "height": "50dp",
                        "skin": "sknflxffffffnoborder"
                    },
                    "flxPastPaymentsEurSelectedMobile": {
                        "height": "50dp",
                        "skin": "sknflxffffffnoborder"
                    },
                    "template": (kony.application.getCurrentBreakpoint() === 640) ? "flxPastPaymentsEurSelectedMobile" : "flxPastPaymentsEurSelected"
                };
                return dataObject;
            });
            scopeObj.view.segPastPaymentsEur.widgetDataMap = dataMap;
            scopeObj.view.segPastPaymentsEur.setData(pastPaymentSegmentData);
            scopeObj.view.forceLayout();
        },

        downloadSingleFile: function(dataItem){
            var scopeObj = this;
            scopeObj.presenter.downloadAttachments(transactionObject, dataItem, 0);
        },
    
        viewAttachments: function(dataItem){
            var scopeObj = this;
            filesToBeDownloaded = dataItem.fileNames;
            transactionObject = dataItem;
            this.view.setContentOffset({x:"0%",y:"0%"}, true);
            scopeObj.view.flxDialogs.setVisibility(true);
            scopeObj.view.flxDownloadsPopup.setVisibility(true);
            if(filesToBeDownloaded.length===1)
                scopeObj.view.btnDownload.text = kony.i18n.getLocalizedString("i18n.common.Download");
            else
                scopeObj.view.btnDownload.text = kony.i18n.getLocalizedString("i18n.common.DownloadAll");
            scopeObj.view.flxButtons.btnCancel.onClick = function() {
                scopeObj.view.flxDialogs.setVisibility(false);
                scopeObj.view.flxDownloadsPopup.setVisibility(false);
            };
            scopeObj.view.flxButtons.btnDownload.onClick = function() {
                if(dataItem.fileNames.length>0){
                var count = 0;
                for (var i = 0; i < dataItem.fileNames.length; i++) {
                    setTimeout(scopeObj.presenter.downloadAttachments.bind(this, null, dataItem, i), count);
                    count += 700;
                }
                }
            };
            this.setDownloadSegmentData(dataItem.fileNames);
        },
    
        setDownloadSegmentData: function(filesList){
            var scopeObj = this;
            var downloadAttachmentsData = [];
            for (var i = 0; i < filesList.length; i++) {
                downloadAttachmentsData[i] = {};
                downloadAttachmentsData[i].filename = filesList[i].fileName;
                downloadAttachmentsData[i]["imgDownloadAttachment"] = {
                "src": "download_blue.png"
                };
            }
            scopeObj.view.segDownloadItems.widgetDataMap = {
                "lblDownloadAttachment": "filename",
                "imgDownloadAttachment": "imgDownloadAttachment",
            };
            scopeObj.view.segDownloadItems.setData(downloadAttachmentsData);
        },

        downloadAttachmentsFile : function(fileUrl){
            var data={"url":fileUrl};
            CommonUtilities.downloadFile(data);
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to fetch previous Past Payments records
         */
        prevPagination: function() {
            var scopeObj = this;
            if (pageNumber === 1) {
                return;
            }
            scopeObj.view.flxFormContent.setContentOffset({
                x: "0%",
                y: "0%"
            }, true);
            pageNumber = pageNumber - 1;
            scopeObj.setSegmentData(scopeObj.getDataOfPage());
        },
        /**
         * Method to fetch next Past Payments records
         */
        nextPagination: function() {
            var scopeObj = this;
            if (pageNumber === Math.ceil(totalNoOfRecords / recordsPerPage)) {
                return;
            }
            scopeObj.view.flxFormContent.setContentOffset({
                x: "0%",
                y: "0%"
            }, true);
            pageNumber = pageNumber + 1;
            scopeObj.setSegmentData(scopeObj.getDataOfPage());
        },
        /**
         * Method to show no past payment scenario 
         */
        showNoPayment: function() {
            var scopeObj = this;
            scopeObj.view.flxNoPayment.setVisibility(true);
            scopeObj.view.segPastPaymentsEur.setVisibility(false);
            scopeObj.view.flxSortHeader.setVisibility(false);
            scopeObj.view.flxPagination.setVisibility(false);
            scopeObj.view.forceLayout();
        },
        /**
         * Method to show server error
         * @param {Boolean} status true/false
         */
        showServerError: function(status) {
            if (status === false) {
                this.view.flxDowntimeWarning.setVisibility(false);
            } else {
                this.view.rtxDowntimeWarning.text = kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
                this.view.flxDowntimeWarning.setVisibility(true);
                this.view.flxDowntimeWarning.setFocus(true);
            }
            this.view.flxMain.forceLayout();
        },
        /**
         * method used to enable or disable the clear button.
         * @param {object} event event
         */
        onTxtSearchKeyUp: function(event) {
            var scopeObj = this;
            var searchKeyword = scopeObj.view.txtSearch.text.trim();
            if (searchKeyword.length > 0) {
                scopeObj.view.flxClearBtn.setVisibility(true);
            } else {
                scopeObj.view.flxClearBtn.setVisibility(false);
            }
            this.view.flxSearch.forceLayout();
        },
        /**
         * method to handle the search functionality
         */
        onSearchBtnClick: function () {
            var scopeObj = this;
            var searchKeyword = scopeObj.view.txtSearch.text.trim();
            if (searchKeyword.length > 0) {
                searchView = true;
                var searchData = records.filter(function (obj) {
                    return ((obj["amount"] && obj["amount"].indexOf(searchKeyword) !== -1)
                        || (obj["transactionId"] && obj["transactionId"].indexOf(searchKeyword.toUpperCase()) !== -1));
                });
                scopeObj.setSegmentData(searchData);
            } else {
                searchView = false;
                scopeObj.presenter.showTransferScreen({ "context": "PastPayments" });
            }
        },
        /**
         * method used to call the service.
         */
        onSearchClearBtnClick: function() {
            var scopeObj = this;
            scopeObj.view.txtSearch.text = "";
            scopeObj.view.flxClearBtn.setVisibility(false);
            if (searchView === true) {
                searchView = false;
                scopeObj.presenter.showTransferScreen({
                    "context": "PastPayments"
                });
            }
        }
    };
});
