define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_f4fb91efcadd493a872127dca0cb5ff0: function AS_FlexContainer_f4fb91efcadd493a872127dca0cb5ff0(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_aec2bdb9d4ff4950bcfbeaadaf2ba349: function AS_Button_aec2bdb9d4ff4950bcfbeaadaf2ba349(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnBreadcrumb2 **/
    AS_Button_h00567af53f54d3bb1befba05aaf783b: function AS_Button_h00567af53f54d3bb1befba05aaf783b(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onSelection defined for lbxFrequencymod **/
    AS_ListBox_ga199241e52e45d7b79ddf20f32f3b12: function AS_ListBox_ga199241e52e45d7b79ddf20f32f3b12(eventobject) {
        var self = this;
        this.getFrequencyAndFormLayout();
    },
    /** onSelection defined for lbxForHowLongmod **/
    AS_ListBox_g2fee7948b264824bd9dfc95386c1ad5: function AS_ListBox_g2fee7948b264824bd9dfc95386c1ad5(eventobject) {
        var self = this;
        this.getForHowLongandFormLayout();
    },
    /** onClick defined for btnbacktopayeelist **/
    AS_Button_e211098807544f31861662de1a33819c: function AS_Button_e211098807544f31861662de1a33819c(eventobject) {
        var self = this;
        this.presenter.getExternalAccounts();
    },
    /** onClick defined for flxAddNonKonyAccount **/
    AS_FlexContainer_c9e5fa80a1af40ef823561618baa08a3: function AS_FlexContainer_c9e5fa80a1af40ef823561618baa08a3(eventobject) {
        var self = this;
        //this.addExternalAccount();
        this.presenter.showDomesticAccounts();
    },
    /** onTouchStart defined for imgViewCVVCode **/
    AS_Image_dcf3b4d5b9674382bb37384d63c2a74d: function AS_Image_dcf3b4d5b9674382bb37384d63c2a74d(eventobject, x, y) {
        var self = this;
        this.showPassword();
    },
    /** onTouchEnd defined for imgViewCVVCode **/
    AS_Image_fef64f781b8a42589fc133350c7d9458: function AS_Image_fef64f781b8a42589fc133350c7d9458(eventobject, x, y) {
        var self = this;
        this.hidePassword();
    },
    /** onEndEditing defined for tbxAnswers1 **/
    AS_TextField_ae78fb3330374778bbb456cbc9bfafa5: function AS_TextField_ae78fb3330374778bbb456cbc9bfafa5(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers1 **/
    AS_TextField_g395089df3e14df287887c99b6cbad72: function AS_TextField_g395089df3e14df287887c99b6cbad72(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onEndEditing defined for tbxAnswers2 **/
    AS_TextField_f6407764abc74bb49a052aa72cc08cf7: function AS_TextField_f6407764abc74bb49a052aa72cc08cf7(eventobject, changedtext) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onKeyUp defined for tbxAnswers2 **/
    AS_TextField_b028a1d8452a46ddb0ed04b80ccd57fe: function AS_TextField_b028a1d8452a46ddb0ed04b80ccd57fe(eventobject) {
        var self = this;
        this.enableSecurityQuestions(this.view.AnswerSecurityQuestionsModule.tbxAnswers1.text, this.view.AnswerSecurityQuestionsModule.tbxAnswers2.text);
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_e8ad267577d347ae947215997b48da11: function AS_Button_e8ad267577d347ae947215997b48da11(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_b024b6dcc824419091c824bdc968a839: function AS_Button_b024b6dcc824419091c824bdc968a839(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_b1ad83ae23534a6392a729fdafe7d0a8: function AS_Button_b1ad83ae23534a6392a729fdafe7d0a8(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_f56b3986fe6f4f73bd3b0907fb19baee: function AS_Button_f56b3986fe6f4f73bd3b0907fb19baee(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_db2cddc1ed79440bad513a59db87238f: function AS_Button_db2cddc1ed79440bad513a59db87238f(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for btnClose **/
    AS_Button_i7d76100d0484e04aa9bcc72096b1aee: function AS_Button_i7d76100d0484e04aa9bcc72096b1aee(eventobject) {
        var self = this;
        this.closeViewReport();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_ac45ce2bd35c4da19c3dcd68b7456c1d: function AS_Button_ac45ce2bd35c4da19c3dcd68b7456c1d(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_bf811fdf2b484267a44b0b6f753a0bf1: function AS_Button_bf811fdf2b484267a44b0b6f753a0bf1(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_c2b935d36f424333a24a32fad777deee: function AS_Button_c2b935d36f424333a24a32fad777deee(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_d955367a5a314c7dbde831eb4e039377: function AS_Button_d955367a5a314c7dbde831eb4e039377(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_b2591cff21bf494ebd6d4dc046034a7c: function AS_Button_b2591cff21bf494ebd6d4dc046034a7c(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** init defined for frmTransfersEur **/
    AS_Form_fe503c0c6c7f42cf87a0a3c0aa3774a4: function AS_Form_fe503c0c6c7f42cf87a0a3c0aa3774a4(eventobject) {
        var self = this;
        this.initActions();
    },
    /** preShow defined for frmTransfersEur **/
    AS_Form_i9c89b97457b4d3db8e24952499f75b4: function AS_Form_i9c89b97457b4d3db8e24952499f75b4(eventobject) {
        var self = this;
        this.initTabsActions();
    },
    /** postShow defined for frmTransfersEur **/
    AS_Form_g0422a5fa353427ca01bcca473de2792: function AS_Form_g0422a5fa353427ca01bcca473de2792(eventobject) {
        var self = this;
        this.postShowtransfers();
    },
    /** onDeviceBack defined for frmTransfersEur **/
    AS_Form_b88c29204d174ad2b31554d17fda9216: function AS_Form_b88c29204d174ad2b31554d17fda9216(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** onTouchEnd defined for frmTransfersEur **/
    AS_Form_f404f9d44ace4923858ddba78261aa10: function AS_Form_f404f9d44ace4923858ddba78261aa10(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
});