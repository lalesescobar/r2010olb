define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_g0852ae817de415a867c5260f502f030: function AS_FlexContainer_g0852ae817de415a867c5260f502f030(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_g32105509b6a412683df235a0c17b4db: function AS_Button_g32105509b6a412683df235a0c17b4db(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnBreadcrumb2 **/
    AS_Button_f4c8e83edcb24b83806c4aad35eb13c6: function AS_Button_f4c8e83edcb24b83806c4aad35eb13c6(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnConfirm **/
    AS_Button_dfaa3325fed64d1bb0df15af72f8cb4b: function AS_Button_dfaa3325fed64d1bb0df15af72f8cb4b(eventobject) {
        var self = this;
        this.addAccount();
    },
    /** onClick defined for btnModify **/
    AS_Button_f2aea2570928401a874423c397f270f3: function AS_Button_f2aea2570928401a874423c397f270f3(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.modifyAccountInfo();
    },
    /** onClick defined for btnCancel **/
    AS_Button_d8e7b3168db346f39279ccbf6797862c: function AS_Button_d8e7b3168db346f39279ccbf6797862c(eventobject) {
        var self = this;
        this.showCancelPopup();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_jb05c1d58b2549f1b308d93e14914596: function AS_Button_jb05c1d58b2549f1b308d93e14914596(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_a4fb222c798149f7bd65cb3b1f9a2c67: function AS_Button_a4fb222c798149f7bd65cb3b1f9a2c67(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_dda95dda678a408d9c5618124fc0159b: function AS_Button_dda95dda678a408d9c5618124fc0159b(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_ab35350bf4e64103af2af809e09e0d29: function AS_Button_ab35350bf4e64103af2af809e09e0d29(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_f911181cb8df4b52beae5b62451677a3: function AS_Button_f911181cb8df4b52beae5b62451677a3(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_c701f3a08e2045a992f4ac2763e54583: function AS_FlexContainer_c701f3a08e2045a992f4ac2763e54583(eventobject) {
        var self = this;
        this.hideCancelPopup();
    },
    /** onClick defined for btnYes **/
    AS_Button_f59626f041d6462c9d96ec1a9110b972: function AS_Button_f59626f041d6462c9d96ec1a9110b972(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.cancelTransaction();
    },
    /** onClick defined for btnNo **/
    AS_Button_c0444a419f314e2ea27295721e60e4b1: function AS_Button_c0444a419f314e2ea27295721e60e4b1(eventobject) {
        var self = this;
        this.hideCancelPopup();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_g762e9d0e0884e55a7a0b0ac4093c113: function AS_Button_g762e9d0e0884e55a7a0b0ac4093c113(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_h678b5ce820c4c65ba9fe31adb4851aa: function AS_Button_h678b5ce820c4c65ba9fe31adb4851aa(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_h13cb9ca44e94c33aa79d7c577daa586: function AS_Button_h13cb9ca44e94c33aa79d7c577daa586(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_a0331d5e60dd4cfb961712e237e98a48: function AS_Button_a0331d5e60dd4cfb961712e237e98a48(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_eeabfec07cce418a8e9952b66e99bd20: function AS_Button_eeabfec07cce418a8e9952b66e99bd20(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** preShow defined for frmConfirmAccountEur **/
    AS_Form_e5095a0953fa4b42ad0c8f92c2177231: function AS_Form_e5095a0953fa4b42ad0c8f92c2177231(eventobject) {
        var self = this;
        this.preShowfrmConfirmAccount();
    },
    /** postShow defined for frmConfirmAccountEur **/
    AS_Form_ef82fb3276654e4092625e7408012924: function AS_Form_ef82fb3276654e4092625e7408012924(eventobject) {
        var self = this;
        this.postShowfrmConfirmAccount();
    },
    /** onDeviceBack defined for frmConfirmAccountEur **/
    AS_Form_c3c0ec66612041ca88cdb052c7924a1b: function AS_Form_c3c0ec66612041ca88cdb052c7924a1b(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** onTouchEnd defined for frmConfirmAccountEur **/
    AS_Form_ed3cddc975794f57956aadc13f42cdab: function AS_Form_ed3cddc975794f57956aadc13f42cdab(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});