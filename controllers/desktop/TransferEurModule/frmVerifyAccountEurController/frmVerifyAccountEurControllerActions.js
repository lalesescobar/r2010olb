define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnMakeTransfer **/
    AS_Button_g7b9b70010704c4784e8ecea54e44190: function AS_Button_g7b9b70010704c4784e8ecea54e44190(eventobject) {
        var self = this;
        this.makeNewTransfer();
    },
    /** preShow defined for frmVerifyAccountEur **/
    AS_Form_a6d95012925e49568753a2faeb1a2486: function AS_Form_a6d95012925e49568753a2faeb1a2486(eventobject) {
        var self = this;
        this.preshowfrmVerifyAccount();
    },
    /** postShow defined for frmVerifyAccountEur **/
    AS_Form_e59049cf0c6c4e9aa0bd936243e5ba03: function AS_Form_e59049cf0c6c4e9aa0bd936243e5ba03(eventobject) {
        var self = this;
        this.postShowVerifyAccount();
    },
});