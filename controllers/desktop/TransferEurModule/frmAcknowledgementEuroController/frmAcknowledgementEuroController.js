define(['FormControllerUtility', 'CommonUtilities', 'ViewConstants', 'OLBConstants', 'jspdf','jspdf_plugin_autotable'], function (FormControllerUtility, CommonUtilities, ViewConstants, OLBConstants, jsPDF, jspdf_plugin_autotable) {
    var responsiveUtils = new ResponsiveUtils();
    
    return {
        init: function () {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function () { };
            this.view.onBreakpointChange = this.onBreakpointChange;
        },
        onBreakpointChange: function (form, width) {
            var scope = this;
    		this.view.CustomPopupLogout.onBreakpointChangeComponent(scope.view.CustomPopupLogout,width);
            FormControllerUtility.setupFormOnTouchEnd(width);
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        },
        /**
         * Method to set formatted beneficiary address
         * @param {Object} data - beneficiary data
         */
        setFormattedAddress: function (data) {
            var scopeObj = this;
            if (!data.addressLine1 && !data.addressLine2 && !data.city && !data.postCode && !data.country) {
                scopeObj.view.lblAddress1.setVisibility(true);
                scopeObj.view.lblAddress2.setVisibility(false);
                scopeObj.view.lblAddress3.setVisibility(false);
                CommonUtilities.setText(scopeObj.view.lblAddress1, "-", CommonUtilities.getaccessibilityConfig());
                return;
            }
            if (!data.addressLine1) {
                scopeObj.view.lblAddress1.setVisibility(false);
            } else {
                scopeObj.view.lblAddress1.setVisibility(true);
                CommonUtilities.setText(scopeObj.view.lblAddress1, data.addressLine1, CommonUtilities.getaccessibilityConfig());
            }
            if (!data.addressLine2) {
                scopeObj.view.lblAddress2.setVisibility(false);
            } else {
                scopeObj.view.lblAddress2.setVisibility(true);
                CommonUtilities.setText(scopeObj.view.lblAddress2, data.addressLine2, CommonUtilities.getaccessibilityConfig());
            }
            if (!data.city && !data.postCode && !data.country) {
                scopeObj.view.lblAddress3.setVisibility(false);
            } else {
                scopeObj.view.lblAddress3.setVisibility(true);
                var strings = [data.city, data.country, data.postCode];
                CommonUtilities.setText(scopeObj.view.lblAddress3, strings.filter(function (string) {
                    if (string) {
                        return true;
                    }
                    return false;
                }).join(', '), CommonUtilities.getaccessibilityConfig());
            }
            scopeObj.view.forceLayout();
        },
        preShow: function () {
        },
        postShow: function () {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.frame.height - this.view.flxFooter.frame.height + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        /**
         * updateFormUI - the entry point method for the form controller.
         * @param {Object} viewModel - it contains the set of view properties and keys.
         */
        updateFormUI: function (viewModel) {
            if (viewModel.isLoading === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewModel.isLoading === false) {
                FormControllerUtility.hideProgressBar(this.view);
            }
            else if (viewModel.transferAcknowledge) {
                this.updateAckUI(viewModel.transferAcknowledge);
            }
        },
        isScheduled: function (data) {
          var sendonDateObject = applicationManager.getFormatUtilManager().getDateObjectFromCalendarString(data.sendOnDate, "dd/mm/yyyy");
            return data.frequency !== "Once" || sendonDateObject.getTime() > CommonUtilities.getServerDateObject().getTime();
        },
        getToAccountName: function (toAccount) {
            var accountName = toAccount.beneficiaryName || toAccount.nickName;
            var nameToShow = "";
            if (accountName) {
                nameToShow = accountName + "...." + CommonUtilities.getLastFourDigit(toAccount.accountNumber || toAccount.accountID);
            }
            else {
                nameToShow = toAccount.accountNumber;
            }
            return nameToShow;
        },
        updateAckUI: function (viewModel) {
            var scopeObj = this;
            var data = viewModel.transferData;
          
          if (data.isOwnAccount) {
            this.showOwnAccountFields();
          }
          else {
            this.showExternalAccountFields(data);
          }

          if (viewModel.chargesDetails && viewModel.chargesDetails.length > 0) {
            var charges = viewModel.chargesDetails;
            this.setSegmentData(charges);
          }else{
            this.view.flxFeeBreakdown.setVisibility(false);
          }
            this.view.flxSupportingDocumentsValue.lblSupportingDocumentsValue.text = "";
            var bankname = data.toAccount.bankName ? data.toAccount.bankName : "-";
            var bankCountry = (data.toAccount.bankCountry || "-");
            if(bankname === "-"){bankCountry = bankCountry;}
            else if(bankCountry ==="-"){bankCountry = bankname}
            else{ bankCountry = data.toAccount.bankName + ", " + bankCountry;}
            this.view.lblFromValue.text = data.fromAccount.nickName + "-" + data.fromAccount.accountID;
            this.view.lblBeneficiaryValue.text = this.getToAccountName(data.toAccount);
            this.view.lblAccountNumberValue.text = data.toAccount.accountNumber || data.toAccount.accountID;
            this.view.lblSWIFTBICValue.text = data.swiftCode;
            this.view.rtxBankAddressValue.text = bankCountry;
            this.view.lblAmountValue.text = CommonUtilities.formatCurrencyWithCommas(data.amount, false, data.currency);           
            this.view.btnDownloadReceipt.setVisibility(true);
            if(data.supportedDocuments.length>0){
              for (var i = 0; i < data.supportedDocuments.length; i++) {
                this.view.flxSupportingDocumentsValue.lblSupportingDocumentsValue.text += data.supportedDocuments[i];
                if(i !== data.supportedDocuments.length-1)
                    this.view.flxSupportingDocumentsValue.lblSupportingDocumentsValue.text += "\n";
              }
            }
            else{
              this.view.flxSupportingDocumentsValue.lblSupportingDocumentsValue.text = "None";
            }
            this.view.forceLayout();
            this.view.lblAmountValue.text = CommonUtilities.formatCurrencyWithCommas(data.amount, false, data.currency);     
                  
            this.view.lblTotalDebitValue.text = CommonUtilities.formatCurrencyWithCommas(viewModel.totalAmount, false, data.fromAccount.currencyCode);
          
          if(data.toAccount.isInternationalAccount === "true" && viewModel.exchangeRate){
            this.view.lblExchangeRateValue.text = '1.00 ' + data.fromAccount.currencyCode + ' = ' + viewModel.exchangeRate + ' ' + data.currency;
          }else{
            this.view.flxExchangeRate.setVisibility(false);
          }
            this.view.btnDownloadReceipt.onClick = function(){
                scopeObj.downloadPDF(viewModel);
              };
            this.view.lblFrequencyValue.text = data.frequency;
            this.view.lblSendOnValue.text = data.sendOnDate;
            this.view.flxEndingDate.setVisibility(data.EndingVisbility);
            this.view.lblEndingOnValue.setVisibility(data.EndingVisbility);
            this.view.lblEndingOnValue.text = data.endOnDate;
            this.view.lblPaymentReferenceValue.text = data.paymentReference || "-";
            this.view.lblBeneficiaryNicknameValue.text = data.toAccount.nickName || "-";
            this.view.lblRefrenceNumberValue.text = data.referenceId;
            this.view.lblCurrencyValue.text = data.currency;
            this.view.forceLayout();
          	this.view.lblPaymentMethodKey.text =kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentMethod");
            this.view.lblPaymentMethodValue.text = data.paymentMethod;
          	this.view.flxPaymentMethod.isVisible = true;
          	if(data.oneTimePayment){this.view.btnSaveBeneficiary.setVisibility(true);}
          	else{this.view.btnSaveBeneficiary.setVisibility(false);}
            if (this.isScheduled(data)) {
                CommonUtilities.setText(this.view.lblSuccessMessage, kony.i18n.getLocalizedString("i18n.TransfersEur.ScheduledTransactionMessage"), CommonUtilities.getaccessibilityConfig());
            } else {
                CommonUtilities.setText(this.view.lblSuccessMessage, kony.i18n.getLocalizedString("i18n.transfers.AcknowledgementMessage"), CommonUtilities.getaccessibilityConfig());
            }
            if (data.isOwnAccount && (data.toAccount.accountType === "Loan" || data.toAccount.accountType === "CreditCard")) {
                this.view.flxPaymentType.setVisibility(true);
                CommonUtilities.setText(this.view.lblPaymentTypeValue, data.paymentType, CommonUtilities.getaccessibilityConfig());
            } else {
                this.view.flxPaymentType.setVisibility(false);
            }
            if (data.isOwnAccount && data.toAccount.accountType === "CreditCard") {
                this.view.lblCreditCardPaymentMessage.setVisibility(true);
            } else {
                this.view.lblCreditCardPaymentMessage.setVisibility(false);
            }
            this.view.btnNewPayment.onClick = function () {
                if (data.isOwnAccount) {
                    applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({ context: "MakePaymentOwnAccounts" });
                } else {
                    applicationManager.getModulesPresentationController("TransferEurModule").showTransferScreen({ context: "MakePayment" });
                }
            }
            this.view.btnHome.onClick = function () {
                var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
                accountsModule.presentationController.showAccountsDashboard();
            }
            // this.view.lblRefrenceNumberValue.text = viewModel.transferData.accountFrom.accountName;
            this.view.lblBalanceValue.text = CommonUtilities.formatCurrencyWithCommas(viewModel.transferData.accountFrom.availableBalance, false, viewModel.transferData.accountFrom.currencyCode);

            this.getPayedByValue(data);
			
          	//add colon to labels - fix for responsive purposes
            CommonUtilities.setText(this.view.lblFromKey, kony.i18n.getLocalizedString("i18n.transfers.lblFrom") + ":", CommonUtilities.getaccessibilityConfig());           
            CommonUtilities.setText(this.view.lblAccountNumberKey, kony.i18n.getLocalizedString("i18n.common.accountNumber") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblPaymentTypeKey, kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentType") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblPaymentMethodKey, kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentMethod") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblSWIFTBICKey, kony.i18n.getLocalizedString("i18n.TransfersEur.SWIFTBIC") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblBankAddressKey, kony.i18n.getLocalizedString("i18n.transfers.bankAddress") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblCurrencyKey, kony.i18n.getLocalizedString("i18n.common.Currency") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblAmountKey, kony.i18n.getLocalizedString("i18n.transfers.lblAmount") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblExchangeRateKey, kony.i18n.getLocalizedString("i18n.TransfersEur.ExchangeRate") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblPaymentMediumKey, kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentMedium") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblFeeBreakdownKey, kony.i18n.getLocalizedString("i18n.TransfersEur.FeeBreakdown") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblTotalDebitKey, kony.i18n.getLocalizedString("i18n.TransfersEur.TotalDebit") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblFeesPaidByKey, kony.i18n.getLocalizedString("i18n.TransfersEur.FeesPaidBy") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblFrequencyKey, kony.i18n.getLocalizedString("i18n.transfers.lblFrequency") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblSendOnKey, kony.i18n.getLocalizedString("i18n.TransfersEur.SendOn") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblEndingOnKey, kony.i18n.getLocalizedString("i18n.PayAPerson.EndingOn") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblSupportingDocumentsKey, kony.i18n.getLocalizedString("i18n.TransfersEur.SupportingDocuments") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblBeneficiaryNicknameKey, kony.i18n.getLocalizedString("i18n.TransfersEur.BeneficiaryNickname") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(this.view.lblBeneficiaryAddressKey, kony.i18n.getLocalizedString("i18n.TransfersEur.BeneficiaryAddress") + ":", CommonUtilities.getaccessibilityConfig());
        },
        showOwnAccountFields: function () {
            var scopeObj = this;
            scopeObj.view.customheadernew.activateMenu("EUROTRANSFERS", "Transfer Between Accounts");
            CommonUtilities.setText(scopeObj.view.customheadernew.lblHeaderMobile, kony.i18n.getLocalizedString("i18n.hamburger.transfers"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblAcknowledgement, kony.i18n.getLocalizedString("i18n.hamburger.transfers"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblBeneficiaryKey, kony.i18n.getLocalizedString("i18n.common.To") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblPaymentReferenceKey, kony.i18n.getLocalizedString("i18n.TransfersEur.TransferReference") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.btnNewPayment, kony.i18n.getLocalizedString("i18n.transfers.makeanothertransfereuro"), CommonUtilities.getaccessibilityConfig());
            scopeObj.view.flxAccountNumber.setVisibility(false);
            scopeObj.view.flxSWIFTBIC.setVisibility(false);
            scopeObj.view.flxBankAddress.setVisibility(false);
            scopeObj.view.flxCurrency.setVisibility(true);
            scopeObj.view.flxExchangeRate.setVisibility(false);
            scopeObj.view.flxPaymentMedium.setVisibility(false);
            scopeObj.view.flxFeeBreakdown.setVisibility(false);
            scopeObj.view.flxTotalDebit.setVisibility(false);
            scopeObj.view.flxFeesPaidBy.setVisibility(false);
            scopeObj.view.flxBeneficiaryNickname.setVisibility(false);
            scopeObj.view.flxBeneficiaryAddress.setVisibility(false);  
        },
        showExternalAccountFields: function (data) {
            var scopeObj = this;
            scopeObj.view.customheadernew.activateMenu("EUROTRANSFERS", "Make a Payment");
            CommonUtilities.setText(scopeObj.view.customheadernew.lblHeaderMobile, kony.i18n.getLocalizedString("i18n.AccountsDetails.PAYMENTS"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblAcknowledgement, kony.i18n.getLocalizedString("i18n.AccountsDetails.PAYMENTS"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblBeneficiaryKey, kony.i18n.getLocalizedString("i18n.TransfersEur.Beneficiary") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblPaymentReferenceKey, kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentReference") + ":", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.btnNewPayment, kony.i18n.getLocalizedString("i18n.TransfersEur.NewPayment"), CommonUtilities.getaccessibilityConfig());
            this.setFormattedAddress(data);
            if (data.toAccount.isSameBankAccount=== "true") {
                scopeObj.view.flxSWIFTBIC.setVisibility(false);
            } else {
                scopeObj.view.flxSWIFTBIC.setVisibility(true);
            }
            if (data.toAccount && data.toAccount.isInternationalAccount === "false" && data.toAccount.isSameBankAccount === "false" && data.frequency === "Once") {
                scopeObj.view.flxPaymentMedium.setVisibility(true);
                CommonUtilities.setText(scopeObj.view.lblPaymentMediumValue, data.paymentMedium, CommonUtilities.getaccessibilityConfig());
            } else {
                scopeObj.view.flxPaymentMedium.setVisibility(false);
            }
            scopeObj.view.flxAccountNumber.setVisibility(true);
            scopeObj.view.flxBankAddress.setVisibility(true);
            scopeObj.view.flxCurrency.setVisibility(false);
            scopeObj.view.flxExchangeRate.setVisibility(true);
            scopeObj.view.flxFeeBreakdown.setVisibility(true);
            scopeObj.view.flxTotalDebit.setVisibility(true);
            scopeObj.view.flxFeesPaidBy.setVisibility(true);
            scopeObj.view.flxBeneficiaryNickname.setVisibility(true);
            scopeObj.view.flxBeneficiaryAddress.setVisibility(true);
        },
      
      getPayedByValue: function(data){
        var scopeObj = this;
        var payedByi18 = '';

        if(data.isPaidBy!==''){
          switch(data.isPaidBy){
            case 'OUR':
              payedByi18 = "i18n.TransfersEur.Me";
              break;
            case 'SHA':
              payedByi18 = "i18n.TransfersEur.Both5050";
              break;
            case 'BEN':
              payedByi18 = "i18n.TransfersEur.Beneficiary";
              break;
          }

          CommonUtilities.setText(scopeObj.view.lblFeesPaidByValue, kony.i18n.getLocalizedString(payedByi18), CommonUtilities.getaccessibilityConfig());
        }
        else{
          this.view.flxFeesPaidBy.setVisibility(false);
        }
      },
      
      downloadPDF: function(viewModel) {
        /**
        * jsPDF - https://github.com/MrRio/jsPDF.
        * docs - http://raw.githack.com/MrRio/jsPDF/master/docs/
        */
        var createPDF = jsPDF.jsPDF({
            orientation: 'p',
            unit: 'mm',
            format: 'a4',
            putOnlyUsedFonts: true,
            floatPrecision: 16 // or "smart", default is 16
        });
        var data = viewModel.transferData;
        /**
             * jsPDF - https://github.com/MrRio/jsPDF.
             * docs - http://raw.githack.com/MrRio/jsPDF/master/docs/
             */
        var bankAddress = kony.i18n.getLocalizedString("i18n.kony.bankaddress");
        var bankAddressParts = bankAddress.split('<br>');
        var tableHeader = kony.i18n.getLocalizedString("i18n.billPay.TransactionDetails");
        var row11 = kony.i18n.getLocalizedString("i18n.transfers.RefrenceNumber") + ":";
        var row12 = kony.i18n.getLocalizedString("i18n.transfers.lblFrom") + ":";
        var row13 = kony.i18n.getLocalizedString("i18n.TransfersEur.Beneficiary") || "Beneficiary Name" + ":";
        var row14 = kony.i18n.getLocalizedString("i18n.common.accountNumber") || "Account Number" + ":";
        if(data.paymentMethod != "Within Bank") {
          var row15 = kony.i18n.getLocalizedString("i18n.TransfersEur.SWIFTBIC") || "SWIFT/BIC" + ":";
          var row16 = kony.i18n.getLocalizedString("i18n.transfers.bankAddress") + ":";
          
          var row25 = data.swiftCode || "-";
          var row26 = data.toAccount.addressLine1 || "-";
          
          var row112 = kony.i18n.getLocalizedString("i18n.TransfersEur.FeesPaidBy") + ":";
          var row212 = data.isPaidBy;
        };
        var row17 = kony.i18n.getLocalizedString("i18n.transfers.lblAmount") + ":";
        if(!!data.paymentMedium){
          var row116 = kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentMedium") + ":";
          var row216 = data.paymentMedium || "-";
        }
        
        
        var row18 = kony.i18n.getLocalizedString("i18n.TransfersEur.TotalDebit") + ":";

        if(data.toAccount.isInternationalAccount === "true" && viewModel.exchangeRate){
          var row19 = kony.i18n.getLocalizedString("i18n.TransfersEur.ExchangeRate") + ":";
          var row29 = '1.00 ' + data.fromAccount.currencyCode + ' = ' + viewModel.exchangeRate + ' ' + data.currency;
        }

        if(viewModel.chargesDetails.length>0){
          var charges = viewModel.chargesDetails;

          //             var row110 = kony.i18n.getLocalizedString("i18n.TransfersEur.FeeBreakdown") + ":";
          var row210 = [];
          for (let k = 0; k < charges.length; k++) {
            const newRow = [charges[k].chargeName,charges[k].chargeAmount + " " +charges[k].chargeCurrency, '' , ''];
            row210.push(newRow);
          }
        };

        var row111 = kony.i18n.getLocalizedString("i18n.TransfersEur.SendOn") || "Date" + ":";
        
        var row113 = this.view.lblPaymentReferenceKey.text + ":";
        var row114 = kony.i18n.getLocalizedString("i18n.TransfersEur.BeneficiaryNickname") + ":";
        var row115 = kony.i18n.getLocalizedString("i18n.TransfersEur.BeneficiaryAddress") + ":";
        var row117 = kony.i18n.getLocalizedString("i18n.transfers.lblFrequency") + ":";
        var row118 = kony.i18n.getLocalizedString("i18n.TransfersEur.SupportingDocuments") + ":";
        var row119 = this.view.lblPaymentMethodKey.text + ":";
        var row21 = data.referenceId;
        var row22 = data.fromAccount.nickName + "-" + data.fromAccount.accountID;
        var row23 = data.toAccount.beneficiaryName || "-";
        var row24 = this.getToAccountName(data.toAccount);
       
        var row27 = data.amount + " " + data.currency;
        var row28 = viewModel.totalAmount + " " + data.fromAccount.currencyCode; //Total Debit

        var row211 = data.sendOnDate || "-";
        
        var row213 = data.paymentReference || "-";
        var row214 = data.toAccount.nickName || "";
        var row215 = data.addressLine1?(data.addressLine1 + (data.addressLine2 ? (',' + data.addressLine2) : '')):'-';
        var row217 = data.frequency;
        var row218 = "";
        if(data.supportedDocuments.length>0){
          for (var i = 0; i < data.supportedDocuments.length; i++) {
            row218 += data.supportedDocuments[i];
            if(i !== data.supportedDocuments.length-1) row218 += "\n";
          }
        }
        else{
          row218 = "None";
        }
        var row219 = data.paymentMethod;
        createPDF.addImage(this.view.customheadernew.flxLogoAndActions.flxLogoAndActionsWrapper.imgKony._kwebfw_.view.lastChild.currentSrc, "png", 15, 15, 50, 20, "NONE", 0);
        createPDF.line(10, 65, 200, 65);
        createPDF.setFontSize(12);
        createPDF.text(bankAddressParts, 15, 40);
        createPDF.setFontSize(16);
        createPDF.text(tableHeader, 15, 75);
        createPDF.line(10, 85, 200, 85);
        createPDF.line(10, 10, 200, 10);
        createPDF.line(200, 10, 200, 287);
        createPDF.line(200, 287, 10, 287);
        createPDF.line(10, 287, 10, 10);
        createPDF.autoTable({
          theme: ['plain'],
          startY: 83,
          head: [
            ['', '', '', '']
          ],
          body: [
            [row11, row21, '', ''],
            [row12, row22, '', ''],
            [row13, row23, '', ''],
            [row14, row24, '', ''],
            ...((this.view.flxPaymentMethod.isVisible === true) ? [[row119, row219, '', '']] : []),
            ...((data.paymentMethod != "Within Bank") ? [[row15, row25, '', '']] : []),
            ...((data.paymentMethod != "Within Bank") ? [[row16, row26, '', '']] : []),
            [row17, row27, '', ''],
            ...((this.view.flxPaymentMedium.isVisible === true) ? [[row116, row216, '', '']] : []),
            ...((this.view.flxTotalDebit.isVisible === true) ? [[row18, row28, '', '']] : []),
            ...((data.toAccount.isInternationalAccount === "true" && viewModel.exchangeRate) ? [[row19, row29, '', '']] : []),
            ...(viewModel.chargesDetails.length ? row210 : []),
            [row111, row211, '', ''],
            ...((data.paymentMethod != "Within Bank") ? [[row112, row212, '', '']] : []),
            ...((this.view.flxFrequency.isVisible === true) ? [[row117, row217, '', '']] : []),            
            [row118, row218, '', ''],
            [row113, row213, '', ''],
            [row114, row214, '', ''],
            [row115, row215, '', ''],
            // ...
          ],
        });
        createPDF.save(data.referenceId + ".pdf"); // generating the pdf file
      },
      
      setSegmentData: function(data){

        for(var i=0;i<data.length;i++){
          data[i].amountCurrency = CommonUtilities.formatCurrencyWithCommas(data[i].chargeAmount, false, data[i].chargeCurrency);    
        }

        this.view.sgmFeeBreakdown.widgetDataMap = this.getWidgetDataMap();
        this.view.sgmFeeBreakdown.setData(data);

      },

      getWidgetDataMap : function () {
        var map = 
            {
              lblChargeName:"chargeName",
              lblChargeValue:"amountCurrency",
            };
        return map;
      },
    };
});