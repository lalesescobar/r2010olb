define(['FormControllerUtility', 'CommonUtilities', 'ViewConstants', 'OLBConstants'], function (FormControllerUtility, CommonUtilities, ViewConstants, OLBConstants) {
    var responsiveUtils = new ResponsiveUtils();
 	return {
        init: function() {
            this.view.preShow = this.preShow;
            this.view.postShow = this.postShow;
            this.view.onDeviceBack = function() {};
            this.view.onBreakpointChange = this.onBreakpointChange;
            this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferEurModule").presentationController;
        },
        onBreakpointChange: function(form, width) {
            var scope = this;
            this.view.CustomPopupLogout.onBreakpointChangeComponent(scope.view.CustomPopupLogout, width);
            this.view.CustomPopupCancel.onBreakpointChangeComponent(scope.view.CustomPopupCancel, width);
            FormControllerUtility.setupFormOnTouchEnd(width);
            responsiveUtils.onOrientationChange(this.onBreakpointChange);
            this.view.customheadernew.onBreakpointChangeComponent(width);
            this.view.customfooternew.onBreakpointChangeComponent(width);
        },
        preShow: function() {
            this.view.customheadernew.activateMenu("EUROTRANSFERS", "Manage Beneficiaries");
        },
        postShow: function() {
            this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.frame.height - this.view.flxFooter.frame.height + "dp";
            applicationManager.getNavigationManager().applyUpdates(this);
        },
        /**
         * updateFormUI - the entry point method for the form controller.
         * @param {Object} viewModel - it contains the set of view properties and keys.
         */
        updateFormUI: function(viewModel) {
            if (viewModel.isLoading === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewModel.isLoading === false) {
                FormControllerUtility.hideProgressBar(this.view);
            } else {
                this.setBeneficiaryData(viewModel);
            }
        },
        /**
         * Method to set beneficiary data for confirmation
         * @param {Object} data beneficiary data
         */
        setBeneficiaryData: function(data) {
            var scopeObj = this;
            scopeObj.setFormattedAddress(data);
            var payment_method;
            if (data.isInternationalAccount === false && data.isSameBankAccount === false) {
                payment_method = "Domestic";
            } else if (data.isInternationalAccount === true && data.isSameBankAccount === false) {
                payment_method = "International";
            } else if (data.isInternationalAccount === false && data.isSameBankAccount === true) {
                payment_method = "Within Bank";
            }
            var bankname = data.bankName ? data.bankName : "-";
              var bankCountry = data.bankCountry ? data.bankCountry :"-";
              if(bankname === "-"){bankCountry = bankCountry;}
              else if(bankCountry ==="-"){bankCountry = bankname}
              else{ bankCountry = data.bankName + "," + bankCountry;}
            CommonUtilities.setText(scopeObj.view.lblAccountNumberValue, data.accountNumber, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblSwiftCodeValue, data.swiftCode ? data.swiftCode : "-", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblBankValue, bankCountry, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblNameValue, data.beneficiaryName, CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblNicknameValue, data.nickName ? data.nickName : "-", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblPhoneNumberValue, data.phone ? data.phone : "-", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblEmailAddressValue, data.email ? data.email : "-", CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblPaymentMethodKey, kony.i18n.getLocalizedString("i18n.TransfersEur.PaymentMethod"), CommonUtilities.getaccessibilityConfig());
            CommonUtilities.setText(scopeObj.view.lblPaymentMethodValue, payment_method, CommonUtilities.getaccessibilityConfig());
            scopeObj.view.forceLayout();
            scopeObj.view.btnCancel.onClick = function() {
                scopeObj.showCancelPopup();
            };
            scopeObj.view.btnConfirm.onClick = function() {
                scopeObj.presenter.addBeneficiaryDetails(data);
            };
          	scopeObj.view.btnModify.onClick = function() {
            	scopeObj.presenter.showView("frmAddBeneficiaryEuro", {
              		"modifyBeneficiary": data
            	});
            // applicationManager.getNavigationManager().navigateTo("frmAddBeneficiaryEuro");
          };
        },
        /**
         * Method to set formatted beneficiary address
         * @param {Object} data - beneficiary data
         */
        setFormattedAddress: function(data) {
            var scopeObj = this;
            if (!data.addressLine1 && !data.addressLine02 && !data.city && !data.zipcode && !data.country) {
                scopeObj.view.lblAddress1.setVisibility(true);
                scopeObj.view.lblAddress2.setVisibility(false);
                scopeObj.view.lblAddress3.setVisibility(false);
                CommonUtilities.setText(scopeObj.view.lblAddress1, "-", CommonUtilities.getaccessibilityConfig());
                return;
            }
            if (!data.addressLine1) {
                scopeObj.view.lblAddress1.setVisibility(false);
            } else {
                scopeObj.view.lblAddress1.setVisibility(true);
                CommonUtilities.setText(scopeObj.view.lblAddress1, data.addressLine1, CommonUtilities.getaccessibilityConfig());
            }
            if (!data.addressLine02) {
                scopeObj.view.lblAddress2.setVisibility(false);
            } else {
                scopeObj.view.lblAddress2.setVisibility(true);
                CommonUtilities.setText(scopeObj.view.lblAddress2, data.addressLine02, CommonUtilities.getaccessibilityConfig());
            }
            if (!data.city && !data.zipcode && !data.country) {
                scopeObj.view.lblAddress3.setVisibility(false);
            } else {
                scopeObj.view.lblAddress3.setVisibility(true);
                var strings = [data.city, data.country, data.zipcode];
                CommonUtilities.setText(scopeObj.view.lblAddress3, strings.filter(function(string) {
                    if (string) {
                        return true;
                    }
                    return false;
                }).join(', '), CommonUtilities.getaccessibilityConfig());
            }
            scopeObj.view.forceLayout();
        },
        /**
         * show or hide cancel popup
         */
        showCancelPopup: function() {
            var scopeObj = this;
            scopeObj.view.flxDialogs.setVisibility(true);
            scopeObj.view.flxCancelPopup.setVisibility(true);
            scopeObj.view.CustomPopupCancel.btnYes.onClick = function() {
                scopeObj.view.flxDialogs.setVisibility(false);
                scopeObj.view.flxCancelPopup.setVisibility(false);
                scopeObj.presenter.showTransferScreen({
                    context: "ManageBeneficiaries"
                });
            };
            scopeObj.view.CustomPopupCancel.btnNo.onClick = function() {
                scopeObj.view.flxDialogs.setVisibility(false);
                scopeObj.view.flxCancelPopup.setVisibility(false);
            };
            scopeObj.view.CustomPopupCancel.flxCross.onClick = function() {
                scopeObj.view.flxDialogs.setVisibility(false);
                scopeObj.view.flxCancelPopup.setVisibility(false);
            };
        }
    };
});