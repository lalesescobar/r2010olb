define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for lblOpenAccounttFrom **/
    AS_Label_a3b6b24fa60945f59e2c2f9524561e41: function AS_Label_a3b6b24fa60945f59e2c2f9524561e41(eventobject, x, y) {
        var nuoModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
        nuoModule.presentationController.showNewAccountOpening();
    },
    /** onTouchStart defined for lblOpenNewAccounttFrom **/
    AS_Label_ha674c641fd540afbe38f43b647bd7d8: function AS_Label_ha674c641fd540afbe38f43b647bd7d8(eventobject, x, y) {
        var nuoModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("NAOModule");
        nuoModule.presentationController.showNewAccountOpening();
    },
    /** onTouchStart defined for lblSendMoneyToNewRecipientTo **/
    AS_Label_fac70f90101844e9824bc606cf71174c: function AS_Label_fac70f90101844e9824bc606cf71174c(eventobject, x, y) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferFastModule").presentationController.showTransferScreen({
            showRecipientGateway: true
        });
    },
    /** onClick defined for flxAddBankAccount **/
    AS_FlexContainer_i7b64630c5de4a47b3edc7acf4023639: function AS_FlexContainer_i7b64630c5de4a47b3edc7acf4023639(eventobject) {
        this.addInternalAccount();
    },
    /** onClick defined for flxAddKonyAccount **/
    AS_FlexContainer_aaa057120f334808aad332476b34cbdc: function AS_FlexContainer_aaa057120f334808aad332476b34cbdc(eventobject) {
        this.addExternalAccount();
    },
    /** onClick defined for flxAddInternationalAccount **/
    AS_FlexContainer_g27b33cf44cb44c9a5d221a0b7a039eb: function AS_FlexContainer_g27b33cf44cb44c9a5d221a0b7a039eb(eventobject) {
        this.addInternationalAccount();
    },
    /** init defined for frmMakePayment **/
    AS_Form_bdd89eb8e22a424cbfcbe72d1b509773: function AS_Form_bdd89eb8e22a424cbfcbe72d1b509773(eventobject) {
		var self = this;
        this.init();
    }
});