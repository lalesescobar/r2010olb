define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_jd7e206bc9cc442ab93bd15d0ad81a6f: function AS_FlexContainer_jd7e206bc9cc442ab93bd15d0ad81a6f(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_bc08f1453afc41e89025069c60299176: function AS_Button_bc08f1453afc41e89025069c60299176(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnBreadcrumb2 **/
    AS_Button_c841f65e7ef54d06b62e93ce874ca566: function AS_Button_c841f65e7ef54d06b62e93ce874ca566(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for flxcheckbox **/
    AS_FlexContainer_i2e03083abb649a3b0bfb76a1ad7ed6c: function AS_FlexContainer_i2e03083abb649a3b0bfb76a1ad7ed6c(eventobject) {
        var self = this;
        this.checkboxActionDomestic();
    },
    /** onKeyUp defined for tbxRoutingNumberKA **/
    AS_TextField_c86fe8a8a4334959acc9a5f3ee4dd4c2: function AS_TextField_c86fe8a8a4334959acc9a5f3ee4dd4c2(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxBankNameKA **/
    AS_TextField_a6b6a538c9b442f6a8abcb872fe1a45b: function AS_TextField_a6b6a538c9b442f6a8abcb872fe1a45b(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxAccountNumberKA **/
    AS_TextField_idaa39ff1f8e4d50a953c1552792ac5c: function AS_TextField_idaa39ff1f8e4d50a953c1552792ac5c(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxAccountNumberAgainKA **/
    AS_TextField_d8aa69c75e124a4baa5f80122645746e: function AS_TextField_d8aa69c75e124a4baa5f80122645746e(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxBeneficiaryNameKA **/
    AS_TextField_c9aa36d27bff42498824966114870a63: function AS_TextField_c9aa36d27bff42498824966114870a63(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxAccountNickNameKA **/
    AS_TextField_aabb3e2e578c468e914c9606c18e0d3c: function AS_TextField_aabb3e2e578c468e914c9606c18e0d3c(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onClick defined for btnCancelKA **/
    AS_Button_ca406ab3e31f4456b14c13270a02b2cb: function AS_Button_ca406ab3e31f4456b14c13270a02b2cb(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.cancelTransaction();
    },
    /** onClick defined for btnAddAccountKA **/
    AS_Button_f88e76871afe4e95a6958c324bb8a74f: function AS_Button_f88e76871afe4e95a6958c324bb8a74f(eventobject) {
        var self = this;
        this.addDomesticAccount();
    },
    /** onClick defined for flxcheckboximg **/
    AS_FlexContainer_a73bc1e0e21a440f9dd435d4c9c2537d: function AS_FlexContainer_a73bc1e0e21a440f9dd435d4c9c2537d(eventobject) {
        var self = this;
        this.checkboxActionInternational();
    },
    /** onKeyUp defined for tbxIntSwiftCodeKA **/
    AS_TextField_ebb0500191b6451cb4dae5ec24c8c4da: function AS_TextField_ebb0500191b6451cb4dae5ec24c8c4da(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntBankNameKA **/
    AS_TextField_fdd2d1348443492ea23cc61a705003e0: function AS_TextField_fdd2d1348443492ea23cc61a705003e0(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntAccountNumberKA **/
    AS_TextField_f72f89778c76498d899384dfa7e0001c: function AS_TextField_f72f89778c76498d899384dfa7e0001c(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntAccountNumberAgainKA **/
    AS_TextField_b13c9e70b3744ec2942af956ff815062: function AS_TextField_b13c9e70b3744ec2942af956ff815062(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntBeneficiaryNameKA **/
    AS_TextField_aba6db52b2c14bca81ed45876dc7c77b: function AS_TextField_aba6db52b2c14bca81ed45876dc7c77b(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntAccountNickNameKA **/
    AS_TextField_d07fa87c6b434b9b8fcb0638e11d28f1: function AS_TextField_d07fa87c6b434b9b8fcb0638e11d28f1(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onClick defined for btnIntCancelKA **/
    AS_Button_ifc75367b1e7454cbb5e6287c7c9b5f1: function AS_Button_ifc75367b1e7454cbb5e6287c7c9b5f1(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.cancelTransaction();
    },
    /** onClick defined for btnIntAddAccountKA **/
    AS_Button_ba1d1482039947fa9b972a907be34fab: function AS_Button_ba1d1482039947fa9b972a907be34fab(eventobject) {
        var self = this;
        this.addInternationalAccount();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_dd2c1943cfb947f4832660d525121ea1: function AS_Button_dd2c1943cfb947f4832660d525121ea1(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_c9b5e85d81654ddcb07a8f91ecae2077: function AS_Button_c9b5e85d81654ddcb07a8f91ecae2077(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_g301114311f6427d9aad100ba1a8126e: function AS_Button_g301114311f6427d9aad100ba1a8126e(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_g54b1df2957a4a5389d879689110d7ae: function AS_Button_g54b1df2957a4a5389d879689110d7ae(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_fd720cd23dbc4c53af0f8239bca0b073: function AS_Button_fd720cd23dbc4c53af0f8239bca0b073(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for flxMainContainer **/
    AS_FlexContainer_c5277f48802b47cd91c1da2354fbd882: function AS_FlexContainer_c5277f48802b47cd91c1da2354fbd882(eventobject) {
        var self = this;
        //test
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_ce0d15c1f63e4655bcb0a80b69e98a01: function AS_Button_ce0d15c1f63e4655bcb0a80b69e98a01(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_d1fe89be49b54ce9950fcf95343a7c38: function AS_Button_d1fe89be49b54ce9950fcf95343a7c38(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_h285f19047c74c708769bfb7b28e97b2: function AS_Button_h285f19047c74c708769bfb7b28e97b2(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_j8d6d1b52e3e4c69969c8f2b9fff68a3: function AS_Button_j8d6d1b52e3e4c69969c8f2b9fff68a3(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_df95f318b7974666be637564ddbe5ab8: function AS_Button_df95f318b7974666be637564ddbe5ab8(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** preShow defined for frmAddExternalAccountEur **/
    AS_Form_d210b83ca5ec47ff8950760e3a1f0049: function AS_Form_d210b83ca5ec47ff8950760e3a1f0049(eventobject) {
        var self = this;
        this.preshowFrmAddAccount();
    },
    /** postShow defined for frmAddExternalAccountEur **/
    AS_Form_he60d67173d04cce9844f58d0a408e63: function AS_Form_he60d67173d04cce9844f58d0a408e63(eventobject) {
        var self = this;
        this.postshowAddExternalAccount();
    },
    /** onDeviceBack defined for frmAddExternalAccountEur **/
    AS_Form_c989b9fa4fce420e895d6bff6dd3812d: function AS_Form_c989b9fa4fce420e895d6bff6dd3812d(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmAddExternalAccountEur **/
    AS_Form_cd04732bb3784f81813535aaedbfc918: function AS_Form_cd04732bb3784f81813535aaedbfc918(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});