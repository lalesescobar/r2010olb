define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_ef1a296786b74a35a167d6cad7d2f5b3: function AS_FlexContainer_ef1a296786b74a35a167d6cad7d2f5b3(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_i3ea0a07838a47a0acf337fb09ef935c: function AS_Button_i3ea0a07838a47a0acf337fb09ef935c(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnBreadcrumb2 **/
    AS_Button_ad88995850b94d81b30fdbc2e832bea9: function AS_Button_ad88995850b94d81b30fdbc2e832bea9(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_e75b527aef8f4c08ab91890e15b59ded: function AS_Button_e75b527aef8f4c08ab91890e15b59ded(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_e1a4831ec26946f9b3e0dda4e1ebe8bb: function AS_Button_e1a4831ec26946f9b3e0dda4e1ebe8bb(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_efa3d27e9c5e44afaf3396be665d378e: function AS_Button_efa3d27e9c5e44afaf3396be665d378e(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_ha6ccdff7a0d4b3baf366c1baeb716ff: function AS_Button_ha6ccdff7a0d4b3baf366c1baeb716ff(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_h3719478a70948e3978255a99f05891e: function AS_Button_h3719478a70948e3978255a99f05891e(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_f3263c1f01d54ebc9f44f1af8c41dc7e: function AS_Button_f3263c1f01d54ebc9f44f1af8c41dc7e(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_b074d66bdfca492cb0249f1a2442393c: function AS_Button_b074d66bdfca492cb0249f1a2442393c(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_acca8bde7a3e4e2892acbcee9522f57c: function AS_Button_acca8bde7a3e4e2892acbcee9522f57c(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_ac743ec266af4528b6b093e6b60860fc: function AS_Button_ac743ec266af4528b6b093e6b60860fc(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_ja86a52984e14c1d8ff79e2d497b2d4c: function AS_Button_ja86a52984e14c1d8ff79e2d497b2d4c(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** init defined for frmAcknowledgementEur **/
    AS_Form_e1e9029a58fe44be97ff8d874405a417: function AS_Form_e1e9029a58fe44be97ff8d874405a417(eventobject) {
        var self = this;
        this.initActions();
    },
    /** postShow defined for frmAcknowledgementEur **/
    AS_Form_abaf8a0646c1481092b69737b88c3f7e: function AS_Form_abaf8a0646c1481092b69737b88c3f7e(eventobject) {
        var self = this;
        this.postShowFrmAcknowledgement();
    },
    /** onDeviceBack defined for frmAcknowledgementEur **/
    AS_Form_f5f706cae5f74d38a8c6719b15ce88b5: function AS_Form_f5f706cae5f74d38a8c6719b15ce88b5(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmAcknowledgementEur **/
    AS_Form_db2311ff6fec4f678ae757053d9cf9dc: function AS_Form_db2311ff6fec4f678ae757053d9cf9dc(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});