define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnRemove **/
    AS_Button_ce8269bf276145c294a70772a87eb64f: function AS_Button_ce8269bf276145c294a70772a87eb64f(eventobject, context) {
        var self = this;
        this.executeOnParent("removeTransactionRow");
    }
});