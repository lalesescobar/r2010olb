define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxAddKonyAccount **/
    AS_FlexContainer_h1a2d0b14cf14794b0d09791095ea51c: function AS_FlexContainer_h1a2d0b14cf14794b0d09791095ea51c(eventobject) {
        var self = this;
        this.ShowDomesticAccount();
    },
    /** onClick defined for flxAddNonKonyAccount **/
    AS_FlexContainer_d34e27f9307e49438d8f99c9f17fabc5: function AS_FlexContainer_d34e27f9307e49438d8f99c9f17fabc5(eventobject) {
        var self = this;
        this.showInternationalAccount();
    },
    /** onClick defined for flxAddInternationalAccount **/
    AS_FlexContainer_c377d9b292ee470b80aa2f4b4daf5849: function AS_FlexContainer_c377d9b292ee470b80aa2f4b4daf5849(eventobject) {
        var self = this;
        this.showOneTimeTransfer();
    },
    /** init defined for frmWireTransferAddKonyAccountStep1 **/
    AS_Form_i336c2cc1516474b9a603e8c84a1fa1c: function AS_Form_i336c2cc1516474b9a603e8c84a1fa1c(eventobject) {
        var self = this;
        this.init();
    }
});