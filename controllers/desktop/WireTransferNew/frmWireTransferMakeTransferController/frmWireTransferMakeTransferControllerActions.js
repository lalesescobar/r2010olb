define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnStepContinue **/
    AS_Button_e7cd20f6201f4f1b812b4392d6ea07ea: function AS_Button_e7cd20f6201f4f1b812b4392d6ea07ea(eventobject) {
        var self = this;
        this.onMakeTransferContinue();
    },
    /** onClick defined for flxAddKonyAccount **/
    AS_FlexContainer_i3ca255fcfc34f71a5873af76573d367: function AS_FlexContainer_i3ca255fcfc34f71a5873af76573d367(eventobject) {
        var self = this;
        this.ShowDomesticAccount();
    },
    /** onClick defined for flxAddNonKonyAccount **/
    AS_FlexContainer_gc8a7491494442eaabea213c895903fd: function AS_FlexContainer_gc8a7491494442eaabea213c895903fd(eventobject) {
        var self = this;
        this.showInternationalAccount();
    },
    /** onClick defined for flxAddInternationalAccount **/
    AS_FlexContainer_d2adc1a58a294d9b84d36235ecd8f8d7: function AS_FlexContainer_d2adc1a58a294d9b84d36235ecd8f8d7(eventobject) {
        var self = this;
        this.showOneTimeTransfer();
    },
    /** init defined for frmWireTransferMakeTransfer **/
    AS_Form_h513afc8426b4179ac476f78f481b24a: function AS_Form_h513afc8426b4179ac476f78f481b24a(eventobject) {
        var self = this;
        this.init();
    }
});