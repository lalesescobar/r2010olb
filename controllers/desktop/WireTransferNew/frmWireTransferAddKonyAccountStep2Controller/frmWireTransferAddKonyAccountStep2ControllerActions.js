define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxAddKonyAccount **/
    AS_FlexContainer_g475c9383ae04b6f9e9ee12dd5981ed9: function AS_FlexContainer_g475c9383ae04b6f9e9ee12dd5981ed9(eventobject) {
        this.ShowDomesticAccount();
    },
    /** onClick defined for flxAddNonKonyAccount **/
    AS_FlexContainer_h95fa993202d4848b8326cf2efe5f0ed: function AS_FlexContainer_h95fa993202d4848b8326cf2efe5f0ed(eventobject) {
        this.showInternationalAccount();
    },
    /** onClick defined for flxAddInternationalAccount **/
    AS_FlexContainer_aad8d2c0794b44b388d4afad86aa25c2: function AS_FlexContainer_aad8d2c0794b44b388d4afad86aa25c2(eventobject) {
        this.showOneTimeTransfer();
    },
    /** init defined for frmWireTransferAddKonyAccountStep2 **/
    AS_Form_g4a4863249ae4f35a37e15db42d3a77b: function AS_Form_g4a4863249ae4f35a37e15db42d3a77b(eventobject) {
        this.init();
    }
});