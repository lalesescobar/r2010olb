define({
  /*
    This is an auto generated file and any modifications to it may result in corruption of the action sequence.
  */
  /** onClick defined for flxAddKonyAccount **/
  AS_FlexContainer_h7c30a6564a04742ae3a2dce91cf5f55: function AS_FlexContainer_h7c30a6564a04742ae3a2dce91cf5f55(eventobject) {
      var self = this;
      this.ShowDomesticAccount();
  },
  /** onClick defined for flxAddNonKonyAccount **/
  AS_FlexContainer_b89cd9381b67445bbee4ce19ec05d2f5: function AS_FlexContainer_b89cd9381b67445bbee4ce19ec05d2f5(eventobject) {
      var self = this;
      this.showInternationalAccount();
  },
  /** onClick defined for flxAddInternationalAccount **/
  AS_FlexContainer_b8894b752a45401cbed3c13d79707866: function AS_FlexContainer_b8894b752a45401cbed3c13d79707866(eventobject) {
      var self = this;
      this.showOneTimeTransfer();
  },
  /** init defined for frmAccountInfoForInboundTransfer **/
  AS_Form_cbe041472f1c4a9c98f2a20077075dbd: function AS_Form_cbe041472f1c4a9c98f2a20077075dbd(eventobject) {
      var self = this;
      this.init();
  }
});