define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_ca9810c2acea49b1b9b7c2042513d2b1: function AS_FlexContainer_ca9810c2acea49b1b9b7c2042513d2b1(eventobject, context) {
        var self = this;
        this.segmentHistoryRowClick();
    },
    /** onClick defined for btnRepeat **/
    AS_Button_c3add177718041faa6182a53c489574d: function AS_Button_c3add177718041faa6182a53c489574d(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for btnEdit **/
    AS_Button_d939fe300a8d405ab5c053bbe90bd8a8: function AS_Button_d939fe300a8d405ab5c053bbe90bd8a8(eventobject, context) {
        var self = this;
        this.showPayABill();
    }
});