define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmFastAddInternationalAccountAcknowledgement **/
    AS_Form_ce6929e545704f6eabfe4eb4a998371c: function AS_Form_ce6929e545704f6eabfe4eb4a998371c(eventobject) {
        var self = this;
        this.init();
    }
});