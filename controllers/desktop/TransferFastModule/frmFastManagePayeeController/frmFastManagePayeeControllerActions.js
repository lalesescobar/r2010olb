define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmFastManagePayee **/
    AS_Form_c645d4c541314dbd9abbbbc6bed0b063: function AS_Form_c645d4c541314dbd9abbbbc6bed0b063(eventobject) {
        var self = this;
        this.init();
    }
});