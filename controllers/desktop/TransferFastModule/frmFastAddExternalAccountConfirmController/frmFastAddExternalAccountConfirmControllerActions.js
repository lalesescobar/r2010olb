define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmFastAddExternalAccountConfirm **/
    AS_Form_a7fda30b2d0f427c80da567eabb92a23: function AS_Form_a7fda30b2d0f427c80da567eabb92a23(eventobject) {
        var self = this;
        this.init();
    }
});