define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmFastVerifyAccount **/
    AS_Form_ec1a346643b5484d9562198b8bc0bf03: function AS_Form_ec1a346643b5484d9562198b8bc0bf03(eventobject) {
        var self = this;
        this.init();
    }
});