define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmFastDeActiveRecipient **/
    AS_Form_a9f0b204f3c04a21bb88a065b2d0e2d9: function AS_Form_a9f0b204f3c04a21bb88a065b2d0e2d9(eventobject) {
        var self = this;
        this.init();
    }
});