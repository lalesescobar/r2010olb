define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onKeyUp defined for tbxAccountNumberKA **/
    AS_TextField_j3c0fae1f0904ed99db3351383296c9c: function AS_TextField_j3c0fae1f0904ed99db3351383296c9c(eventobject) {
        var self = this;
        this.validateDBXAccountFields();
    },
    /** onKeyUp defined for tbxAccountNumberAgainKA **/
    AS_TextField_hc1e53ea31304f8ba871cdf126e7fb05: function AS_TextField_hc1e53ea31304f8ba871cdf126e7fb05(eventobject) {
        var self = this;
        this.validateDBXAccountFields();
    },
    /** onKeyUp defined for tbxBeneficiaryNameKA **/
    AS_TextField_fcd6d786b8444568ba37f0733c702255: function AS_TextField_fcd6d786b8444568ba37f0733c702255(eventobject) {
        var self = this;
        this.validateDBXAccountFields();
    },
    /** onKeyUp defined for tbxAccountNickNameKA **/
    AS_TextField_dc231ec4ffe145e3a46b53b61ea7c11b: function AS_TextField_dc231ec4ffe145e3a46b53b61ea7c11b(eventobject) {
        var self = this;
        this.validateDBXAccountFields();
    },
    /** init defined for frmFastAddDBXAccount **/
    AS_Form_dc7d614af1f84657ae6869ffaea74d7f: function AS_Form_dc7d614af1f84657ae6869ffaea74d7f(eventobject) {
        var self = this;
        this.init();
    }
});