define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmFastAddInternationalAccountConfirm **/
    AS_Form_e9861f8d73aa4ebc886496d2fb43f965: function AS_Form_e9861f8d73aa4ebc886496d2fb43f965(eventobject) {
        var self = this;
        this.init();
    }
});