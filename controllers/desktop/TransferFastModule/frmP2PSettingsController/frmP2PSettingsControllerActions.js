define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmP2PSettings **/
    AS_Form_d8e05c2b03ea405f9a9988bf95dcc2bd: function AS_Form_d8e05c2b03ea405f9a9988bf95dcc2bd(eventobject) {
        var self = this;
        this.init();
    }
});