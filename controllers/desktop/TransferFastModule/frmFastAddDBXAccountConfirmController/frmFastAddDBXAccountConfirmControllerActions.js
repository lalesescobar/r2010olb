define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmFastAddDBXAccountConfirm **/
    AS_Form_fc92632a893b4cf28a7f9c3083b051bc: function AS_Form_fc92632a893b4cf28a7f9c3083b051bc(eventobject) {
        var self = this;
        this.init();
    }
});