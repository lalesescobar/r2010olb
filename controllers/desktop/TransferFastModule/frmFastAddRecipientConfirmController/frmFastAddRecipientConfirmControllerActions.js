define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmFastAddRecipientConfirm **/
    AS_Form_fe5862632cd349f3ba1d2c8d3177d7dd: function AS_Form_fe5862632cd349f3ba1d2c8d3177d7dd(eventobject) {
        var self = this;
        this.init();
    }
});