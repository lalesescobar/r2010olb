define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmFastAddExternalAccountAcknowledgement **/
    AS_Form_ef6624ee6d774226ba6ea4fc90f7bd04: function AS_Form_ef6624ee6d774226ba6ea4fc90f7bd04(eventobject) {
        var self = this;
        this.init();
    }
});