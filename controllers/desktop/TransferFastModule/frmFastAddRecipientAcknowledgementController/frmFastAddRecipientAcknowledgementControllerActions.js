define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmFastAddRecipientAcknowledgement **/
    AS_Form_c8189873528a4f8090824b6ac61b7c45: function AS_Form_c8189873528a4f8090824b6ac61b7c45(eventobject) {
        var self = this;
        this.init();
    }
});