define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** init defined for frmReview **/
    AS_Form_gc80c5fcf99249978d6cd9112149ae76: function AS_Form_gc80c5fcf99249978d6cd9112149ae76(eventobject) {
        var self = this;
        this.init();
    }
});