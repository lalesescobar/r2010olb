define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onSelection defined for lstType **/
    AS_ListBox_jaa01a743ea746369350e390869e7174: function AS_ListBox_jaa01a743ea746369350e390869e7174(eventobject, context) {
        var self = this;
        return self.onTypeSelection.call(this, eventobject, context);
    },
});