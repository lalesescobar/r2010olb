define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContent **/
    AS_FlexContainer_c8ac098d4943424a8bc68ab666719e55: function AS_FlexContainer_c8ac098d4943424a8bc68ab666719e55(eventobject, x, y, context) {
        var self = this;
        this.accountPressed(context);
    },
    /** onTouchStart defined for flxMenu **/
    AS_FlexContainer_eddabd2827d142e781edcb38358b310b: function AS_FlexContainer_eddabd2827d142e781edcb38358b310b(eventobject, x, y, context) {
        var self = this;
        this.setClickOrigin();
    },
    /** onClick defined for flxMenu **/
    AS_FlexContainer_j68e1e4184f9457dae4a73b672400fa8: function AS_FlexContainer_j68e1e4184f9457dae4a73b672400fa8(eventobject, context) {
        var self = this;
        this.menuPressed();
    }
});