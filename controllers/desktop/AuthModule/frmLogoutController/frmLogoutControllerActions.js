define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for AlterneteActionsSignIn **/
    AS_UWI_je608273a6454f06a4d2f35db01cc151: function AS_UWI_je608273a6454f06a4d2f35db01cc151(eventobject, x, y) {
        var self = this;
        this.loginWithVerifiedUserName();
    },
    /** onTouchStart defined for AlterneteActionsResetPassword **/
    AS_UWI_ca0c9310a1aa47c38fc7ccf76469ece5: function AS_UWI_ca0c9310a1aa47c38fc7ccf76469ece5(eventobject, x, y) {
        var self = this;
        this.goToPasswordResetOptionsPage();
    },
    /** onClick defined for btnProceed **/
    AS_Button_jd5c081d37894791b4928aedb8beaa5f: function AS_Button_jd5c081d37894791b4928aedb8beaa5f(eventobject) {
        var self = this;
        this.letsGetStarted();
    },
    /** onRowClick defined for segLanguagesList **/
    AS_Segment_e35327828272483cb069075c61b9360a: function AS_Segment_e35327828272483cb069075c61b9360a(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.selectYourLanguage();
    },
    /** onClick defined for btnYes **/
    AS_Button_c226fb7b57314d28894f20c40ee1ff1b: function AS_Button_c226fb7b57314d28894f20c40ee1ff1b(eventobject) {
        var self = this;
        this.btnYesTakeSurvey();
    },
    /** onClick defined for btnNo **/
    AS_Button_fa0c887a1cb1471cbd4cf406d78c0655: function AS_Button_fa0c887a1cb1471cbd4cf406d78c0655(eventobject) {
        var self = this;
        this.btnNoTakeSurvey();
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_c3c3e8028f404228abd717613254d512: function AS_FlexContainer_c3c3e8028f404228abd717613254d512(eventobject) {
        var self = this;
        this.onFeedbackCrossClick();
    },
    /** init defined for frmLogout **/
    AS_Form_b7812fe2d9234913a9f7aa9e8775f40e: function AS_Form_b7812fe2d9234913a9f7aa9e8775f40e(eventobject) {
        var self = this;
        this.frmLoginInit();
    },
    /** preShow defined for frmLogout **/
    AS_Form_fe0db28461124300ba070ab83648aec3: function AS_Form_fe0db28461124300ba070ab83648aec3(eventobject) {
        var self = this;
        this.onPreShow();
    },
    /** postShow defined for frmLogout **/
    AS_Form_fdde57133c1e42fcb2459cab9b31c488: function AS_Form_fdde57133c1e42fcb2459cab9b31c488(eventobject) {
        var self = this;
        this.onPostShow();
    },
    /** onDeviceBack defined for frmLogout **/
    AS_Form_df9059e362b6444a82d194b315dc1526: function AS_Form_df9059e362b6444a82d194b315dc1526(eventobject) {
        var self = this;
        kony.print("User pressed back button");
    },
    /** onTouchEnd defined for frmLogout **/
    AS_Form_ja6a1c95eec5449aad1834e0f47bfc18: function AS_Form_ja6a1c95eec5449aad1834e0f47bfc18(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onBreakpointChange defined for frmLogout **/
    AS_Form_j13c2775b263479a9a1d152b0d608c0d: function AS_Form_j13c2775b263479a9a1d152b0d608c0d(eventobject, breakpoint) {
        var self = this;
        return self.onBreakpointChange.call(this, breakpoint);
    }
});