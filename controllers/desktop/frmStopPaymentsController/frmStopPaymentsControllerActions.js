define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_d5e410b3c47f487f85aa149adc755739: function AS_FlexContainer_d5e410b3c47f487f85aa149adc755739(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_fb5a0d943cbd487ea26c5488b18a8748: function AS_Button_fb5a0d943cbd487ea26c5488b18a8748(eventobject) {
        var self = this;
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountModule.presentationController.showAccountsDashboard();
    },
    /** init defined for frmStopPayments **/
    AS_Form_f3e78a5e6a0342a7b9f626ade63f074d: function AS_Form_f3e78a5e6a0342a7b9f626ade63f074d(eventobject) {
        var self = this;
        this.frmStopPaymentsInitAction();
    },
    /** postShow defined for frmStopPayments **/
    AS_Form_f72b4190d92c4539a0ddde4bfa559352: function AS_Form_f72b4190d92c4539a0ddde4bfa559352(eventobject) {
        var self = this;
        this.postShowfrmStopCheckPayments();
    }
});