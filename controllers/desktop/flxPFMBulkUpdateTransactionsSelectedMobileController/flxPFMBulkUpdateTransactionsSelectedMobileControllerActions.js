define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_gb2ce9fd6cc4456488671e9661f270d8: function AS_FlexContainer_gb2ce9fd6cc4456488671e9661f270d8(eventobject, context) {
        var self = this;
        this.showSelectedRow();
    },
    /** onClick defined for flxCheckbox **/
    AS_FlexContainer_g3f01022a0114c01a387c7531b5567d6: function AS_FlexContainer_g3f01022a0114c01a387c7531b5567d6(eventobject, context) {
        var self = this;
        this.toggleCheckBox();
    }
});