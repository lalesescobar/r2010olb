define({
  removeClicked: function() {
    var currentForm = kony.application.getCurrentForm();
    currentForm.setContentOffset({x:"0%",y:"0%"}, true);
    currentForm.flxDialogs.setVisibility(true);
    currentForm.flxAttachmentsPopup.setVisibility(true);
    currentForm.flxAttachmentsPopup.AttachmentsPopup.lblHeading.text = kony.i18n.getLocalizedString("i18n.TransfersEur.RemoveAttachmentPopupHeading");
    currentForm.flxAttachmentsPopup.AttachmentsPopup.lblPopupMessage.text = kony.i18n.getLocalizedString("i18n.TransfersEur.RemoveAttachmentPopupMsg");
    currentForm.flxAttachmentsPopup.AttachmentsPopup.btnYes.onClick = this.deleteAttachment;
    currentForm.flxAttachmentsPopup.AttachmentsPopup.btnNo.onClick = this.closeAttachmentsPopup;
    currentForm.forceLayout();
  },
  deleteAttachment: function() {
    var index = kony.application.getCurrentForm().segAddedDocuments.selectedRowIndex;
    var sectionIndex = index[0];
    var rowIndex = index[1];
    var deletedAttachment = kony.application.getCurrentForm().segAddedDocuments.data[rowIndex];
    kony.application.getCurrentForm().segAddedDocuments.removeAt(rowIndex, sectionIndex);
    this.closeAttachmentsPopup();
    var controller = _kony.mvc.GetController('frmMakePayment', true);
    controller.removeAttachments(deletedAttachment);
  },
  closeAttachmentsPopup: function() {
    var currentForm = kony.application.getCurrentForm();
    currentForm.flxDialogs.setVisibility(false);
    currentForm.flxAttachmentsPopup.setVisibility(false);
  },
  showDownloadPopup: function() {
    var currentForm = kony.application.getCurrentForm();
    currentForm.flxDialogs.setVisibility(true);
    currentForm.flxDownloadsPopup.setVisibility(true);
  }
});