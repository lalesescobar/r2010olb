define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for lblAttachedDocument **/
    AS_Label_e995abd3dc7240118eb2ef6c36219d19: function AS_Label_e995abd3dc7240118eb2ef6c36219d19(eventobject, x, y, context) {
        var self = this;
        return self.showDownloadPopup.call(this);
    },
    /** onTouchEnd defined for imgRemoveAttachment **/
    AS_Image_e196bc99f47c463fa06191b61a5be30d: function AS_Image_e196bc99f47c463fa06191b61a5be30d(eventobject, x, y, context) {
        var self = this;
        return self.removeClicked.call(this);
    }
});