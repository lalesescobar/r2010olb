define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_c88e895a86ea472295423e6b33e2aac3: function AS_FlexContainer_c88e895a86ea472295423e6b33e2aac3(eventobject, context) {
        var self = this;
        this.segmentAllPayeesRowClick();
    },
    /** onClick defined for btnEbill **/
    AS_Button_b602b942ebe241718297a7a27be78607: function AS_Button_b602b942ebe241718297a7a27be78607(eventobject, context) {
        var self = this;
        this.viewEBill();
    },
    /** onClick defined for btnActivateEbill **/
    AS_Button_f8f563b61cc040ee810364c1ef5c1efd: function AS_Button_f8f563b61cc040ee810364c1ef5c1efd(eventobject, context) {
        var self = this;
        this.executeOnParent("activateEBill");
    },
    /** onClick defined for btnViewEBill **/
    AS_Button_jefb24740cd74440ba392e3af763c0fe: function AS_Button_jefb24740cd74440ba392e3af763c0fe(eventobject, context) {
        var self = this;
        //test
    }
});