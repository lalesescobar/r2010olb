define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxResetUserImg **/
    AS_FlexContainer_b6fcbba1515746e6870815fd80586308: function AS_FlexContainer_b6fcbba1515746e6870815fd80586308(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onClick defined for flxUserId **/
    AS_FlexContainer_bf266e1affd246c9b02b95f614ee15f9: function AS_FlexContainer_bf266e1affd246c9b02b95f614ee15f9(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_f614fdb2aaa24596a3cac37123a34974: function AS_FlexContainer_f614fdb2aaa24596a3cac37123a34974(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for flxClose **/
    AS_FlexContainer_jcc82eb02e964112ba41e910ebfb1a98: function AS_FlexContainer_jcc82eb02e964112ba41e910ebfb1a98(eventobject) {
        var self = this;
        //this.closeHamburgerMenu();
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_b85de28e434d4cc09e0d4eaf17a7d421: function AS_Button_b85de28e434d4cc09e0d4eaf17a7d421(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnBreadcrumb2 **/
    AS_Button_h45941de98084387b3e0e0c5028c3cd7: function AS_Button_h45941de98084387b3e0e0c5028c3cd7(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onTouchEnd defined for lblRadioType1 **/
    AS_Label_c9a082d05099404da37c569748d6ab48: function AS_Label_c9a082d05099404da37c569748d6ab48(eventobject, x, y) {
        var self = this;
        this.setActionsOnCredential();
    },
    /** onSelection defined for rbtVerifyBankCredential **/
    AS_RadioButtonGroup_cde58caaf6a441659ea91cba5e8cf4ac: function AS_RadioButtonGroup_cde58caaf6a441659ea91cba5e8cf4ac(eventobject) {
        var self = this;
        this.setActionsOnCredential();
    },
    /** onTouchEnd defined for lblRadioType2 **/
    AS_Label_e81cf4d8d2ab4b298d277b2724e8b065: function AS_Label_e81cf4d8d2ab4b298d277b2724e8b065(eventobject, x, y) {
        var self = this;
        this.setActionsOnTrialDeposit();
    },
    /** onSelection defined for rbtVerifyByDeposits **/
    AS_RadioButtonGroup_d477bbfcdf08478f9a07d7019e06269c: function AS_RadioButtonGroup_d477bbfcdf08478f9a07d7019e06269c(eventobject) {
        var self = this;
        this.setActionsOnTrialDeposit();
    },
    /** onTextChange defined for tbxUsername **/
    AS_TextField_e2ee1e6112584d2da00fd200f6476819: function AS_TextField_e2ee1e6112584d2da00fd200f6476819(eventobject, changedtext) {
        var self = this;
        this.checkSkin();
    },
    /** onTextChange defined for tbxPassword **/
    AS_TextField_a7ce666989574efc88f03ae251000527: function AS_TextField_a7ce666989574efc88f03ae251000527(eventobject, changedtext) {
        var self = this;
        this.checkSkin();
    },
    /** onClick defined for btnCancel **/
    AS_Button_d94bbced113e44c88756355bb42e4fe0: function AS_Button_d94bbced113e44c88756355bb42e4fe0(eventobject) {
        var self = this;
        this.presenter.cancelTransaction(this);
    },
    /** onClick defined for btnConfirm **/
    AS_Button_f6dc931dc9c541dc95e653ee5d8b9f41: function AS_Button_f6dc931dc9c541dc95e653ee5d8b9f41(eventobject) {
        var self = this;
        this.checkVerificationOptions();
    },
    /** onClick defined for btnMakeTransfer **/
    AS_Button_h1b5bdcacfc8419f8ecef287d33ec0a6: function AS_Button_h1b5bdcacfc8419f8ecef287d33ec0a6(eventobject) {
        var self = this;
        this.makeNewTransfer();
    },
    /** onClick defined for btnAddAnotherAccount **/
    AS_Button_g2f132f0d99c46b997b6661b923a59bc: function AS_Button_g2f132f0d99c46b997b6661b923a59bc(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.addAnotherAccount();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_c71eed3782714061b7d031b55a03c4f2: function AS_Button_c71eed3782714061b7d031b55a03c4f2(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_db244c1498f941189ae85b06b77d4131: function AS_Button_db244c1498f941189ae85b06b77d4131(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_je26c5600c8d40a9b9583d0a38def344: function AS_Button_je26c5600c8d40a9b9583d0a38def344(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_jb6eea5d249e4d1189b5860c0268bcc3: function AS_Button_jb6eea5d249e4d1189b5860c0268bcc3(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_aa3cb165617c46a99edc578a4b96de0b: function AS_Button_aa3cb165617c46a99edc578a4b96de0b(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_j4f936006ab445bea35205c1def97946: function AS_Button_j4f936006ab445bea35205c1def97946(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_e973df63ce7743919a9e50f408d3df40: function AS_Button_e973df63ce7743919a9e50f408d3df40(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_gdbe108f9b9542bfb62818d0e0851d53: function AS_Button_gdbe108f9b9542bfb62818d0e0851d53(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_dab156eec8e44818bb36ec785c8ad6bf: function AS_Button_dab156eec8e44818bb36ec785c8ad6bf(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_a06257b36ce14d8fafece96a672e64a1: function AS_Button_a06257b36ce14d8fafece96a672e64a1(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** preShow defined for frmVerifyAccountEur **/
    AS_Form_aedb1f11e53d49c4855deefba50006ad: function AS_Form_aedb1f11e53d49c4855deefba50006ad(eventobject) {
        var self = this;
        this.preshowfrmVerifyAccount();
    },
    /** postShow defined for frmVerifyAccountEur **/
    AS_Form_f604881d41c54a6bb475436e7386163e: function AS_Form_f604881d41c54a6bb475436e7386163e(eventobject) {
        var self = this;
        this.postShowVerifyAccount();
    },
    /** onDeviceBack defined for frmVerifyAccountEur **/
    AS_Form_aae59db829dc40f3ad09acbb1925a3a0: function AS_Form_aae59db829dc40f3ad09acbb1925a3a0(eventobject) {
        var self = this;
        //Need to Consolidate
        kony.print("Back button pressed");
    },
    /** onTouchEnd defined for frmVerifyAccountEur **/
    AS_Form_h3e47c8487d44411b0513e016a046f66: function AS_Form_h3e47c8487d44411b0513e016a046f66(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});