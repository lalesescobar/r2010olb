define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onSelection defined for lstFrmAccount **/
    AS_ListBox_g36503f916824bd3a115a75e2fc26759: function AS_ListBox_g36503f916824bd3a115a75e2fc26759(eventobject, context) {
        var self = this;
        return self.onTypeSelection.call(this, eventobject, context);
    }
});