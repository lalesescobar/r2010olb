define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnAddRecipient **/
    AS_Button_cd21c2b6b19740f28ff9235f79e2071f: function AS_Button_cd21c2b6b19740f28ff9235f79e2071f(eventobject) {
        var self = this;
        this.showAddRecipient();
    },
    /** onClick defined for btnSendMoneyNewRecipient **/
    AS_Button_g66ff048c65146a0add1b1f7bf8a840b: function AS_Button_g66ff048c65146a0add1b1f7bf8a840b(eventobject) {
        var self = this;
        this.showAddRecipient();
    },
    /** init defined for frmP2PActivity **/
    AS_Form_c4a8154978a74eae98d70878a77e3597: function AS_Form_c4a8154978a74eae98d70878a77e3597(eventobject) {
        var self = this;
        return self.init.call(this);
    }
});