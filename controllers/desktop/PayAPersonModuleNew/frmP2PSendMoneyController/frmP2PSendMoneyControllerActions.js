define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnAddRecipient **/
    AS_Button_c307a2436c65413b901b185f7308b3f2: function AS_Button_c307a2436c65413b901b185f7308b3f2(eventobject) {
        var self = this;
        this.showAddRecipient();
    },
    /** onClick defined for btnSendMoneyNewRecipient **/
    AS_Button_cfe41502db464a89b7150c7d9eb5c98e: function AS_Button_cfe41502db464a89b7150c7d9eb5c98e(eventobject) {
        var self = this;
        this.showAddRecipient();
    },
    /** init defined for frmP2PSendMoney **/
    AS_Form_c8e617d1061d44848b27329e7d19cbac: function AS_Form_c8e617d1061d44848b27329e7d19cbac(eventobject) {
        var self = this;
        return self.init.call(this);
    }
});