define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmP2PSendMoney **/
    AS_Form_e5320cb9a7e745ec9196632d99037e2c: function AS_Form_e5320cb9a7e745ec9196632d99037e2c(eventobject) {
        var self = this;
        return self.init.call(this);
    },
    /** onClick defined for btnAddRecipient **/
    AS_Button_h2dd44b258e748fa92f01ef4a32f31e8: function AS_Button_h2dd44b258e748fa92f01ef4a32f31e8(eventobject) {
        var self = this;
        this.showAddRecipient();
    },
    /** onClick defined for btnSendMoneyNewRecipient **/
    AS_Button_c51f0f5be05b4ce8a8f5cb03f4e025ed: function AS_Button_c51f0f5be05b4ce8a8f5cb03f4e025ed(eventobject) {
        var self = this;
        this.showAddRecipient();
    }
});