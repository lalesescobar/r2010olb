define(['CommonUtilities','FormControllerUtility','ViewConstants','OLBConstants','CampaignUtility'], function (CommonUtilities, FormControllerUtility, ViewConstants,OLBConstants,CampaignUtility) {
    var orientationHandler = new OrientationHandler();
  	return {
        /**
         * Update Ui method starts here.
         * @param {String} - View Model.
         */
        updateFormUI: function (viewModel) {
            if (viewModel.progressBar === true) {
                FormControllerUtility.showProgressBar(this.view);
            } else if (viewModel.progressBar === false) {
                CommonUtilities.hideProgressBar(this.view);
            }
    		if (viewModel.campaignRes) {
           this.campaignSuccess(viewModel.campaignRes);
                }
 			if (viewModel.campaignError) {
           this.view.flxBannerContainerDesktop.setVisibility(false);
           this.view.flxBannerContainerMobile.setVisibility(false);
                }
    		
            if (viewModel.serverError) {
                if (viewModel.serverError === true){
                viewModel.show = true;
                this.showServerError(viewModel);
             } else {
                viewModel.show = false;
                this.showServerError(viewModel);
                }
            }
            if (viewModel.serverDown) {
                CommonUtilities.showServerDownScreen();
            }
            if (viewModel.allUsers) {
              this.focusedTabId = 1;
              this.usersList = viewModel.allUsers.users;
              this.setUsersDataToDashboard(viewModel.allUsers);
            }
            if (viewModel.activationLinkSuccess) {
               //code for success activation
            }
            if (viewModel.statusSuccess) {
                this.updateUserStatusSuccess(viewModel.statusSuccess);
            }
            if (viewModel.manageUser) {
                this.showManageUser(viewModel.manageUser);
            }
            if (viewModel.createUserSuccess) {
                this.showAcknowledgementManageUser();
            }
            if(viewModel.isPrintCancelled){
                this.showAcknowledgementScreenOnPrintCancel();
            }
            if (viewModel.showRolesDashboard) {
              this.focusedTabId = 2;
              this.tabClickListener({
                "text" : kony.i18n.getLocalizedString("i18n.customRoles.userRoles")
              });
            }
            if (viewModel.organizationRolesFailure) {
              FormControllerUtility.hideProgressBar(this.view);
            }
            if (viewModel.organizationRolesSuccess) {
              this.setDataForUserRolesDashboard(viewModel.organizationRolesSuccess);
            }
            if( viewModel.CustomRoleDeletedSuccessfully ) {
              FormControllerUtility.hideProgressBar(this.view);
            }
            if( viewModel.CustomRoleDeletionFailed ) {
              viewModel.show = true;
              this.showServerError(viewModel);              
            }
        },
    
        campaignSuccess : function(data){
			  var CampaignManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('CampaignManagement');
        CampaignManagementModule.presentationController.updateCampaignDetails(data);
        var self =this;
        if(data.length === 0){
       
        this.view.dbRightContainerNew.flxBannerWrapper.setVisibility(false);
      	}
        else {
            this.view.dbRightContainerNew.flxBannerWrapper.setVisibility(true);
           
        	this.view.dbRightContainerNew.imgBanner.src = data[0].imageURL;
       		this.view.dbRightContainerNew.imgBanner.onTouchStart =function(){
              CampaignUtility.onClickofInAppCampaign(data[0].destinationURL); 
            };
        }
        
        this.adjustScreen();
		},
    
		tabClickListener : function(eventobject) {
          var btnText = eventobject.text;
          var focusTabId = 1;
          if (btnText === kony.i18n.getLocalizedString("i18n.userManagement.allUsers")) {
            focusTabId = 1;
            this.loadBusinessBankingModule().presentationController.navigateToUsers(this.loadBusinessBankingModule().presentationController.fetchSubUsersSuccess.bind(this.loadBusinessBankingModule().presentationController));
            this.updateHamburgerMenu("User Management", "All Users");
            this.view.TabPane.TabSearchBarNew.tbxSearch.placeholder = kony.i18n.getLocalizedString("i18n.billPay.Search"); 
          } else if (btnText === kony.i18n.getLocalizedString("i18n.customRoles.userRoles")) {
            focusTabId = 2;
            this.loadBusinessBankingModule().presentationController.fetchUserRoles();
            this.updateHamburgerMenu("User Management", "User Roles");
            this.view.TabPane.TabSearchBarNew.tbxSearch.placeholder = kony.i18n.getLocalizedString("i18n.customRole.searchPlaceholder");
          }
          this.focusedTabId = focusTabId;
          this.view.TabPane.TabsHeaderNew.focusTab(focusTabId);
        },
      
          /**
         * Show No Search results - UI logic
         * @param {String} errorMsg to be displayed
         **/
         showNoRecords: function(errorMsg){
             this.resetUI();
             this.view.TabPane.TabBodyNew.flxNoTransactions.setVisibility(true);
             this.view.TabPane.TabSearchBarNew.flxSearch.setVisibility(true);
             this.view.flxContent.setVisibility(true);
             this.view.TabPane.TabBodyNew.rtxNoPaymentMessage.text = errorMsg;
        },
          /**
         * Method to load data after successful update of user's status
         * @param {object} userStatus - response
         */
        updateUserStatusSuccess: function(userStatus){
            if(this.view.flxContentDetails.isVisible){
                //manage user screen
                this.view.lblStatusKeyValue.text = this.statusConfig(userStatus,false);
                this.view.lblUserStatusIndicator.skin = this.setStatusImageConfig(userStatus,true);
                this.showActivateButton(userStatus,this.view.lblUserNameValue.text);
            }
            else {
                //BB users dashboard
                var scopeObj = this;
                var segData = this.view.TabPane.TabBodyNew.segTemplates.data;
                var index = this.selectedUserRowIndex;
                var tempData = segData[index];
              	this.usersList[index].status = userStatus;
                tempData.lblUserStatus = {
                    "text":  scopeObj.statusConfig(userStatus,false),
                };
                tempData.btnChangeStatus = {
                    onClick: scopeObj.onClickOfChangeStatus.bind(this, index),
                    "text": scopeObj.setStatusImageConfig(userStatus, false)
                };
                tempData.lblUserStatusIndicator = {
                    isVisible: true,
                    skin: scopeObj.setStatusImageConfig(userStatus, true)
                };
                this.view.TabPane.TabBodyNew.segTemplates.setDataAt(tempData, index);
            }
            this.adjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
        },
        /**
         * Method to load and returns the Business User Module Object
         * @returns {object} Method to load and returns the Business User Module Object
         */
        loadBusinessBankingModule: function () {
            return kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
        },
         /**
         * hide all ui flexes in user management form
         */
        resetUI: function () {
            this.view.flxContent.setVisibility(false);
            this.view.flxContentDetails.setVisibility(false);
            this.view.flxPopupConfirmation.setVisibility(false);
            this.view.TabPane.TabBodyNew.flxNoTransactions.setVisibility(false);
            this.view.TabPane.TabBodyNew.segTemplates.setVisibility(false);
            this.view.TabPane.TabSearchBarNew.flxNoSearchResult.setVisibility(false);
            this.view.TabPane.TabSearchBarNew.flxSearch.setVisibility(false);
            this.view.TabPane.TabBodyNew.flxBBUsersDashboardHeader.setVisibility(false);
            this.showServerError({ show: false });
            },
          /**
         * Method to navigate to create User page
         */
        navigateToCreateUser: function() {
            this.loadBusinessBankingModule().presentationController.showCreateUser(false,"fromAllUsers");
        },
      
		setDataForTabsHeader : function() {
          this.view.TabPane.TabsHeaderNew.btnTab1.text = kony.i18n.getLocalizedString("i18n.userManagement.allUsers");
          this.view.TabPane.TabsHeaderNew.btnTab1.skin = "sknBtnAccountSummaryUnselected";
          this.view.TabPane.TabsHeaderNew.btnTab1.onClick = this.tabClickListener;
          this.view.TabPane.TabsHeaderNew.btnTab1.setVisibility(true);
			
          this.view.TabPane.TabsHeaderNew.btnTab2.text = kony.i18n.getLocalizedString("i18n.customRoles.userRoles");
          this.view.TabPane.TabsHeaderNew.btnTab2.skin = "sknBtnAccountSummaryUnselected";
          this.view.TabPane.TabsHeaderNew.btnTab2.onClick = this.tabClickListener;
          this.view.TabPane.TabsHeaderNew.btnTab2.setVisibility(applicationManager.getConfigurationManager().checkUserPermission("CUSTOM_ROLES_VIEW")
                                                               		&& !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile) );
          
          this.view.TabPane.TabsHeaderNew.btnTab3.setVisibility(false);
          this.view.TabPane.TabsHeaderNew.btnTab4.setVisibility(false);
          this.view.TabPane.TabsHeaderNew.btnTab5.setVisibility(false);
          this.view.TabPane.TabsHeaderNew.btnTab6.setVisibility(false);
        },

		setDataForAllUsersDashboard: function(){
          this.usersSortMap = [ 
            {
              name: 'FirstName',
              imageFlx:this.view.TabPane.TabBodyNew.imgName,
              clickContainer: this.view.TabPane.TabBodyNew.flxName
            },
            {
              name: 'role_name',
              imageFlx: this.view.TabPane.TabBodyNew.imgRole,
              clickContainer: this.view.TabPane.TabBodyNew.flxRole
            },
            {
              name: 'UserName',
              imageFlx: this.view.TabPane.TabBodyNew.imgUsername,
              clickContainer: this.view.TabPane.TabBodyNew.flxUsername
            },
            {
              name: 'Status_id',
              imageFlx: this.view.TabPane.TabBodyNew.imgStatus,
              clickContainer: this.view.TabPane.TabBodyNew.flxStatus
            }
          ];
          this.view.TabPane.TabBodyNew.segTemplates.widgetDataMap = {
            "imgBottonSeparator":"imgBottonSeparator",
            "flxBottomSeperator":"flxBottomSeperator",
            "flxActions":"flxActions",
            "flxManageUsers":"flxManageUsers",
            "lblUserStatusIndicator":"lblUserStatusIndicator",
            "btnManageUsers":"btnManageUsers",
            "flxDetails":"flxDetails",
            "lblLastSignedInTitle":"lblLastSignedInTitle",
            "lblEmailTitle":"lblEmailTitle",
            "lblLastSignedIn":"lblLastSignedIn",
            "lblEmailValue":"lblEmailValue",
            "lblPhoneNumberTitle":"lblPhoneNumberTitle",
            "lblRoleTitle":"lblRoleTitle",
            "lblPhoneNumber":"lblPhoneNumber",
            "lblUsernameValue":"lblUsernameValue",
            "flxUserDetails":"flxUserDetails",
            "flxUserCommonRowDetails":"flxUserCommonRowDetails",
            "flxUserCommonRowHeader":"flxUserCommonRowHeader",
            "flxViewUserDetails":"flxViewUserDetails",
            "btnViewDetails":"btnViewDetails",
            "flxBottomSeperatorEnd":"flxBottomSeperatorEnd",
            "imgBottomSeperatorEnd":"imgBottomSeperatorEnd",
            "flxDropDown":"flxDropDown",
            "lblDropdown":"lblDropdown",
            "flxUserHeader":"flxUserHeader",
            "lblName":"lblName",
            "lblRole":"lblRole",
            "lblUserName":"lblUserName",
            "lblUserNameTitle":"lblUserNameTitle",
            "flxUserStatusIcon":"flxUserStatusIcon",
            "lblUserStatus":"lblUserStatus",
            "flxUserStatusChange":"flxUserStatusChange",
            "btnChangeStatus":"btnChangeStatus",
            "lblLastSignIn":"lblLastSignIn",
            "flxSeparatorForHeader":"flxSeparatorForHeader",
            "imgSeparatorHeader":"imgSeparatorHeader",
          };
		},
      
		navigateToCreateCustomRole : function() {
			this.loadBusinessBankingModule().presentationController.showCreatePermissionsTemplate();
        },
      
        /**
         * Method will invoke on form init
         */
        onInit: function () {
            var self = this;
            var val = applicationManager.getConfigurationManager().checkUserPermission("CUSTOM_ROLES_CREATE") && !(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile);
            this.view.onDeviceBack = function(){};
            this.setDataForTabsHeader();

            this.view.dbRightContainerNew.flxActions.isVisible=applicationManager.getConfigurationManager().checkUserPermission("USER_MANAGEMENT_CREATE");
            this.view.dbRightContainerNew.btnAction1.onClick = this.navigateToCreateUser;

			this.view.dbRightContainerNew.flxAction2.setVisibility(val);
            this.view.dbRightContainerNew.btnAction2.text = kony.i18n.getLocalizedString("i18n.customRole.createCustomRole");
			this.view.dbRightContainerNew.btnAction2.onClick = this.navigateToCreateCustomRole;
          
            this.view.TabPane.TabSearchBarNew.tbxSearch.onKeyUp = this.checkSearchForm.bind(this);
            this.view.imgCloseDowntimeWarning.onTouchEnd = this.showServerError.bind(this,{ show: false });
            this.view.onBreakpointChange = this.onBreakpointChange.bind(this, kony.application.getCurrentBreakpoint());
          
            //Manager user code
            this.view.btnEdit.onClick = function(){
              if(kony.application.getCurrentBreakpoint() !== 640){
                self.loadBusinessBankingModule().presentationController.showCreateUser({"edit": true});
              }
              else{}
            };
            this.view.btnEditUserRoles.onClick = function(){
              if(kony.application.getCurrentBreakpoint() !== 640){
                self.loadBusinessBankingModule().presentationController.fetchRoles(this.loadBusinessBankingModule().presentationController.
                                                                                   onFetchUserRolesSuccess.bind(this.loadBusinessBankingModule().presentationController),"frmUserManagement");
              }
              else{}
            };
            this.view.btnEditUserPermissions.onClick = function(){
              if(kony.application.getCurrentBreakpoint() !== 640){
                self.loadBusinessBankingModule().presentationController.showAllAccounts();
              }
              else{}
            };
            this.view.btnViewAllUsers.onClick = function(){
                //code for going back to all users
                self.loadBusinessBankingModule().presentationController.navigateToUsers(this.loadBusinessBankingModule().presentationController.fetchSubUsersSuccess.bind(this.loadBusinessBankingModule().presentationController));
            };
            this.view.flxInfoIcon.onClick = function() {
                if (self.view.InfoIconPopup.isVisible === true) {
                    self.view.InfoIconPopup.setVisibility(false);
                    // Populate the correct flex here
                } else {
                    self.view.InfoIconPopup.setVisibility(true);
                    self.view.InfoIconPopup.left = self.view.flxInfoIcon.frame.x + self.view.flxContentContainer.frame.x - 40 + "dp";
                    if(self.view.flxAcknowledgement.isVisible === true){
                    self.view.InfoIconPopup.top = self.view.flxInfoIcon.frame.y + self.view.flxAcknowledgement.info.frame.height + 755 + "dp";
                    }
                    else
                    self.view.InfoIconPopup.top = self.view.flxInfoIcon.frame.y + 755 + "dp";
                }
                self.view.InfoIconPopup.lblInfo.text = kony.i18n.getLocalizedString("i18n.konybb.accountAcessDetailstext");
            };
            this.view.flxInfoTransactionAccess.onClick = function() {
                if (self.view.InfoIconPopup.isVisible === true) {
                    self.view.InfoIconPopup.setVisibility(false);
                    // Populate the correct flex here
                } else {
                    self.view.InfoIconPopup.setVisibility(true);
                    if(self.view.flxAcknowledgement.isVisible === true){
                        if (self.view.flxAccountsAndPermissons.isVisible === true) {
                            self.view.InfoIconPopup.top = self.view.flxInfoTransactionAccess.frame.y + self.view.flxAccountsAndPermissons.info.frame.height + self.view.flxAcknowledgement.info.frame.height  + 816 + "dp";
                        } else
                            self.view.InfoIconPopup.top = self.view.flxInfoTransactionAccess.frame.y + self.view.flxAcknowledgement.info.frame.height + 816 + "dp";
                    }
                    else{
                        if (self.view.flxAccountsAndPermissons.isVisible === true) {
                            self.view.InfoIconPopup.top = self.view.flxInfoTransactionAccess.frame.y + self.view.flxAccountsAndPermissons.info.frame.height + 816 + "dp";
                        } else
                            self.view.InfoIconPopup.top = self.view.flxInfoTransactionAccess.frame.y + 816 + "dp";
                    }
                    self.view.InfoIconPopup.left = self.view.flxInfoTransactionAccess.frame.x + self.view.flxContentContainer.frame.x - 40 + "dp";
                }
                self.view.InfoIconPopup.lblInfo.text = kony.i18n.getLocalizedString("i18n.konybb.transactionAccessDetailstext");
            };
            this.view.InfoIconPopup.flxCross.onClick = function () {
                self.view.InfoIconPopup.isVisible = false;
            };
        },
        /**
       *  Pre show Method
       */
        onPreShow: function () {
              this.view.customheader.topmenu.flxMenu.skin = ViewConstants.SKINS.BLANK_SKIN_FLEX;
              FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxAcknowledgment', 'flxAccountsAndPermissons','flxHeader', 'flxMain', 'flxFooter']);
              this.view.customheader.topmenu.flxTransfersAndPay.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
              this.view.customheader.topmenu.flxaccounts.skin = ViewConstants.SKINS.BLANK_SKIN_TOPMENU_HOVER;
              this.view.customheader.forceCloseHamburger();
          	this.view.TabPane.TabBodyNew.btnName.skin = "sknBtnAccountSummaryUnselectedTransfer424242";
          	if(kony.application.getCurrentBreakpoint()==640 || orientationHandler.isMobile){
          		this.view.TabPane.TabsHeaderNew.btnTab1.skin = "sknLblSSP42424213px";
              	this.view.TabPane.TabSearchBarNew.flxSearch.skin = "bbSknFlxf8f7f8";
          		this.view.TabPane.TabSearchBarNew.flxBoxSearch.skin = "sknFlxffffffBordere3e3e3Radius3px";
             	this.view.PopupHeaderUM.lblHeader.skin = "bbSknLbl424242SSP15Px";
              	this.view.PopupHeaderUM.lblPopupMsg.skin = "bbSknLbl424242SSP15Px";
                this.view.PopupHeaderUM.imgClose.src = "icon_close_grey.png";
       		}
              else{
                  this.view.TabPane.TabsHeaderNew.btnTab1.skin = "sknBtnAccountSummaryUnselected";
                  this.view.TabPane.TabBodyNew.flxBBUsersDashboardHeader.skin = "sknFlxf9f9f9Bordere3e3e32px";
              }
          	this.view.TabPane.TabBodyNew.btnRole.skin = "sknBtnAccountSummaryUnselectedTransfer424242";
          	this.view.TabPane.TabBodyNew.btnUsername.skin = "sknBtnAccountSummaryUnselectedTransfer424242";
          	this.view.TabPane.TabBodyNew.btnStatus.skin = "sknBtnAccountSummaryUnselectedTransfer424242";
          	this.view.TabPane.TabBodyNew.lblActions.skin = "sknBBLabelSSP42424215px";
          	this.view.TabPane.TabBodyNew.imgName.src = "sorting_next.png";
          	this.view.TabPane.TabBodyNew.imgRole.src = "sorting_next.png";
          	this.view.TabPane.TabBodyNew.imgUsername.src = "sorting_next.png";
          	this.view.TabPane.TabBodyNew.imgStatus.src = "sorting_next.png";
            this.view.TabPane.TabSearchBarNew.flxSearchimg.isVisible = true;
          	this.view.PopupHeaderUM.lblHeader.skin="bbSknLbl424242SSP20Px";
          	this.view.PopupHeaderUM.lblPopupMsg.skin="bbSknLbl424242SSP20Px";
            this.view.flxAcknowledgementNew1.flxImgPrint.setVisibility(false);
        },
        /**
         * onPostshow event Function
         **/
	    onPostShow: function() {
          var scopeObj = this;
          this.adjustScreen();
          applicationManager.getNavigationManager().applyUpdates(this);
          this.accessibilityFocusSetup();
          var val = applicationManager.getConfigurationManager().checkUserPermission("CUSTOM_ROLES_CREATE") && !(kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile);
            this.view.dbRightContainerNew.flxAction2.setVisibility(val) ;
        },
        /**
         * Set foucs handlers for skin of parent flex on input focus 
         */
        accessibilityFocusSetup: function(){
            let widgets = [
            [this.view.TabPane.TabSearchBarNew.tbxSearch, this.view.TabPane.TabSearchBarNew.flxBoxSearch],
            ]
            for(let i=0; i<widgets.length; i++){
            CommonUtilities.setA11yFoucsHandlers(widgets[i][0], widgets[i][1], this)
            }
        },
        /**
         * Method to load status image
         * @param {String} status -  contains Status
         * @param {Boolean} needImage -  contains Boolean if need image or not
         * @returns {String} - may contain Skin for label or Status
         */
        setStatusImageConfig: function(status,needImage){
            switch(status.toLowerCase().trim()){
                case  kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE").toLowerCase().trim():
                    return needImage === true ? ViewConstants.SKINS.ACTIVE_STATUS_SKIN : kony.i18n.getLocalizedString("i18n.userManagement.deactivate");
                case kony.i18n.getLocalizedString("i18n.konybb.manageUser.Suspend").toLowerCase().trim() || kony.i18n.getLocalizedString("i18n.userManagement.suspended").toLowerCase().trim():
                    return needImage === true ? ViewConstants.SKINS.SUSPENDED_STATUS_SKIN : kony.i18n.getLocalizedString("i18n.userManagement.activate");
                case kony.i18n.getLocalizedString("i18n.userManagement.suspended").toLowerCase().trim():
                    return needImage === true ? ViewConstants.SKINS.SUSPENDED_STATUS_SKIN : kony.i18n.getLocalizedString("i18n.userManagement.activate");
                default:
                    return needImage === true ? ViewConstants.SKINS.NEW_STATUS_SKIN : kony.i18n.getLocalizedString("i18n.userManagement.resendLink");
             }
        },
      
		getDataMapForUserRoles : function(){
			return {
              "lblRoleName" : "name",
              "lblCreatedOn" : "createdts",
              "lblCreatedBy" : "userName",
              "btnViewPermissions" : "btnViewPermissions",
              "lblSeparator" : "lblSeparator",
              "parentRoleName": "parentRoleName",
              "statusValue": "statusValue",
              "description": "description",
              "userName": "userName",
              "softdeleteflag": "softdeleteflag",
              "parent_id": "parent_id",
              "organization_id": "organization_id",
              "modifiedby": "modifiedby",
              "id": "id",             
            }; 
        },
      
		getDefaultValuesForCustomRoles : function() {
          return {
            btnViewPermissions : {
              "text" : kony.i18n.getLocalizedString("i18n.customRoles.ViewPermissions"),
              "onClick" : this.viewPermissionsOfCustomRole
            },
            lblSeparator : {
              "text" : "-"
            }
          }
        },
      
      viewPermissionsOfCustomRole : function(eventobject, context){
        FormControllerUtility.showProgressBar(this.view);
        var row = context.rowIndex;
        var section = context.sectionIndex;
        var selectedRowData = this.view.TabPane.TabBodyNew.getData()[section][1][row];
        this.loadBusinessBankingModule().presentationController.navigateToManagePermissions(selectedRowData.id, selectedRowData.parent_id);
      },

       /**
           * Method to sort the list of custom roles based on the property sent as param
           *  @param  param - value in the array of json
           */
      getSortOrder: function(param) {
        return function (a, b) {
          let property1 = a[param];
          let property2 = b[param];
          if( !kony.sdk.isNullOrUndefined(property1["toolTip"]) ) {
            property1 = property1["toolTip"].toLowerCase();
            property2 = property2["toolTip"].toLowerCase();
          }
          
          if (property1 > property2) {
            return 1;
          } else if (property1 < property2) {
            return -1;
          }
          return 0;
        };
    }, 
      
      sortCustomRoles : function( keyForSorting, imgForSorting, otherImages ) {
        var scopeObj = this;
        var section = scopeObj.segDataForCustomRoles[0];
        var sectionHeader = section[0];
        var rows = section[1];
        rows.sort( scopeObj.getSortOrder( keyForSorting ) );
        if(scopeObj.view.TabPane.TabBodyNew[imgForSorting].src === "sorting.png" ) {
          scopeObj.view.TabPane.TabBodyNew[imgForSorting].src = "sorting_next.png";
        }
        else if(scopeObj.view.TabPane.TabBodyNew[imgForSorting].src === "sorting_previous.png") {
          scopeObj.view.TabPane.TabBodyNew[imgForSorting].src = "sorting_next.png";
        }
        else {
          scopeObj.view.TabPane.TabBodyNew[imgForSorting].src = "sorting_previous.png";
          rows.reverse();
        }

        for( var i = 0; i < otherImages.length; i++ ) {
          scopeObj.view.TabPane.TabBodyNew[ otherImages[i] ].src = "sorting.png";
        }
        var finalData = [ sectionHeader, rows ]; 
        scopeObj.view.TabPane.TabBodyNew.segTemplates.setData( [ finalData ] );        
      },
      
      setDataForUserRolesDashboard : function(rolesData) {
        var scopeObj = this;
        var errorMsg = "";
        scopeObj.view.TabPane.TabBodyNew.flxUserRolesHeader.setVisibility(true);
        scopeObj.view.TabPane.TabBodyNew.flxRoleName.onClick = function() {
          scopeObj.sortCustomRoles( "lblRoleName", "imgRoleName", [ "imgCreatedOn", "imgCreatedBy" ] );
        };

        scopeObj.view.TabPane.TabBodyNew.flxCreatedOn.onClick = function(){
          scopeObj.sortCustomRoles( "lblCreatedOn", "imgCreatedOn", [ "imgRoleName", "imgCreatedBy" ] );
        };

        scopeObj.view.TabPane.TabBodyNew.flxCreatedBy.onClick = function(){
          scopeObj.sortCustomRoles( "lblCreatedBy", "imgCreatedBy", [ "imgRoleName", "imgCreatedOn" ] );
        };        
        
        scopeObj.view.TabPane.TabBodyNew.flxBBUsersDashboardHeader.setVisibility(false);
        if(rolesData.length !== 0)
         {
        scopeObj.view.TabPane.TabBodyNew.segTemplates.sectionHeaderTemplate = "flxRoleSectionHeader";
        scopeObj.view.TabPane.TabBodyNew.segTemplates.rowTemplate = "flxUserRoles";
        scopeObj.view.TabPane.TabBodyNew.setRowDataMap([scopeObj.getDataMapForUserRoles()]);
        scopeObj.view.TabPane.TabBodyNew.setDefaultValues([scopeObj.getDefaultValuesForCustomRoles()]);
        scopeObj.view.TabPane.TabBodyNew.setSectionData(scopeObj.getSectionDataForRoles(rolesData));
        scopeObj.view.TabPane.TabBodyNew.addDataForSections([scopeObj. getRowDataForRoles(rolesData)]);
        
        scopeObj.segDataForCustomRoles = scopeObj.view.TabPane.TabBodyNew.segTemplates.data;
          }
        else
          {
            errorMsg = kony.i18n.getLocalizedString("i18n.konybb.NoCustomRoleFound");
            this.showNoRecords(errorMsg);
          }
        scopeObj.configureSearch("", scopeObj.searchRolesHandler);
        FormControllerUtility.hideProgressBar(scopeObj.view); 
        scopeObj.adjustScreen();
      },
        /**
         * Method to configure  sorting handler  for the list of custom roles
         *  @param {JSON} obj - sorting parameters
         */
      searchRolesHandler: function(obj) {
        var scopeObj = this;
        var section = scopeObj.segDataForCustomRoles[0];
        var sectionHeader = section[0];
        var rows = section[1];          
        if( obj.searchString !== "" ) {
          var filteredRows = rows.filter(  function( customRoleJson ) {
            return customRoleJson.lblRoleName.toolTip.toLowerCase().includes(obj.searchString.toLowerCase()) || customRoleJson.lblCreatedBy.toLowerCase().includes(obj.searchString.toLowerCase())
              || customRoleJson.lblCreatedOn.includes(obj.searchString) || customRoleJson.parentRoleName.toLowerCase().includes(obj.searchString.toLowerCase());
          }  ); 

          if (filteredRows.length === 0) {
            var errorMsg =  kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.noRecords");
            if(! kony.sdk.isNullOrUndefined(obj.searchString) && obj.searchString !== "" ){
              errorMsg = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.emptySearchMsg");
            }
            this.showNoRecords(errorMsg);              
          }  
          else {
            var finalData = [ sectionHeader, filteredRows ];                 
            scopeObj.view.TabPane.TabBodyNew.segTemplates.setData( [ finalData ] );   
            this.showRolesRecords();            
          }
        }
        else {
          scopeObj.view.TabPane.TabBodyNew.segTemplates.setData( [ [ sectionHeader, rows ] ] );
          this.showRolesRecords();
        }          
      },

      showRolesRecords : function() {
        this.view.TabPane.TabBodyNew.flxNoTransactions.setVisibility(false);
        this.view.TabPane.TabBodyNew.segTemplates.setVisibility(true);          
      },
      
      getSectionDataForRoles: function(roles) {
        return [{
          lblHeader : {
            "text" : "Custom Roles ("+roles.length+")"
          },
          lblSeparator : {
            "text" : "-"
          }
        }];
      },
      getRowDataForRoles: function(roles) {       
        for( var i = 0; i < roles.length; i++ ) {
          roles[i].createdts = roles[i].createdts.replace(" ", "T");
          roles[i].createdts = roles[i].createdts ? CommonUtilities.getFrontendDateString(roles[i].createdts) : "N/A";
		  roles[i].name = {
             "text": CommonUtilities.truncateStringWithGivenLength(roles[i].name, 18),
             "toolTip":roles[i].name
          };
        }
        return roles;
      },
        /**
            *  Method on click of btnYes of deactivate/activate popup.
            * @param {Object} params -  contains Status and Username
       */
        updateUserStatus : function(params){
            FormControllerUtility.showProgressBar(this.view);
            this.view.flxPopupConfirmation.setVisibility(false);
            this.loadBusinessBankingModule().presentationController.setUserStatus(params);
        },
        /**
            *  Method to provide status to updateUserStatus method basing on present status of the user
            * @param {String} status -  status to which the user has to change
            * @param {Boolean} needConversion -  Bpolean that specifies conversion needed or not
            * @returns {String} - Status
       */
      statusConfig : function(status, needConversion){
        switch(status.toLowerCase().trim()){
            case  "Active".toLowerCase().trim():
                return needConversion === true ?  kony.i18n.getLocalizedString("i18n.userManagement.suspended").toUpperCase() : kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE");
            case "Suspend".toLowerCase().trim():
                return needConversion === true ?  kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE").toUpperCase() : kony.i18n.getLocalizedString("i18n.userManagement.suspended");
            case "Suspended".toLowerCase().trim():
                return needConversion === true ?  kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE").toUpperCase() : kony.i18n.getLocalizedString("i18n.userManagement.suspended");
            case "New".toLowerCase().trim():
                return needConversion === true ?  kony.i18n.getLocalizedString("i18n.userManagement.new").toUpperCase() : kony.i18n.getLocalizedString("i18n.userManagement.new");
            default:
                return status;
        }
    },
       /**
            *  Method on click of activate/deactivate button.
            * @param {String} status - Status
            * @param {String} userName - username
       */
        onClickOfChangeStatus: function(index){
          var scopeObj = this;
          this.selectedUserRowIndex = index;
          var status = this.usersList[index].status;
          var userName = this.usersList[index].UserName;
          var btnChangeStatusText = this.view.TabPane.TabBodyNew.segTemplates.data[index].btnChangeStatus.text;
          if(btnChangeStatusText === kony.i18n.getLocalizedString("i18n.customRoles.ViewPermissions")){
            this.showUserDetails(userName, this.usersList[index].Group_id);
          }
          else{
            this.view.PopupHeaderUM.formActionsNew.btnCancel.onClick = function(){
              scopeObj.view.flxPopupConfirmation.setVisibility(false);
              FormControllerUtility.hideProgressBar(scopeObj.view);
            };
            this.view.PopupHeaderUM.imgClose.onTouchEnd = function(){
              scopeObj.view.flxPopupConfirmation.setVisibility(false);
              FormControllerUtility.hideProgressBar(scopeObj.view);
            };
            var statusName = scopeObj.statusConfig(status.toLowerCase().trim(), true);
            var params = {
              "Status": statusName,
              "UserName": userName
            };
            FormControllerUtility.enableButton(scopeObj.view.PopupHeaderUM.formActionsNew.btnNext);
            this.view.PopupHeaderUM.formActionsNew.btnNext.onClick = this.updateUserStatus.bind(this,params);
            if(status.toLowerCase().trim() === kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE").toLowerCase().trim()){
              this.view.flxPopupConfirmation.setVisibility(true);
              this.view.flxPopupConfirmation.setFocus(true);
              this.view.flxPopupConfirmation.height = this.view.flxHeader.info.frame.height + this.view.flxMain.info.frame.height + this.view.flxFooter.info.frame.height + "dp";
              this.view.PopupHeaderUM.lblHeader.text = kony.i18n.getLocalizedString("i18n.userManagement.DeactivateUser");
              this.view.PopupHeaderUM.lblPopupMsg.text = kony.i18n.getLocalizedString("i18n.konybb.manageUser.SuspendUserConfirm");
            }
            else if(status.toLowerCase().trim() === kony.i18n.getLocalizedString("i18n.konybb.manageUser.Suspend").toLowerCase().trim() || status.toLowerCase().trim() === kony.i18n.getLocalizedString("i18n.userManagement.suspended").toLowerCase().trim()){
              this.view.flxPopupConfirmation.setVisibility(true);
              this.view.flxPopupConfirmation.setFocus(true);
              this.view.flxPopupConfirmation.height = this.view.flxHeader.info.frame.height + this.view.flxMain.info.frame.height + this.view.flxFooter.info.frame.height + "dp";
              this.view.PopupHeaderUM.lblHeader.text = kony.i18n.getLocalizedString("i18n.userManagement.ActivateUser");
              this.view.PopupHeaderUM.lblPopupMsg.text = kony.i18n.getLocalizedString("i18n.konybb.manageUser.ActivateUserConfirm");
            }
            else {
              this.view.flxPopupConfirmation.setVisibility(false);
              this.resendActivationLink(userName);
            }
          }
        },
        /**
         * on click of dropdown in segment row
         *  @param {object} userObj - user object
         */
        onUserDropdownClick: function(userObj, eventObject, x, y, context) {
            var scopeObj = this;
          	var break_point = kony.application.getCurrentBreakpoint();
            var segData = this.view.TabPane.TabBodyNew.segTemplates.data;
            var sectionIndex = context.sectionIndex;
            var rowIndex = context.rowIndex;
            var collapseAll = function(segments) {
                segments.forEach(function(segment, i) {
                    if (segment.template === "flxBBUserBasicDetailsSelected" || segment.template === "flxBBUserBasicDetailsMobileSelected") {
                        if(break_point === 640){
                            segment.template = "flxBBUserBasicDetailsMobile";
                        }
                        else
                        	segment.template = "flxBBUserBasicDetails";
                        segment.lblDropdown ={
                            text: "O",
                            onTouchEnd: scopeObj.onUserDropdownClick.bind(this, userObj)
                        };
                        scopeObj.view.TabPane.TabBodyNew.segTemplates.setDataAt(segment, i, sectionIndex);
                    }
                });
            };
            var prevData = segData[rowIndex].template;
            collapseAll(segData);
            var tempData = scopeObj.view.TabPane.TabBodyNew.segTemplates.data[rowIndex];
            if ((tempData.template === "flxBBUserBasicDetails" && prevData === "flxBBUserBasicDetailsSelected") || (tempData.template === "flxBBUserBasicDetailsMobile" && prevData === "flxBBUserBasicDetailsMobileSelected")) {
                if(break_point === 640){
                    tempData.template = "flxBBUserBasicDetailsMobile";
                }
                else
                    tempData.template = "flxBBUserBasicDetails";
                tempData.lblDropdown = {
                    text: "O",
                    onTouchEnd: scopeObj.onUserDropdownClick.bind(this, userObj)
                };
            } else {
                tempData.lblDropdown = {
                    text: "P",
                    onTouchEnd: scopeObj.onUserDropdownClick.bind(this, userObj)
                };
                if(break_point === 640){
                    tempData.template = "flxBBUserBasicDetailsMobileSelected";
                }
                else
                    tempData.template = "flxBBUserBasicDetailsSelected";
            }
            this.view.TabPane.TabBodyNew.segTemplates.setDataAt(tempData, rowIndex);
            this.adjustScreen();
        },
        /**
           *  Method to show error flex.
           * @param {String} response - Response containing Error message and boolean variable to validate whether error flex should be displayed.
           */
        showServerError: function(response) {
            this.view.flxDowntimeWarning.setVisibility(response.show);
            this.view.lblDowntimeWarning.text = response.errorMessage || kony.i18n.getLocalizedString("i18n.common.OoopsServerError");
            this.view.flxMainWrapper.setVisibility(response.show);
            this.adjustScreen();
            this.view.flxDowntimeWarning.setFocus(response.show);
        },
        /**
        * AdjustScreen - Method that sets the height of footer properly.
        */
        adjustScreen: function () {
            this.view.forceLayout();
            this.view.flxFooter.isVisible = true;
            var mainheight = 0;
            var screenheight = kony.os.deviceInfo().screenHeight;
            mainheight = this.view.flxHeader.info.frame.height + this.view.flxMain.info.frame.height;
            var diff = screenheight - mainheight;
            if (mainheight < screenheight) {
                diff = diff - this.view.flxFooter.info.frame.height;
                if (diff > 0) {
                    this.view.flxFooter.top = mainheight + diff + "dp";
                }
                else {
                    this.view.flxFooter.top = mainheight + "dp";
                }
                this.view.forceLayout();
            } else {
                this.view.flxFooter.top = mainheight + "dp";
                this.view.forceLayout();
            }
            this.view.forceLayout();
        },
        /**
         * Method that updates Hamburger Menu.
         */
        updateHamburgerMenu: function (menuId, subMenuId) {
            this.view.customheader.customhamburger.activateMenu(menuId, subMenuId);
        },
        /**
         * Method to show User Details
         * @param {String} username - contains the username
         */
        showUserDetails : function(username, rolename){
            FormControllerUtility.showProgressBar(this.view);
            this.loadBusinessBankingModule().presentationController.fetchUserDetails(username, rolename, this.loadBusinessBankingModule().presentationController.fetchUserDetailsCompletionCallback.bind(this.loadBusinessBankingModule().presentationController),
                                                                                    this.loadBusinessBankingModule().presentationController.fetchUserDetailsFailure.bind(this.loadBusinessBankingModule().presentationController),"frmUserManagement");
        },
        /**
         * Method to Resend Activation link
         * @param {String} username - contains the username
         */
        resendActivationLink : function(username){
            FormControllerUtility.showProgressBar(this.view);
            this.loadBusinessBankingModule().presentationController.resendActivationLink(username);
        },
        /**
         * Method to show manage user flex.
         * @param {Object} userDetails - contains the user details
         */
        showManageUser: function (userDetails) {
            this.populateUserDetails(userDetails);
            this.view.flxAcknowledgement.isVisible = false;
            if (applicationManager.getUserPreferencesManager().getCurrentUserName() !== userDetails.Username){
                this.showActivateButton(userDetails.Status, userDetails.Username);
                this.view.btnEdit.isVisible = true;
                this.view.btnEditUserRoles.isVisible = true;
                this.view.btnEditUserPermissions.isVisible = true;
            }
            else{
                this.view.btnAddAnother.isVisible = false;
                this.view.btnEdit.isVisible = false;
                this.view.btnEditUserRoles.isVisible = false;
                this.view.btnEditUserPermissions.isVisible = false;
            }
            this.view.btnViewAllUsers.text = kony.i18n.getLocalizedString("i18n.userManagement.BackToAllUsers");
            this.view.btnViewAllUsers.toolTip = kony.i18n.getLocalizedString("i18n.userManagement.BackToAllUsers");
            this.view.lblStatusKey.isVisible = true;
            this.view.flxStatusValue.isVisible = true;
            this.view.lblStatusKeyValue.text = this.statusConfig(userDetails.Status,false);
            this.view.lblUserStatusIndicator.skin = this.setStatusImageConfig(userDetails.Status,true);
            this.adjustScreen();
        },
        /**
         * Method to manipulate text and click functionality of button btnAddAnother
         * @param {String} status - contains the status
         * @param {String} username - contains the username
         */
        showActivateButton : function(status,username){
            this.view.btnAddAnother.isVisible = true;
            var scopeObj = this;
            if (status.toLowerCase().trim() === kony.i18n.getLocalizedString("i18n.CardManagement.ACTIVE").toLowerCase().trim()) {
                this.view.btnAddAnother.text = kony.i18n.getLocalizedString("i18n.userManagement.DeactivateUser");
                this.view.btnAddAnother.onClick = this.updateUserStatus.bind(this,{ "UserName" :username, "Status": scopeObj.statusConfig(status.toLowerCase().trim(), true)});
                this.view.btnAddAnother.toolTip = kony.i18n.getLocalizedString("i18n.userManagement.DeactivateUser");
            }
            else if (status.toLowerCase().trim() === kony.i18n.getLocalizedString("i18n.konybb.manageUser.Suspend").toLowerCase().trim() || status.toLowerCase().trim() === kony.i18n.getLocalizedString("i18n.userManagement.suspended").toLowerCase().trim()) {
                this.view.btnAddAnother.text = kony.i18n.getLocalizedString("i18n.userManagement.ActivateUser");
                this.view.btnAddAnother.onClick = this.updateUserStatus.bind(this,{ "UserName" :username, "Status": scopeObj.statusConfig(status.toLowerCase().trim(), true)});
                this.view.btnAddAnother.toolTip = kony.i18n.getLocalizedString("i18n.userManagement.ActivateUser");
            }
            else {
                this.view.btnAddAnother.text = kony.i18n.getLocalizedString("i18n.userManagement.resendLink");
                this.view.btnAddAnother.onClick = this.resendActivationLink.bind(this,username);
                this.view.btnAddAnother.toolTip = kony.i18n.getLocalizedString("i18n.userManagement.resendLink");
            }
        },
        /**
         * Method to show manage user flex.
         * @param {Object} userDetails - contains the user details
         * @param {String} referenceNumber - contains the reference Number
         */
        showAcknowledgement: function(userDetails,referenceNumber){
            this.populateUserDetails(userDetails);
            if (CommonUtilities.isPrintEnabled()) {
                this.view.flxAcknowledgementNew1.flxImgPrint.setVisibility(true);
                this.view.flxAcknowledgementNew1.flxImgPrint.onClick = this.printAcknowledgement.bind(this);
              } else {
                this.view.flxAcknowledgementNew1.flxImgPrint.setVisibility(false);
              }
            this.view.flxAcknowledgement.isVisible = true;
            var ackMsg = "";
            if(referenceNumber !== undefined && referenceNumber !== null && referenceNumber !== ""){
                ackMsg = kony.i18n.getLocalizedString("i18n.konybb.manageUser.UserAddedSuccess")+"\n"+kony.i18n.getLocalizedString("i18n.konybb.common.ReferenceNumber") +" "+ referenceNumber;
            }
            else{
                ackMsg = kony.i18n.getLocalizedString("i18n.konybb.manageUser.UserUpdatedSuccess")+"\n"+kony.i18n.getLocalizedString("i18n.konybb.common.ReferenceNumber") +" "+ userDetails.id;
            }
            this.view.flxAcknowledgementNew1.rTextSuccess.text = ackMsg;
            this.view.btnEdit.isVisible = false;
            this.view.btnEditUserRoles.isVisible = false;
            this.view.btnEditUserPermissions.isVisible = false;
            this.view.btnAddAnother.isVisible = true;
            this.view.btnAddAnother.text =  kony.i18n.getLocalizedString("i18n.userManagement.AddAnotherUser");
            var self = this;
            this.view.btnAddAnother.onClick = function(){
                self.loadBusinessBankingModule().presentationController.showCreateUser();
            };
            this.view.btnViewAllUsers.text = kony.i18n.getLocalizedString("i18n.userManagement.ViewAllUsers");
            this.view.btnViewAllUsers.toolTip = kony.i18n.getLocalizedString("i18n.userManagement.ViewAllUsers");
            this.view.lblStatusKey.isVisible = false;
            this.view.flxStatusValue.isVisible = false;
            this.adjustScreen();
        },
      showAcknowledgementManageUser : function(){
 			this.loadBusinessBankingModule().presentationController.navigateToUsers(this.loadBusinessBankingModule().presentationController.fetchSubUsersSuccess.bind(this.loadBusinessBankingModule().presentationController));
      },
        /**
         * Method to populate User details
         * @param {Object} userDetails - contains the user details
         */
        populateUserDetails : function(userDetails){
            this.view.flxContent.isVisible = false;
            this.view.flxContentDetails.isVisible = true;
          	if(userDetails.MiddleName === undefined){
              	userDetails.MiddleName = "";
            }
            this.view.lblFullNameValue.text = CommonUtilities.getFullName( userDetails.FirstName, userDetails.MiddleName, userDetails.LastName);
            this.view.lblEmailIdValue.text = userDetails.Email;
            this.view.lblDateOfBirthValue.text = CommonUtilities.getFrontendDateString(userDetails.DateOfBirth);
            this.view.lblSSNValue.text = userDetails.Ssn;
            this.view.lblDriverLicenseValue.text = userDetails.DrivingLicenseNumber || "--";
            this.view.lblPhoneNumberValue.text = userDetails.Phone;
            this.view.lblUserNameValue.text = userDetails.Username || userDetails.UserName;
            this.view.lblSelectedRole.text = userDetails.Group_Name;
            this.view.RichTextSelectedRole.text = userDetails.Group_Description;
            var self = this;
            var hasAccounts = false;
            var hasServices = false;
            var segData;
            //Resetting User Persmissions flexes
            this.view.flxNoAccountAccessUsers.isVisible = false;
            this.view.flxAccountsAndPermissons.isVisible = false;
            this.view.imgDropDownAccount.src = ViewConstants.IMAGES.BUTTON_DOWN;
            this.view.flxTransferPermissions.isVisible = false;
            this.view.flxNoPermissionsUsers.isVisible = false;
            this.view.imgDropDownTransaction.src = ViewConstants.IMAGES.BUTTON_DOWN;
            if (userDetails.accounts[0].hasOwnProperty("AccountName") || userDetails.accounts[0].hasOwnProperty("Account_Name")) {
                segData = userDetails.accounts.map(function (dataItem) {
                    var data = {
                        "lblGeneralAccountName": (dataItem.AccountName || dataItem.Account_Name) + "-X"+ CommonUtilities.getLastFourDigit(dataItem.Account_id || dataItem.id) //need to update object pro
                    }
                    return data;
                });
                this.view.segAccountPermissions.setData(segData);
                hasAccounts = true;
            }
            else{
                this.view.segAccountPermissions.setData([{}]);
                hasAccounts = false;
            }
            this.view.flxDropDownAccount.onClick = function () {
                if (self.view.flxAccountsAndPermissons.isVisible) {
                    self.view.flxNoAccountAccessUsers.isVisible = false;
                    self.view.flxAccountsAndPermissons.isVisible = false;
                    self.view.imgDropDownAccount.src = ViewConstants.IMAGES.BUTTON_DOWN;
                }
                else {
                    if(hasAccounts)
                        self.view.flxNoAccountAccessUsers.isVisible = false;
                    else
                        self.view.flxNoAccountAccessUsers.isVisible = true;
                    self.view.flxAccountsAndPermissons.isVisible = true;
                    self.view.imgDropDownAccount.src = ViewConstants.IMAGES.BUTTON_UP;
                }
                self.adjustScreen();
            }
            if (userDetails.services === null){
                hasServices = false;
                this.view.segTransferLimits.setData([{}]);
            }
            else if(userDetails.services.length === 0){
                hasServices = false;
                this.view.segTransferLimits.setData([{}]);
            }
            else{
                hasServices = true;
                var segData = userDetails.services.map(function (dataItem) {
                    var data = {
                        "lblTransferType": dataItem.Name,
                        "lblMaxTransactionLimit": kony.i18n.getLocalizedString("i18n.konybb.createUser.MaxTransactionLimit"),
                        "lblMaxDailyLimit": kony.i18n.getLocalizedString("i18n.konybb.createUser.MaxDailyLimit"),
                        "lblMaxTransactionLimitValue": CommonUtilities.formatCurrencyWithCommas(dataItem.MaxTransactionLimit),
                        "lblMaxDailyLimitValue": CommonUtilities.formatCurrencyWithCommas(dataItem.MaxDailyLimit)
                    }
                    return data;
                });
                this.view.segTransferLimits.setData(segData);
            }
            this.view.flxDropDownTransaction.onClick = function () {
                if (self.view.flxTransferPermissions.isVisible) {
                    self.view.flxTransferPermissions.isVisible = false;
                    self.view.flxNoPermissionsUsers.isVisible = false;
                    self.view.imgDropDownTransaction.src = ViewConstants.IMAGES.BUTTON_DOWN;
                }
                else {
                    if (hasServices)
                        self.view.flxNoPermissionsUsers.isVisible = false;
                    else
                        self.view.flxNoPermissionsUsers.isVisible = true;
                    self.view.flxTransferPermissions.isVisible = true;
                    self.view.imgDropDownTransaction.src = ViewConstants.IMAGES.BUTTON_UP;
                };
                self.adjustScreen();
            }
            this.view.customheader.imgKony.setFocus(true);
        },
        /**
         * Method to switch to users dashboard UI
         */
        showDashboardUI: function(){
            this.resetUI();
            this.view.TabPane.TabSearchBarNew.flxSearch.setVisibility(true);
            this.view.TabPane.TabBodyNew.segTemplates.setVisibility(true);
            this.view.TabPane.TabSearchBarNew.tbxSearch.placeholder = kony.i18n.getLocalizedString("kony.userMgmt.SearchUserByNameRole");
          	if(kony.application.getCurrentBreakpoint() === 640){
                this.view.TabPane.TabBodyNew.flxBBUsersDashboardHeader.setVisibility(false);
            }
            else
            	this.view.TabPane.TabBodyNew.flxBBUsersDashboardHeader.setVisibility(true);
            this.view.flxContent.setVisibility(true);
            this.view.lblContentHeader.text = kony.i18n.getLocalizedString("i18n.common.UserManagement");
        },
        /**
         * Method to populate the Segment with user-list
         *  @param {object} context - which consists of list of users, context
         *  Searching|Sorting
         */
        setUsersDataToDashboard: function (context) {
            var scopeObj = this;
          if( ! kony.sdk.isNullOrUndefined(context.users) && Array.isArray(context.users)){
            if (context.users.length === 0) {
                var errorMsg =  kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.noRecords");
                if(! kony.sdk.isNullOrUndefined(context.searchString) && context.searchString !== "" ){
                   errorMsg = kony.i18n.getLocalizedString("i18n.NotificationsAndMessages.emptySearchMsg");
                }
                this.showNoRecords(errorMsg);
            }
            else if(context.users.length > 0){
              this.showDashboardUI();
              this.setDataForAllUsersDashboard();
              var segData = context.users.map(function (dataItem, index) {
                var lblUserStatusValue = "N/A";
                var statusIndicatorForSkin = scopeObj.setStatusImageConfig("",true);
                var statusIndicatorForText = scopeObj.setStatusImageConfig("",false);
                if(!kony.sdk.isNullOrUndefined(dataItem.status)) {
                  lblUserStatusValue = scopeObj.statusConfig(dataItem.status,false);
                  statusIndicatorForSkin = scopeObj.setStatusImageConfig(dataItem.status,true);
                  if(dataItem.UserName !== applicationManager.getUserPreferencesManager().getCurrentUserName() && applicationManager.getConfigurationManager().checkUserPermission("USER_MANAGEMENT_ACTIVATE_OR_SUSPEND")){
                    statusIndicatorForText = scopeObj.setStatusImageConfig(dataItem.status,false);
                  }
                  else{
                    statusIndicatorForText = kony.i18n.getLocalizedString("i18n.customRoles.ViewPermissions");
                  }
                }
                var isBtnChangeStatusVisible = (dataItem.UserName !== applicationManager.getUserPreferencesManager().getCurrentUserName() 
                                                && applicationManager.getConfigurationManager().checkUserPermission("USER_MANAGEMENT_ACTIVATE_OR_SUSPEND") )
                								|| (applicationManager.getConfigurationManager().checkUserPermission("USER_MANAGEMENT"));
                var userFirstName = dataItem.FirstName;
                if(dataItem.UserName === applicationManager.getUserPreferencesManager().getCurrentUserName()){
                  userFirstName = userFirstName + " " + kony.i18n.getLocalizedString("konybb.i18n.userMgmt.loggedInUserTitle");
                }
                var data = {
                  "flxUserCommonRowDetails":"flxUserCommonRowDetails",
                  "btnManageUsers":{
                    onClick: scopeObj.showUserDetails.bind(this, dataItem.UserName, dataItem.Group_id),
                    text: applicationManager.getConfigurationManager().checkUserPermission("USER_MANAGEMENT_EDIT") ? kony.i18n.getLocalizedString("i18n.konybb.manageUser.manageUsers") : kony.i18n.getLocalizedString("i18n.customRoles.ViewPermissions"),
                    "isVisible" : (dataItem.UserName !== applicationManager.getUserPreferencesManager().getCurrentUserName()) && applicationManager.getConfigurationManager().checkUserPermission("USER_MANAGEMENT") && applicationManager.getConfigurationManager().checkUserPermission("USER_MANAGEMENT_ACTIVATE_OR_SUSPEND")
                  },
                  "flxMain":"flxMain",
                  "flxDropDown": "flxDropDown",
                  "lblDropdown":{
                    text: "O",
                    onTouchEnd : scopeObj.onUserDropdownClick.bind(this,dataItem)
                  },
                  "flxDetailsHighlighter":"flxDetailsHighlighter",
                  "flxDetails":"flxDetails",
                  "imgSample":"imgSample",
                  "flxSeparatorForHeader":"flxSeparatorForHeader",
                  "imgSeparatorHeader":"imgSeparatorHeader",
                  "lblRoleTitle":{
                    "text":kony.i18n.getLocalizedString("i18n.userManagement.userRole"),
                  },
                  "lblPhoneNumberTitle":{
                    "text":kony.i18n.getLocalizedString("i18n.konybb.manageUser.PhoneNo"),
                  },
                  "lblLastSignedInTitle":{
                    "text":kony.i18n.getLocalizedString("i18n.konybb.manageUser.LastSignedIn"),
                  },
                  "lblUserNameTitle":{
                    "text":kony.i18n.getLocalizedString("i18n.konybb.Common.UserName"),
                  },
                  "lblEmailTitle":{
                    "text":kony.i18n.getLocalizedString("i18n.konybb.manageUser.EmailID"),
                  },
                  "flxActions":"flxActions",
                  "flxManageUsers":"flxManageUsers",
                  "flxViewUserDetails":"flxViewUserDetails",
                  "btnViewDetails":{
                    "text":kony.i18n.getLocalizedString("i18n.konybb.common.ViewUserDetails"),
                    onClick: scopeObj.showUserDetails.bind(this, dataItem.UserName, dataItem.Group_id), 
                  },
                  "flxBottomSeperatorEnd":"flxBottomSeperatorEnd",
                  "imgBottomSeperatorEnd":"imgBottomSeperatorEnd",
                  "lblEmailValue":  {
                    "text":dataItem.Email ? dataItem.Email : "",
                  },
                  "lblLastSignedIn" :  {
                    "text": dataItem.Lastlogintime ? CommonUtilities.getFrontendDateString(dataItem.Lastlogintime) : "N/A",
                  },
                  "lblUserName" :  {
                    "text": CommonUtilities.truncateStringWithGivenLength(dataItem.UserName, 15),
                    "toolTip": dataItem.UserName,
                  },
                  "lblLastSignIn" :  {
                    "text": dataItem.Lastlogintime ? CommonUtilities.getFrontendDateString(dataItem.Lastlogintime) : "N/A",
                  },
                  "lblPhoneNumber":  {
                    "text":dataItem.Phone ? dataItem.Phone : "",
                  },
                  "lblName": {
                    "text": CommonUtilities.truncateStringWithGivenLength(userFirstName, 15),
                    "toolTip":userFirstName,
                  },
                  "lblUsernameValue":{
                    "text": CommonUtilities.truncateStringWithGivenLength(dataItem.UserName, 25),
                    "toolTip":dataItem.UserName,
                  },
                  //"template":"flxBBUserBasicDetails",
                  "lblRole": {
                    "text": CommonUtilities.truncateStringWithGivenLength(dataItem.role_name, 15),
                    "toolTip":dataItem.role_name,
                  },
                  "lblUsername": {
                    "text": CommonUtilities.truncateStringWithGivenLength(dataItem.UserName, 15),
                    "toolTip":dataItem.UserName,
                  },
                  "flxUserStatusIcon":"flxUserStatusIcon",
                  "lblUserStatus": {
                    "text":lblUserStatusValue,                 
                  },
                  "btnChangeStatus": {
                    onClick: scopeObj.onClickOfChangeStatus.bind(this, index),
                    "text": statusIndicatorForText,
                    isVisible : isBtnChangeStatusVisible
                  },
                  "lblUserStatusIndicator":{
                    isVisible : true,
                    skin :statusIndicatorForSkin
                  },
                };
                return data;
              });
            if(kony.application.getCurrentBreakpoint() === 640){
                this.view.TabPane.TabBodyNew.segTemplates.rowTemplate = "flxBBUserBasicDetailsMobile";
              	this.view.TabPane.TabBodyNew.flxBBUsersDashboardHeader.setVisibility(false);
                this.view.customheader.flxSeperatorHor2.setVisibility(false);
		    }
		    else{
               	this.view.TabPane.TabBodyNew.segTemplates.rowTemplate = "flxBBUserBasicDetails";
              	this.view.TabPane.TabBodyNew.flxBBUsersDashboardHeader.setVisibility(true);
              	this.view.customheader.flxSeperatorHor2.setVisibility(true);
            }
            this.view.TabPane.TabBodyNew.segTemplates.setData(segData);
            FormControllerUtility.setSortingHandlers(this.usersSortMap,  this.sortUsersHandler, this);
            FormControllerUtility.updateSortFlex(this.usersSortMap, context.config);
            }
          }
          	this.view.TabPane.TabBodyNew.flxUserRolesHeader.setVisibility(false);
          	this.view.TabPane.TabsHeaderNew.focusTab(1);
          	this.updateHamburgerMenu("User Management", "All Users");
          	this.configureSearch(context.searchString, this.searchUsersHandler);
            this.adjustScreen();
            FormControllerUtility.hideProgressBar(this.view);
         },
        /**
         * Method to configure  sorting handler  for the list of users
         *  @param {JSON} obj - sorting parameters
         */
        searchUsersHandler: function(obj) {
            this.loadBusinessBankingModule().presentationController.navigateToUsers(this.loadBusinessBankingModule().presentationController.fetchSubUsersSuccess.bind(this.loadBusinessBankingModule().presentationController),obj);
        },
        /**
         * Method to configure  sorting handler  for the list of users
         * @param {EventObject} event - event
         *  @param {JSON} obj - sorting parameters
         */
        sortUsersHandler: function(event,obj){
            this.loadBusinessBankingModule().presentationController.navigateToUsers(this.loadBusinessBankingModule().presentationController.fetchSubUsersSuccess.bind(this.loadBusinessBankingModule().presentationController),obj);
        },
        /**
         * Method to configure search logic  for the list of users
         *  @param {String} searchText - search String
         *  @param {Method} onSearch - callback
         */
    configureSearch: function (searchText, onSearch) {
        this.view.TabPane.TabSearchBarNew.flxSearch.setVisibility(true);
        this.view.TabPane.TabSearchBarNew.tbxSearch.text = searchText || "";
        this.checkSearchForm();
        this.view.TabPane.TabSearchBarNew.tbxSearch.onDone = function () {
            onSearch({
                searchString: this.view.TabPane.TabSearchBarNew.tbxSearch.text.trim()
            });
            this.checkSearchForm();
        }.bind(this);
        this.view.TabPane.TabSearchBarNew.flxSearchimg.onClick =  function () {
            onSearch({
                searchString: this.view.TabPane.TabSearchBarNew.tbxSearch.text.trim()
            });
            this.checkSearchForm();
        }.bind(this);
        this.view.TabPane.TabSearchBarNew.flxClose.onClick = function () {
            onSearch({
                searchString: ""
            });
            this.view.TabPane.TabSearchBarNew.tbxSearch.text = "";
            this.checkSearchForm();
        }.bind(this)
    },
     /**
         * Method to check whether the user typed anything in the search bar and making the UI compatible according to the actions of the user
         */
    checkSearchForm: function () {
        if (this.view.TabPane.TabSearchBarNew.tbxSearch.text.trim() === "") {
            this.view.TabPane.TabSearchBarNew.flxClose.setVisibility(false);
        }
        else {
            this.view.TabPane.TabSearchBarNew.flxClose.setVisibility(true);
        }
        this.view.TabPane.TabSearchBarNew.forceLayout();
    },
    orientationHandler:null,
    onBreakpointChange: function(width){
        kony.print('on breakpoint change');
        var scope = this;
		this.view.CustomPopup.onBreakpointChangeComponent(scope.view.CustomPopup,width);
        if (this.orientationHandler === null) {
         this.orientationHandler = new OrientationHandler();
        }
        this.orientationHandler.onOrientationChange(this.onBreakpointChange);
      	this.view.customheader.onBreakpointChangeComponent(width);
            //var responsiveFonts = new ResponsiveFonts();
            if(width ===640 || this.orientationHandler.isMobile){
                    //this.view.TabPaneNew.TabBodyNew.segTemplates.sectionHeaderTemplate = "flxempty";
                    this.view.TabPane.TabBodyNew.segTemplates.rowTemplate = "flxBBUserBasicDetailsMobile";
                    //this.view.TabPaneNew.TabBodyNew.setExpandableRowHeight(250);
                    this.view.PopupHeaderUM.lblHeader.skin = "bbSknLbl424242SSP15Px";
                    this.view.PopupHeaderUM.lblPopupMsg.skin = "bbSknLbl424242SSP15Px";
              		this.view.PopupHeaderUM.imgClose.src = "icon_close_grey.png";
                    this.view.customheader.lblHeaderMobile.isVisible = true;
                    //responsiveFonts.setMobileFonts();
          	}else{
                  //this.view.TabPaneNew.TabBodyNew.segTemplates.sectionHeaderTemplate = "flxBBUserDashboardCommonHeader";
                  if(this.focusedTabId && this.focusedTabId == 1)
                    this.view.TabPane.TabBodyNew.segTemplates.rowTemplate = "flxBBUserBasicDetails";
              	  this.view.TabPane.TabBodyNew.flxBBUsersDashboardHeader.skin = "sknFlxf9f9f9Bordere3e3e32px";
              	  this.view.TabPane.TabBodyNew.flxUserRolesHeader.skin = "sknFlxf9f9f9Bordere3e3e32px";
                  this.view.customheader.lblHeaderMobile.isVisible = false;
                  this.view.customheader.lblHeaderMobile.text = "";
                   //responsiveFonts.setDesktopFonts();
                }
       this.adjustScreen();
       this.loadBusinessBankingModule().presentationController.getBBCampaigns();
       },
      
       printAcknowledgement : function(){
         var tableList  = [
                {
                    tableHeader : kony.i18n.getLocalizedString("i18n.userManagement.userInfo"),
                    tableRows : this.getUserData()
                },
                {
                    tableHeader  : kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    tableRows : this.getUserPermissions()
                }
            ];
            if( this.view.segTransferLimits.data.length > 1){
            tableList.push(   {
                tableHeader : kony.i18n.getLocalizedString("i18n.userManagement.transPermission"),
                tableRows : this.getTransactionsAccess()
            });
        }
            var viewModel = {
                moduleHeader :  this.view.lblContentHeader.text,
                tableList : tableList
           };
           this.loadBusinessBankingModule().presentationController.showPrintPage({printKeyValueGroupModel:viewModel});
    },
       getUserData : function(){
        var self = this;
        var userData  = [];
        userData.push({
            key : kony.i18n.getLocalizedString("i18n.common.status"),
            value : self.view.flxAcknowledgementNew1.rTextSuccess.text,
        });
        userData.push({
            key : self.view.lblFullNameKey.text,
            value : self.view.lblFullNameValue.text
        })
        userData.push({
            key : self.view.lblDriverLicenseKey.text,
            value : self.view.lblDriverLicenseValue.text
       })
       userData.push({
           key  : self.view.lblEmailIdKey.text,
           value : self.view.lblEmailIdValue.text
       })
       userData.push({
           key : self.view.lblPhoneNumberKey.text,
           value : self.view.lblPhoneNumberValue.text,
       })
       userData.push({
           key : self.view.lblDateOfBirthKey.text,
           value : self.view.lblDateOfBirthValue.text
       })
       userData.push({
           key  : self.view.lblUserNameKey.text,
           value : self.view.lblUserNameValue.text
       })
       userData.push({
           key : self.view.lblSSNKey.text,
           value : self.view.lblSSNValue.text,
       })
       userData.push({
           key : kony.i18n.getLocalizedString("i18n.konybb.manageUser.UserRoles"),
           value : self.view.lblSelectedRole.text
       })
       userData.push({
           key : " ",
           value : self.view.RichTextSelectedRole.text
       })
       return userData
    },
    getUserPermissions : function(){
          var accounts = [];
          var data =  this.view.segAccountPermissions.data;
          if(data){
              data.forEach(function(item){
                  accounts.push({
                     key : kony.i18n.getLocalizedString("i18n.topmenu.accounts"),
                    value :  item.lblGeneralAccountName
                });
              });
          }
        return accounts;
    },
    getTransactionsAccess : function(){
        var transactionsAccess = [];
        var data = this.view.segTransferLimits.data;
        if(data){
            data.forEach(function(item){
                transactionsAccess.push(
                    {
                        key  : item.lblTransferType,
                        value : item.lblMaxDailyLimit+" - "+item.lblMaxDailyLimitValue+"\n"+ item.lblMaxTransactionLimit+" - "+item.lblMaxTransactionLimitValue
                    });
            });
        }
        return transactionsAccess;
   },
    showAcknowledgementScreenOnPrintCancel : function(){
         this.view.flxAcknowledgement.setVisibility(true);
         if (CommonUtilities.isPrintEnabled()) {
            this.view.flxAcknowledgementNew1.flxImgPrint.setVisibility(true);
          } else {
            this.view.flxAcknowledgementNew1.flxImgPrint.setVisibility(false);
          }
    },
  }
});