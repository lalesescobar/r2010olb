define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_fb5a0d943cbd487ea26c5488b18a8748: function AS_Button_fb5a0d943cbd487ea26c5488b18a8748(eventobject) {
        var self = this;
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
        accountModule.presentationController.showAccountsDashboard();
    },
    /** init defined for frmCardManagement **/
    AS_Form_jeea1b6f492642399617bfdf6d3fe509: function AS_Form_jeea1b6f492642399617bfdf6d3fe509(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmCardManagement **/
    AS_Form_e6c0940ed634451a841fe1815961cbfa: function AS_Form_e6c0940ed634451a841fe1815961cbfa(eventobject) {
        var self = this;
        this.formPreShowFunction();
    },
    /** postShow defined for frmCardManagement **/
    AS_Form_cf48919e45714f268daa73ee7b928732: function AS_Form_cf48919e45714f268daa73ee7b928732(eventobject) {
        var self = this;
        this.PostShowfrmCardManagement();
    },
    /** onDeviceBack defined for frmCardManagement **/
    AS_Form_bd9d54643e8e4dbea529d79f89d9f901: function AS_Form_bd9d54643e8e4dbea529d79f89d9f901(eventobject) {
        var self = this;
        kony.print("on device back");
    },
    /** onTouchEnd defined for frmCardManagement **/
    AS_Form_gd108bb6da8042b7b4ec54ad6ed88408: function AS_Form_gd108bb6da8042b7b4ec54ad6ed88408(eventobject, x, y) {
        var self = this;
        hidePopups();
    },
    /** onSlide defined for limitPurchaseSlider **/
    AS_Slider_c8dbfb620d114a7a87984a6ecf00801a: function AS_Slider_c8dbfb620d114a7a87984a6ecf00801a(eventobject, selectedvalue) {
        var self = this;
        return self.changeLimitPurchase.call(this);
    },
    /** onSlide defined for limitWithdrawalSlider **/
    AS_Slider_ba78fd68c7a74bf5ac1a71cc692899e0: function AS_Slider_ba78fd68c7a74bf5ac1a71cc692899e0(eventobject, selectedvalue) {
        var self = this;
        return self.changeLimitWithdrawal.call(this);
    },
    /** onClick defined for btnRestoreDefaults **/
    AS_Button_aaf843b0c49c417aae66f3b3bb5e698a: function AS_Button_aaf843b0c49c417aae66f3b3bb5e698a(eventobject) {
        var self = this;
        self.restoreDefaultsWithdrawalLim.call(this);
        self.restoreDefaultsPurchaseLim.call(this);
    },
    /** onClick defined for btnConfirm **/
    AS_Button_jb5e3299d9824550bfa9de7e2d9636a7: function AS_Button_jb5e3299d9824550bfa9de7e2d9636a7(eventobject) {
        var self = this;
        return self.updateCardLimits.call(this, null);
    }
});