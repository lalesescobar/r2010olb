/**
 * Description of Module representing a Confirm form.
 * @module frmAckowledgementController
 */
define(['CommonUtilities', 'OLBConstants', 'ViewConstants', 'FormControllerUtility','CampaignUtility'], function (CommonUtilities, OLBConstants, ViewConstants, FormControllerUtility,CampaignUtility) {
  var responsiveUtils = new ResponsiveUtils();
  var orientationHandler = new OrientationHandler();
  return{
    init: function () {
      var scopeObj = this;
      this.view.preShow = this.preShow;
      this.view.postShow = this.postShow;
      this.view.onDeviceBack = function(){};
      this.view.onBreakpointChange = this.onBreakpointChange;
      this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule").presentationController;
      this.initActions();
    },
    preShow: function () {
      this.view.customheadernew.activateMenu("Bill Pay", "Add Payee");
      this.view.flxDowntimeWarning.setVisibility(false);
      CampaignUtility.fetchPopupCampaigns();
      FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxHeader','flxFooter','flxMain']);
    },
    postShow: function () {
      this.view.btnSearchPayee.setFocus(true);
      this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.info.frame.height - this.view.flxFooter.info.frame.height +"dp";
      applicationManager.getNavigationManager().applyUpdates(this);
      applicationManager.executeAuthorizationFramework(this);
    },
    initActions: function () {
      this.view.btnNext.onClick = this.goToSerchedBillerDetails.bind(this);
      this.view.btnEnterPayeeInfo.onClick = this.showFlxEnterPayeeInfo.bind(this);
      this.view.btnEnterManually.onClick = this.showFlxEnterPayeeInfo.bind(this);
      this.view.btnResetPayeeInfo.onClick = this.resetPayeeInformation.bind(this);
      this.view.flxBillPayActivities.onClick = function () { this.presenter.showBillPaymentScreen({ context: "History", loadBills: true }) }.bind(this);
      if (kony.application.getCurrentBreakpoint() == 640 || orientationHandler.isMobile) {
        this.view.tbxCustomerName.placeholder = kony.i18n.getLocalizedString("i18n.BillPay.SearchforPayee");
        this.view.tbxAccountNumber.placeholder = kony.i18n.getLocalizedString("i18n.addPayee.RelationshipNumberPlaceholder");
      } else {
        this.view.tbxCustomerName.placeholder = kony.i18n.getLocalizedString("i18n.BillPay.SearchforPayeebyCompanyNameExATTorComcast");
        this.view.tbxAccountNumber.placeholder = kony.i18n.getLocalizedString("i18n.AddPayee.EnterAccountNumberasit");
      }
    },
    onBreakpointChange: function (form, width) {
      var scope = this;
      FormControllerUtility.setupFormOnTouchEnd(width);
      responsiveUtils.onOrientationChange(this.onBreakpointChange);
      this.view.customheadernew.onBreakpointChangeComponent(width);
      this.view.customfooternew.onBreakpointChangeComponent(width);
      this.view.CustomPopupLogout.onBreakpointChangeComponent(scope.view.CustomPopupLogout,width);
    },
    /** @alias module:frmAddPayeeController */ 
    /** updates the present Form based on required function.
    * @param {list} viewModel used to load a view
    */
    updateFormUI: function (viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
      if (viewModel.firstLoad) { this.initiateAddPayee(viewModel.firstLoad);}
      if (viewModel.registeredPayeeList) { this.updateRegisteredPayees(viewModel.registeredPayeeList);}
      if (viewModel.billersList) { this.showPayeeList(viewModel.billersList);}
      if (viewModel.billerDetails) { this.selectPayeeName(viewModel.billerDetails);}    
      if (viewModel.isInvalidPayee) { this.enterCorrectBillerName();}
      if (viewModel.serverError) {
        this.view.rtxDowntimeWarning.text = viewModel.serverError;
        this.view.flxDowntimeWarning.setVisibility(true);
        FormControllerUtility.hideProgressBar(this.view);
        this.view.flxFormContent.forceLayout();
      } 
      if (viewModel.campaign) {
        CampaignUtility.showCampaign(viewModel.campaign, this.view, "flxMain");
      }
    },

    /**
     * higlightBoxes
     */
    higlightBoxes: function () {
      for (var i = 0; i < arguments.length; i++) {
        arguments[i].skin = 'sknTbxSSPffffff15PxBorderFF0000opa50';
      }
    },
    /**
     * normalizeBoxes
     */
    normalizeBoxes: function () {
      for (var i = 0; i < arguments.length; i++) {
        arguments[i].skin = ViewConstants.SKINS.SKNTBXLAT0FFFFFF15PXBORDER727272OPA20;
      }
    },
    /**
     * used to show the payee
     * @param {list} payeeList list of payees
     */
    showPayeeList: function (payeeList) {
      this.view.segPayeesName.widgetDataMap = {
        "flxNewPayees": "flxNewPayees",
        "lblNewPayees": "lblNewPayees"
      };
      if (payeeList.length > 0) {
        this.view.segPayeesName.setData(this.createNewPayeeModel(payeeList));
        this.view.flxPayeeList.isVisible = true;
        this.view.forceLayout();
      }
      else {
        this.view.flxPayeeList.isVisible = false;
        this.view.forceLayout();
      }

    },
    /**
     * used to navigate the history tab(On click on RightBar)
     */
    navigateToBillPayHistoryTab: function () {
      this.presenter.showBillPaymentScreen({ context: "History" });
    },
    /**
     * used to create the new payee
     *  @param {list} billerList list of billers
     * @returns {object} list of billers
     */
    createNewPayeeModel: function (billerList) {
      return billerList.map(function (biller) {
        return {
          "lblNewPayees": biller.billerName,
          "flxNewPayees": {
            "onClick": biller.onBillerSelection
          }
        };
      }).reduce(function (p, e) { return p.concat(e); }, []);
    },

    /**
     * used to show the add payee screen
     */
    initiateAddPayee: function () {
      this.initializePayeeInformation();
      this.registerWidgetActions();
      this.accountNumberAvailable = true;
    },
    /**
     * used to initilize the payee information
     */
    initializePayeeInformation: function () {
      this.view.tbxCustomerName.text = "";
      this.view.tbxZipcode.text = "";
      this.view.tbxAccountNumber.text = "";
      this.view.tbxConfirmAccountNumber.text = "";
      this.view.tbxPhoneNumber.text = "";
      this.normalizeBoxes(this.view.tbxCustomerName, this.view.tbxZipcode);
      FormControllerUtility.disableButton(this.view.btnNext);
    },
    /**
     * navigate to frmAddPayeeInformation
     */
    showFlxEnterPayeeInfo: function () {
      this.presenter.navigateToPayeeInformation({});
    },
    /**
     * used to register the widget actions
     */
    registerWidgetActions: function () {
      var scopeObj = this;
      [this.view.tbxZipcode, this.view.tbxAccountNumber, this.view.tbxConfirmAccountNumber, this.view.tbxPhoneNumber].forEach(function (element) {
        element.onKeyUp = scopeObj.checkIfAllSearchFieldsAreFilled;
        element.text = "";
      });
      this.view.tbxCustomerName.onKeyUp = CommonUtilities.debounce(scopeObj.callOnKeyUp.bind(scopeObj),OLBConstants.FUNCTION_WAIT,false);
    },
    /**
     * used to validates the on key up information
     */
    callOnKeyUp: function () {
      this.presenter.fetchBillerList(this.view.tbxCustomerName.text);
      this.checkIfAllSearchFieldsAreFilled();
    },
    /**
     * used to validates the all payee search fields
     */
    checkIfAllSearchFieldsAreFilled: function() {
      var validationUtilityManager = applicationManager.getValidationUtilManager();
      if (this.view.tbxPhoneNumber.isVisible === true) {
        if (this.view.tbxCustomerName.text.trim() && this.view.tbxZipcode.text.trim() && this.view.tbxAccountNumber.text.trim() && this.view.tbxConfirmAccountNumber.text.trim() && (this.view.tbxPhoneNumber.text.trim() && validationUtilityManager.phoneNumberRegex.test(this.view.tbxPhoneNumber.text.trim()))) {
          FormControllerUtility.enableButton(this.view.btnNext);
        }
        else {
          FormControllerUtility.disableButton(this.view.btnNext);
        }
      }
      else {
        if (this.view.tbxCustomerName.text.trim() && this.view.tbxZipcode.text.trim() && this.view.tbxAccountNumber.text.trim() && this.view.tbxConfirmAccountNumber.text.trim()) {
          FormControllerUtility.enableButton(this.view.btnNext);
        }
        else {
          FormControllerUtility.disableButton(this.view.btnNext);
        }
      }
    },
    /**
     * used to set the payee widget Map
     * @param {list} registeredPayees list of payees
     */
    updateRegisteredPayees: function (registeredPayees) {
      this.view.segRegisteredPayees.widgetDataMap = {
        "flxRegistered": "flxRegistered",
        "lblCustomer": "lblCustomer",
        "lblAmount": "lblAmount",
        "lblDate": "lblDate",
        "btnViewDetails": "btnViewDetails",
        "btnPayBills": "btnPayBills",
        "lblHorizontalLine": "lblHorizontalLine",
        "lblIcon":"lblIcon",
        "flxIcon": "flxIcon"
      };
      if(registeredPayees.length === 0){
        this.view.flxRegisteredPayees.setVisibility(false);
      }
      else{
        this.view.flxRegisteredPayees.setVisibility(true);
        this.view.segRegisteredPayees.setData(this.createRegisteredPayeesSegmentModel(registeredPayees));
      }
      this.view.forceLayout();
    },
    /**
     * used to show the  list of all payees
     * @param {list} payees list of payees
     * @returns {object} payees object
     */
    createRegisteredPayeesSegmentModel: function (payees) {
      return payees.map(function (payee) {
        return {
          "lblCustomer": payee.payeeName,
          "lblHorizontalLine": "A",
          "lblDate": CommonUtilities.getFrontendDateString(payee.lastPaidDate),
          "lblAmount": CommonUtilities.formatCurrencyWithCommas(payee.lastPaidAmount),
          "btnViewDetails": {
            text: kony.i18n.getLocalizedString("i18n.common.ViewDetails"),
            isVisible: applicationManager.getConfigurationManager().checkUserPermission("BILL_PAY_VIEW_PAYEES"),
            onClick: payee.onViewDetailsClick
          },
          "btnPayBills": {
            text: kony.i18n.getLocalizedString("i18n.Pay.PayBills"),
            isVisible: applicationManager.getConfigurationManager().checkUserPermission("BILL_PAY_CREATE"),
            onClick: payee.onPayBillsClick
          },
          "lblIcon": {
            isVisible : applicationManager.getConfigurationManager().isCombinedUser === "true" ? true : false,
            text: payee.isBusinessPayee === "1" ? "r" : "s"
          },
          "flxIcon":{
            isVisible : applicationManager.getConfigurationManager().isCombinedUser === "true" ? true : false
          }
        };
      }).reduce(function (p, e) { return p.concat(e); }, []);
    },
    /**
     * used to select the payee name
     * @param {object} viewModel object
     */
    selectPayeeName: function (viewModel) {
      this.view.tbxCustomerName.text = viewModel.billerName;
      this.view.flxPayeeList.isVisible = false
      this.setDynamicFields(viewModel.billerCategoryName);
      this.checkIfAllSearchFieldsAreFilled();
      this.view.forceLayout();
    },
    /**
     * used to set the payee data dynamicllay
     * @param {object} billerCategory billerCategory
    */
    setDynamicFields: function (billerCategory) {
      if (billerCategory == 'Credit Card' || billerCategory == 'Utilities') {
        this.doDynamicChangesAccordingToCategory(kony.i18n.getLocalizedString("i18n.common.accountNumber"),
                                                 kony.i18n.getLocalizedString("i18n.common.AccountNumberPlaceholder"),
                                                 { 'fieldHolds': 'AccountNumber' },
                                                 kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber"),
                                                 kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber")
                                                );
      }
      else if (billerCategory == 'Phone') {
        this.doDynamicChangesAccordingToCategory(kony.i18n.getLocalizedString("i18n.common.accountNumber"),
                                                 kony.i18n.getLocalizedString("i18n.addPayee.RelationshipNumberPlaceholder"),
                                                 { 'fieldHolds': 'RelationshipNumber' },
                                                 kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber"),
                                                 kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber"),
                                                 kony.i18n.getLocalizedString("i18n.common.MobilePhone"),
                                                 kony.i18n.getLocalizedString("i18n.common.MobilePhonePlaceholder"),
                                                 { 'fieldHolds': 'MobileNumber' });
      }
      else if (billerCategory == 'Insurance') {
        this.doDynamicChangesAccordingToCategory(kony.i18n.getLocalizedString("i18n.common.accountNumber"),
                                                 kony.i18n.getLocalizedString("i18n.common.AccountNumberPlaceholder"),
                                                 { 'fieldHolds': 'AccountNumber' },
                                                 kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber"),
                                                 kony.i18n.getLocalizedString("i18n.addPayee.ConfirmRelationshipNumber"),
                                                 kony.i18n.getLocalizedString("i18n.addPayee.PolicyNumber"),
                                                 kony.i18n.getLocalizedString("i18n.addPayee.PolicyNumberPlaceholder"),
                                                 { 'fieldHolds': 'PolicyNumber' });
      }
    },
    /**
     * used to change the fields according to category.
     * @param {string} displaylabel1 label information
     * @param {string} placeholder1  place holder information
     * @param {string} info1 description
     * @param {string} displaylabel2 label information
     * @param {string} placeholder2  place holder information
     * @param {string} displaylabel3 label information
     * @param {string} placeholder3  place holder information
     * @param {string} info3 description
     */
    doDynamicChangesAccordingToCategory: function (displaylabel1, placeholder1, info1, displaylabel2, placeholder2, displaylabel3, placeholder3, info3) {
      CommonUtilities.setText(this.view.lblAccountNumber, displaylabel1 , CommonUtilities.getaccessibilityConfig());
      //this.view.tbxAccountNumber.placeholder = placeholder1;
      this.view.tbxAccountNumber.info = info1;
      CommonUtilities.setText(this.view.lblConfirmAccountNumber, displaylabel2 , CommonUtilities.getaccessibilityConfig());
      this.view.tbxConfirmAccountNumber.placeholder = placeholder2;
      if (displaylabel3 !== undefined && placeholder3 !== undefined && info3 !== undefined) {
        CommonUtilities.setText(this.view.lblPhoneNumber, displaylabel3 , CommonUtilities.getaccessibilityConfig());
        this.view.tbxPhoneNumber.placeholder = placeholder3;
        this.view.tbxPhoneNumber.info = info3;
        this.view.lblPhoneNumber.isVisible = true;
        this.view.tbxPhoneNumber.isVisible = true;
      }
      else {
        this.view.lblPhoneNumber.isVisible = false;
        this.view.tbxPhoneNumber.isVisible = false;
      }

    },
    /**
     * reset the payee information Onclick of Reset Button
     */
    resetPayeeInformation: function () {
      this.view.lblNotMatching.isVisible = false;
      this.view.flxMisMatch.isVisible = false;
      this.view.tbxCustomerName.text = "";
      this.view.tbxZipcode.text = "";
      this.view.tbxAccountNumber.text = "";
      this.view.tbxConfirmAccountNumber.text = "";
      this.view.tbxPhoneNumber.text = "";
      this.normalizeBoxes(this.view.tbxAccountNumber, this.view.tbxConfirmAccountNumber);
      FormControllerUtility.disableButton(this.view.btnNext);
      this.view.forceLayout();
    },

    /**
     * used to search the biller details(Onclick of btnNext)
     */
    goToSerchedBillerDetails: function () {
      var manuallyAddedPayee = {};
      this.view.flxMisMatch.isVisible = false;
      if (this.handleErrorInBillerSearch()) {
        manuallyAddedPayee.isManualUpdate = false;
        this.view.flxPayeeList.isVisible = false;
        if (this.view.tbxAccountNumber.info.fieldHolds === 'AccountNumber') {
          manuallyAddedPayee.accountNumber = this.view.tbxAccountNumber.text;
          if (this.view.tbxPhoneNumber.isVisible) {
            if (this.view.tbxPhoneNumber.info.fieldHolds === 'PolicyNumber') {
              manuallyAddedPayee.policyNumber = this.view.tbxPhoneNumber.text;
            }
          }
        }
        else if (this.view.tbxAccountNumber.info.fieldHolds === 'RelationshipNumber') {
          manuallyAddedPayee.relationShipNumber = this.view.tbxAccountNumber.text;
          manuallyAddedPayee.mobileNumber = this.view.tbxPhoneNumber.text;
        }
        manuallyAddedPayee.billerName = this.view.tbxCustomerName.text;
        manuallyAddedPayee.zipCode = this.view.tbxZipcode.text;
        this.presenter.showUpdateBillerPage("frmAddPayee1", manuallyAddedPayee);
        // this.resetManualInformation();
      }
    },
    /**
     * used to handle the error schenarion in biller search
     * @returns {boolean} status
     */
    handleErrorInBillerSearch: function () {
      var response = this.validateSearchedPayeeDetails();
      if (response === 'VALIDATION_SUCCESS') {
        this.normalizeBoxes(this.view.tbxAccountNumber, this.view.tbxConfirmAccountNumber);
        this.view.lblNotMatching.isVisible = false;
        return true;
      }
      else if (response === 'IDENTITY_NUMBER_MISMATCH') {
        if (this.view.tbxAccountNumber.info.fieldHolds === 'AccountNumber') {
          CommonUtilities.setText(this.view.lblNotMatching, kony.i18n.getLocalizedString("i18n.addPayee.AccountNumberMismatch") , CommonUtilities.getaccessibilityConfig());
        }
        else if (this.view.tbxAccountNumber.info.fieldHolds === 'RelationshipNumber') {
          CommonUtilities.setText(this.view.lblNotMatching, kony.i18n.getLocalizedString("i18n.addPayee.RelationshipNumberMismatch") , CommonUtilities.getaccessibilityConfig());
        }
        this.view.flxMisMatch.isVisible = false;
        this.view.lblNotMatching.isVisible = true;
        FormControllerUtility.disableButton(this.view.btnNext);
        this.higlightBoxes(this.view.tbxAccountNumber, this.view.tbxConfirmAccountNumber);
        this.view.forceLayout();
        return false;
      }
      else if (response === 'NO_BILLER_SELECTED_FROM_LIST') {
        CommonUtilities.setText(this.view.lblNotMatching, kony.i18n.getLocalizedString("i18n.addPayee.SelectBiller") , CommonUtilities.getaccessibilityConfig());
        this.view.flxMisMatch.isVisible = false;
        this.view.lblNotMatching.isVisible = true;
        FormControllerUtility.disableButton(this.view.btnNext);
        this.view.forceLayout();
        return false;
      }
    },
    /**
     * used to validate the payee details
     * @returns {string} status of payee validation
     */
    validateSearchedPayeeDetails: function () {
      var identityNumber = this.view.tbxAccountNumber.text;
      var duplicateIdentityNumber = this.view.tbxConfirmAccountNumber.text;
      if (this.view.tbxAccountNumber.info === undefined) {
        return 'NO_BILLER_SELECTED_FROM_LIST';
      }
      else {
        if (identityNumber === duplicateIdentityNumber) {
          return 'VALIDATION_SUCCESS';
        }
        else {
          return 'IDENTITY_NUMBER_MISMATCH';
        }
      }
    },
    /**
     * validate  the biller Name
     */
    enterCorrectBillerName: function () {
      this.view.rtxMisMatch.text = kony.i18n.getLocalizedString("i18n.addPayee.SelectBiller");
      this.view.lblNotMatching.isVisible = false;
      this.view.flxMisMatch.isVisible = true;
      FormControllerUtility.disableButton(this.view.btnNext2);
      this.view.forceLayout();
    },
    /**
    * used to show the permission based UI
    */
    showHistoryOption: function () {
      this.view.flxBillPayActivities.setVisibility(true);
    },

    /**
     * used to hide the permission based UI
     */
    hideHistoryOption: function () {
      this.view.flxBillPayActivities.setVisibility(false);
    }
  };
});