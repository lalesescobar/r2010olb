define(['FormControllerUtility', 'CommonUtilities', 'ViewConstants', 'OLBConstants'], function (FormControllerUtility, CommonUtilities, ViewConstants, OLBConstants) {
  var responsiveUtils = new ResponsiveUtils();
  return {
    init: function () {
      var scopeObj = this;
      this.view.preShow = this.preShow;
      this.view.postShow = this.postShow;
      this.view.onDeviceBack = function(){};
      this.initActions();
      this.view.onBreakpointChange = function () {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      };
    },
    preShow: function () {
      this.view.customheadernew.activateMenu("Bill Pay", "Add Payee");
      FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxHeader','flxFooter']);
    },
    postShow: function () {
      this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.info.frame.height - this.view.flxFooter.info.frame.height +"dp";
      applicationManager.getNavigationManager().applyUpdates(this);
      applicationManager.executeAuthorizationFramework(this);
    },
    initActions: function () {
       this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule").presentationController;
    },
    onBreakpointChange: function (form, width) {
      var scopeObj = this;
      FormControllerUtility.setupFormOnTouchEnd(width);
      responsiveUtils.onOrientationChange(this.onBreakpointChange);
      this.view.customheadernew.onBreakpointChangeComponent(width);
      this.view.customfooternew.onBreakpointChangeComponent(width);
      this.view.CustomPopupLogout.onBreakpointChangeComponent(scopeObj.view.CustomPopupLogout, width);
      this.view.CustomPopupCancel.onBreakpointChangeComponent(scopeObj.view.CustomPopupCancel, width);
    },
    /** updates the present Form based on required function.
    * @param {list} viewModel used to load a view
    */
    updateFormUI: function (viewModel) {
      if (viewModel.isLoading === true) {
        FormControllerUtility.showProgressBar(this.view);
      } else if (viewModel.isLoading === false) {
        FormControllerUtility.hideProgressBar(this.view);
      }
      if (viewModel.payeeSuccessDetails) { this.prePopulateAcknowledgementDetails(viewModel.payeeSuccessDetails);}
    },
    /**
     * used to show the acknowledgement screen with prepopulated
     * @param {object} model response details
     */
    prePopulateAcknowledgementDetails: function (model) {      
      var scopeObj = this;
      if(applicationManager.getConfigurationManager().isCombinedUser === "true"){
        this.view.flxBillerValUser.setVisibility(true);
this.view.lblBillerValUser.setVisibility(true);
        this.view.lblBillerValUser.text = model.isBusinessPayee === "1" ? "r" : "s";
      }
else{
	this.view.flxBillerValUser.setVisibility(false);
this.view.lblBillerValUser.setVisibility(false);
}
      CommonUtilities.setText(scopeObj.view.lblAcknowledgementMessage, "‘" + model.billerName + "’ " + kony.i18n.getLocalizedString("i18n.common.HasBeenAddedSuccessfully") , CommonUtilities.getaccessibilityConfig());
      CommonUtilities.setText(scopeObj.view.lblBillerVal, model.billerName , CommonUtilities.getaccessibilityConfig());
      scopeObj.view.rtxDetailsAddressVal.text = model.billerAddress;
      if (model.accountNumber === "") {
        CommonUtilities.setText(scopeObj.view.lblAccNumberVal, kony.i18n.getLocalizedString("i18n.common.NotAvailable") , CommonUtilities.getaccessibilityConfig());
      }
      else {
        CommonUtilities.setText(scopeObj.view.lblAccNumberVal, model.accountNumber , CommonUtilities.getaccessibilityConfig());
      }
      CommonUtilities.setText(scopeObj.view.lblDetailsNickVal,model.payeeNickName , CommonUtilities.getaccessibilityConfig());
      CommonUtilities.setText(scopeObj.view.lblDetailsNOBVal, model.nameOnBill , CommonUtilities.getaccessibilityConfig());
      scopeObj.view.btnMakeBillPay.onClick =function(){
        scopeObj.presenter.makePayment();
      };
      scopeObj.view.btnViewAllPayees.onClick =function(){
        scopeObj.presenter.showBillPaymentScreen({ context: "BulkPayees","loadBills": true});
      };
      this.view.forceLayout();
    },
    /**
    * used to show the permission based UI
    */
    showBillPaymentOption: function () {
      this.view.btnMakeBillPay.setVisibility(true);
      this.view.btnViewAllPayees.setVisibility(true);
    },
    /**
     * used to hide the permission based UI
     */
    hideBillPaymentOption: function () {
      this.view.btnMakeBillPay.setVisibility(false);
      this.view.btnViewAllPayees.setVisibility(false);
    }

  };
});