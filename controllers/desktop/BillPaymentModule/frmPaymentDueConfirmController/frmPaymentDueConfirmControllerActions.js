define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxCross **/
    AS_FlexContainer_df952a99dd314983b147195f879eeaab: function AS_FlexContainer_df952a99dd314983b147195f879eeaab(eventobject) {
        this.hideCancelPopup();
    },
    /** onClick defined for btnYes **/
    AS_Button_f1b4fd40fb7b44dfa521340ea6a7f5ad: function AS_Button_f1b4fd40fb7b44dfa521340ea6a7f5ad(eventobject) {
        var obj1 = {
            "tabname": "payees"
        };
        var navObj = new kony.mvc.Navigation("frmBillPay");
        navObj.navigate(obj1);
    },
    /** onClick defined for btnNo **/
    AS_Button_e942c681083748d28d0484fd077be946: function AS_Button_e942c681083748d28d0484fd077be946(eventobject) {
        this.hideCancelPopup();
    },
});