define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnSavePayee **/
    AS_Button_d276e7128d154221ba703a9a2a56af37: function AS_Button_d276e7128d154221ba703a9a2a56af37(eventobject) {
        this.viewallpayeesbtnaction();
    },
    /** onClick defined for btnMakeAnotherPayment **/
    AS_Button_db9f7d2983fd46389d41abece5f218a5: function AS_Button_db9f7d2983fd46389d41abece5f218a5(eventobject) {
        var self = this;
        this.presenter.makePayment();
    },
    /** onClick defined for flxCross **/
    AS_FlexContainer_eb5bf0d44e5a4587b90429b94812e1ed: function AS_FlexContainer_eb5bf0d44e5a4587b90429b94812e1ed(eventobject) {
        this.hideCancelPopup();
    },
    /** onClick defined for btnYes **/
    AS_Button_f6f5006fae2148839714b4a4ddd76c60: function AS_Button_f6f5006fae2148839714b4a4ddd76c60(eventobject) {
        var obj1 = {
            "tabname": "payees"
        };
        var navObj = new kony.mvc.Navigation("frmBillPay");
        navObj.navigate(obj1);
    },
    /** onClick defined for btnNo **/
    AS_Button_ff221501935b45aa8a16ed6dfe2cd7f0: function AS_Button_ff221501935b45aa8a16ed6dfe2cd7f0(eventobject) {
        this.hideCancelPopup();
    }
});