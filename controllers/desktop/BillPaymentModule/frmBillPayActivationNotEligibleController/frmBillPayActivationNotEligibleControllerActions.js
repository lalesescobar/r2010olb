define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxCross **/
    AS_FlexContainer_e2ab1bc70e78472c877594e138be9d34: function AS_FlexContainer_e2ab1bc70e78472c877594e138be9d34(eventobject) {
        var self = this;
        this.hideCancelPopup();
    },
    /** onClick defined for btnYes **/
    AS_Button_b1bacc1f5d9b40c395a6d5ae51511b3d: function AS_Button_b1bacc1f5d9b40c395a6d5ae51511b3d(eventobject) {
        var self = this;
        var obj1 = {
            "tabname": "payees"
        };
        var navObj = new kony.mvc.Navigation("frmBillPay");
        navObj.navigate(obj1);
    },
    /** onClick defined for btnNo **/
    AS_Button_b8f5b4a231834cd5937aa1c28c8a62f6: function AS_Button_b8f5b4a231834cd5937aa1c28c8a62f6(eventobject) {
        var self = this;
        this.hideCancelPopup();
    },
    /** init defined for frmBillPayActivationNotEligible **/
    AS_Form_dad4efe2694848d18327750900799155: function AS_Form_dad4efe2694848d18327750900799155(eventobject) {
        var self = this;
        this.init();
    }
});