define(['FormControllerUtility', 'CommonUtilities', 'ViewConstants', 'OLBConstants'], function (FormControllerUtility, CommonUtilities, ViewConstants, OLBConstants) {
  var responsiveUtils = new ResponsiveUtils();
  return {
    init: function () {
      var scopeObj = this;
      this.view.preShow = this.preShow;
      this.view.postShow = this.postShow;
      this.view.onDeviceBack = function(){};
      this.initActions();
      this.view.onBreakpointChange = function () {
        scopeObj.onBreakpointChange(kony.application.getCurrentBreakpoint());
      };
    },
    preShow: function () {
      this.view.customheadernew.activateMenu("Bill Pay", "Add Payee");
	    this.view.CustomPopupCancel.btnYes.onClick = this.viewallpayeesbtnaction;
      this.view.CustomPopupCancel.btnNo.onClick = this.hideCancelPopup;
      this.view.CustomPopupCancel.flxCross.onClick = this.hideCancelPopup;
      FormControllerUtility.updateWidgetsHeightInInfo(this, ['flxHeader','flxFooter']);
    },
    postShow: function () {
      this.view.flxMain.minHeight = kony.os.deviceInfo().screenHeight - this.view.flxHeader.info.frame.height - this.view.flxFooter.info.frame.height +"dp";
      applicationManager.getNavigationManager().applyUpdates(this);
    },
    initActions: function () {
       this.presenter = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPaymentModule").presentationController;
    },
    onBreakpointChange: function (form, width) {
      var scopeObj = this;
      FormControllerUtility.setupFormOnTouchEnd(width);
      responsiveUtils.onOrientationChange(this.onBreakpointChange);
      this.view.customheadernew.onBreakpointChangeComponent(width);
      this.view.customfooternew.onBreakpointChangeComponent(width);
      this.view.CustomPopupLogout.onBreakpointChangeComponent(scopeObj.view.CustomPopupLogout, width);
      this.view.CustomPopupCancel.onBreakpointChangeComponent(scopeObj.view.CustomPopupCancel, width);
    },
    /** updates the present Form based on required function.
    * @param {list} uiDataMap used to load a view
    */
    updateFormUI: function (uiDataMap) {
      if (uiDataMap.ProgressBar) {
        if (uiDataMap.ProgressBar.show) {
          FormControllerUtility.showProgressBar(this.view);
        }
        else {
          FormControllerUtility.hideProgressBar(this.view);
        }
      }
      if (uiDataMap.payeeUpdateDetails) { this.prePopulatePayeeDetails(uiDataMap.payeeUpdateDetails, uiDataMap.frm);}
    },
    /**
     * used to show the payee details
     * @param {object} payeeDetails payee details
     */
    prePopulatePayeeDetails: function (payeeDetails,frm) {
      var scopeObj = this;
      if (payeeDetails.accountNumber === "") {
        CommonUtilities.setText(scopeObj.view.lblAddAccNumValue, kony.i18n.getLocalizedString("i18n.common.NotAvailable") , CommonUtilities.getaccessibilityConfig());
      } else {
        CommonUtilities.setText(scopeObj.view.lblAddAccNumValue, payeeDetails.accountNumber , CommonUtilities.getaccessibilityConfig());
      }
      CommonUtilities.setText(scopeObj.view.lblAddBillerValue, payeeDetails.billerName , CommonUtilities.getaccessibilityConfig());
      scopeObj.view.rtxAddAddressValue.text = payeeDetails.billerAddress;
      scopeObj.view.tbxAddNickValue.text = payeeDetails.payeeNickName;
      scopeObj.view.tbxAddNameOnBillValue.text = payeeDetails.nameOnBill;
     // scopeObj.view.btnDetailsConfirm.onClick = this.goToVerifyPayeeDetails.bind(this,frm);
      scopeObj.view.btnDetailsConfirm.onClick = function () {
       if(applicationManager.getConfigurationManager().isCombinedUser === "true"){
         scopeObj.goToAddRecepientDetails(frm);
       }
        else{
          scopeObj.goToVerifyPayeeDetails(frm);
        }
      };
      scopeObj.view.btnDetailsBack.onClick = function () {
        applicationManager.getNavigationManager().navigateTo(frm);
      };
      scopeObj.view.btnDetailsCancel.onClick = function () {
        scopeObj.showCancelPopup();
      };
      
      scopeObj.view.forceLayout();
    },
    /**
     * used to show the confirmation payee details screen  onClick of btnDetailsConfirm
     */
    goToVerifyPayeeDetails: function(frm) {
      var data = {
        payeeNickName: null,
        nameOnBill: null,
        isBusinessPayee: null
      };
      data.payeeNickName = this.view.tbxAddNickValue.text;
      data.nameOnBill = this.view.tbxAddNameOnBillValue.text;
      if(applicationManager.getConfigurationManager().isSMEUser === "true"){
        data.isBusinessPayee = "1";
      }
      if(applicationManager.getConfigurationManager().isRBUser === "true"){
        data.isBusinessPayee = "0";
      }
       if(applicationManager.getConfigurationManager().isCombinedUser === "true"){
          if(this.view.lblPersonalBankingSelect.text === "M"){
            data.isBusinessPayee = "0";
          }else{
            data.isBusinessPayee = "1";
          }
         if(this.view.lblBusinessBankingSelect.text === "M"){
           data.isBusinessPayee = "1";
         }else{
           data.isBusinessPayee = "0";
         }
       }
      this.presenter.showAddPayeeConfirmPage(data,frm);
    },
    /**
     * show or hide cancel popup
     */
    showCancelPopup: function () {
      var scopeObj = this;
      scopeObj.view.flxDialogs.setVisibility(true);
      scopeObj.view.flxCancelPopup.setVisibility(true);
      scopeObj.view.CustomPopupCancel.btnYes.onClick = function () {
        scopeObj.view.flxDialogs.setVisibility(false);
        scopeObj.view.flxCancelPopup.setVisibility(false);
        scopeObj.presenter.showBillPaymentScreen({ context: "BulkPayees", loadBills: true });
      };
      scopeObj.view.CustomPopupCancel.btnNo.onClick = function () {
        scopeObj.view.flxDialogs.setVisibility(false);
        scopeObj.view.flxCancelPopup.setVisibility(false);
      }
      scopeObj.view.CustomPopupCancel.flxCross.onClick = function () {
        scopeObj.view.flxDialogs.setVisibility(false);
        scopeObj.view.flxCancelPopup.setVisibility(false);
      }
    },
    
    goToAddRecepientDetails: function(frm){
      var scopeObj = this;
      scopeObj.view.flxAddBillerDetails.setVisibility(false);
      scopeObj.view.flxAddRecepientTo.setVisibility(true);
      scopeObj.radioButtonClick();
      scopeObj.view.btnRecepientBack.onClick = function() {
        scopeObj.view.flxAddBillerDetails.setVisibility(true);
      scopeObj.view.flxAddRecepientTo.setVisibility(false);
      };
      scopeObj.view.btnRecepientCancel.onClick = function () {
        scopeObj.showCancelPopup();
      };
      scopeObj.view.btnAddRecepientContinue.onClick = function() {
        scopeObj.goToVerifyPayeeDetails(frm);	
      };
    },
    
    radioButtonClick: function(){
      var scopeObj = this;
      scopeObj.view.lblPersonalBankingSelect.text = "M";
      scopeObj.view.lblBusinessBankingSelect.text = "L";
      scopeObj.view.lblPersonalBankingSelect.onTouchStart = function() {
        scopeObj.view.lblPersonalBankingSelect.text = "M";
        scopeObj.view.lblBusinessBankingSelect.text = "L";
      };
      scopeObj.view.lblBusinessBankingSelect.onTouchStart = function() {
        scopeObj.view.lblPersonalBankingSelect.text = "L";
        scopeObj.view.lblBusinessBankingSelect.text = "M";
      };
    }
    
  };
});