define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxResetUserImg **/
    AS_FlexContainer_a705d44b12a24c4cbb7934e8a10052ff: function AS_FlexContainer_a705d44b12a24c4cbb7934e8a10052ff(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onClick defined for flxUserId **/
    AS_FlexContainer_be4b9f81b1fd410cb3447a59967c6378: function AS_FlexContainer_be4b9f81b1fd410cb3447a59967c6378(eventobject) {
        var self = this;
        this.showUserActions();
    },
    /** onTouchEnd defined for flxContextualMenu **/
    AS_FlexContainer_c3db5c9b2fec4281b869b022f331d110: function AS_FlexContainer_c3db5c9b2fec4281b869b022f331d110(eventobject, x, y) {
        var self = this;
        this.view.flxContextualMenu.setVisibility(false);
    },
    /** onClick defined for flxClose **/
    AS_FlexContainer_id211689cb10490f84b6b4329e993ad0: function AS_FlexContainer_id211689cb10490f84b6b4329e993ad0(eventobject) {
        var self = this;
        //this.closeHamburgerMenu();
    },
    /** onClick defined for btnBreadcrumb1 **/
    AS_Button_c16c13743f584e68b7bb38d103945f7d: function AS_Button_c16c13743f584e68b7bb38d103945f7d(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnBreadcrumb2 **/
    AS_Button_hc7032af67c045ce91d959a5487c0394: function AS_Button_hc7032af67c045ce91d959a5487c0394(eventobject) {
        var self = this;
        this.navigateForm();
    },
    /** onClick defined for btnDomesticAccountKA **/
    AS_Button_h98b4edafdca4690ba2ed38f043ccd71: function AS_Button_h98b4edafdca4690ba2ed38f043ccd71(eventobject) {
        var self = this;
        //this.addDomesticAccount();
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.showDomesticAccounts();
    },
    /** onClick defined for btnInternationalAccountKA **/
    AS_Button_ge0b55102eb14e79bb02107f5f17b975: function AS_Button_ge0b55102eb14e79bb02107f5f17b975(eventobject) {
        var self = this;
        //this.addInternationalAccount();
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.showInternationalAccounts();
    },
    /** onClick defined for flxcheckbox **/
    AS_FlexContainer_gf23e497d0a94dd8855ae9a57eeeb355: function AS_FlexContainer_gf23e497d0a94dd8855ae9a57eeeb355(eventobject) {
        var self = this;
        this.checkboxActionDomestic();
    },
    /** onKeyUp defined for tbxRoutingNumberKA **/
    AS_TextField_gee9dbac026944ed88bacecc715a3f82: function AS_TextField_gee9dbac026944ed88bacecc715a3f82(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxBankNameKA **/
    AS_TextField_db8104edef9f4449ab7c2844925c2d43: function AS_TextField_db8104edef9f4449ab7c2844925c2d43(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxAccountNumberKA **/
    AS_TextField_ab51bfd5dab6459b840f9abde0fe21c8: function AS_TextField_ab51bfd5dab6459b840f9abde0fe21c8(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxAccountNumberAgainKA **/
    AS_TextField_ecf6edd2a63b427a973621da9456632b: function AS_TextField_ecf6edd2a63b427a973621da9456632b(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxBeneficiaryNameKA **/
    AS_TextField_i7d5843b043240ec9123d1be2a744889: function AS_TextField_i7d5843b043240ec9123d1be2a744889(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onKeyUp defined for tbxAccountNickNameKA **/
    AS_TextField_icd762bba0284600a50dc7a5640ed462: function AS_TextField_icd762bba0284600a50dc7a5640ed462(eventobject) {
        var self = this;
        this.validateDomesticFields();
    },
    /** onClick defined for btnCancelKA **/
    AS_Button_bf0fc04945504b4d82292644f1e6da54: function AS_Button_bf0fc04945504b4d82292644f1e6da54(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.cancelTransaction();
    },
    /** onClick defined for btnAddAccountKA **/
    AS_Button_fee1e1af2ea04b4d91192be5790cc8b7: function AS_Button_fee1e1af2ea04b4d91192be5790cc8b7(eventobject) {
        var self = this;
        this.addDomesticAccount();
    },
    /** onClick defined for flxcheckboximg **/
    AS_FlexContainer_f77a2b98d10c4786b8ba4ceb69d24803: function AS_FlexContainer_f77a2b98d10c4786b8ba4ceb69d24803(eventobject) {
        var self = this;
        this.checkboxActionInternational();
    },
    /** onKeyUp defined for tbxIntSwiftCodeKA **/
    AS_TextField_ia9890ca6bad4d64b3e8f09f738d319a: function AS_TextField_ia9890ca6bad4d64b3e8f09f738d319a(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntBankNameKA **/
    AS_TextField_c3a19fbfe14c4ec98fd67b33a23769e7: function AS_TextField_c3a19fbfe14c4ec98fd67b33a23769e7(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntAccountNumberKA **/
    AS_TextField_b35effa457584f3691cc1231ced5c83a: function AS_TextField_b35effa457584f3691cc1231ced5c83a(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntAccountNumberAgainKA **/
    AS_TextField_e646ca250fcb4fd2afcbcf685d9cb3e0: function AS_TextField_e646ca250fcb4fd2afcbcf685d9cb3e0(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntBeneficiaryNameKA **/
    AS_TextField_d661d5c8f95544729307d08e3e5a4f12: function AS_TextField_d661d5c8f95544729307d08e3e5a4f12(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onKeyUp defined for tbxIntAccountNickNameKA **/
    AS_TextField_c3c3b0535ee54c01a9eb055efe68fd96: function AS_TextField_c3c3b0535ee54c01a9eb055efe68fd96(eventobject) {
        var self = this;
        this.validateInternationalFields();
    },
    /** onClick defined for btnIntCancelKA **/
    AS_Button_e52a336fe0154458b84ada93d2fa498b: function AS_Button_e52a336fe0154458b84ada93d2fa498b(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transferModule.presentationController.cancelTransaction();
    },
    /** onClick defined for btnIntAddAccountKA **/
    AS_Button_f90716835c674adf81219303f2c15ca2: function AS_Button_f90716835c674adf81219303f2c15ca2(eventobject) {
        var self = this;
        this.addInternationalAccount();
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_d395ef6bc78d4f79b4782f2dd112cb70: function AS_Button_d395ef6bc78d4f79b4782f2dd112cb70(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_iecb64ded12543628cd0dcb90268706e: function AS_Button_iecb64ded12543628cd0dcb90268706e(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_fe33abcd68ca4619a2730aaf3d4fd9cb: function AS_Button_fe33abcd68ca4619a2730aaf3d4fd9cb(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_da5fad0a0dcf4510ab06c43b8bd1f117: function AS_Button_da5fad0a0dcf4510ab06c43b8bd1f117(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_d2cf114e6e204f1f9703fe6f7438a862: function AS_Button_d2cf114e6e204f1f9703fe6f7438a862(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** onClick defined for flxMainContainer **/
    AS_FlexContainer_d6088473be854744b3da071fedf57058: function AS_FlexContainer_d6088473be854744b3da071fedf57058(eventobject) {
        var self = this;
        //test
    },
    /** onClick defined for btnLocateUs **/
    AS_Button_b0b7c8cf6d734572a43079ebc74197cb: function AS_Button_b0b7c8cf6d734572a43079ebc74197cb(eventobject) {
        var self = this;
        this.showLocateUsPage();
    },
    /** onClick defined for btnContactUs **/
    AS_Button_b9204a70529e4d33a1971d28cc9929fd: function AS_Button_b9204a70529e4d33a1971d28cc9929fd(eventobject) {
        var self = this;
        this.showContactUsPage();
    },
    /** onClick defined for btnPrivacy **/
    AS_Button_d46253994f854572b31b60aa394d4915: function AS_Button_d46253994f854572b31b60aa394d4915(eventobject) {
        var self = this;
        this.showPrivacyPolicyPage();
    },
    /** onClick defined for btnTermsAndConditions **/
    AS_Button_f498adf8a5ff4af6943b9ce229268966: function AS_Button_f498adf8a5ff4af6943b9ce229268966(eventobject) {
        var self = this;
        this.showTermsAndConditions();
    },
    /** onClick defined for btnFaqs **/
    AS_Button_h1f940d093b941e79d2285e2ce3746fe: function AS_Button_h1f940d093b941e79d2285e2ce3746fe(eventobject) {
        var self = this;
        this.showFAQs();
    },
    /** preShow defined for frmAddExternalAccountEur **/
    AS_Form_ff628e5e3c61424b8ce3d867a8d76ed4: function AS_Form_ff628e5e3c61424b8ce3d867a8d76ed4(eventobject) {
        var self = this;
        this.preshowFrmAddAccount();
    },
    /** postShow defined for frmAddExternalAccountEur **/
    AS_Form_cfe782ce52204fbb96feceaba5548951: function AS_Form_cfe782ce52204fbb96feceaba5548951(eventobject) {
        var self = this;
        this.postshowAddExternalAccount();
    },
    /** onDeviceBack defined for frmAddExternalAccountEur **/
    AS_Form_dccbc9d3fec34ef3963941bbc75986c9: function AS_Form_dccbc9d3fec34ef3963941bbc75986c9(eventobject) {
        var self = this;
        //Have to Consolidate
        kony.print("Back Button Pressed");
    },
    /** onTouchEnd defined for frmAddExternalAccountEur **/
    AS_Form_d03a14bf09c945c48688e8bae320a00a: function AS_Form_d03a14bf09c945c48688e8bae320a00a(eventobject, x, y) {
        var self = this;
        hidePopups();
    }
});