define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxDropdown **/
    AS_FlexContainer_fe56bbc447b34c84a09bd24f2e29478f: function AS_FlexContainer_fe56bbc447b34c84a09bd24f2e29478f(eventobject, context) {
        var self = this;
        this.segmentHistoryRowClick();
    },
    /** onClick defined for btnEdit **/
    AS_Button_eb30c2ef32274f5caa60b838d41676ce: function AS_Button_eb30c2ef32274f5caa60b838d41676ce(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for btnEbill **/
    AS_Button_a6960393897d44fa82b471c15a0ba576: function AS_Button_a6960393897d44fa82b471c15a0ba576(eventobject, context) {
        var self = this;
        this.showPayABill();
    },
    /** onClick defined for btnCancel **/
    AS_Button_e5d64e8ede4e4502886c298a210e697b: function AS_Button_e5d64e8ede4e4502886c298a210e697b(eventobject, context) {
        var self = this;
        this.showPayABill();
    }
});