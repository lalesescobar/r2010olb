/**
 * BusinessUserManager class contain all functions related to business user model and services.
 *@module BusinessUserManager
 */
define([], function() {
    /**
     *BusinessUserManager class contain all the function related to business user APIs
     * @alias module:BusinessUserManager
     * @class
     */
    function BusinessUserManager() {
        /**@member {object} userObj used to hold the user related information while creating/updating user*/
        this.userObj = null;
        /**@member {Array} allUsers used to hold the all created users*/
        this.allUsers = [];
        this.allAccounts= [];
        this.roles = [];
		this.transactionLimits = [];
    }
    inheritsFrom(BusinessUserManager, kony.mvc.Business.Delegator);
    /**
     * Method to clear data members
     */
    BusinessUserManager.prototype.clearDataMembers = function() {
        this.userObj = null;
        this.allUsers = [];
        this.allAccounts = [];
        this.roles = [];
		this.transactionLimits = [];
      
        this.customRoleObj = {};
    };
    /**
     * Method to creat new user object using user model
     * //Model object will change once service model is defined.
     */
    BusinessUserManager.prototype.createUserObject = function() {
        this.userObj = {};
    };
    /**
     * Method to get user object
     * @return {object} user object.
     */
    BusinessUserManager.prototype.getUserObject = function() {
        return this.userObj;
    };
    /**
     * Method to set individual attribute
     * @param {string} key - user attribute key
     * @param {any} value - user attribute value
     */
    BusinessUserManager.prototype.setUserAttribute = function(key, value) {
        this.userObj[key] = value;
    };
    /**
     * Method to get individual attribute
     * @param {string} key - user attribute key
     * @return {any} value - user attribute value
     */
    BusinessUserManager.prototype.getUserAttribute = function(key) {
        return this.userObj[key];
    };
    /**
     * Sets usersList Object to data store
     * @param {object} usersList - list of sub-users which comes from backend
     */
    BusinessUserManager.prototype.setUsersObj = function(usersList) {
        this.allUsers = usersList;
    };
    /**
     * Method to creat new customRole object
     * //Model object will change once service model is defined.
     */
    BusinessUserManager.prototype.createCustomRoleObject = function() {
        this.customRoleObj = {};
    };  
     /**
     * Method to get customRole object
     * @return {object} customRoleObj object.
     */
    BusinessUserManager.prototype.getCustomRoleObject = function() {
        return this.customRoleObj;
    }; 
    /**
     * Method to get individual attribute
     * @param {string} key - custom role attribute key
     * @return {any} value - custom role attribute value
     */
    BusinessUserManager.prototype.getCustomRoleAttribute = function(key) {
        return this.customRoleObj[key];
    };  
    /**
     * Method to set individual attribute
     * @param {string} key - custom role attribute key
     * @param {any} value - custom role attribute value
     */
    BusinessUserManager.prototype.setCustomRoleAttribute = function(key, value) {
        this.customRoleObj[key] = value;
    };  
  
    /**
     * Method to fetch all users from service.
     * @param {object} params - presentation Success
     * @param {method} presentationSuccess - presentation Success
     * @param {method} presentationError - presentation Error
     */
    BusinessUserManager.prototype.getAllUsers = function(params,presentationSuccess,presentationError) {
        var  businessUser  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Organization");
        var scope = this;
        businessUser.customVerb('getDbxOrganizationUsers', params, getAllCompletionCallback);
        function  getAllCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error, presentationSuccess, presentationError);
            if (obj["status"] === true) {
                scope.setUsersObj(obj["data"].organizationEmployee);
                presentationSuccess(obj["data"].organizationEmployee);
            } else {
                presentationError(obj["errmsg"]);
            }
        }
    };
    /**
     * Method to update status of a user
     * @param {object} params - required params.
     * @param {method} presentationSuccess - presentation Success
     * @param {method} presentationError - presentation Error
     */
    BusinessUserManager.prototype.updateUserStatus = function(params,presentationSuccess,presentationError) {
        var  businessUser  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
        var scope = this;
        businessUser.customVerb('updateDBXUserStatus', params, getAllCompletionCallback);
        function  getAllCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error, presentationSuccess, presentationError);
            if (obj["status"] === true) {
                scope.setUsersObj(obj["data"]);
                presentationSuccess(params.Status);
            } else {
                presentationError(obj["errmsg"]);
            }
        }
    };
    /**
     * Method to validate user name for availability
     * @param {object} params - parameter for validate useruser - username
     * @param {string} params.UserName - username
     * @param {method} successcallback - presentation Success
     * @param {method} errorcallback - presentation Error
     */
    BusinessUserManager.prototype.validateUserName = function(params, successcallback,errorcallback) {
        var  dbxUser  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
        params.IDMidentifier = "DBX";
        dbxUser.customVerb('verifyDbxUserName', params, getAllCompletionCallback);
        function  getAllCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error, successcallback, successcallback);
            if (obj["status"] === true) {
                if(obj["data"].isUserNameExists === "false") {
                    successcallback({
                        isAvailable: true
                    });
                }else{
                    successcallback({
                        isAvailable: false
                    });
                }
            } else {
                if(obj["errmsg"].isServerUnreachable) {
                    errorcallback(obj["errmsg"]);
                    return;
                }
                successcallback({
                    isAvailable: false
                });
            }
        }
    };
	/**
     * Method to check if SSN Exists or not
     * @param {object} params - parameter for validating SSN 
     * @param {string} params.SSN - Ssn
     * @param {method} successcallback - presentation Success
     * @param {method} errorcallback - presentation Error
     */
    BusinessUserManager.prototype.checkIfOrganisationUserExists = function(params, successcallback,errorcallback) {
        var  dbxUser  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
        params.IDMidentifier = "DBX";
        dbxUser.customVerb('checkIfOrganisationUserExists', params, getAllCompletionCallback);
        function  getAllCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error, successcallback, successcallback);
            if (obj["status"] === true) {
                successcallback(obj["data"]);
            } else {
                errorcallback(obj["errmsg"]);
            }
        }
    };
    /**
     * Method to validate user w.r.t FI to be processed via OFAC and CIP
     * @param {object} userDetails - User details required for validating User w.r.t OFAC and CIP
     * @param {string} userDetails.Snn - SSN Number
     * @param {string} userDetails.DateOfBirth - Date Of Birth
     * @param {method} successcallback - presentation Success
     * @param {method} errorcallback - presentation Error
     */
    BusinessUserManager.prototype.validateUser = function(userDetails, successcallback,errorcallback) {
        var  dbxUser =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
        dbxUser.customVerb('OFACAndCIPChecks', userDetails, getAllCompletionCallback);
        function  getAllCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error, successcallback, successcallback);
            if (obj["status"] === true) {
                successcallback({
                    isValid: true
                });
            } else {
                if(obj["errmsg"].isServerUnreachable) {
                    errorcallback(obj["errmsg"]);
                    return;
                }
                successcallback({
                    isValid: false
                });
            }
        }
    };
    /**
     * Method to fetch all available user roles
     * @param {method} successcallback - presentation Success
     * @param {method} errorcallback - presentation Error
     */
    BusinessUserManager.prototype.fetchUserRoles = function( successcallback,errorcallback) {
        var scopeObj = this;
        var typeID = applicationManager.getConfigurationManager().customerTypeId;
        var  dbxUser =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
      	 dbxUser.customVerb('getGroups', {"typeId" : typeID}, getAllCompletionCallback);
        function  getAllCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error, successcallback, errorcallback);
            if (obj["status"] === true) {
                successcallback(obj["data"]);
            } else {
                errorcallback(obj["errmsg"]);
            }
        }
    };
    /**
     * Method to get roles from cache or fetch from service.
     * @param {method} successcallback - presentation Success
     * @param {method} errorcallback - presentation Error
     */
    BusinessUserManager.prototype.getUserRoles = function(successcallback, errorcallback) {
        if(this.roles.length>0)  {
            successcallback(this.roles);
        } else {
            this.fetchUserRoles(successcallback, errorcallback);
        }
	};
	/**
     * Method to clear the transaction limits on change of Role
     */
	BusinessUserManager.prototype.resetTransactionLimits = function () {
		this.transactionLimits = [];
	};
    /**
     * Method to fetch all account for user
     * @param {method} successcallback - presentation Success
     * @param {method} errorcallback - presentation Error
     */
    BusinessUserManager.prototype.fetchAllAccounts = function(successcallback, errorcallback) {
        var scopeObj = this;
		    var accountsRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Accounts");
        accountsRepo.customVerb('getOrganizationAccounts', {}, getAllCompletionCallback);
	    	function getAllCompletionCallback(status, data, error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status, data, error, successcallback, errorcallback);
            if (obj["status"] === true) {
                scopeObj.allAccounts = obj["data"].OgranizationAccounts;
                successcallback(scopeObj.allAccounts);
            } else{
				        errorcallback(obj["errmsg"]);
			      }
        }
    };

    /**
     * Method to fetch all roles in an organisation
     * @param {method} successcallback - presentation Success
     * @param {method} errorcallback - presentation Error
     */
    BusinessUserManager.prototype.fetchAllRoles = function(successcallback, errorcallback) {
       var customRoleModel = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("CustomRole");
       customRoleModel.customVerb('getOrganizationCustomRoles', {}, getAllCompletionCallback);

       function getAllCompletionCallback(status, data, error) {
         var srh = applicationManager.getServiceResponseHandler();
         var obj = srh.manageResponse(status, data, error, successcallback, errorcallback);
         if (obj["status"] === true) {
            successcallback(obj["data"]);
         }
         else {
            errorcallback(obj["errmsg"]);
         }
       }
    };

    /**
     * Method to get all account from cache or fetch from service.
     * @param {method} successcallback - presentation Success
     * @param {method} errorcallback - presentation Error
     */
    BusinessUserManager.prototype.getAllAccounts = function(successcallback, errorcallback) {
        if(this.allAccounts.length === 0)  {
            this.fetchAllAccounts(successcallback, errorcallback);
        }
      else{
        successcallback(this.allAccounts);
		}
	};
    /**
     * Method to get all account from cache or fetch from service.
     * @param {string} roleId - presentation Success
     * @param {method} successcallback - presentation Success
     * @param {method} errorcallback - presentation Error
     */
	BusinessUserManager.prototype.getTransactionLimits = function (roleId, successcallback, errorcallback) {
		if(this.transactionLimits.length) {
			successcallback(this.transactionLimits);
		} else {
			this.fetchTransactionLimits (roleId, successcallback, errorcallback);
		}
	};
    /**
     * Method to fetch all transaction limits
     * @param {string} roleId - presentation Success
     * @param {method} successcallback - presentation Success
     * @param {method} errorcallback - presentation Error
     */
    BusinessUserManager.prototype.fetchTransactionLimits = function(roleId, successcallback, errorcallback) {
		var scopeObj =  this;
        var limitsRepo = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
        limitsRepo.customVerb('getGroupEntitlements', {"Group_id": roleId}, getAllCompletionCallback);
		function getAllCompletionCallback(status, data, error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status, data, error, successcallback, errorcallback);
            if (obj["status"] === true) {
				scopeObj.transactionLimits = obj["data"].GroupEntitlements.filter(function(account){
					return account.Type_id === "SER_TYPE_TRNS";
				});
                successcallback(scopeObj.transactionLimits);
            } else{
				errorcallback(obj["errmsg"]);
			}
        }
    };
    /**
     * Method to create the user using user object
     * @param {method} successcallback - presentation Success
     * @param {method} errorcallback - presentation Error
     */
    BusinessUserManager.prototype.createOrUpdateUser = function (successcallback, errorcallback) {
        var scopeObj = this;
        var createUser = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
        if(scopeObj.userObj.accounts){
			scopeObj.userObj.accounts = JSON.stringify(scopeObj.userObj.accounts);
		}
		if(scopeObj.userObj.services){
			scopeObj.userObj.services = JSON.stringify(scopeObj.userObj.services);
		}
        if(scopeObj.userObj.id) {
			createUser.customVerb('updateOrganizationEmployee', scopeObj.userObj, getAllCompletionCallback);
		} else {
            createUser.customVerb('CreateMicroOrganizationEmployee', scopeObj.userObj, getAllCompletionCallback);
        }
        function getAllCompletionCallback(status, data, error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status, data, error, successcallback, errorcallback);
            if(scopeObj.userObj.accounts){
				scopeObj.userObj.accounts = JSON.parse(scopeObj.userObj.accounts);
			}
			if(scopeObj.userObj.services){
				scopeObj.userObj.services = JSON.parse(scopeObj.userObj.services);
			}
			if (obj["status"] === true) {
                successcallback(obj["data"]);
            } else{
				if(obj["errmsg"].isServerUnreachable) {
                    errorcallback(obj["errmsg"]);
                    return;
                }
                successcallback(obj["errmsg"]);
			}
        }
    };
    /**
     * Method to fetch specific user details
     * @param {string} username - username
     * @param {method} presentationSuccess - presentation Success
     * @param {method} presentationError - presentation Error
     */
    BusinessUserManager.prototype.fetchUserDetails = function(username,presentationSuccess,presentationError) {
        var params = {"UserName": username};
        var  businessUser  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
        businessUser.customVerb("GetOrgEmployeeDetails",params,fetchUserDetailsCompletionCallback);
        function  fetchUserDetailsCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error, presentationSuccess, presentationError);
            if (obj["status"] === true) {
                presentationSuccess(obj["data"]);
            } else {
                presentationError(obj["errmsg"]);
            }
        }
    };
    /**
     * Method to resend Activation Link
     * @param {string} username - username
     * @param {method} presentationSuccess - presentation Success
     * @param {method} presentationError - presentation Error
     */
    BusinessUserManager.prototype.resendActivationLink = function(username,presentationSuccess,presentationError) {
        var params = {"UserName": username};
        var  businessUser  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("DbxUser");
        var scope = this;
        businessUser.customVerb("ResendActivationLink",params,resendActivationLinkCompletionCallback);
        function  resendActivationLinkCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error, presentationSuccess, presentationError);
            if (obj["status"] === true) {
                presentationSuccess(obj["data"]);
            } else {
                presentationError(obj["errmsg"]);
            }
        }
    };
   /**
     * Method to getUserRoleActions
     * @param {method} presentationSuccess - presentation Success
     * @param {method} presentationError - presentation Error
     */
    BusinessUserManager.prototype.getUserRoleActions = function(inputPayLoad,presentationSuccess,presentationError) {
        var  businessUser  =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("Organization");
        var scope = this;
	    
        businessUser.customVerb("getOrganizationGroupActionLimits",inputPayLoad,getUserRoleActionsCompletionCallback);
         function  getUserRoleActionsCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error, presentationSuccess, presentationError);
            if (obj["status"] === true) {
                  presentationSuccess(obj["data"]);
                } 
            else {
                    presentationError(obj["error"]);
                 }
         }
    };
  	/**
  		Method which invokes the service to fetch Accounts of an organization
  	**/
  	BusinessUserManager.prototype.fetchAllAccountsForOrganization = function (presentationSuccessCallback, presentationFailureCallback) {
      var AccountsModel = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Accounts"); 
    	AccountsModel.customVerb("getOrganizationAccounts",{},getCompletionCallback);
    	function getCompletionCallback(status, data, error) {
      		var srh = applicationManager.getServiceResponseHandler();
      		var obj = srh.manageResponse(status, data, error);
      		if (obj["status"] === true) 
        		presentationSuccessCallback(obj.data);
      		else
        		presentationFailureCallback(obj.errmsg);
    	}
  	};  
  
  	/**
  		Method which invokes the service to fetch list of approvers for given accountId and actionId
  	**/
  	BusinessUserManager.prototype.getAccountActionCustomerApproverList = function ( request, presentationSuccessCallback, presentationFailureCallback) {
      var DbxUser = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("DbxUser"); 
    	DbxUser.customVerb("getAccountActionCustomerApproverList",request,getCompletionCallback);
    	function getCompletionCallback(status, data, error) {
      		var srh = applicationManager.getServiceResponseHandler();
      		var obj = srh.manageResponse(status, data, error);
      		if (obj["status"] === true) 
        		presentationSuccessCallback(obj.data);
      		else
        		presentationFailureCallback(obj.errmsg);
    	}
  	};   

  /**
  		Method which invokes the service to create the user
  **/
  BusinessUserManager.prototype.createBBUser = function ( request, presentationSuccessCallback, presentationFailureCallback) {
    var DbxUser = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("DbxUser"); 
    DbxUser.customVerb("CreateSmallOrganizationEmployee",request,getCompletionCallback);
    function getCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) 
        presentationSuccessCallback(obj.data);
      else
        presentationFailureCallback(obj.errmsg);
    }
  };
  
  //method which invokes the service to create custom Role Template
  BusinessUserManager.prototype.createCustomRole = function ( request, presentationSuccessCallback, presentationFailureCallback) {
   var customRoleModel = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("CustomRole");
    customRoleModel.customVerb("createCustomRole",request,getCompletionCallback);
    function getCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) 
        presentationSuccessCallback(obj.data);
      else
        presentationFailureCallback(obj.errmsg);
    }
  };
  
  //method which invokes the service to check duplicate role name
  BusinessUserManager.prototype.duplicateCheckCustomRole = function ( request, presentationSuccessCallback, presentationFailureCallback) {
   var customRoleModel = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("CustomRole");
    customRoleModel.customVerb("verifyCustomRoleName",request,getCompletionCallback);
    function getCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) 
        presentationSuccessCallback(obj.data);
      else
        presentationFailureCallback(obj.errmsg);
    }
  };

  /**
  		Method which invokes the service to fetch the approval matrix
  **/
  BusinessUserManager.prototype.fetchApprovalMatrixBasedOnAccountIdAndLimitTypeId = function ( request, presentationSuccessCallback, presentationFailureCallback) {
    var approvalMatrix = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ApprovalMatrix"); 
    approvalMatrix.customVerb("getApprovalMatrix",request,getCompletionCallback);
    function getCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) 
        presentationSuccessCallback(obj.data);
      else
        presentationFailureCallback(obj.errmsg);
    }
  };
  
  	/**
  		Method which invokes the service to fetch all approval rules of an organization
  	**/
  	BusinessUserManager.prototype.fetchApprovalRules = function (presentationSuccessCallback, presentationFailureCallback) {
      var approvalRules = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ApprovalRules"); 
    	approvalRules.customVerb("getApprovalRules",{},getCompletionCallback);
    	function getCompletionCallback(status, data, error) {
      		var srh = applicationManager.getServiceResponseHandler();
      		var obj = srh.manageResponse(status, data, error);
      		if (obj["status"] === true) 
        		presentationSuccessCallback(obj.data);
      		else
        		presentationFailureCallback(obj.errmsg);
    	}
  	};  
  
  /**
  		Method which invokes the service to update the approval matrix
  **/
  BusinessUserManager.prototype.updateApprovalMatrixPerFeatureAction = function ( request, presentationSuccessCallback, presentationFailureCallback) {
    var approvalMatrix = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("ApprovalMatrix"); 
    approvalMatrix.customVerb("updateApprovalMatrix",request,getCompletionCallback);
    function getCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) 
        presentationSuccessCallback(obj.data);
      else
        presentationFailureCallback(obj.errmsg);
    }
  }; 
  
  /**
  		Method which invokes the service to get the organization limit for given actionId passed in request
  **/
 BusinessUserManager.prototype.getOrganizationLimitForGivenFeatureAction = function ( request, presentationSuccessCallback, presentationFailureCallback) {
    var organization = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("Organization"); 
    organization.customVerb("getOrganizationGroupActionLimits",request,getCompletionCallback);
    function getCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) 
        presentationSuccessCallback(obj.data);
      else
        presentationFailureCallback(obj.errmsg);
    }
  }; 

  /**
  		Method which invokes the update user
  **/
  BusinessUserManager.prototype.UpdateBBUser = function ( request, presentationSuccessCallback, presentationFailureCallback) {
    var DbxUser = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("DbxOrganization"); 
    DbxUser.customVerb("updateOrganizationEmployee",request,getCompletionCallback);
    function getCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) 
        presentationSuccessCallback(obj.data);
      else
        presentationFailureCallback(obj.errmsg);
    }
  };  
  
   /**
  		Method which invokes the update custom role.
  **/
 BusinessUserManager.prototype.updateCustomRole = function ( request, presentationSuccessCallback, presentationFailureCallback) {
    var customRole = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("CustomRole"); 
    customRole.customVerb("updateCustomRole",request,updateCompletionCallback);
    function updateCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) 
        presentationSuccessCallback(obj.data);
      else
        presentationFailureCallback(obj.errmsg);
    }
  };  
 
   /**
  		Method which invokes the service to apply custom role to selected users.
  **/
  BusinessUserManager.prototype.applyCustomRole = function ( request, presentationSuccessCallback, presentationFailureCallback) {
    var customRole = kony.mvc.MDAApplication.getSharedInstance().modelStore.getModelDefinition("CustomRole"); 
    customRole.customVerb("applyCustomRole",request,getCompletionCallback);
    function getCompletionCallback(status, data, error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      if (obj["status"] === true) 
        presentationSuccessCallback(obj.data);
      else
        presentationFailureCallback(obj.errmsg);
    }
  };

    /**
     * Method to fetch specific custom role details
     * @param {string} params - customerRoleId
     * @param {method} presentationSuccess - presentation Success
     * @param {method} presentationError - presentation Error
     */
    BusinessUserManager.prototype.getDetailsOfCustomRole = function(params, presentationSuccess, presentationError) {
        var  customRole =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("CustomRole");
        customRole.customVerb("getCustomRoleDetails", params, getDetailsOfCustomRoleCompletionCallback);
        function  getDetailsOfCustomRoleCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error, presentationSuccess, presentationError);
            if (obj["status"] === true) {
                presentationSuccess(obj["data"]);
            } else {
                presentationError(obj["errmsg"]);
            }
        }
    };   

    /**
     * Method to delete specific custom role
     * @param {string} customRoleId - id
     * @param {method} presentationSuccess - presentation Success
     * @param {method} presentationError - presentation Error
     */
    BusinessUserManager.prototype.deleteTheCustomRole = function(params, presentationSuccess, presentationError) {
        var  customRole =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("CustomRole");
        customRole.customVerb("deleteCustomRole", params, deleteCustomRoleCompletionCallback);
        function  deleteCustomRoleCompletionCallback(status,  data,  error) {
            var srh = applicationManager.getServiceResponseHandler();
            var obj = srh.manageResponse(status,  data,  error, presentationSuccess, presentationError);
            if (obj["status"] === true) {
                presentationSuccess(obj["data"]);
            } else {
                presentationError(obj["errmsg"]);
            }
        }
    };

    return BusinessUserManager;
});