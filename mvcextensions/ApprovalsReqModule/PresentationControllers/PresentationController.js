define(["CommonUtilities"], function(CommonUtilities) {
    /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
    function ApprovalsReqModule_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
      	this.initializePresentationController();
    }

    inheritsFrom(ApprovalsReqModule_PresentationController, kony.mvc.Presentation.BasePresenter);

    /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
    ApprovalsReqModule_PresentationController.prototype.initializePresentationController = function() {
      this.ACHManager = applicationManager.getACHManager(); 
      this.ApprovalsReqManager = applicationManager.getApprovalsReqManager();
    };

  
  
  	/*
    * Method to check permissions
    */
    ApprovalsReqModule_PresentationController.prototype.checkUserPermission = function (permission) {
      return applicationManager.getConfigurationManager().checkUserPermission(permission);
    };

   ApprovalsReqModule_PresentationController.prototype.checkAtLeastOnePermission = function (permissions) {
      return permissions.some(this.checkUserPermission);
    };
  
    /**
  	* noServiceNavigate :  This method navigates to the froms screen without any service call
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} navObj - Navigation with SUccess and Failure flows
  	* @return {} 
  	* @throws {}
  	*/ 
	ApprovalsReqModule_PresentationController.prototype.noServiceNavigate = function( navObj ) {
      	applicationManager.getNavigationManager().navigateTo(navObj.onSuccess.form);
      	applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          }, navObj.onSuccess.form);      
      	applicationManager.getNavigationManager().updateForm({
                                                           "key"          : navObj.onSuccess.context.key,
                                                           "responseData" : navObj.requestData
                                                          },navObj.onSuccess.form);
    }; 
  
  	/**
  	* dataProcessorForGenTransaction :  General Transaction Data Processing
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} response - Raw response form the Service call
  	* @return {JSON Object} response -  Massaged/formatted JSON data of the Transactions 
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.dataProcessorForGenTransaction = function( response ) { 
      	response = response.BBGeneralTransaction;
      	response.forEach(function(transaction){
           transaction.Payee = {
            text : kony.sdk.isNullOrUndefined(transaction.Payee) ? "N/A" : CommonUtilities.truncateStringWithGivenLength(transaction.Payee, 15),
            toolTip : kony.sdk.isNullOrUndefined(transaction.Payee) ? "N/A" : transaction.Payee
          };
          transaction.TransactionType = {
            text :  kony.sdk.isNullOrUndefined(transaction.featureName) ? "N/A" : CommonUtilities.truncateStringWithGivenLength(transaction.featureName, 18),
            toolTip : kony.sdk.isNullOrUndefined(transaction.featureName) ? "N/A" : transaction.featureName
          };
          transaction.userName = {
            text :  kony.sdk.isNullOrUndefined(transaction.userName) ?"N/A" : CommonUtilities.truncateStringWithGivenLength(transaction.userName, 15),
            toolTip : kony.sdk.isNullOrUndefined(transaction.userName) ? "N/A" : transaction.userName
          };
          transaction.Action = "Withdraw";
          transaction.flxBtnReject = {
            "isVisible": false,
            "width": transaction.btnViewDetails
          };
          transaction.flxTrActions1 = {
            "width": transaction.btnViewDetails
          };
          if(transaction.Status === BBConstants.TRANSACTION_STATUS.PENDING) {
            transaction.flxBtnReject.isVisible = true;
          }
          transaction.Frequency = kony.sdk.isNullOrUndefined(transaction.Frequency) ? "N/A" : transaction.Frequency;
          transaction.Reccurence = kony.sdk.isNullOrUndefined(transaction.Reccurence) ? "N/A" : transaction.Reccurence;
		  transaction.Amount = CommonUtilities.formatCurrencyWithCommas(transaction.Amount,true);
          transaction.Date = CommonUtilities.getFrontendDateStringInUTC(transaction.TransactionDate,"mm/dd/yyyy");
          if (transaction.Status === BBConstants.TRANSACTION_STATUS.REJECTED) {
          	transaction.Approval = 1 + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Rejection");
            } 
          else if (transaction.Status === BBConstants.TRANSACTION_STATUS.WITHDRAWN) {
            transaction.Approval = 1 + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Withdrawn");
            }
          else if(transaction.Status === BBConstants.TRANSACTION_STATUS.PENDING) {
          	transaction.Approval = transaction.receivedApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.of") + " " + transaction.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approved");
            }
          else if (!kony.sdk.isNullOrUndefined(transaction.requiredApprovals)) {
                transaction.Approval = transaction.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approvals");
            } 
          else {
            transaction.Approval = "N/A";
          }
          transaction.isBusinessAccount = {
        text: transaction.DebitOrCreditAccount
      };
          transaction.DebitAccount = {
            text : CommonUtilities.truncateStringWithGivenLength(transaction.AccountName+"....",16)+CommonUtilities.getLastFourDigit(transaction.DebitOrCreditAccount),
            toolTip : transaction.AccountName+"...."+CommonUtilities.getLastFourDigit(transaction.DebitOrCreditAccount)
          };
          transaction.DebitOrCreditAccount = {
            text : CommonUtilities.truncateStringWithGivenLength(transaction.AccountName+"....",16)+CommonUtilities.getLastFourDigit(transaction.DebitOrCreditAccount),
            toolTip : transaction.AccountName+"...."+CommonUtilities.getLastFourDigit(transaction.DebitOrCreditAccount)
          };
        });
      	return(response);
    };
  
  	/**
  	* fetchTransactionRequests :  This method fetches the requests made by the user that are related to transactions
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} navObj - Navigation with SUccess and Failure flows
  	* @return {} 
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.fetchTransactionRequests = function( navObj ) {       	
      	var scopeObj = this;
      	applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);
      	scopeObj.ApprovalsReqManager.fetchAllTransactionRequests(
          	navObj.requestData,
          	scopeObj.completeSuccessCall.bind(scopeObj,navObj,"transactionRequestsSuccess"),
          	scopeObj.completeFailedCall.bind(scopeObj,navObj,"showTransactionRequestsFailure")
        );
    };
  

 	/**
  	* transactionRequestsSuccess :  Success callback - which is invoked after fetching transaction requests successfully
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} response - Raw response form the Service call
  	* @return {JSON Object} response -  Massaged/formatted JSON data of the Transactions 
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.transactionRequestsSuccess = function( response ) { 
      	response.BBGeneralTransaction.forEach(function(responseObj) { 
        var widgets=1;
        if(responseObj.Status === BBConstants.TRANSACTION_STATUS.PENDING) {
          if(!kony.sdk.isNullOrUndefined(responseObj.amIApprover) && (responseObj.amIApprover === "true")) {					
            widgets=widgets+1;
          }

        }
        var orientationHandler = new OrientationHandler();
        orientationHandler.onOrientationChange(this.onBreakpointChange);
        var break_point = kony.application.getCurrentBreakpoint();
        if (break_point === 640 || orientationHandler.isMobile) {
          var width=(100-widgets)/widgets;
          var widthValue = width + "%";
          responseObj.flxBtnReject=widthValue;
          responseObj.btnViewDetails=widthValue;
          responseObj.flxTrActions1=widthValue;

        }
      });
      	return this.dataProcessorForGenTransaction(response);
    };
  

  	/**
  	* showTransactionRequestsFailure :  Failure callback - which is invoked when fetching of transaction requests is failed
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} responseError - Respose Error from the Service call 
  	* @return {JSON Object} responseError - Respose Error from the Service call  
  	* @throws {}
  	*/ 
    ApprovalsReqModule_PresentationController.prototype.showTransactionRequestsFailure = function(responseError) { 
      	return(responseError);
    };
  

  	/**
  	* fetchACHTransactionRequests :  This method fetches the requests made by the user that are related to ACH transactions
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} navObj - Navigation with SUccess and Failure flows
  	* @return {} 
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.fetchACHTransactionRequests = function( navObj ) {      	
      	var scopeObj = this;
      	applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);

      	scopeObj.ApprovalsReqManager.fetchAllACHTransactionRequests(
          	navObj.requestData,
          	scopeObj.completeSuccessCall.bind(scopeObj,navObj,"ACHTransactionRequestsSuccess"),
          	scopeObj.completeFailedCall.bind(scopeObj,navObj,"ACHTransactionRequestsFailure")
        );
    };
  

  	/**
  	* transactionRequestsSuccess :  Success callback - which is invoked after fetching ACH transaction requests successfully
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} response - Raw response form the Service call
  	* @return {JSON Object} response -  Massaged/formatted JSON data of the Transactions 
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.ACHTransactionRequestsSuccess = function( response ) { 
        response = response.ACHTransactions;	
        response.forEach(function(achTransactionObj){
            if(achTransactionObj.RequestType.includes("PPD") || achTransactionObj.RequestType.includes("CCD") || achTransactionObj.RequestType.includes("CTX")){
              achTransactionObj.RequestType=(achTransactionObj.RequestType).substring(0, 3) + " " + achTransactionObj.TransactionTypeValue;
            }
            
          	var break_point = kony.application.getCurrentBreakpoint();
			if (break_point === 640) {
                achTransactionObj.TemplateName = {
                text: CommonUtilities.truncateStringWithGivenLength(achTransactionObj.TemplateName, 27),
                toolTip: achTransactionObj.TemplateName
            };
			achTransactionObj.DebitOrCreditAccount = {
                text: CommonUtilities.truncateStringWithGivenLength(achTransactionObj.AccountName + "....", 27) + CommonUtilities.getLastFourDigit(achTransactionObj.DebitAccount),
                toolTip: achTransactionObj.AccountName + "...." + CommonUtilities.getLastFourDigit(achTransactionObj.DebitAccount)
            };
            } else {
                achTransactionObj.TemplateName = {
                text: CommonUtilities.truncateStringWithGivenLength(achTransactionObj.TemplateName, 15),
                toolTip: achTransactionObj.TemplateName
            };
			achTransactionObj.DebitOrCreditAccount = {
                text: CommonUtilities.truncateStringWithGivenLength(achTransactionObj.AccountName + "....", 16) + CommonUtilities.getLastFourDigit(achTransactionObj.DebitAccount),
                toolTip: achTransactionObj.AccountName + "...." + CommonUtilities.getLastFourDigit(achTransactionObj.DebitAccount)
            };
            }
            achTransactionObj.userName = {
              text : CommonUtilities.truncateStringWithGivenLength(achTransactionObj.userName, 15),
              toolTip : achTransactionObj.userName
            };
            achTransactionObj.Amount = CommonUtilities.formatCurrencyWithCommas(achTransactionObj.TotalAmount, true);
          	achTransactionObj.Action = "Withdraw";
            achTransactionObj.flxBtnReject = {
                                      "isVisible" : false
                                    };
          achTransactionObj.flxTrActions1 = {
            "isVisible": true
          };
            if(achTransactionObj.Status === BBConstants.TRANSACTION_STATUS.PENDING) {
              achTransactionObj.flxBtnReject.isVisible = true;
              var break_point = kony.application.getCurrentBreakpoint();
              if (break_point === 640) {
              	achTransactionObj.flxBtnReject.width="49%";
              	achTransactionObj.flxTrActions1.width="49%";
              }
            }
          else
				achTransactionObj.flxTrActions1.width="100%";
            achTransactionObj.CreatedOn = CommonUtilities.getFrontendDateStringInUTC(achTransactionObj.CreatedOn,"mm/dd/yyyy");
            achTransactionObj.TransmittedDate = achTransactionObj.CreatedOn;
          	if (achTransactionObj.Status === BBConstants.TRANSACTION_STATUS.PENDING) {
          		achTransactionObj.Approval = achTransactionObj.receivedApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.of") + " " + achTransactionObj.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approved");
            }
            else if (achTransactionObj.Status === BBConstants.TRANSACTION_STATUS.REJECTED) {
                achTransactionObj.Approval = 1 + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Rejection");
            }
            else if (!kony.sdk.isNullOrUndefined(achTransactionObj.requiredApprovals)) {
                achTransactionObj.Approval = achTransactionObj.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approvals");
            } 
            else {
            	achTransactionObj.Approval = "N/A";
         	}
            achTransactionObj.AccountName = {
                text : CommonUtilities.truncateStringWithGivenLength(achTransactionObj.AccountName + "....", 16) + CommonUtilities.getLastFourDigit(achTransactionObj.DebitAccount),
                toolTip : achTransactionObj.AccountName+"...."+CommonUtilities.getLastFourDigit(achTransactionObj.DebitAccount)
          };
        });
      	return(response);
    };
  	

  	/**
  	* showTransactionRequestsFailure :  Failure callback - which is invoked when fetching of ACH transaction requests is failed
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} responseError - Respose Error from the Service call 
  	* @return {JSON Object} responseError - Respose Error from the Service call  
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.ACHTransactionRequestsFailure = function( responseError ) { 
      	return(responseError);
    };
  

  	/**
  	* fetchACHFileRequests :  This method fetches the requests made by the user that are related to ACH Files
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} navObj - Navigation with SUccess and Failure flows
  	* @return {} 
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.fetchACHFileRequests = function( navObj ) {
      	var scopeObj = this;
      	applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);

      	scopeObj.ApprovalsReqManager.fetchAllACHFileRequests(
          	navObj.requestData,
          	scopeObj.completeSuccessCall.bind(scopeObj,navObj,"ACHFileRequestsSuccess"),
          	scopeObj.completeFailedCall.bind(scopeObj,navObj,"ACHFileRequestsFailure")
        );
    };
  

  	/**
  	* ACHFileRequestsSuccess :  Success callback - which is invoked after fetching ACH File requests successfully
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} response - Raw response form the Service call
  	* @return {JSON Object} response -  Massaged/formatted JSON data of the Transactions 
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.ACHFileRequestsSuccess = function( response ) { 
      	response = response.ACHFile;
      	response.forEach(function(achFileReq){
          achFileReq.Action = "Withdraw";
          achFileReq.flxBtnReject = {
                                      "isVisible" : false
                                    };
          if(achFileReq.FileStatus === BBConstants.TRANSACTION_STATUS.PENDING) {
          	achFileReq.flxBtnReject.isVisible = true;
          }
          achFileReq.FileName = {
            text : CommonUtilities.truncateStringWithGivenLength(achFileReq.FileName, 15),
            toolTip : achFileReq.FileName
          };
          achFileReq.userName = {
            text : CommonUtilities.truncateStringWithGivenLength(achFileReq.userName, 15),
            toolTip : achFileReq.userName
          };
          if((achFileReq.FileStatus === BBConstants.TRANSACTION_STATUS.PENDING)&& (!kony.sdk.isNullOrUndefined(achFileReq.requiredApprovals))&& (!kony.sdk.isNullOrUndefined(achFileReq.receivedApprovals))) {
				achFileReq.Approver = achFileReq.receivedApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.of") + " " + achFileReq.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approved");
          }
          else if(achFileReq.FileStatus === BBConstants.TRANSACTION_STATUS.REJECTED) {
                achFileReq.Approver = 1 + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Rejection");
          }
          else if (achFileReq.FileStatus === BBConstants.TRANSACTION_STATUS.WITHDRAWN) {
                achFileReq.Approver = kony.i18n.getLocalizedString("i18n.konybb.Common.Withdrawn");
          }
          else if (!kony.sdk.isNullOrUndefined(achFileReq.requiredApprovals)) {
                achFileReq.Approver = achFileReq.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approvals");
          }
          else{
                achFileReq.Approver = kony.i18n.getLocalizedString("i18n.common.NA");
          }
		  achFileReq.TotalCreditAmount = CommonUtilities.formatCurrencyWithCommas(achFileReq.TotalCreditAmount, true);
          achFileReq.TotalDebitAmount = CommonUtilities.formatCurrencyWithCommas(achFileReq.TotalDebitAmount, true);
          achFileReq.UpdatedDateAndTime = CommonUtilities.getDateAndTimeInUTC(achFileReq.UpdatedDateAndTime,"mm/dd/yyyy");
        });
      	return(response);
    };
  

  	/**
  	* ACHFileRequestsFailure : Failure callback - which is invoked when fetching of ACH File requests is failed
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} responseError - Respose Error from the Service call 
  	* @return {JSON Object} responseError - Respose Error from the Service call  
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.ACHFileRequestsFailure = function( responseError ){
      	return(responseError);
    };
  	

  	/**
  	* withdrawTransactionRequest :  This method is used to withdraw a transaction request made by the user 
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} navObj - navigation object with success and failure flows
  	* @return {}
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.withdrawTransactionRequest = function( navObj ){ 
      var scopeObj = this;
      applicationManager.getNavigationManager().navigateTo(navObj.onSuccess.form);
      applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);
      
      scopeObj.ApprovalsReqManager.withdrawTransactionRequest(
        	navObj.requestData,
        	scopeObj.transactionWithdrawlSuccess.bind(scopeObj,navObj),
        	scopeObj.completeFailedCall.bind(scopeObj,navObj,"transactionWithdrawlFailure")
      );
    };
  	

  	/**
  	* transactionWithdrawlSuccess :  Success callback invoked after the successful withdrawl of a transaction
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} navObj - navigation object with success and failure flows
    * @param {JSON Object} response - response object from the service call 
  	* @return {}  
  	* @throws {}
  	*/ 
	ApprovalsReqModule_PresentationController.prototype.transactionWithdrawlSuccess = function ( navObject, response ) { 
      var scopeObj = this;
      scopeObj.ACHManager.getGenTransactionDataByID(
        	{
              "Transaction_id":response.Transaction_id,
              "featureActionId":response.FeatureAction_id
            },
        	scopeObj.completeSuccessCall.bind(scopeObj,navObject,"ongetGenTransactionDataByIDSuccess"),
        	scopeObj.completeFailedCall.bind(scopeObj,navObject,"ongetGenTransactionDataByIDFailure")
      );
    };
  

  	/**
  	* transactionWithdrawlFailure :  Failure callback invoked after the failure of a transaction withdrawl 
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} error - error object form failure callback of service
  	* @return {}  
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.transactionWithdrawlFailure = function( error ){
      	return(error);
    };
    

  	/**
  	* withdrawACHTransactionRequest :  This method is used to withdraw an ACH transaction request made by the user
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} navObj - navigation object with success and failure flows
  	* @return {}
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.withdrawACHTransactionRequest = function( navObj ) { 
      var scopeObj = this;
      applicationManager.getNavigationManager().navigateTo(navObj.onSuccess.form);
      applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);
      
      scopeObj.ApprovalsReqManager.withdrawACHTransactionRequest(
        	navObj.requestData,
        	scopeObj.ACHTransactionWithdrawlSuccess.bind(scopeObj,navObj),
        	scopeObj.completeFailedCall.bind(scopeObj,navObj,"ACHTransactionWithdrawlFailure")
      );
    };
  	

    /**
  	* ACHTransactionWithdrawlSuccess :  Success callback invoked after the successful withdrawl of a ACH Transaction
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} navObj - navigation object with success and failure flows
    * @param {JSON Object} response - response object from the service call 
  	* @return {}  
  	* @throws {}
  	*/ 
	ApprovalsReqModule_PresentationController.prototype.ACHTransactionWithdrawlSuccess = function ( navObject, response ) {
        var scopeObj = this;
      	scopeObj.ACHManager.getACHTransactionDataByID(
          	{"Transaction_id":response.Transaction_id},
          	scopeObj.completeSuccessCall.bind(scopeObj,navObject,"ongetACHTransactionDataByIDSuccess"),
          	scopeObj.completeFailedCall.bind(scopeObj,navObject,"ongetACHTransactionDataByIDFailure")
        );
    };
  

    /**
  	* ACHTransactionWithdrawlFailure :  Failure callback invoked after the failure of an ACH Transaction withdrawl 
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} error - error object form failure callback of service
  	* @return {}  
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.ACHTransactionWithdrawlFailure = function( responseError ) {
      	return(responseError);
    };
  

  	/**
  	* withdrawACHFileRequest :  This method is used to withdraw an ACH File request made by the user
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} navObj - navigation object with success and failure flows
  	* @return {}
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.withdrawACHFileRequest = function( navObj ) { 
      var scopeObj = this;
      applicationManager.getNavigationManager().navigateTo(navObj.onSuccess.form);
      applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);

      scopeObj.ApprovalsReqManager.withdrawACHFileRequest(
        	navObj.requestData,
        	scopeObj.ACHFileWithdrawlSuccess.bind(scopeObj,navObj),
        	scopeObj.completeFailedCall.bind(scopeObj,navObj,"ACHFileWithdrawlFailure")
      );
    };
  	
  
	/**
  	* ACHFileWithdrawlSuccess :  Success callback invoked after the successful withdrawl of an ACH File
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} navObj - navigation object with success and failure flows
    * @param {JSON Object} response - response object from the service call 
  	* @return {}  
  	* @throws {}
  	*/ 
	ApprovalsReqModule_PresentationController.prototype.ACHFileWithdrawlSuccess = function( navObject, response ) {
      	var scopeObj = this;
      	scopeObj.ACHManager.getFilesDataByID(
          	{"ACHFileID":response.ACHFile_id},
          	scopeObj.completeSuccessCall.bind(scopeObj,navObject,"ongetFilesDataByIDSuccess"),
          	scopeObj.completeFailedCall.bind(scopeObj,navObject,"ongetFilesDataByIDFailure")
        );
    };
  
	
  	/**
  	* ACHFileWithdrawlFailure :  Failure callback invoked after the failure of an ACH File withdrawl 
  	* @member of {ApprovalsReqModule_PresentationController}
  	* @param {JSON Object} error - error object form failure callback of service
  	* @return {}  
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.ACHFileWithdrawlFailure = function( responseError ){
      	return(responseError);
    };
  
  
  /**
  * getGeneralTransactionsPendingForMyApprovals :  This is the function which is used to fetch Transactions Pending for Approvals 
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - navigation object with success and failure flows
  * @return {}
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.getGeneralTransactionsPendingForMyApprovals = function (navObj){ 
    var scopeObj = this;
    applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);	
    //TransactionPendingApprovalData = 
    scopeObj.ApprovalsReqManager.fetchGeneralTransactionsPendingForMyApprovals(
      navObj.requestData,
      scopeObj.completeSuccessCall.bind(scopeObj,navObj,"onFetchGeneralTransactionsPendingForMyApprovalsSuccess"),
      scopeObj.completeFailedCall.bind(scopeObj,navObj,"onFetchGeneralTransactionsPendingForMyApprovalsFailure")
    );
  };

  
 /**
 * onFetchGeneralTransactionsPendingForMyApprovalsSuccess :  Method to handle success response of fetching Transactions Pending for My Approvals
 * @member of {ApprovalsReqModule_PresentationController}
 * @param {JSON Object} response - response object from the service call 
 * @return {}  
 * @throws {}
 */ 
  ApprovalsReqModule_PresentationController.prototype.onFetchGeneralTransactionsPendingForMyApprovalsSuccess = function (response) {
    return this.dataProcessorForGenTransaction(response);
  };


  /**
  * ACHFileWithdrawlFailure :  Method to handle failure response of fetching General Transactions Pending for My Approvals
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} responseError - error object form failure callback of service
  * @return {}  
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.onFetchGeneralTransactionsPendingForMyApprovalsFailure = function (responseError) {
    return responseError;
  };


  /**
  * getACHTransactionsPendingForMyApprovals :  This is the function which is used to fetch ACH Transactions Pending for Approvals 
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - navigation object with success and failure flows
  * @return {}
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.getACHTransactionsPendingForMyApprovals = function (navObj) {
    var scopeObj = this;
    applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);	
    ACHTransactionPendingApprovalData = scopeObj.ApprovalsReqManager.fetchACHTransactionsPendingForMyApprovals(
      navObj.requestData,
      scopeObj.completeSuccessCall.bind(scopeObj,navObj,"onFetchACHTransactionsSuccess"),
      scopeObj.completeFailedCall.bind(scopeObj,navObj,"onFetchACHTransactionsFailure")
    );
  };

  /**
  * getRejectedACHTransactions :  This is the function which is used to fetch rejcted ACH Transactions
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - navigation object with success and failure flows
  * @return {}
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.getRejectedACHTransactions = function (navObj) {
    var scopeObj = this;
    applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);	
    scopeObj.ApprovalsReqManager.fetchRejectedACHTransactions(
      navObj.requestData,
      scopeObj.completeSuccessCall.bind(scopeObj,navObj,"onFetchACHTransactionsSuccess"),
      scopeObj.completeFailedCall.bind(scopeObj,navObj,"onFetchACHTransactionsFailure")
    );
  };
	
  /**
  * getRejectedACHFiles :  This is the function which is used to fetch rejcted ACH Transactions
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - navigation object with success and failure flows
  * @return {}
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.getRejectedACHFiles = function (navObj) {
    var scopeObj = this;
    applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);	
    scopeObj.ApprovalsReqManager.fetchRejectedACHFiles(
      navObj.requestData,
      scopeObj.completeSuccessCall.bind(scopeObj,navObj,"onFetchACHFilesSuccess"),
      scopeObj.completeFailedCall.bind(scopeObj,navObj,"onFetchACHFilesFailure")
    );
  };
  
 /**
 * onFetchACHTransactionsSuccess :  Method to handle success response of fetching ACH Transactions Pending for My Approvals
 * @member of {ApprovalsReqModule_PresentationController}
 * @param {JSON Object} response - response object from the service call 
 * @return {}  
 * @throws {}
 */ 
  ApprovalsReqModule_PresentationController.prototype.onFetchACHTransactionsSuccess = function (response) {
    response = response.ACHTransactions;
    response.forEach(function (responseObj) {
      responseObj.userName = 
        {
        	text : CommonUtilities.truncateStringWithGivenLength(responseObj.userName, 15),
        	toolTip : responseObj.userName
      	};
      responseObj.CreatedBy = 
        {
        	text : CommonUtilities.truncateStringWithGivenLength(responseObj.createdby, 15),
        	toolTip : responseObj.createdby
      	};
      var break_point = kony.application.getCurrentBreakpoint();
      if (break_point === 640) {
          responseObj.TemplateName = {
          text: CommonUtilities.truncateStringWithGivenLength(responseObj.TemplateName, 27),
          toolTip: responseObj.TemplateName
      };
      responseObj.DebitOrCreditAccount = {
          text: CommonUtilities.truncateStringWithGivenLength(responseObj.AccountName + "....", 27) + CommonUtilities.getLastFourDigit(responseObj.DebitAccount),
          toolTip: responseObj.AccountName + "...." + CommonUtilities.getLastFourDigit(responseObj.DebitAccount)
      };			
      } else {
          responseObj.TemplateName = {
          text: CommonUtilities.truncateStringWithGivenLength(responseObj.TemplateName, 15),
          toolTip: responseObj.TemplateName
      };
      responseObj.DebitOrCreditAccount = {
          text: CommonUtilities.truncateStringWithGivenLength(responseObj.AccountName + "....", 16) + CommonUtilities.getLastFourDigit(responseObj.DebitAccount),
          toolTip: responseObj.AccountName + "...." + CommonUtilities.getLastFourDigit(responseObj.DebitAccount)
      };
      }
      responseObj.Amount = CommonUtilities.formatCurrencyWithCommas(responseObj.TotalAmount, true);
      if(responseObj.RequestType.includes("PPD") || responseObj.RequestType.includes("CCD") || responseObj.RequestType.includes("CTX")){
        responseObj.RequestType = (responseObj.RequestType).substring(0, 3) + " " + responseObj.TransactionTypeValue;
      }
      responseObj.CreatedOn = CommonUtilities.getFrontendDateStringInUTC(responseObj.CreatedOn,"mm/dd/yyyy");
      responseObj.TransmittedDate = responseObj.CreatedOn;
      responseObj.Approval = responseObj.receivedApprovals + " of " + responseObj.requiredApprovals + " Approved";
      responseObj.AccountName = {
          text : CommonUtilities.truncateStringWithGivenLength(responseObj.AccountName + "....", 16) + CommonUtilities.getLastFourDigit(responseObj.DebitAccount),
          toolTip : responseObj.AccountName+"...."+CommonUtilities.getLastFourDigit(responseObj.DebitAccount)
      };
    });
    return(response);
  };


  /**
  * onFetchACHTransactionsFailure :  Method to handle failure response of fetching ACH Transactions Pending for My Approvals
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} responseError - error object form failure callback of service
  * @return {}  
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.onFetchACHTransactionsFailure = function (responseError) {
    return(responseError);
  };
  
  
  /**
  * getACHFilesPendingForMyApprovals :  This is the function which is used to fetch ACH Files Pending for Approvals 
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - navigation object with success and failure flows
  * @return {}
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.getACHFilesPendingForMyApprovals = function (navObj) {
      	var scopeObj = this;
    	applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);	
    
      scopeObj.ApprovalsReqManager.fetchACHFilesPendingForMyApprovals(
        navObj.requestData,
        scopeObj.completeSuccessCall.bind(scopeObj,navObj,"onFetchACHFilesSuccess"),
        scopeObj.completeFailedCall.bind(scopeObj,navObj,"onFetchACHFilesFailure")
      );
  };

 
 /**
 * onFetchACHFilesSuccess :  Method to handle success response of fetching ACH Files Pending for My Approvals
 * @member of {ApprovalsReqModule_PresentationController}
 * @param {JSON Object} response - response object from the service call 
 * @return {}  
 * @throws {}
 */ 
  ApprovalsReqModule_PresentationController.prototype.onFetchACHFilesSuccess = function (response) {
    response = response.ACHFile;
    response.forEach(function (responseObj) {
      responseObj.FileName = 
        {
        	text : CommonUtilities.truncateStringWithGivenLength(responseObj.FileName, 15),
        	toolTip : responseObj.FileName
      	};
      responseObj.userName = 
        {
        	text : CommonUtilities.truncateStringWithGivenLength(responseObj.userName, 15),
        	toolTip : responseObj.userName
      	};
      if((responseObj.FileStatus === BBConstants.TRANSACTION_STATUS.PENDING)&& (!kony.sdk.isNullOrUndefined(responseObj.requiredApprovals))&& (!kony.sdk.isNullOrUndefined(responseObj.receivedApprovals))) {
                  responseObj.Approver = responseObj.receivedApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.of") + " " + responseObj.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approved");
         }
         else if(responseObj.FileStatus === BBConstants.TRANSACTION_STATUS.REJECTED) {
                  responseObj.Approver = 1 + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Rejection");
         }
         else if (responseObj.FileStatus === BBConstants.TRANSACTION_STATUS.WITHDRAWN) {
                  responseObj.Approver = kony.i18n.getLocalizedString("i18n.konybb.Common.Withdrawn");
         }
         else if (!kony.sdk.isNullOrUndefined(responseObj.requiredApprovals)) {
                  responseObj.Approver = responseObj.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approvals");
         }
         else{
                  responseObj.Approver = kony.i18n.getLocalizedString("i18n.common.NA");
         }
      responseObj.TotalCreditAmount = CommonUtilities.formatCurrencyWithCommas(responseObj.TotalCreditAmount, true);
      responseObj.TotalDebitAmount = CommonUtilities.formatCurrencyWithCommas(responseObj.TotalDebitAmount, true);
      responseObj.UpdatedDateAndTime = CommonUtilities.getDateAndTimeInUTC(responseObj.UpdatedDateAndTime,"mm/dd/yyyy");
    });
    return(response);
  };


  /**
  * onFetchACHTransactionsFailure :  Method to handle failure response of fetching ACH Files Pending for My Approvals
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} responseError - error object form failure callback of service
  * @return {}  
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.onFetchACHFilesFailure = function (responseError) {
    return(responseError);
  };

  
  /**
  * approveBBGeneralTransactions :  This is the function to Approve an General Transactions
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - Navigation with SUccess and Failure flows
  * @return {} 
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.approveBBGeneralTransactions = function (navObj) {
    var scopeObj = this;
    applicationManager.getNavigationManager().navigateTo(navObj.onSuccess.form);
    applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);
	scopeObj.ApprovalsReqManager.approveBBGeneralTransactions(
      	navObj.requestData,
      	scopeObj.onApproveBBGeneralTransactionsSuccess.bind(scopeObj,navObj),
      	scopeObj.completeFailedCall.bind(scopeObj,navObj,"onApproveBBGeneralTransactionsFailure")
    );
  };
	

  /**
  * onApproveBBGeneralTransactionsSuccess : Upon Success of an Approval, function to fetch the an General Transaction Details
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObject - Navigation with SUccess and Failure flows
  * @param {JSON Object} response - success response data of the previous call
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.onApproveBBGeneralTransactionsSuccess = function (navObject, response) {
    var scopeObj = this;
    scopeObj.ACHManager.getGenTransactionDataByID(
      	{
          "Transaction_id":response.Transaction_id,
          "featureActionId":response.FeatureAction_id
        },
      	scopeObj.completeSuccessCall.bind(scopeObj,navObject,"ongetGenTransactionDataByIDSuccess"),
      	scopeObj.completeFailedCall.bind(scopeObj,navObject,"ongetGenTransactionDataByIDFailure")
    );
  };
  
  
  /**
  * ongetGenTransactionDataByIDSuccess : Successfully fetched the an General Transaction Details based on ID
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} response - success response data with ID
  * @return {} 
  * @throws {}
  */  
  ApprovalsReqModule_PresentationController.prototype.ongetGenTransactionDataByIDSuccess= function (response) { 
      var scopeObj = this;
      response = scopeObj.dataProcessorForGenTransaction(response);
      return(response[0]);
  };
  
  
  /**
  * ongetGenTransactionDataByIDFailure :  Failed to Fetch the an General Transaction Details based on ID
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} responseError - Service returned error object
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.ongetGenTransactionDataByIDFailure= function (responseError) { 
      var scopeObj = this;
      return(responseError);
  };
  
  
  /**
  * onApproveBBGeneralTransactionsFailure : Failed to Approve General Transaction 
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} responseError - Service returned error object
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.onApproveBBGeneralTransactionsFailure = function (responseError) {
      return(responseError);
  };
  
  
  /**
  * approveACHTransactions :  This is the function to Approve an ACH Transactions
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - Navigation with SUccess and Failure flows
  * @return {} 
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.approveACHTransactions = function (navObj) { 
    var scopeObj = this;     
    applicationManager.getNavigationManager().navigateTo(navObj.onSuccess.form);
    applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);	
    scopeObj.ApprovalsReqManager.approveACHTransactions(
      	navObj.requestData,
      	scopeObj.onApproveACHTransactionsSuccess.bind(scopeObj,navObj),
      	scopeObj.completeFailedCall.bind(scopeObj,navObj,"onApproveACHTransactionsFailure")
    );
  };

  
  /**
  * onApproveACHTransactionsSuccess : Upon Success of an Approval, function to fetch the an ACH Transaction Details
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObject - Navigation with SUccess and Failure flows
  * @param {JSON Object} response - success response data of the previous call
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.onApproveACHTransactionsSuccess = function (navObject, response) { 
    var scopeObj = this;
    scopeObj.ACHManager.getACHTransactionDataByID(
      	{"Transaction_id":response.Transaction_id},
      	scopeObj.completeSuccessCall.bind(scopeObj,navObject,"ongetACHTransactionDataByIDSuccess"),
      	scopeObj.completeFailedCall.bind(scopeObj,navObject,"ongetACHTransactionDataByIDFailure")
    );
  };
  
  
  /**
  * ongetACHTransactionDataByIDSuccess : Successfully fetched the an ACH Transaction Details based on ID
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} response - success response data with ID
  * @return {} 
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.ongetACHTransactionDataByIDSuccess = function (response) {
      var scopeObj = this;
      response.forEach(function (responseObj) {
        responseObj.Amount = CommonUtilities.formatCurrencyWithCommas(responseObj.Amount, true);
        if (responseObj.Status === BBConstants.TRANSACTION_STATUS.REJECTED) {
			responseObj.Approver = 1 + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Rejection");
		}
		else if (responseObj.Status === BBConstants.TRANSACTION_STATUS.WITHDRAWN) {
			responseObj.Approver = 1 + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Withdrawn");
		}
		else if(responseObj.Status === BBConstants.TRANSACTION_STATUS.PENDING) {
			responseObj.Approver = responseObj.receivedApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.of") + " " + responseObj.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approved");
		}
		else if (!kony.sdk.isNullOrUndefined(responseObj.requiredApprovals)) {
			responseObj.Approver = responseObj.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approvals");
		}
		else {
			responseObj.Approver = "N/A";
		}
      });
      return(response[0]);
  };
  
  
  /**
  * ongetACHTransactionDataByIDFailure : Failed to Approve ACH Transaction 
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} responseError - Service returned error object
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.ongetACHTransactionDataByIDFailure = function (responseError) {
      var scopeObj = this;
      return(responseError);
  };

  /**
   * Method to handle failure response of Approve ACH Transactions
   * @param {object} responseError - failure response object
   */
  ApprovalsReqModule_PresentationController.prototype.onApproveACHTransactionsFailure = function ( responseError ) {
      return(responseError);
  };
  
  
  /**
  * approveACHFiles : call the service to Approve ACH File
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - Navigation with SUccess and Failure flows
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.approveACHFiles = function (navObj) {
    var scopeObj = this;
	applicationManager.getNavigationManager().navigateTo(navObj.onSuccess.form);
    applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);
    scopeObj.ApprovalsReqManager.approveACHFiles(
      	navObj.requestData,
      	scopeObj.onApproveACHFilesSuccess.bind(scopeObj,navObj),
      	scopeObj.completeFailedCall.bind(scopeObj,navObj,"onApproveACHFilesFailure")
    );
  };

  
  /**
  * onApproveACHFilesSuccess : On success of Approve, Fetch Details of ACHFile by ID
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObject - Navigation with SUccess and Failure flows
  * @param {JSON Object} response - Success Data which is reponse to previous call
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.onApproveACHFilesSuccess = function (navObject, response) {
    var scopeObj = this;
    scopeObj.ACHManager.getFilesDataByID(
      	{"ACHFileID":response.ACHFile_id},
      	scopeObj.completeSuccessCall.bind(scopeObj,navObject,"ongetFilesDataByIDSuccess"),
      	scopeObj.completeFailedCall.bind(scopeObj,navObject,"ongetFilesDataByIDFailure")
    );
  };
  
  
  /**
  * ongetFilesDataByIDSuccess : Success to fetch ACHFile data by ID
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} response - Success Data which is reponse to previous call
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.ongetFilesDataByIDSuccess= function (response) {
    response = this.onFetchACHFilesSuccess({
                ACHFile : response
              });
    return(response[0]);
  };
  
  /**
  * ongetFilesDataByIDFailure : Failed to fetch ACHFile data by ID
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} responseError - Service error data
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.ongetFilesDataByIDFailure= function (responseError) {
     var scopeObj = this;
     return(responseError);
  };

  
  /**
  * onApproveACHFilesFailure : Failed to Approve ACHFile 
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} responseError - Service error data
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.onApproveACHFilesFailure = function (responseError) {
      return(responseError);
  };
  
  
  /**
  * rejectBBGeneralTransactions : This is the function for Reject BB General Transactions
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - Navigation with SUccess and Failure flows
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.rejectBBGeneralTransactions = function (navObj) { 
    var scopeObj = this;
    applicationManager.getNavigationManager().navigateTo(navObj.onSuccess.form);
    applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);
    scopeObj.ApprovalsReqManager.rejectBBGeneralTransactions(
      	navObj.requestData,
      	scopeObj.onRejectBBGeneralTransactionsSuccess.bind(scopeObj,navObj),
      	scopeObj.completeFailedCall.bind(scopeObj,navObj,"onRejectBBGeneralTransactionsFailure")
    );
  };

  
  /**
  * onRejectBBGeneralTransactionsSuccess : Method to handle success response of Reject BB General Transactions
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - Navigation with SUccess and Failure flows
  * @param {JSON Object} response - Success Data which is reponse to previous call
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.onRejectBBGeneralTransactionsSuccess = function (navObject, response) { 
    var scopeObj = this;
    scopeObj.ACHManager.getGenTransactionDataByID(
      	{
          "Transaction_id":response.Transaction_id,
          "featureActionId":response.FeatureAction_id
        },
      	scopeObj.completeSuccessCall.bind(scopeObj,navObject,"ongetGenTransactionDataByIDSuccess"),
      	scopeObj.completeFailedCall.bind(scopeObj,navObject,"ongetGenTransactionDataByIDFailure")
    );
  };

  
  /**
  * onRejectBBGeneralTransactionsSuccess : Method to handle failure response of Reject BB General Transactions
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} responseError - failure response object
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.onRejectBBGeneralTransactionsFailure = function (responseError) {
      return(responseError);
  };
  
  
  /**
  * rejectBBGeneralTransactions : This is the function for Reject ACH Transactions
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - Navigation with SUccess and Failure flows
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.rejectACHTransactions = function (navObj) { 
    var scopeObj = this;
    applicationManager.getNavigationManager().navigateTo(navObj.onSuccess.form);
    applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);
    scopeObj.ApprovalsReqManager.rejectACHTransactions(
      	navObj.requestData,
      	scopeObj.onRejectACHTransactionsSuccess.bind(scopeObj,navObj),
      	scopeObj.completeFailedCall.bind(scopeObj,navObj,"onRejectACHTransactionsFailure")
    );
  };
  

  /**
  * onRejectACHTransactionsSuccess : Method to handle success response of Reject ACH Transactions
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - Navigation with SUccess and Failure flows
  * @param {JSON Object} response - Success Data which is reponse to previous call
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.onRejectACHTransactionsSuccess = function (navObject, response) { 
    var scopeObj = this;
    
    scopeObj.ACHManager.getACHTransactionDataByID(
      	{"Transaction_id":response.Transaction_id},
      	scopeObj.completeSuccessCall.bind(scopeObj,navObject,"ongetACHTransactionDataByIDSuccess"),
      	scopeObj.completeFailedCall.bind(scopeObj,navObject,"ongetACHTransactionDataByIDFailure")
    );
  };


  /**
  * onRejectACHTransactionsFailure : Method to handle failure response of Reject ACH Transactions
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} responseError - Service error object from backend
  * @return {JSON Object} response Error 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.onRejectACHTransactionsFailure = function (responseError) {
      var scopeObj = this;
      return(responseError);
  };
  
  
  /**
  * rejectACHFiles : Method to handle success response of Reject ACHFile
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObj - Navigation with SUccess and Failure flows
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.rejectACHFiles = function (navObj) { 
    var scopeObj = this;
    applicationManager.getNavigationManager().navigateTo(navObj.onSuccess.form);
    applicationManager.getNavigationManager().updateForm(
          												  {
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navObj.onSuccess.form);
    scopeObj.ApprovalsReqManager.rejectACHFiles(
      	navObj.requestData,
      	scopeObj.onRejectACHFilesSuccess.bind(scopeObj,navObj),
      	scopeObj.completeFailedCall.bind(scopeObj,navObj,"onRejectACHFilesFailure")
    );
  };

  
  /**
  * onRejectACHFilesSuccess : Method to handle success response of Reject ACH Files
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Object} navObject - Navigation with SUccess and Failure flows
  * @param {JSON Object} response - success reponse from pervious call
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.onRejectACHFilesSuccess = function (navObject, response) {
    var scopeObj = this;
    scopeObj.ACHManager.getFilesDataByID(
      	{"ACHFileID":response.ACHFile_id},
      	scopeObj.completeSuccessCall.bind(scopeObj,navObject,"ongetFilesDataByIDSuccess"),
      	scopeObj.completeFailedCall.bind(scopeObj,navObject,"ongetFilesDataByIDFailure")
    );
  };

  
  /**
  * onRejectACHFilesSuccess : Method to handle failure response of Reject ACH Files
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {object} responseError - failure response object
  * @return {} 
  * @throws {}
  */
  ApprovalsReqModule_PresentationController.prototype.onRejectACHFilesFailure= function (responseError) {
      var scopeObj = this;
      return(responseError);
  };
  
  
  /**
  * getACHPendingApprovals : Method to Fetch all appropriate Pending ACH related Approvals
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {object} navigationObject - JSON object with info about Successforms, Context and Module
  * @return {} 
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.getACHPendingApprovals = function( navigationObject ) {   
      var scopeObj = this;
      var params = navigationObject.requestData;
      var asynCalls = [];
    
      applicationManager.getNavigationManager().updateForm({
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navigationObject.onSuccess.form);
      var asyncManager = applicationManager.getAsyncManager();
    
      if(this.checkAtLeastOnePermission(['ACH_COLLECTION_APPROVE', 'ACH_COLLECTION_SELF_APPROVAL', 'ACH_PAYMENT_APPROVE','ACH_PAYMENT_SELF_APPROVAL'])){
        asynCalls.push(asyncManager.asyncItem(scopeObj.ApprovalsReqManager, "fetchACHTransactionsPendingForMyApprovals", [params]));
      }
      if(this.checkAtLeastOnePermission(['ACH_FILES'])){
        asynCalls.push(asyncManager.asyncItem(scopeObj.ApprovalsReqManager, "fetchACHFilesPendingForMyApprovals", [params]));
      }
    
      asyncManager.callAsync(
        asynCalls,  this.processACHApprovals.bind(this,navigationObject)
      );
  };
  
  
  /**
  * processACHApprovals : Method to callCompleteSucces if all needed are fetched 
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {object} navigationObject - JSON object with info about Successforms, Context and Module
  * @param {JSON Array} asyncResponse - Array of responses called in asyncManager.
  * @return {} 
  * @throws {}
  */ 
  ApprovalsReqModule_PresentationController.prototype.processACHApprovals = function(navigationObject,asyncResponse){
      if (asyncResponse.isAllSuccess()) {
        this.completeSuccessCall(navigationObject, "prepareFinalACHApprovals", asyncResponse.responses);              
      }
    };
  
  /**
  	* getACHRejectedApprovals : Method to Fetch all appropriate ACH related Approvals
  	* @member of {ACHModule_PresentationController}
  	* @param {object} navigationObject - JSON object with info about Successforms, Context and Module
 	* @return {} 
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.getACHRejectedApprovals = function( navigationObject ) {   
      var scopeObj = this;
      var params = navigationObject.requestData;
      
      var asynCalls = [];
    
      applicationManager.getNavigationManager().updateForm({
                                                           "key"          : BBConstants.LOADING_INDICATOR,
                                                           "responseData" : null
                                                          },navigationObject.onSuccess.form);
      var asyncManager = applicationManager.getAsyncManager();
      
      if(this.checkAtLeastOnePermission(['ACH_COLLECTION_APPROVE', 'ACH_COLLECTION_SELF_APPROVAL', 'ACH_PAYMENT_APPROVE','ACH_PAYMENT_SELF_APPROVAL'])){
        asynCalls.push(asyncManager.asyncItem(scopeObj.ApprovalsReqManager, "fetchRejectedACHTransactions", [params]));
      }
      if(this.checkAtLeastOnePermission(['ACH_FILES'])){
        asynCalls.push(asyncManager.asyncItem(scopeObj.ApprovalsReqManager, "fetchRejectedACHFiles", [params]));
      }
      
      asyncManager.callAsync(
        asynCalls,  this.processACHRejected.bind(this,navigationObject)
      );
    };
  
  /**
  * prepareFinalACHRejected : Method to format the data if all needed are fetched 
  * @member of {ApprovalsReqModule_PresentationController}
  * @param {JSON Array} ACHRejected - Array of responses where ACHRejected[0] is for ACH Files and ACHRejected[1] for Transactions.
  * @return {} 
  * @throws {}
  */  
  ApprovalsReqModule_PresentationController.prototype.prepareFinalACHApprovals = function( ACHApprovals ) {
    	
      var i = 0;
      var achApprovalData = {};
    
      if(this.checkAtLeastOnePermission(['ACH_COLLECTION_APPROVE', 'ACH_COLLECTION_SELF_APPROVAL', 'ACH_PAYMENT_APPROVE','ACH_PAYMENT_SELF_APPROVAL'])){
		var ACHTransactions = this.onFetchACHTransactionsSuccess(ACHApprovals[i].data);
        achApprovalData.ACHTransactions = ACHTransactions;
        i++;
      }
    
      if(this.checkAtLeastOnePermission(['ACH_FILES'])){
        var ACHFiles  = this.onFetchACHFilesSuccess(ACHApprovals[i].data);
        achApprovalData.ACHFiles = ACHFiles;
      }

      return(achApprovalData);
  };
  
  
  	/**
  	* processACHRejected : Method to callCompleteSucces if all needed are fetched 
  	* @member of {ACHModule_PresentationController}
  	* @param {object} navigationObject - JSON object with info about Successforms, Context and Module
    * @param {JSON Array} asyncResponse - Array of responses called in asyncManager.
 	* @return {} 
  	* @throws {}
  	*/ 
   	ApprovalsReqModule_PresentationController.prototype.processACHRejected = function(navigationObject,asyncResponse){
      if (asyncResponse.isAllSuccess()) {
        this.completeSuccessCall(navigationObject, "prepareFinalACHRejected", asyncResponse.responses);              
      }
    };
  
  /**
  	* prepareFinalACHRejected : Method to format the data if all needed are fetched 
  	* @member of {ACHModule_PresentationController}
    * @param {JSON Array} ACHRejected - Array of responses where ACHRejected[0] is for ACH Files and ACHRejected[1] for Transactions.
 	* @return {} 
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.prepareFinalACHRejected = function( ACHRejected ) {
      
      var i = 0;
      var achRejectedData = {};
    
      if(this.checkAtLeastOnePermission(['ACH_COLLECTION_APPROVE', 'ACH_COLLECTION_SELF_APPROVAL', 'ACH_PAYMENT_APPROVE','ACH_PAYMENT_SELF_APPROVAL'])){
		var ACHTransactions = this.onFetchACHTransactionsSuccess(ACHRejected[i].data);
        achRejectedData.ACHTransactions = ACHTransactions;
        i++;
      }
      
      if(this.checkAtLeastOnePermission(['ACH_FILES'])){
        var ACHFiles  = this.onFetchACHFilesSuccess(ACHRejected[i].data);
        achRejectedData.ACHFiles = ACHFiles;
      }

      return(achRejectedData);
      
    };
  
  ApprovalsReqModule_PresentationController.prototype.formPresenter = function(frmName,data){
    this.presentUserInterface(frmName,data);	
  };

  ApprovalsReqModule_PresentationController.prototype.getRequestsHistory = function(navObj){
     var params = navObj.requestData;
     applicationManager.getNavigationManager().updateForm({
                                                          "key"          : BBConstants.LOADING_INDICATOR,
                                                          "responseData" : null
                                                         },navObj.onSuccess.form);
     this.ApprovalsReqManager.getRequestsHistory(
       params,
       this.completeSuccessCall.bind(this,navObj,"ongetRequestsHistorySuccess"),
       this.completeFailedCall.bind(this,navObj,"ongetRequestsHistoryFailure")
     );
  };
  
  ApprovalsReqModule_PresentationController.prototype.ongetRequestsHistorySuccess = function(response) {
    return response.RequestHistory;
  };
  
  ApprovalsReqModule_PresentationController.prototype.ongetRequestsHistoryFailure = function(error) {
    return(error);
  };
  
    /**
     * Wrapper method to be passed as successcallback,updates desired form with help navigationObject,dataProcessor,and response
     *@param {object} navigaton object
     *@param {function} dataProcessor - this function is used to process and return formatted response.
     *@param response from service call
     */
    ApprovalsReqModule_PresentationController.prototype.completeSuccessCall = function(navigationObject,dataProcessor,response){     
      if(!kony.sdk.isNullOrUndefined(navigationObject)){
        if(!kony.sdk.isNullOrUndefined(navigationObject.onSuccess)){
          var processedData;
          if(!kony.sdk.isNullOrUndefined(dataProcessor) && dataProcessor !== "" && this[dataProcessor] instanceof Function){
             processedData = this[dataProcessor](response);
          }
          else{
            processedData = response;
          }
          
          if(!kony.sdk.isNullOrUndefined(processedData)){
            processedData.CreatedOn=CommonUtilities.getFrontendDateStringInUTC(processedData.CreatedOn,"mm/dd/yyyy");
            processedData.EffectiveDate=CommonUtilities.getFrontendDateStringInUTC(processedData.EffectiveDate,"mm/dd/yyyy");
            var navigationContext = {};
            navigationContext.context = {
              key :  navigationObject.onSuccess.context.key,
              responseData : processedData
            };
            navigationContext.form = navigationObject.onSuccess.form;
            if(kony.application.getCurrentForm().id !== navigationObject.onSuccess.form) {
              applicationManager.getNavigationManager().navigateTo(navigationObject.onSuccess.form);
            }
            applicationManager.getNavigationManager().updateForm(navigationContext.context, navigationContext.form);            
          } 
          else{
            kony.print("Response is null or undefined");
          }
        }
      }     
    };
  
  	ApprovalsReqModule_PresentationController.prototype.getApprovalsReqCampaigns = function(){
        var scope = this;
        var asyncManager = applicationManager.getAsyncManager();
        var accountsManager = applicationManager.getAccountManager();
        var breakpoint = kony.application.getCurrentBreakpoint();
        asyncManager.callAsync(
            [
              asyncManager.asyncItem(accountsManager, 'getCampaigns', [{
              "screenName" : "ACCOUNT_DASHBOARD",
              "scale" : (breakpoint >= 1366) ? "1366" : breakpoint
              }])
        ],
        function (asyncResponses) {
            scope.getCampaigns(asyncResponses.responses[0].data);
        }
        )
    };
   /**
    *Method is used for fetching of campaigns
    * @param {Object}- list of campaigns
    */
  ApprovalsReqModule_PresentationController.prototype.getCampaigns = function(response) {
     	if(response.campaignSpecifications)
          this.getCampaignsSuccess(response);
     	else
          this.getCampaignsFailure(response);
   };
    /**
     * Method that gets called when fetching unread messages is successful
     * @param {Object} messagesObj List of messages Object
     */
    ApprovalsReqModule_PresentationController.prototype.getCampaignsSuccess = function(res) {
       applicationManager.getNavigationManager().updateForm({
           "campaignRes" : res["campaignSpecifications"]
        });
    };
    /**
     * Method that gets called when there is an error in fetching unread messages for account dashboard
     * @param {Object} error Error Object
     */
    ApprovalsReqModule_PresentationController.prototype.getCampaignsFailure = function(error) {
       applicationManager.getNavigationManager().updateForm({
            "campaignError" : error
        });
    };
  
  
    /**
     * Wrapper method to be passed as failurecallback,updates desired form with help navigationObject,dataProcessor,and response
     *@param {object} navigaton object
     *@param {function} dataProcessor - this function is used to process and return formatted response.
     *@param response from service call
     */
    ApprovalsReqModule_PresentationController.prototype.completeFailedCall = function(navigationObject,dataProcessor,response){     
      if(!kony.sdk.isNullOrUndefined(navigationObject)){
        if(!kony.sdk.isNullOrUndefined(navigationObject.onFailure)){
          var processedData;
          if(!kony.sdk.isNullOrUndefined(dataProcessor) && dataProcessor !== "" && this[dataProcessor] instanceof Function){
             processedData = this[dataProcessor](response);
          }
          else{
            processedData = response;
          }
          
          if(!kony.sdk.isNullOrUndefined(processedData)){
            var navigationContext = {};
            navigationContext.context = {
              key :  navigationObject.onFailure.context.key,
              responseData : processedData
            };
            navigationContext.form = navigationObject.onFailure.form;
            if(kony.application.getCurrentForm().id !== navigationObject.onFailure.form) {
              applicationManager.getNavigationManager().navigateTo(navigationObject.onFailure.form);
            }
            applicationManager.getNavigationManager().updateForm(navigationContext.context, navigationContext.form);            
          } 
          else{
            kony.print("Response is null or undefined");
          }
        }
      }     
    }; 
  
  /**
  	 * no service call navigation to forms
  	*/
	ApprovalsReqModule_PresentationController.prototype.noServiceNavigateToForm = function(formName,contextKey,resData) {
		// Null check for empty parameter
		if(!kony.sdk.isNullOrUndefined(formName)){
		  if(kony.application.getCurrentForm().id !== formName){
			applicationManager.getNavigationManager().navigateTo(formName);
            applicationManager.getNavigationManager().updateForm({
              "key"          : contextKey,
              "responseData" : resData
            },formName);
		  }
		}
	};
		
	/**
  	 * Fetch Pending Approvals for Bulk Payments
  	*/
	ApprovalsReqModule_PresentationController.prototype.fetchPendingBulkPaymentApprovals = function (params) { 		
		applicationManager.getNavigationManager().updateForm({
			"progressBar": true,
		});
		this.ApprovalsReqManager.fetchPendingBulkPaymentApprovals(params,this.fetchPendingBulkPaymentApprovalsSuccess.bind(this), this.fetchPendingBulkPaymentApprovalsFailure.bind(this));
    };

    ApprovalsReqModule_PresentationController.prototype.fetchPendingBulkPaymentApprovalsSuccess = function(response) {  
			
		var formattedResponse = this.formatBulkpaymentAppReqData(response,0);	
		applicationManager.getNavigationManager().updateForm({
		  "fetchApprovalsForBulkPay": formattedResponse,
		}, "frmBBMyApprovals");
    };

    ApprovalsReqModule_PresentationController.prototype.fetchPendingBulkPaymentApprovalsFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
            "SERVICE_ERROR": responseError.errorMessage,
		}, "frmBBMyApprovals");
    };
	
	/**
  	 * Fetch Pending Requests for Bulk Payments
  	*/
	ApprovalsReqModule_PresentationController.prototype.fetchPendingBulkPaymentRequests = function (params) { 		
		applicationManager.getNavigationManager().updateForm({
			"progressBar": true,
		});
		this.ApprovalsReqManager.fetchPendingBulkPaymentRequests(params,this.fetchPendingBulkPaymentRequestsSuccess.bind(this), this.fetchPendingBulkPaymentRequestsFailure.bind(this));
    };

    ApprovalsReqModule_PresentationController.prototype.fetchPendingBulkPaymentRequestsSuccess = function(response) {  
		
	var formattedResponse = this.formatBulkpaymentAppReqData(response,0);		
		applicationManager.getNavigationManager().updateForm({
		  "fetchRequestsForBulkPay": formattedResponse,
		}, "frmBBMyRequests");
    };

    ApprovalsReqModule_PresentationController.prototype.fetchPendingBulkPaymentRequestsFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
            "SERVICE_ERROR": responseError.errorMessage,
		}, "frmBBMyRequests");
    };
	
	/**
  	 * Fetch Approval History for Bulk Payments
  	*/
	ApprovalsReqModule_PresentationController.prototype.fetchBulkPaymentApprovalHistory = function (params) { 		
		applicationManager.getNavigationManager().updateForm({
			"progressBar": true,
		});
		this.ApprovalsReqManager.fetchRecordsHistoryActedByMe(params,this.fetchBulkPaymentApprovalHistorySuccess.bind(this), this.fetchBulkPaymentApprovalHistoryFailure.bind(this));
    };

    ApprovalsReqModule_PresentationController.prototype.fetchBulkPaymentApprovalHistorySuccess = function(response) {  
	
		var formattedResponse = this.formatBulkpaymentAppReqData(response,1);
		applicationManager.getNavigationManager().updateForm({
		  "fetchApprovalHistoryForBulkPay": formattedResponse,
		}, "frmBBApprovalHistory");
    };

    ApprovalsReqModule_PresentationController.prototype.fetchBulkPaymentApprovalHistoryFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
            "SERVICE_ERROR": responseError.errorMessage,
		}, "frmBBApprovalHistory");
    };
		
	/**
  	 * Fetch Request History for Bulk Payments
  	*/
	ApprovalsReqModule_PresentationController.prototype.fetchBulkPaymentRequestHistory = function (params) { 		
		applicationManager.getNavigationManager().updateForm({
			"progressBar": true,
		});
		this.ApprovalsReqManager.fetchRecordsHistoryReviewedByMe(params,this.fetchBulkPaymentRequestHistorySuccess.bind(this), this.fetchBulkPaymentRequestHistoryFailure.bind(this));
    };

    ApprovalsReqModule_PresentationController.prototype.fetchBulkPaymentRequestHistorySuccess = function(response) {  
	
		var formattedResponse = this.formatBulkpaymentAppReqData(response,1);
		applicationManager.getNavigationManager().updateForm({
		  "fetchRequestHistoryForBulkPay": formattedResponse,
		}, "frmBBRequestHistory");
    };

    ApprovalsReqModule_PresentationController.prototype.fetchBulkPaymentRequestHistoryFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
            "SERVICE_ERROR": responseError.errorMessage,
		}, "frmBBRequestHistory");
    };
	
	/**
  	*   formatBulkpaymentAppReqData : formatting the data as required for the form controller.
  	* @throws {}
  	*/ 
  	ApprovalsReqModule_PresentationController.prototype.formatBulkpaymentAppReqData = function(response,historyFlag) {
		var res = [];
    response = response.records;
		response.forEach(function(transaction){
			var OpStatus= "";	
			if(historyFlag === 0){
				OpStatus=transaction.initiatedBy;
			}						
			else{
				OpStatus=transaction.status;
			}
			
			if((transaction.status === BBConstants.TRANSACTION_STATUS.PENDING)&& (!kony.sdk.isNullOrUndefined(transaction.receivedApprovals))&& (!kony.sdk.isNullOrUndefined(transaction.requiredApprovals))) {
				transaction.Approver = transaction.receivedApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.of") + " " + transaction.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approved");
			}
			else if(transaction.status === BBConstants.TRANSACTION_STATUS.REJECTED) {
				transaction.Approver = 1 + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Rejection");
			}				
			else if (!kony.sdk.isNullOrUndefined(transaction.requiredApprovals)) {
				transaction.Approver = transaction.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approvals");
			}
			else{
				transaction.Approver = kony.i18n.getLocalizedString("i18n.common.NA");
			}
			transaction.fromAccountMasked =  CommonUtilities.getMaskedAccName(transaction.fromAccount);
			
			res.push({				   
			   "lblOpDescription": {
				  "text": kony.sdk.isNullOrUndefined(transaction.description) ? "N/A" : transaction.description,
				  "toolTip" : kony.sdk.isNullOrUndefined(transaction.description) ? "N/A" : transaction.description
				},	
				"lblDate": {
				  "text": kony.sdk.isNullOrUndefined(transaction.paymentDate) ? "N/A" : CommonUtilities.getFrontendDateStringInUTC(transaction.paymentDate,"mm/dd/yyyy")
				},
				"lblOpStatus": {					
				  "text": kony.sdk.isNullOrUndefined(OpStatus) ? "N/A" : OpStatus,
				  "toolTip" : kony.sdk.isNullOrUndefined(OpStatus) ? "N/A" : OpStatus
				},
				"lblOPFromAccountValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.fromAccountMasked) ? "N/A" : transaction.fromAccountMasked,
				},
				"lblOPTotalTransactionsValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.totalTransactions) ? "N/A" : transaction.totalTransactions,
				  "toolTip" : kony.sdk.isNullOrUndefined(transaction.totalTransactions) ? "N/A" : transaction.totalTransactions
				},
				"lblOPTotalAmountValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.totalAmount) ? "N/A" : CommonUtilities.formatCurrencyWithCommas(transaction.totalAmount, false, transaction.currency),
				  "toolTip" : kony.sdk.isNullOrUndefined(transaction.totalAmount) ? "N/A" : CommonUtilities.formatCurrencyWithCommas(transaction.totalAmount, false, transaction.currency)
				},
				"lblOPPaymentIDValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.paymentId) ? "N/A" : transaction.paymentId,
				  "toolTip" : kony.sdk.isNullOrUndefined(transaction.paymentId) ? "N/A" : transaction.paymentId
				},	
				"lblOPnitiatedByValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.status) ? "N/A" : transaction.status,
				  "toolTip" : kony.sdk.isNullOrUndefined(transaction.status) ? "N/A" : transaction.status
				},	
				"requestId" : kony.sdk.isNullOrUndefined(transaction.requestId) ? "N/A" :  transaction.requestId,					
				"recordId": kony.sdk.isNullOrUndefined(transaction.recordId) ? "N/A" :  transaction.recordId,					
				"paymentId": kony.sdk.isNullOrUndefined(transaction.paymentId) ? "N/A" :  transaction.paymentId,					
				"description": kony.sdk.isNullOrUndefined(transaction.description) ? "N/A" :  transaction.description,					
				"paymentDate": kony.sdk.isNullOrUndefined(transaction.paymentDate) ? "N/A" :  CommonUtilities.getFrontendDateStringInUTC(transaction.paymentDate,"mm/dd/yyyy"),					
				"scheduledDate":kony.sdk.isNullOrUndefined(transaction.scheduledDate) ? "N/A" :  CommonUtilities.getFrontendDateStringInUTC(transaction.scheduledDate,"mm/dd/yyyy"),					
				"status": kony.sdk.isNullOrUndefined(transaction.status) ? "N/A" :  transaction.status,					
				"fromAccount": kony.sdk.isNullOrUndefined(transaction.fromAccountMasked) ? "N/A" : transaction.fromAccountMasked,
				"totalAmount": kony.sdk.isNullOrUndefined(transaction.totalAmount) ? "N/A" :  CommonUtilities.formatCurrencyWithCommas(transaction.totalAmount, false, transaction.currency),					
				"initiatedby": kony.sdk.isNullOrUndefined(transaction.initiatedBy) ? "N/A" :  transaction.initiatedBy,					
				"totalTransactions": kony.sdk.isNullOrUndefined(transaction.totalTransactions) ? "N/A" :  transaction.totalTransactions,					
				"companyId":  kony.sdk.isNullOrUndefined(transaction.companyId) ? "N/A" :  transaction.companyId,					
				"amIAprover":kony.sdk.isNullOrUndefined(transaction.amIAprover) ? "N/A" :  transaction.amIAprover,					
				"amICreator":kony.sdk.isNullOrUndefined(transaction.amICreator) ? "N/A" :  transaction.amICreator,					
				"receivedApprovals":kony.sdk.isNullOrUndefined(transaction.receivedApprovals) ? "N/A" :  transaction.receivedApprovals,					
				"requiredApprovals":kony.sdk.isNullOrUndefined(transaction.requiredApprovals) ? "N/A" :  transaction.requiredApprovals,					
				"approvers":kony.sdk.isNullOrUndefined(transaction.approvers) ? "N/A" :  transaction.approvers,		
				"Approver" : transaction.Approver,
				"currency":transaction.currency				
			});
        });
      	return(res);		
	}; 

    return ApprovalsReqModule_PresentationController;
});