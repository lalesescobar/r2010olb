define([],function () {
	var BusinessController = kony.mvc.Business.Controller;
	var CommandExecutionEngine = kony.mvc.Business.CommandExecutionEngine;
	var CommandHandler = kony.mvc.Business.CommandHandler;
	var Command = kony.mvc.Business.Command;
  	var BillPay_BusinessController = function(){
      	BusinessController.call(this);
    }
  	inheritsFrom(BillPay_BusinessController,BusinessController);
	createBillCommand = function (params, completionCallback) {
    	return new Command("com.kony.billpay.createBill", params, completionCallback);
    };
	getBillPayeesCommand = function (params, completionCallback) {
    	return new Command("com.kony.billpay.getBillPayees", params, completionCallback);
    };
	createPaymentCommand = function (params, completionCallback) {
    	return new Command("com.kony.billpay.createPayment", params, completionCallback);
    };
	deleteBillPayeeCommand = function (params, completionCallback) {
    	return new Command("com.kony.billpay.deleteBillPayee", params, completionCallback);
    };
	updateBillPayeeCommand = function (params, completionCallback) {
    	return new Command("com.kony.billpay.updateBillPayee", params, completionCallback);
    };
	createPayeeCommand = function (params, completionCallback) {
    	return new Command("com.kony.billpay.createPayee", params, completionCallback);
    };
	getBillsForUserCommand = function (params, completionCallback) {
    	return new Command("com.kony.billpay.getBillsForUser", params, completionCallback);
    };
	BillPay_BusinessController.prototype.initializeBusinessController = function(){
    };
    BillPay_BusinessController.prototype.execute = function(command)
    {
      BusinessController.prototype.execute.call(this,command);
    };
    return BillPay_BusinessController;
});
