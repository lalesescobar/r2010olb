define(["CommonUtilities", "ViewConstants"], function(CommonUtilities, ViewConstants) {
  
    /**
     * Bulk Payment Presenation to handle all BulkPayment related functionalities. intialize members.
     * @class
     * @alias module:BulkPayment_PresentationController
     */
    function BulkPaymentPresentationController() {
      
        kony.mvc.Presentation.BasePresenter.call(this);
        this.initializePresentationController();
    }
  
    inheritsFrom(BulkPaymentPresentationController, kony.mvc.Presentation.BasePresenter);
    /**
     * Method to intialize Enroll presentation scope data.
     */
    BulkPaymentPresentationController.prototype.initializePresentationController = function () {
        this.bulkPaymentFormName = "frmBulkPaymentsDashboard";
    },
  
	/**
  	 * no service call navigation to frmBulkPaymentsDashboard
  	*/
	BulkPaymentPresentationController.prototype.noServiceNavigate = function(formName,id,resData) {
		// Null check for empty parameter
		  if (!kony.sdk.isNullOrUndefined(formName)) {
             if (!kony.sdk.isNullOrUndefined(id)){
					applicationManager.getNavigationManager().navigateTo(formName);
					applicationManager.getNavigationManager().updateForm({
						"key":id,
						"responseData" : resData
						}, formName);
              }
			}
	};
  
	BulkPaymentPresentationController.prototype.getBulkPayCampaigns = function(){
		var scope = this;
		var asyncManager = applicationManager.getAsyncManager();
		var accountsManager = applicationManager.getAccountManager();
		var breakpoint = kony.application.getCurrentBreakpoint();
		asyncManager.callAsync(
			[
			  asyncManager.asyncItem(accountsManager, 'getCampaigns', [{
			  "screenName" : "ACCOUNT_DASHBOARD",
			  "scale" : (breakpoint >= 1366) ? "1366" : breakpoint
			  }])
		],
		function (asyncResponses) {
			scope.getCampaigns(asyncResponses.responses[0].data);
		})
	};
	
   /**
    *Method is used for fetching of campaigns
    * @param {Object}- list of campaigns
    */
	BulkPaymentPresentationController.prototype.getCampaigns = function(response) {
		if(response.campaignSpecifications)
		  this.getCampaignsSuccess(response);
		else
		  this.getCampaignsFailure(response);
	};
	
    /**
     * Method that gets called when fetching unread messages is successful
     * @param {Object} messagesObj List of messages Object
     */
	BulkPaymentPresentationController.prototype.getCampaignsSuccess = function(res) {
		applicationManager.getNavigationManager().updateForm({
		   "campaignRes" : res["campaignSpecifications"]
		});
	};
	
    /**
     * Method that gets called when there is an error in fetching unread messages for account dashboard
     * @param {Object} error Error Object
     */
	BulkPaymentPresentationController.prototype.getCampaignsFailure = function(error) {
	   applicationManager.getNavigationManager().updateForm({
			"campaignError" : error
		});
	};
  
  BulkPaymentPresentationController.prototype.downloadSampleFile = function () {
    applicationManager.getBulkManager().fetchSampleFileforDownload({},this.dowloadSampleFileSuccess.bind(this),this.downloadSampleFileFailure.bind(this));      
  };
  
  BulkPaymentPresentationController.prototype.dowloadSampleFileSuccess = function(response) { 
    var data = response ["sampleFiles"];
    for (var i = 0; i < data.length; i++) {
      var csvContent = atob(data[i].content)
      var blob = new Blob([csvContent], {type: "data:application/octet-stream;base64"});
      var url  = window.URL.createObjectURL(blob);

      var element = document.createElement('a');
      element.setAttribute('href', url);
      element.setAttribute('download',  data[i].fileName+"."+data[i].description.toLowerCase());
      element.setAttribute('target', '_blank');
      element.style.display = 'none';
      document.body.appendChild(element);
      element.click();
      document.body.removeChild(element);
    }
  };

  BulkPaymentPresentationController.prototype.downloadSampleFileFailure = function(responseError) {
       kony.ui.Alert(kony.i18n.getLocalizedString("i18n.navigation.norecordsfound"));
  };
  
  BulkPaymentPresentationController.prototype.uploadBulkPaymentFile = function (params) { 
    applicationManager.getNavigationManager().updateForm({
      "progressBar": true
    }, "frmBulkPaymentsUploadFile");
    applicationManager.getBulkManager().uploadBulkPaymentFile(params,this.uploadBulkPaymentFileSuccess.bind(this),this.uploadBulkPaymentFileFailure.bind(this));      
  };
  
  	BulkPaymentPresentationController.prototype.uploadBulkPaymentFileSuccess = function(response) {
    	if(!kony.sdk.isNullOrUndefined(response.dbpErrMsg) &&""!==response.dbpErrMsg){
      	applicationManager.getNavigationManager().updateForm({
          "serverError": true,
          "errorMessage": response.dbpErrMsg,
     	 }, "frmBulkPaymentsUploadFile");
    	} else{
      	applicationManager.getNavigationManager().updateForm({
           "uploadFileSuccess": response,
      	}, "frmBulkPaymentsUploadFile");
    	}
  	};

    BulkPaymentPresentationController.prototype.uploadBulkPaymentFileFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
             "errorMessage": responseError.errorMessage,
		}, "frmBulkPaymentsUploadFile");
    };

	BulkPaymentPresentationController.prototype.fetchOnGoingPayments = function (params) { 
     applicationManager.getNavigationManager().updateForm({
       "progressBar": true
     }, "frmBulkPaymentsDashboard");
		applicationManager.getBulkManager().fetchOnGoingPayments(params, this.fetchOnGoingPaymentsSuccess.bind(this), this.fetchOnGoingPaymentsFailure.bind(this));      
    };
  
   BulkPaymentPresentationController.prototype.fetchOnGoingPaymentsSuccess = function(response) { 
       var formattedResponse = this.formatOngoingPaymentsData(response.onGoingPayments);
		applicationManager.getNavigationManager().updateForm({
		  "fetchOnGoingPaymentsSuccess" : formattedResponse,
        }, "frmBulkPaymentsDashboard");
    };    

    BulkPaymentPresentationController.prototype.fetchOnGoingPaymentsFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
             "errorMessage": responseError,
		}, "frmBulkPaymentsDashboard");
    };

	BulkPaymentPresentationController.prototype.formatOngoingPaymentsData = function(response) {
		var res = [];
		var topSeparatordisable;
        if(response.length > 10)
        {
          topSeparatordisable=response.length-2;
        }
        else
        {
          topSeparatordisable=response.length-1;
        }
		response.forEach(function(transaction){
             if ((transaction.status === BBConstants.TRANSACTION_STATUS.PENDING) || (transaction.status === BBConstants.TRANSACTION_STATUS.CANCELLED) || (transaction.status === BBConstants.TRANSACTION_STATUS.DISCARDED)) {
                  transaction.actionsValue = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.viewPayment");
              } else {
                  transaction.actionsValue = kony.i18n.getLocalizedString("i18n.kony.BulkPayments.ReviewPayment");
              }
              if (res.length === topSeparatordisable)
              {
                transaction.topseparatorval = false;
              }
              else{
                transaction.topseparatorval = true;
              }
			  
			if((transaction.status === BBConstants.TRANSACTION_STATUS.PENDING)&& (!kony.sdk.isNullOrUndefined(transaction.receivedApprovals))&& (!kony.sdk.			isNullOrUndefined(transaction.requiredApprovals))) {
				transaction.Approver = transaction.receivedApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.of") + " " + transaction.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approved");
			}
			else if(transaction.status === BBConstants.TRANSACTION_STATUS.REJECTED) {
				transaction.Approver = 1 + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Rejection");
			}				
			else if (!kony.sdk.isNullOrUndefined(transaction.requiredApprovals)) {
				transaction.Approver = transaction.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approvals");
			}
			else{
				transaction.Approver = kony.i18n.getLocalizedString("i18n.common.NA");
			}
            transaction.fromAccountMasked =  CommonUtilities.getMaskedAccName(transaction.fromAccount);

			res.push({				   
			   "lblDate": {
				  "text": kony.sdk.isNullOrUndefined(transaction.paymentDate) ? "N/A" : CommonUtilities.getFrontendDateStringInUTC(transaction.paymentDate, applicationManager.getConfigurationManager().getConfigurationValue("frontendDateFormat")),				  
				},	
				"lblOpDescription": {
				  "text": kony.sdk.isNullOrUndefined(transaction.description) ? "N/A" : CommonUtilities.truncateStringWithGivenLength(transaction.description, 25),
				  "toolTip" : kony.sdk.isNullOrUndefined(transaction.description) ? "N/A" : transaction.description
				},
				"lblOpStatus": {					
				  "text": kony.sdk.isNullOrUndefined(transaction.status) ? "N/A" : CommonUtilities.getModifiedstatusForPending(transaction.status),				  
				},
				"lblOPFromAccountValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.fromAccountMasked) ? "N/A" : transaction.fromAccountMasked,					  
				},
				"lblOPTotalTransactionsValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.totalTransactions) ? "N/A" : transaction.totalTransactions,				  
				},
				"lblOPTotalAmountValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.totalAmount) ? "N/A" : CommonUtilities.formatCurrencyWithCommas(transaction.totalAmount, false, transaction.currency),				  
				},
				"lblOPnitiatedByValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.initiatedBy) ? "N/A" : transaction.initiatedBy,				  
				},	
				"lblOPPaymentIDValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.companyId) ? "N/A" : transaction.companyId,				  
				},
             	"lblValueDateValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.scheduledDate) ? "N/A" : CommonUtilities.getFrontendDateStringInUTC(transaction.scheduledDate, applicationManager.getConfigurationManager().getConfigurationValue("frontendDateFormat")),				  
				},
				"lblOpActions": {
				  "text": kony.sdk.isNullOrUndefined(transaction.actionsValue) ? "N/A" : transaction.actionsValue,				  
				},	
				 "flxTopSeperator": {
                   "isVisible": transaction.topseparatorval,
                },
              	"description": kony.sdk.isNullOrUndefined(transaction.description) ? "N/A" : transaction.description,
                "initiatedBy": kony.sdk.isNullOrUndefined(transaction.initiatedBy) ? "N/A" : transaction.initiatedBy,
                "scheduledDate": kony.sdk.isNullOrUndefined(transaction.scheduledDate) ? "N/A" : CommonUtilities.getFrontendDateStringInUTC(transaction.scheduledDate, applicationManager.getConfigurationManager().getConfigurationValue("frontendDateFormat")),
                "paymentDate": kony.sdk.isNullOrUndefined(transaction.paymentDate) ? "N/A" : CommonUtilities.getFrontendDateStringInUTC(transaction.paymentDate, applicationManager.getConfigurationManager().getConfigurationValue("frontendDateFormat")),
                "totalAmount": kony.sdk.isNullOrUndefined(transaction.totalAmount) ? "N/A" : CommonUtilities.formatCurrencyWithCommas(transaction.totalAmount,false,transaction.currency),
                "fromAccountMasked": kony.sdk.isNullOrUndefined(transaction.fromAccountMasked) ? "N/A" : transaction.fromAccountMasked,
               	"fromAccount": kony.sdk.isNullOrUndefined(transaction.fromAccount) ? "N/A" : transaction.fromAccount,
                "totalTransactions": kony.sdk.isNullOrUndefined(transaction.totalTransactions) ? "N/A" : transaction.totalTransactions,
                "recordId": kony.sdk.isNullOrUndefined(transaction.recordId) ? "N/A" : transaction.recordId,
               	"amIAprover": kony.sdk.isNullOrUndefined(transaction.amIAprover) ? "N/A" : transaction.amIAprover,
                "amICreator": kony.sdk.isNullOrUndefined(transaction.amICreator) ? "N/A" : transaction.amICreator,
				"requestId" : kony.sdk.isNullOrUndefined(transaction.requestId) ? "N/A" :  transaction.requestId,
				"receivedApprovals":kony.sdk.isNullOrUndefined(transaction.receivedApprovals) ? "N/A" :  transaction.receivedApprovals,					
				"requiredApprovals":kony.sdk.isNullOrUndefined(transaction.requiredApprovals) ? "N/A" :  transaction.requiredApprovals,
				"Approver" : transaction.Approver,
                "currency":transaction.currency
			});
        });
      	return(res);		
	}; 
	
	BulkPaymentPresentationController.prototype.fetchUploadedFiles = function (params) { 
     applicationManager.getNavigationManager().updateForm({
       "progressBar": true
     }, "frmBulkPaymentsDashboard");
		applicationManager.getBulkManager().fetchUploadedFiles(params, this.fetchUploadedFilesSuccess.bind(this), this.fetchUploadedFilesFailure.bind(this));      
    };
  
   BulkPaymentPresentationController.prototype.fetchUploadedFilesSuccess = function(response) { 
        var formattedResponse = this.formatUploadedFilesData(response.uploadedFiles);
        applicationManager.getNavigationManager().updateForm({
		  "fetchUploadedFilesSuccess" : formattedResponse,
        }, "frmBulkPaymentsDashboard");
    };

    BulkPaymentPresentationController.prototype.fetchUploadedFilesFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
             "errorMessage": responseError.errorMessage,
		}, "frmBulkPaymentsDashboard");
    };

	BulkPaymentPresentationController.prototype.formatUploadedFilesData = function(response) {
		var res = [];
		var topSeparatordisable;
		if(response.length > 10)
			{
				topSeparatordisable=response.length-2;
			}
			else
			{
				topSeparatordisable=response.length-1;
			}
			
		response.forEach(function(transaction){
           if (res.length === topSeparatordisable)
			{
				transaction.topseparatorval = false;
			}
			else{
				transaction.topseparatorval = true;
			}
			res.push({				   
			   "lblUfDate": {
				  "text": kony.sdk.isNullOrUndefined(transaction.uploadedDate) ? "N/A" : CommonUtilities.getFrontendDateStringInUTC(transaction.uploadedDate, applicationManager.getConfigurationManager().getConfigurationValue("frontendDateFormat")),				  
				},	
				"lblFileDescription": {
				  "text": kony.sdk.isNullOrUndefined(transaction.description) ? "N/A" : transaction.description,	
				},
				"lblUfStatus": {					
				  "text": kony.sdk.isNullOrUndefined(transaction.status) ? "N/A" : CommonUtilities.getModifiedstatusForPending(transaction.status),				  
				},
				"lblUploadID": {
				  "text": kony.sdk.isNullOrUndefined(transaction.fileId) ? "N/A" : transaction.fileId,					  
				},
				"lblFileNameValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.fileName) ? "N/A" : transaction.fileName,				  
				},
				"lblUploadedByValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.uploadedBy) ? "N/A" : transaction.uploadedBy,				  
				},
				"lblSystemFileNameValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.sysGeneratedFileName) ? "N/A" : transaction.sysGeneratedFileName,				  
				},	 
				"flxTopSeperator": {
                   "isVisible": transaction.topseparatorval,
                },
				
			});
        });
      	return(res);		
	};
	
	BulkPaymentPresentationController.prototype.fetchHistory = function (params) { 
     applicationManager.getNavigationManager().updateForm({
       "progressBar": true
     }, "frmBulkPaymentsDashboard");
		applicationManager.getBulkManager().fetchHistory(params,this.fetchHistorySuccess.bind(this),this.fetchHistoryFailure.bind(this));      
    };
  
   BulkPaymentPresentationController.prototype.fetchHistorySuccess = function(response) { 
        var formattedResponse = this.formatHistoryData(response.history);
		applicationManager.getNavigationManager().updateForm({
		  "fetchHistorySuccess" : formattedResponse,
        }, "frmBulkPaymentsDashboard");
    };

    BulkPaymentPresentationController.prototype.fetchHistoryFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
             "errorMessage": responseError.errorMessage,
		}, "frmBulkPaymentsDashboard");
    };

	BulkPaymentPresentationController.prototype.formatHistoryData = function(response) {
		var res = [];
		var topSeparatordisable;
        if(response.length > 10)
        {
          topSeparatordisable=response.length-2;
        }
        else
        {
          topSeparatordisable=response.length-1;
        }
        response.forEach(function(transaction){	
           if (res.length === topSeparatordisable)
			{
				transaction.topseparatorval = false;
			}
			else{
				transaction.topseparatorval = true;
			}
			
			if((transaction.status === BBConstants.TRANSACTION_STATUS.PENDING)&& (!kony.sdk.isNullOrUndefined(transaction.receivedApprovals))&& (!kony.sdk.			isNullOrUndefined(transaction.requiredApprovals))) {
				transaction.Approver = transaction.receivedApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.of") + " " + transaction.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approved");
			}
			else if(transaction.status === BBConstants.TRANSACTION_STATUS.REJECTED) {
				transaction.Approver = 1 + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Rejection");
			}				
			else if (!kony.sdk.isNullOrUndefined(transaction.requiredApprovals)) {
				transaction.Approver = transaction.requiredApprovals + " " + kony.i18n.getLocalizedString("i18n.konybb.Common.Approvals");
			}
			else{
				transaction.Approver = kony.i18n.getLocalizedString("i18n.common.NA");
			}
			transaction.fromAccountMasked =  CommonUtilities.getMaskedAccName(transaction.fromAccount);
			res.push({				   
			   "lblPhDate": {
				  "text": kony.sdk.isNullOrUndefined(transaction.paymentDate) ? "N/A" : CommonUtilities.getFrontendDateStringInUTC(transaction.paymentDate, applicationManager.getConfigurationManager().getConfigurationValue("frontendDateFormat")),				  
				},	
				"lblPhDescription": {
				  "text": kony.sdk.isNullOrUndefined(transaction.description) ? "N/A" : transaction.description,	
				},
				"lblPhStatus": {					
				  "text": kony.sdk.isNullOrUndefined(transaction.totalAmount) ? "N/A" : CommonUtilities.formatCurrencyWithCommas(transaction.totalAmount, false, transaction.currency),				  
				},
				"lblFromAccountValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.fromAccountMasked) ? "N/A" : transaction.fromAccountMasked,					  
				},
				"lblTotalTransactionsValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.totalTransactions) ? "N/A" : transaction.totalTransactions,				  
				},
				"lblTotalAmountValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.Approver) ? "N/A" : transaction.Approver,				  
				},
				"lblInitiatedByValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.initiatedBy) ? "N/A" : transaction.initiatedBy,				  
				},	
				"lblPaymentStatusValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.status) ? "N/A" : CommonUtilities.getModifiedstatusForPending(transaction.status),				  
				},
				"lblPaymentIDValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.fileId) ? "N/A" : transaction.fileId,				  
				},
				"flxTopSeperator": {
                   "isVisible": transaction.topseparatorval,
                },
              	"description": kony.sdk.isNullOrUndefined(transaction.description) ? "N/A" : transaction.description,
                "initiatedBy": kony.sdk.isNullOrUndefined(transaction.initiatedBy) ? "N/A" : transaction.initiatedBy,
                "fileId": kony.sdk.isNullOrUndefined(transaction.fileId) ? "N/A" : transaction.fileId,
                "totalAmount": kony.sdk.isNullOrUndefined(transaction.totalAmount) ? "N/A" : CommonUtilities.formatCurrencyWithCommas(transaction.totalAmount,false,transaction.currency),
                "fromAccountMasked": kony.sdk.isNullOrUndefined(transaction.fromAccountMasked) ? "N/A" : transaction.fromAccountMasked,
               	"fromAccount": kony.sdk.isNullOrUndefined(transaction.fromAccount) ? "N/A" : transaction.fromAccount,
                "totalTransactions": kony.sdk.isNullOrUndefined(transaction.totalTransactions) ? "N/A" : transaction.totalTransactions,
                "recordId": kony.sdk.isNullOrUndefined(transaction.recordId) ? "N/A" : transaction.recordId,
                "amIAprover": kony.sdk.isNullOrUndefined(transaction.amIAprover) ? "N/A" : transaction.amIAprover,
                "amICreator": kony.sdk.isNullOrUndefined(transaction.amICreator) ? "N/A" : transaction.amICreator,
				"requestId" : kony.sdk.isNullOrUndefined(transaction.requestId) ? "N/A" :  transaction.requestId,
				"receivedApprovals":kony.sdk.isNullOrUndefined(transaction.receivedApprovals) ? "N/A" :  transaction.receivedApprovals,					
				"requiredApprovals":kony.sdk.isNullOrUndefined(transaction.requiredApprovals) ? "N/A" :  transaction.requiredApprovals,
				"scheduledDate": kony.sdk.isNullOrUndefined(transaction.scheduledDate) ? "N/A" : CommonUtilities.getFrontendDateStringInUTC(transaction.scheduledDate, applicationManager.getConfigurationManager().getConfigurationValue("frontendDateFormat")),
                "paymentDate": kony.sdk.isNullOrUndefined(transaction.paymentDate) ? "N/A" : CommonUtilities.getFrontendDateStringInUTC(transaction.paymentDate, applicationManager.getConfigurationManager().getConfigurationValue("frontendDateFormat")),
				"Approver" : transaction.Approver					
			});
        });
      	return(res);		
	}; 
	
	BulkPaymentPresentationController.prototype.fetchPaymentOrders = function (params) { 
     applicationManager.getNavigationManager().updateForm({
       "progressBar": true
     }, "frmBulkPaymentsReview");
		applicationManager.getBulkManager().fetchPaymentOrders(params,this.fetchPaymentOrdersSuccess.bind(this),this.fetchPaymentOrdersFailure.bind(this)); 
	};
  
	BulkPaymentPresentationController.prototype.fetchPaymentOrdersSuccess = function(response) { 
		var formattedResponse = this.formatPaymentOrdersData(response.paymentOrders);
		applicationManager.getNavigationManager().updateForm({
		  "fetchPaymentOrdersSuccess" : formattedResponse,
        }, "frmBulkPaymentsReview");
    };

    BulkPaymentPresentationController.prototype.fetchPaymentOrdersFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
             "errorMessage": responseError.errorMessage,
		}, "frmBulkPaymentsReview");
    };
	
	/**
  	*   formatPaymentOrdersData : formatting the data as required for the form controller.
  	*/ 
  	BulkPaymentPresentationController.prototype.formatPaymentOrdersData = function(response) {
		var res = [];
		response.forEach(function(transaction){						
			res.push({				   
			   "lblViewBankName": {
				  "text": kony.sdk.isNullOrUndefined(transaction.amount) ? "-" : CommonUtilities.formatCurrencyWithCommas(transaction.amount, false, transaction.currency),				  
				},	
				"lblFeesValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.feesPaidBy) ? "-" : transaction.feesPaidBy,	
				},
				"lblViewAmount": {					
				  "text": kony.sdk.isNullOrUndefined(transaction.status) ? "-" : transaction.status,				  
				},
				"lblAccountNoValue": {
                  "text": kony.sdk.isNullOrUndefined(transaction.accountNumber) ? "-" : 
                  (transaction.accountNumber.length > 7 ? CommonUtilities.truncateStringWithGivenLength(transaction.accountNumber + "....", 6) + CommonUtilities.getLastFourDigit(transaction.accountNumber) : transaction.accountNumber),
                },
				"lblAccTypeValue": {
				  "text": (kony.sdk.isNullOrUndefined(transaction.swift)||CommonUtilities.isEmptyString(transaction.swift)) ? "-" : transaction.swift,
				},
				"lblSwiftCodeValue": {
				  "text": kony.sdk.isNullOrUndefined(transaction.bankName) ? "-" : transaction.bankName,				  
				},
				"lblViewRecipientName": {
				  "text": kony.sdk.isNullOrUndefined(transaction.recipientName) ? "-" : transaction.recipientName,				  
				},	
				"lblPayRefValue": {
				  "text": (kony.sdk.isNullOrUndefined(transaction.paymentReference)||CommonUtilities.isEmptyString(transaction.paymentReference)) ? "-" : transaction.paymentReference,				  
				},
                "lblPaymentMethodValue": {
                  "isVisible": true,
                  "text": kony.sdk.isNullOrUndefined(transaction.paymentMethod) ? "-" : transaction.paymentMethod,
                },             
				"paymentOrderId": kony.sdk.isNullOrUndefined(transaction.paymentOrderId) ? "N/A" :  transaction.paymentOrderId,
				"recipientName": kony.sdk.isNullOrUndefined(transaction.recipientName) ? "N/A" :  transaction.recipientName,
				"accountNumber":kony.sdk.isNullOrUndefined(transaction.accountNumber) ? "-" :  transaction.accountNumber,
				"bankName":kony.sdk.isNullOrUndefined(transaction.bankName) ? "N/A" :  transaction.bankName,
				"swift":kony.sdk.isNullOrUndefined(transaction.switft) ? "N/A" :  transaction.swift,
				"currency":kony.sdk.isNullOrUndefined(transaction.currency) ? "N/A" :  transaction.currency,
				"amount":kony.sdk.isNullOrUndefined(transaction.amount) ? "N/A" :  transaction.amount,
				"feesPaidBy":kony.sdk.isNullOrUndefined(transaction.feesPaidBy) ? "N/A" :  transaction.feesPaidBy,
				"paymentReference": transaction.paymentReference,
			});
        });
      	return(res);		
	}; 

	BulkPaymentPresentationController.prototype.editPaymentOrder = function (params) { 
     applicationManager.getNavigationManager().updateForm({
       "progressBar": true
     }, "frmBulkPaymentsReview");
		applicationManager.getBulkManager().editPaymentOrder(params,this.editPaymentOrderSuccess.bind(this),this.editPaymentOrderFailure.bind(this)); 
	};
  
	BulkPaymentPresentationController.prototype.editPaymentOrderSuccess = function(response) { 
 
		applicationManager.getNavigationManager().updateForm({
		  "editPaymentOrderSuccess" : response,
        }, "frmBulkPaymentsReview");
    };

    BulkPaymentPresentationController.prototype.editPaymentOrderFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
             "errorMessage": responseError.errorMessage,
		}, "frmBulkPaymentsReview");
    };

	BulkPaymentPresentationController.prototype.deletePaymentOrder = function (params) { 
     applicationManager.getNavigationManager().updateForm({
       "progressBar": true
     }, "frmBulkPaymentsReview");
		applicationManager.getBulkManager().deletePaymentOrder(params,this.deletePaymentOrderSuccess.bind(this),this.deletePaymentOrderFailure.bind(this)); 
	};
  
	BulkPaymentPresentationController.prototype.deletePaymentOrderSuccess = function(response) { 
 
		applicationManager.getNavigationManager().updateForm({
		  "deletePaymentOrderSuccess" : response,
        }, "frmBulkPaymentsReview");
    };

    BulkPaymentPresentationController.prototype.deletePaymentOrderFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
             "errorMessage": responseError.errorMessage,
		}, "frmBulkPaymentsReview");
    };

	BulkPaymentPresentationController.prototype.addPaymentOrder = function (params) { 
     applicationManager.getNavigationManager().updateForm({
       "progressBar": true
     }, "frmBulkPaymentsReview");
		applicationManager.getBulkManager().addPaymentOrder(params,this.addPaymentOrderSuccess.bind(this),this.addPaymentOrderFailure.bind(this)); 
	};
  
	BulkPaymentPresentationController.prototype.addPaymentOrderSuccess = function(response) { 
 
		applicationManager.getNavigationManager().updateForm({
		  "addPaymentOrderSuccess" : response,
        }, "frmBulkPaymentsReview");
    };

    BulkPaymentPresentationController.prototype.addPaymentOrderFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
             "errorMessage": responseError.errorMessage,
		}, "frmBulkPaymentsReview");
    };

	BulkPaymentPresentationController.prototype.submitPaymentOrder = function (params) { 
     applicationManager.getNavigationManager().updateForm({
       "progressBar": true
     }, "frmBulkPaymentsReview");
		applicationManager.getBulkManager().submitPaymentOrder(params,this.submitPaymentOrderSuccess.bind(this),this.submitPaymentOrderFailure.bind(this)); 
	};
  
	BulkPaymentPresentationController.prototype.submitPaymentOrderSuccess = function(response) { 
 
		applicationManager.getNavigationManager().updateForm({
		  "submitPaymentOrderSuccess" : response,
        }, "frmBulkPaymentsReview");
    };

    BulkPaymentPresentationController.prototype.submitPaymentOrderFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
             "errorMessage": responseError.errorMessage,
		}, "frmBulkPaymentsReview");
    };
  
	BulkPaymentPresentationController.prototype.cancelBulkPaymentRecord = function (params) { 
     applicationManager.getNavigationManager().updateForm({
       "progressBar": true
     }, "frmBulkPaymentsReview");
		applicationManager.getBulkManager().cancelBulkPaymentRecord(params,this.cancelBulkPaymentRecordSuccess.bind(this),this.cancelBulkPaymentRecordFailure.bind(this)); 
	};
  
	BulkPaymentPresentationController.prototype.cancelBulkPaymentRecordSuccess = function(response) { 
 
		applicationManager.getNavigationManager().updateForm({
		  "cancelBulkPaymentRecordSuccess" : response,
        }, "frmBulkPaymentsReview");
    };

    BulkPaymentPresentationController.prototype.cancelBulkPaymentRecordFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
             "errorMessage": responseError.errorMessage,
			  "closePopup": true,
		}, "frmBulkPaymentsReview");
    };
  
	BulkPaymentPresentationController.prototype.updateBulkPaymentRecord = function (params) { 
     applicationManager.getNavigationManager().updateForm({
       "progressBar": true
     }, "frmBulkPaymentsReview");
		applicationManager.getBulkManager().updateBulkPaymentRecord(params,this.updateBulkPaymentRecordSuccess.bind(this),this.updateBulkPaymentRecordFailure.bind(this)); 
	};
  
	BulkPaymentPresentationController.prototype.updateBulkPaymentRecordSuccess = function(response) { 
 
		applicationManager.getNavigationManager().updateForm({
		  "updateBulkPaymentRecordSuccess" : response,
        }, "frmBulkPaymentsReview");
    };

    BulkPaymentPresentationController.prototype.updateBulkPaymentRecordFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
             "errorMessage": responseError.errorMessage,
		}, "frmBulkPaymentsReview");
    };
  
  /** Get External accounts from backend
     * @param {object} value - Sorting and pagination parameters
     */
  BulkPaymentPresentationController.prototype.getExistingBeneficiaries = function (params) {

    applicationManager.getNavigationManager().updateForm({
      "progressBar": true
    }, "frmBulkPaymentsReview");  
    var recipientManager = applicationManager.getRecipientsManager();
    recipientManager.fetchAllExternalAccountsWithPagination(params, this.getExistingBeneficiariesSuccess.bind(this), this.getExistingBeneficiariesFailure.bind(this));
  };
  
  BulkPaymentPresentationController.prototype.getExistingBeneficiariesSuccess = function(response) { 
    
    response=response.ExternalAccounts;
    
    response.forEach(function(responseObj) {

      responseObj.lblAccountNoValue = {
        "isVisible": true,
        "text": responseObj.accountNumber
      };

      if(responseObj.isInternationalAccount==="true")
      {
        responseObj.lblAccountType = {
          "isVisible": true,
          "text": "International"
        };
        responseObj.lblAccTypeValue = {
          "isVisible": true,
          "text": responseObj.swiftCode
        };
        responseObj.lblSwiftName = {
          "isVisible": true,
          "text": kony.i18n.getLocalizedString("kony.i18n.common.swiftcode")
        };
      }
      else if (responseObj.isSameBankAccount==="true")
      {
        responseObj.lblAccountType = {
          "isVisible": true,
          "text": "Internal"
        };
        responseObj.lblAccTypeValue = {
          "isVisible": false,
          "text": responseObj.swiftCode
        };
        responseObj.lblSwiftName = {
          "isVisible": false,
          "text": kony.i18n.getLocalizedString("kony.i18n.common.swiftcode")
        };
      }
      else if ((responseObj.isInternationalAccount==="false") &&(responseObj.isSameBankAccount==="false") )
      {
        responseObj.lblAccountType = {
          "isVisible": true,
          "text": "Domestic"
        };
        responseObj.lblAccTypeValue = {
          "isVisible": true,
          "text": responseObj.swiftCode
        };
        responseObj.lblSwiftName = {
          "isVisible": true,
          "text": kony.i18n.getLocalizedString("kony.i18n.common.swiftcode")
        };
      }

      responseObj.lblNickName = {
        "isVisible": true,
        "text": responseObj.nickName
      };						

    });

		applicationManager.getNavigationManager().updateForm({
		  "getExistingBeneficiariesSuccess" : response,
        }, "frmBulkPaymentsReview");
    };

    BulkPaymentPresentationController.prototype.getExistingBeneficiariesFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({        
          "getExistingBeneficiariesFailure" : responseError,
		}, "frmBulkPaymentsReview");
    };
  
   BulkPaymentPresentationController.prototype.showProgressBar = function () {
    applicationManager.getNavigationManager().updateForm({ isLoading: true });
  }
  
   BulkPaymentPresentationController.prototype.approvePayment = function (params, btnText) { 
     applicationManager.getNavigationManager().updateForm({
       "progressBar": true
     }, "frmBulkPaymentsReview");
     applicationManager.getBulkManager().approvePayment(params,this.approvePaymentSuccess.bind(this, btnText),this.approvePaymentFailure.bind(this));      
   };

  BulkPaymentPresentationController.prototype.approvePaymentSuccess = function(btnText, response) {
    response.btnText = btnText;
    applicationManager.getNavigationManager().updateForm({
      "onApproveSuccess" : response,
    }, "frmBulkPaymentsReview");
  };

  BulkPaymentPresentationController.prototype.approvePaymentFailure = function(responseError) {
    applicationManager.getNavigationManager().updateForm({
      "serverError": true,
      "errorMessage": responseError.errorMessage,
    }, "frmBulkPaymentsReview");
  };

  BulkPaymentPresentationController.prototype.rejectPayment = function (params, btnText) { 
    applicationManager.getNavigationManager().updateForm({
      "progressBar": true
    }, "frmBulkPaymentsReview");
    applicationManager.getBulkManager().rejectPayment(params,this.rejectPaymentSuccess.bind(this, btnText),this.rejectPaymentFailure.bind(this));      
  };

  BulkPaymentPresentationController.prototype.rejectPaymentSuccess = function(btnText, response) {
    response.btnText = btnText;
    applicationManager.getNavigationManager().updateForm({
      "onRejectSuccess" : response,
    }, "frmBulkPaymentsReview");
  };

  BulkPaymentPresentationController.prototype.rejectPaymentFailure = function(responseError) {
    applicationManager.getNavigationManager().updateForm({
      "serverError": true,
      "errorMessage": responseError.errorMessage,
    }, "frmBulkPaymentsReview");
  };
  
   BulkPaymentPresentationController.prototype.getRequestsHistory = function (params) { 
    applicationManager.getNavigationManager().updateForm({
      "progressBar": true
    }, "frmBulkPaymentsReview");
    applicationManager.getBulkManager().getRequestsHistory(params,this.getRequestsHistorySuccess.bind(this),this.getRequestsHistoryFailure.bind(this));      
  };

  BulkPaymentPresentationController.prototype.getRequestsHistorySuccess = function(response) {    
    applicationManager.getNavigationManager().updateForm({
      "onRequestsHistorySuccess" : response,
    }, "frmBulkPaymentsReview");
  };

  BulkPaymentPresentationController.prototype.getRequestsHistoryFailure = function(responseError) {
    applicationManager.getNavigationManager().updateForm({
      "serverError": true,
      "errorMessage": responseError.errorMessage,
    }, "frmBulkPaymentsReview");
  };
  
  BulkPaymentPresentationController.prototype.fetchBulkPaymentRecordDetailsById = function (params) { 
     applicationManager.getNavigationManager().updateForm({
       "progressBar": true
     }, "frmBulkPaymentsReview");
		applicationManager.getBulkManager().fetchBulkPaymentRecordDetailsById(params, this.fetchBulkPaymentRecordDetailsByIdSuccess.bind(this), this.fetchBulkPaymentRecordDetailsByIdFailure.bind(this));      
    };
  
   BulkPaymentPresentationController.prototype.fetchBulkPaymentRecordDetailsByIdSuccess = function(response) { 
        response.totalAmount= CommonUtilities.formatCurrencyWithCommas(response.totalAmount, false, response.currency);				  
		applicationManager.getNavigationManager().updateForm({
		  "fetchBulkPaymentRecordDetailsByIdSuccess" : response,
        }, "frmBulkPaymentsReview");
    };    

    BulkPaymentPresentationController.prototype.fetchBulkPaymentRecordDetailsByIdFailure = function(responseError) {
		applicationManager.getNavigationManager().updateForm({
            "serverError": true,
             "errorMessage": responseError,
		}, "frmBulkPaymentsReview");
    };
    
  return BulkPaymentPresentationController;
});