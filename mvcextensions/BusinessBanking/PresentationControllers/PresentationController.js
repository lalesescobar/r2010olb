define(['OLBConstants','CommonUtilities'], function (OLBConstants,CommonUtilities) {
  function BusinessBankingPresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
    this.initializePresentationController();
  }
  inheritsFrom(BusinessBankingPresentationController, kony.mvc.Presentation.BasePresenter);
  BusinessBankingPresentationController.prototype.initializePresentationController = function () {
    this.navManager = applicationManager.getNavigationManager();
    this.frmUserManagementForm = "frmUserManagement";
    this.frmBBUsersDashboardForm = "frmBBUsersDashboard";
    this.frmPermissionsTemplateForm = "frmPermissionsTemplate";
    this.businessUserManager = applicationManager.getBusinessUserManager();
	this.authManager = applicationManager.getAuthManager();
    this.formatUtil = applicationManager.getFormatUtilManager();
    this.defaultSortConfig = {
      'sortBy': 'createdts',
      'order': OLBConstants.DESCENDING_KEY,
      'offset': OLBConstants.DEFAULT_OFFSET,
      'limit': OLBConstants.PAGING_ROWS_LIMIT
    };
  };
  /**
   * Entry Method for business banking module
   * @param {object} context - context object for
   */
  BusinessBankingPresentationController.prototype.showUserManagent = function (context) {
    switch (context.show) {
      case "createNewUser":
        this.showCreateUser();
        break;
      case "showAllUsers": 
        {
        this.navigateToUsers(this.fetchSubUsersSuccess.bind(this));
        break;
      }
      case "createNewCustomRole":
        this.showCreatePermissionsTemplate("","",context.userObject);
        break;
      case "showUserRoles":
        this.navigateToUserRoles();
      default:
      //no context.
    }
  };
  
   /**
    * Method to navigate user roles dashboard.
   */
   BusinessBankingPresentationController.prototype.navigateToUserRoles = function() {
     this.navManager.navigateTo(this.frmBBUsersDashboardForm);
	     this.navManager.updateForm({
          progressBar: true
     });
     this.navManager.updateForm({
         "showRolesDashboard" : true,
     }, this.frmBBUsersDashboardForm);
  };
  

  BusinessBankingPresentationController.prototype.navigateToManagePermissions = function(customRoleID, parentRoleID, completionCallback = this.navigateToManagePermissionsSuccess.bind(this) ,failureCallback = this.navigateToManagePermissionsFailure.bind(this),formName = this.frmPermissionsTemplateForm) {
    this.navManager.updateForm({
      progressBar : true
    });
    var asyncManager = applicationManager.getAsyncManager();
    asyncManager.callAsync(
      [
        asyncManager.asyncItem(applicationManager.getBusinessUserManager(), "getDetailsOfCustomRole", [ { "customRoleId" : customRoleID } ] ),
        asyncManager.asyncItem(applicationManager.getBusinessUserManager(), "getUserRoleActions", [ { "groupId": parentRoleID } ] )
      ],  completionCallback.bind(this,formName,failureCallback) );
    
  };

  
   BusinessBankingPresentationController.prototype.fetchExistingCustomRoleDetailsSuccess = function(formName,failureCallback,asyncResponse) {
     if ( asyncResponse.isAllSuccess() ) {
       var customRoleObject = asyncResponse.responses[0].data; 
       this.businessUserManager.createCustomRoleObject();
       this.businessUserManager.setCustomRoleAttribute("customRoleName", customRoleObject.CustomRoleName);
       this.businessUserManager.setCustomRoleAttribute("Group_Name", customRoleObject.ParentRole);
       this.businessUserManager.setCustomRoleAttribute("Group_id", customRoleObject.ParentRoleId);
       this.businessUserManager.setCustomRoleAttribute("accounts", customRoleObject.accounts); 
       this.businessUserManager.setCustomRoleAttribute("Role_id", customRoleObject.id);
       this.businessUserManager.setCustomRoleAttribute("isExistingUserSelected", true);
       this.businessUserManager.setCustomRoleAttribute("isPermissionsFromSelectedUserEdited", false);
       this.businessUserManager.setCustomRoleAttribute("SelectedRoleId", customRoleObject.ParentRoleId);
       this.businessUserManager.setCustomRoleAttribute("defaultOrexistingRoleSelected", true);

       customRoleObject["defaultLimitsForSelectedRole"] = segregateFeatureData(asyncResponse.responses[1].data.FeatureActions) || [];
       
        if( kony.application.getCurrentForm().id !== formName ) {
        applicationManager.getNavigationManager().navigateTo(this.frmPermissionsTemplateForm);
      }
       applicationManager.getNavigationManager().updateForm({
         "selectedUser": customRoleObject,
         "progressBar": false
       }, formName);
     } else {
       var errorData={};
       for(i=0;i<2;i++){
         if (!asyncResponse.responses[i].isSuccess) {
           errorData = asyncResponse.responses[i].data;
         }
       }   
       failureCallback(errorData);
     }   
  };

  BusinessBankingPresentationController.prototype.fetchExistingCustomRoleDetailsForUserCreationSuccess = function(formName,failureCallback,asyncResponse) {
    if ( asyncResponse.isAllSuccess() ) {
      var userObject = asyncResponse.responses[0].data; 
      this.businessUserManager.createUserObject();
      this.businessUserManager.setUserAttribute("customRoleName", userObject.CustomRoleName);
      this.businessUserManager.setUserAttribute("Group_Name", userObject.ParentRole);
      this.businessUserManager.setUserAttribute("Group_id", userObject.ParentRoleId);
      this.businessUserManager.setUserAttribute("accounts", userObject.accounts); 
      this.businessUserManager.setUserAttribute("Role_id", userObject.id);
      this.businessUserManager.setUserAttribute("isExistingUserSelected", true);
      this.businessUserManager.setUserAttribute("isPermissionsFromSelectedUserEdited", false);
      this.businessUserManager.setUserAttribute("SelectedRoleId", userObject.ParentRoleId);
      this.businessUserManager.setUserAttribute("defaultOrexistingRoleSelected", true);

      userObject["defaultLimitsForSelectedRole"] = segregateFeatureData(asyncResponse.responses[1].data.FeatureActions) || [];

      if( kony.application.getCurrentForm().id !== formName ) {
        applicationManager.getNavigationManager().navigateTo(formName);
      }
      applicationManager.getNavigationManager().updateForm({
        "selectedUser": userObject,
        "progressBar": false
      }, formName);
    } else {
      var errorData={};
      for(i=0;i<2;i++){
        if(! asyncResponse.responses[i].isSuccess){
          errorData = asyncResponse.responses[i].data;
        }
      }
      failureCallback(errorData);
    }  
  };

   BusinessBankingPresentationController.prototype.fetchExistingCustomRoleDetailsFailureCallback = function(error){
    this.navManager.updateForm({
      "errorData":error,
      "fetchUserDetailsFailure": true,
      "progressBar": false
    });
  };
   
  BusinessBankingPresentationController.prototype.navigateToManagePermissionsSuccess = function( formName,failureCallback,asyncResponse ) {
    if ( asyncResponse.isAllSuccess() ) {
      var customRoleObject = asyncResponse.responses[0].data; 
      this.businessUserManager.createCustomRoleObject();
      this.businessUserManager.setCustomRoleAttribute("id", customRoleObject.id);
      this.businessUserManager.setCustomRoleAttribute("customRoleName", customRoleObject.CustomRoleName);
      this.businessUserManager.setCustomRoleAttribute("Group_Name", customRoleObject.ParentRole);
      this.businessUserManager.setCustomRoleAttribute("Group_id", customRoleObject.ParentRoleId);
      this.businessUserManager.setCustomRoleAttribute("accounts", customRoleObject.accounts);
      this.businessUserManager.setCustomRoleAttribute("SelectedRoleId", customRoleObject.ParentRoleId);
      this.businessUserManager.setCustomRoleAttribute("postShowflag", 0);

      customRoleObject["defaultLimitsForSelectedRole"] = segregateFeatureData(asyncResponse.responses[1].data.FeatureActions) || [];

      if( kony.application.getCurrentForm().id !== this.frmPermissionsTemplateForm ) {
        applicationManager.getNavigationManager().navigateTo(this.frmPermissionsTemplateForm);
      }  
      applicationManager.getNavigationManager().updateForm({
        "manageCustomRoleDetailsSuccess": customRoleObject,
        "progressBar": false
      }, this.frmPermissionsTemplateForm);              

    } else {
      failureCallback();
    }    

  };
  
  BusinessBankingPresentationController.prototype.navigateToManagePermissionsFailure = function(error) {
    this.navManager.updateForm({
         "serverError": true,
         "progressBar": false
     }, this.frmBBUsersDashboardForm);
  }; 
  
  /**
    * Method to call service to apply custom role to users.
    @param - json containing CustomRolename, CustomRoleId and list of users to apply custom role for.
   */
  BusinessBankingPresentationController.prototype.applyCustomRole = function(param){
    this.businessUserManager.applyCustomRole(param,this.applyCustomRoleSuccess.bind(this), this.applyCustomRoleFailure.bind(this));
  };
  
  /**
   * applyCustomRole SucessCallBack
   * @param {object} - response
   */
  BusinessBankingPresentationController.prototype.applyCustomRoleSuccess = function(response) {
    this.navManager.updateForm({
      "applyCustomeRoleSuccess" : response
    }, this.frmPermissionsTemplateForm);
  };
  
  /**
   * applyCustomRole FailureCallBack
   * @param {object} error - error response
   */
  BusinessBankingPresentationController.prototype.applyCustomRoleFailure = function(error) {
    this.navManager.updateForm({
      "applyCustomRoleFailure" : error
    }, this.frmPermissionsTemplateForm);
  };
  
  /**
    * Method to fetch permissions for custom role flow.
   */
   BusinessBankingPresentationController.prototype.fetchCustomRoleDetails = function(customRoleID, successcallback , failurecallback) {
    // this.businessUserManager.fetchAllRoles(customRoleID, successcallback , failurecallback);
     this.fetchCustomRoleDetailsSuccess();
   };
  BusinessBankingPresentationController.prototype.callfetchCustomRoleDetails = function(customRoleID) {
     this.fetchCustomRoleDetails(customRoleID, this.fetchCustomRoleDetailsSuccess.bind(this), this.fetchCustomRoleDetailsFailure.bind(this));
   };
  
  /**
   * fetchUserPermissions SuccessCallBack
   * @param {object} response response
   */
   BusinessBankingPresentationController.prototype.fetchCustomRoleDetailsSuccess = function(response) {
    response = {};
    this.navManager.updateForm({
      "customRoleDetailsSuccess" : response
    }, this.frmPermissionsTemplateForm);
  };
  
  /**
   * fetchUserPermissions FailureCallBack
   * @param {object} error - error response
   */
  BusinessBankingPresentationController.prototype.fetchCustomRoleDetailsFailure = function(error) {
    this.navManager.updateForm({
      "customRoleDetailsFailure" : error
    }, this.frmPermissionsTemplateForm);
  };
  
  BusinessBankingPresentationController.prototype.deleteCustomRole = function( roleId, successCallback = this.deleteCustomRoleSuccess.bind(this), 
    failureCallBack = this.deleteCustomRoleFailure.bind(this)) {
    this.navManager.updateForm({
      progressBar: true
    });
    this.businessUserManager.deleteTheCustomRole( { "customRoleId" : roleId }, successCallback, failureCallBack );    
  },
    
  BusinessBankingPresentationController.prototype.deleteCustomRoleSuccess = function(response) {
    this.navigateToUserRoles();
     this.navManager.updateForm({
         "CustomRoleDeletedSuccessfully" : response,
     }, this.frmBBUsersDashboardForm);    
  },
  
  BusinessBankingPresentationController.prototype.deleteCustomRoleFailure = function(error) {
    this.navigateToUserRoles();
     this.navManager.updateForm({
         "CustomRoleDeletionFailed" : error,
     }, this.frmBBUsersDashboardForm);     
  },
    
  BusinessBankingPresentationController.prototype.fetchUserRoles = function(success = this.fetchUserRolesSuccess.bind(this), failure = this.fetchUserRolesFailure.bind(this), formName = this.frmBBUsersDashboardForm) {
     this.navManager.updateForm({
      progressBar: true
    }, formName); 
    this.businessUserManager.fetchAllRoles(success.bind(this, formName),failure.bind(this, formName));
   };
  
  BusinessBankingPresentationController.prototype.fetchUserRolesSuccess = function(formName, response) {
    this.navManager.updateForm({
      "organizationRolesSuccess": response.CustomRoles
    }, formName );
  };
  
  BusinessBankingPresentationController.prototype.fetchUserRolesFailure = function(formName, error) {
    this.navManager.updateForm({
      "organizationRolesFailure" : error
    }, formName );
  };
  
  
  BusinessBankingPresentationController.prototype.fetchCustomRoleSuccess = function(formName, response) {
    var customRoleList = [];
	var roleRecords=[];
	roleRecords = response.CustomRoles;
    for (var i = 0; i < roleRecords.length; i++)
    {
      var customRole = {
        imgSelectRole : {"src" : "radioinactivebb.png" , "isVisible" : true},
        lblRoleName: roleRecords[i].name,
        lblRoleId: roleRecords[i].id,
        parent_id : roleRecords[i].parent_id,
        flxInnerRole : {skin : "sknBGFFFFFBdrE3E3E3BdrRadius2Px"}
      };
     
      customRole.lblRoleName = {
        text : truncateFeatureName(roleRecords[i].name,25, false),
        toolTip : roleRecords[i].name
      };
       
      customRole.imgArrow = {isVisible : false};
      customRoleList.push(customRole);
    }
    this.navManager.updateForm({
      "organizationRolesSuccess": customRoleList
    }, formName);
  };
  

  BusinessBankingPresentationController.prototype.fetchCustomRoleFailure = function(formName,errorResponse) {
    var errorMessage  = "";
    if(!kony.sdk.isNullOrUndefined(errorResponse.errorMessage)){
      errorMessage = errorResponse.errorMessage;
    }
    if(!kony.sdk.isNullOrUndefined(errorResponse.dbpErrMsg)){
      errorMessage = errorResponse.dbpErrMsg;
    }
    this.navManager.updateForm({
      "organizationRolesFailure" : true,
      "errorMessage": errorMessage,
    }, formName);
  };
  

  /**
   * Method to navigate crete user ui.
   * @param {object} userObj - user object /user id for update User - as per final imp .
   */
  BusinessBankingPresentationController.prototype.showCreateUser = function (userObj,formName) {    
    if (userObj) { //update user ui
      this.navManager.updateForm({
        "updateUser": this.businessUserManager.getUserObject(),
		    "id": this.businessUserManager.getUserAttribute("id")
      }, this.frmUserManagementForm);
    } else {
      if(kony.application.getCurrentForm().id !== this.frmUserManagementForm){
        this.navManager.navigateTo(this.frmUserManagementForm);			
      }
	  this.businessUserManager.clearDataMembers();
      this.businessUserManager.createUserObject();
	  var prevForm=false;
	  if(formName==="fromAllUsers"){
	    prevForm=true;
	  }
      this.navManager.updateForm({
        "createNewUser": "createNewUser",
        "progressBar":true,
		"prevForm":prevForm
      }, this.frmUserManagementForm);
    }
  };
   /**
     * used to show the username policies
     */
  BusinessBankingPresentationController.prototype.getUserNamePolicies = function() {
      var self = this;
      applicationManager.getUserPreferencesManager().fetchUsernameRulesAndPolicy(self.getUserNamePoliciesSuccessCallBack.bind(this), self.onServerError.bind(this));
  };
  /**
   * getUserNamePoliciesSuccessCallBack
   * @param {object} response response
   */
  BusinessBankingPresentationController.prototype.getUserNamePoliciesSuccessCallBack = function(response) {
      var validationUtility = applicationManager.getValidationUtilManager();
      validationUtility.createRegexForUsernameValidation(response.usernamerules);
      this.navManager.updateForm({
          "userNamePolicies": {
              usernamerules: response.usernamerules
          },
          progressBar: false
      }, this.frmUserManagementForm);
  };
  /**
   * Method to handle server error.
   * @param {object} errorResponse - error response object
   */
  BusinessBankingPresentationController.prototype.onServerError = function (errorResponse) {
    var errorMessage  = "";
    if(!kony.sdk.isNullOrUndefined(errorResponse.errorMessage)){
      errorMessage = errorResponse.errorMessage;
    }
    if(!kony.sdk.isNullOrUndefined(errorResponse.dbpErrMsg)){
      errorMessage = errorResponse.dbpErrMsg;
    }
    this.navManager.updateForm({
      serverError: true,
      errorMessage: errorMessage,
      progressBar: false
    });
  };
  /**
   * Method to update user details in user module.
   * @param {object} userObj - user details
   */
  BusinessBankingPresentationController.prototype.updateUserDetails = function (userObj, isBack,isManageUser) {
    this.navManager.updateForm({
      progressBar: true
    });
    this.businessUserManager.validateUser({
      "Ssn": userObj.ssn,
      "DateOfBirth": CommonUtilities.sendDateToBackend(userObj.dob)
    }, this.onValidateUserSuccess.bind(this, userObj,isBack,isManageUser), this.onServerError.bind(this));
  };
  /**
   * Method to update user details in user module on sucess of valid user.
   * @param {object} userObj - user details
   * @param {object} response - respose object
   */
  BusinessBankingPresentationController.prototype.onValidateUserSuccess = function (userObj,isBack,isManageUser, response) {
    this.navManager.updateForm({
      progressBar: true
    }, this.frmUserManagementForm);
    if (response.isValid) {
      this.businessUserManager.setUserAttribute("FirstName", userObj.firstName);
      this.businessUserManager.setUserAttribute("MiddleName", userObj.middleName);
      this.businessUserManager.setUserAttribute("LastName", userObj.lastName);
      this.businessUserManager.setUserAttribute("Email", userObj.email);
      this.businessUserManager.setUserAttribute("Ssn", userObj.ssn);
      this.businessUserManager.setUserAttribute("DrivingLicenseNumber", userObj.driverLicenseNumber);
      this.businessUserManager.setUserAttribute("Phone", userObj.phoneNumber);
      this.businessUserManager.setUserAttribute("UserName", userObj.userName);
      this.businessUserManager.setUserAttribute("DateOfBirth", CommonUtilities.sendDateToBackend(userObj.dob));
      if (!isBack) {
        //this.fetchRoles( this.onFetchUserRolesSuccess.bind( this ) );
        this.navManager.updateForm({
          "createNewUserRole": "createNewUserRole",
          progressBar: false
        }, this.frmUserManagementForm);
      }
      else {
        applicationManager.getBusinessUserManager().setUserAttribute("EditFlagForUpdateUser", true);
		if(!isManageUser){
			this.saveAccountListAndNavigate(this.businessUserManager.getUserAttribute("SelectedAccounts"));
		}
      }
      
    }
    else {
      this.navManager.updateForm({
        "invalidUserError": response,
        progressBar: false
      }, this.frmUserManagementForm);
    }
  };
  /**
   * Validate username availability errorcallback
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.onValidateUsernameError = function (response) {
    this.navManager.updateForm({
      "invalidUserError": response,
      progressBar: false
    }, this.frmUserManagementForm);
  };
  /**
   * Validate user name availability
   * @param {object} username  user name
   * */
  BusinessBankingPresentationController.prototype.validateUserName = function (username) {
    this.navManager.updateForm({ progressBar: true }, this.frmUserManagementForm)
    this.businessUserManager.validateUserName(username, this.onValidateUserNameSuccess.bind(this), this.onValidateUserNameError.bind(this));
  };
  /**
   * Validate user name availability success callback
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.onValidateUserNameSuccess = function (response) {
    this.navManager.updateForm({
      userNameAvailability: { isAvailable: response.isAvailable },
      progressBar: false
    }, this.frmUserManagementForm);
  };
  /**
   * Validate user name availability errorcallback
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.onValidateUserNameError = function (response) {
    this.navManager.updateForm({
      "invalidUser": true,
      progressBar: false
    }, this.frmUserManagementForm);
  };
  /**
   * Validate Identification Number
   * @param {object} Identification No. SSN
   * */
  BusinessBankingPresentationController.prototype.validateIdentificationNumber = function (identificationnumber) {
    this.navManager.updateForm({ progressBar: true }, this.frmUserManagementForm)
    this.businessUserManager.checkIfOrganisationUserExists({"Ssn": identificationnumber}, this.onValidateIdentificationNumberSuccess.bind(this), this.onServerError.bind(this));
  };
  /**
   * Validate Identification Number success callback
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.onValidateIdentificationNumberSuccess = function (response) {
    
	var status = false;	
	if(response.isUserExists === "true") {
		status=true;
	}
	this.navManager.updateForm({
      "identificationNumberExists": status,
      progressBar: false
    }, this.frmUserManagementForm);
  };
  
  /**
   * Method to know if its Edit flow or Create flow
   */
  BusinessBankingPresentationController.prototype.isEditMode = function() {
    return !(this.businessUserManager.getUserAttribute("id") === undefined || this.businessUserManager.getUserAttribute("id") === null || this.businessUserManager.getUserAttribute("id") === "");
  };
  

  
    /**
   * filter other features actions and set the widget master data
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.fetchOtherFeaturesActions = function (store) {
  
    var NONM = store.NON_MONETARY;
    var len = NONM.length;
    var main = [];
    for (var i = 0; i < len; i++) {
      var FeatureData = NONM[i];
      var obj = {
        "lblFeatureHeader": FeatureData.featureName
      };
      for (var j = 0; j < FeatureData.Actions.length; j++) {
        obj["lblActionName" + (j + 1)] = {
          "text": FeatureData.Actions[j].actionName
        };
        obj["lblActiveSelection" + (j + 1)] = {
          "text": "F",
          "skin": (FeatureData.Actions[j].isSelected) ? "sknBBLblOLBFontsActive04A615" : "sknBBLblOLBFontsInActiveC0C0C0"
        };
      }
      main.push(obj);
    }
    
    return main;
  };
  
  
  BusinessBankingPresentationController.prototype.fetchAllFeaturesAction =  function (response) {
    var len = response.FeatureActions.length;
    var main=[];
    for(var i=0;i<len;i++){
        var JsonArr=[];
        var FeatureData = response.FeatureActions[i];
      	
        var header = {
          	"lblSegRoleSeparator":{"text":"-"},
            "headerName":FeatureData.featureName
        };
        var arrayActions=[];
        for(var j =0;j< FeatureData.Actions.length;j++){
            arrayActions.push({"id":FeatureData.Actions[j].actionName});
        }
        JsonArr.push(header);
        JsonArr.push(arrayActions);
        main.push(JsonArr);
        }
    
    return main;
    
  };

   /**
   * Method to fetch user role feature-actions set based on the selected rolw.
   */
  BusinessBankingPresentationController.prototype.fetchUserRoleActions = function (selectedRoleId) {
    this.navManager.updateForm({
      progressBar: true
    });
   this.getUserRoleActionsCall({"groupId" : selectedRoleId},this.fetchUserRoleActionsSuccess.bind(this), this.onServerError.bind(this));
  };
  
  BusinessBankingPresentationController.prototype.getUserRoleActionsCall = function( params, successCallback, failureCallback ) {
    this.businessUserManager.getUserRoleActions(params,successCallback, failureCallback );
  }
  
   /**
   * Success callback for fetchUserRolesAction
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.fetchUserRoleActionsSuccess = function (response) {
    
    /* segregate the data on monetary and non monetary basis and prepare two separate payloads */
    var store = segregateFeatureData(response.FeatureActions);
    var allFeatureData = this.fetchAllFeaturesAction(response);
    var otherFeatureData = this.fetchOtherFeaturesActions(store);
    
    this.navManager.updateForm({
          fetchOtherRoleAction :otherFeatureData,
          fetchRoleAction : allFeatureData,
          storeUserDataTransaction : store,
          progressBar: false
        }, this.frmUserManagementForm);
  };
  /**
   * Method to fetch user roles or groups.
   */
  BusinessBankingPresentationController.prototype.fetchRoles = function ( successCallback,formName = this.frmPermissionsTemplateForm) {
    this.navManager.updateForm({
      progressBar: true
    });
    this.businessUserManager.getUserRoles( successCallback.bind(this,formName), this.onServerError.bind(this));
  };
  /**
   * fetch user roles success scenario
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.onFetchUserRolesSuccess = function (formName,response) {
    var ids = [];
	var grecords=[];
    var isEditManager = this.businessUserManager.getUserAttribute("isEditManager");
	grecords = response["GroupRecords"];
    this.businessUserManager.setUserAttribute("NumRoles",grecords.length);
    for (var i = 0; i < grecords.length; i++)
    {
      var userRole = {
        description : grecords[i].Description,
        imgSelectRole : {"src" : "radioinactivebb.png" , "isVisible" : true},
        lblRoleName: grecords[i].Name,
        lblRoleId: grecords[i].id,
        flxInnerRole : {skin : "sknBGFFFFFBdrE3E3E3BdrRadius2Px"}
      };
      
      userRole.lblRoleName = {
        text : truncateFeatureName(grecords[i].Name,25, false),
        toolTip : grecords[i].Name
      };
      
      if(grecords[i].id === this.businessUserManager.getUserAttribute("SelectedRoleId")){
         userRole.imgArrow = {isVisible : true};
       }
      else{
        userRole.imgArrow = {isVisible : false};
      }
      ids.push(userRole);
    }
    
    if(this.isEditMode() && !isEditManager){
      this.navManager.navigateTo(this.frmUserManagementForm);
    }
    this.navManager.updateForm({
      userRoles: ids,
      selectedRoleId: this.businessUserManager.getUserAttribute("SelectedRoleId"),
      id : this.businessUserManager.getUserAttribute("id"),
      progressBar: false
    }, this.frmUserManagementForm);
  };
  
  
  /**
   * update user roles
   * @param {object} roleObj - response object
   * */
  BusinessBankingPresentationController.prototype.confirmRole = function (roleObj) {
    var prevRoleId = this.businessUserManager.getUserAttribute("Role_id");
    this.businessUserManager.setUserAttribute("Role_id", roleObj.id);// create user
    this.businessUserManager.setUserAttribute("Group_Name", roleObj.Name); //for user ack screen
    this.businessUserManager.setUserAttribute("Group_Description", roleObj.Description); //for user ack screen
	  if(roleObj.id !== prevRoleId){
    	this.businessUserManager.resetTransactionLimits();
		this.businessUserManager.setUserAttribute("services",[]);
    }
  };
  /**
 * Method to navigate to user management dashboard and fetch list of users
 * @param {JSON} viewModel - consists context and parameters
 */

  BusinessBankingPresentationController.prototype.navigateToUsers = function (successCallBack,sortingInputs, formName = this.frmBBUsersDashboardForm , failureCallBack = this.onServerError.bind(this)) {
    if( kony.application.getCurrentForm().id !== formName) {
      this.navManager.navigateTo(formName);
    }
    this.navManager.updateForm({ "progressBar": true }, formName);
    applicationManager.getPaginationManager().resetValues();
    var searchString = sortingInputs ? sortingInputs.searchString : null;
    var params = {};
    params = applicationManager.getPaginationManager().getValues(this.defaultSortConfig, sortingInputs);
    if (typeof searchString === "string" && searchString.length > 0) {
      params.searchString = searchString;
    } else if(searchString !== null && searchString !== undefined){
      if(searchString.trim().length === 0){
        params.sortBy = 'createdts';
        params.order = OLBConstants.DESCENDING_KEY;
      }
    }
    else{}
    params.limit = "";
    params.offset = "";
    this.businessUserManager.getAllUsers(params, successCallBack.bind(this, searchString, formName), failureCallBack.bind(this));
  };
  
  //fetchSubUsersSuccess
    /** Method to Navigate to Accountslanding dashboard on create user cancellation    
    */
  BusinessBankingPresentationController.prototype.navigateToBBAccountsLandingDashboard = function (sortingInputs) {
	this.businessUserManager.clearDataMembers();  	
	var accountsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountsModule");
	accountsModule.presentationController.showAccountsDashboard();   
  };
  
  
  /** Method to handle success response of updating user status
    * @param {object} viewModel - which consists of response from backend
    */
  BusinessBankingPresentationController.prototype.setUserStatusSuccess = function (viewModel) {
    var viewProperties = {};
    viewProperties.statusSuccess = viewModel;
    viewProperties.progressBar = false;
    this.navManager.updateForm(viewProperties, this.frmBBUsersDashboardForm);
  };
  
  /** Need to add more fields when the service is called
  * Method to handle success response of fetch sub users
  * @param {object} viewModel - which consists of response from backend
  */
  BusinessBankingPresentationController.prototype.fetchSubUsersSuccess = function (searchString, formName, viewModel) {
    var paginationValues = applicationManager.getPaginationManager().getValues(this.defaultSortConfig);
    if(searchString !== null && searchString !== undefined){
      if(searchString.trim().length === 0){
        paginationValues.sortBy = 'createdts';
        paginationValues.order = OLBConstants.DESCENDING_KEY;
      }
    }
    paginationValues.limit = viewModel.length;
    if (viewModel.length > 0 || paginationValues.offset === 0) {
      this.navManager.updateForm({
        "allUsers": {
          users: viewModel,
          config: paginationValues,
          searchString: searchString
        }
      }, formName);
    } else if (paginationValues.offset !== 0) {
      this.navManager.updateForm({ noMoreRecords: true });
    }
  };
  /** Need to add more fields and parameters when the service is called
  * Method to update user status
  */
  BusinessBankingPresentationController.prototype.setUserStatus = function (params) {
    this.businessUserManager.updateUserStatus(params, this.setUserStatusSuccess.bind(this),  this.onServerError.bind(this));
  };
  
  /** Method to handle success response of updating user status
    * @param {object} viewModel - which consists of response from backend
    */
  BusinessBankingPresentationController.prototype.setUserStatusSuccess = function (viewModel) {
    var viewProperties = {};
    viewProperties.statusSuccess = viewModel;
    viewProperties.progressBar = false;
    this.navManager.updateForm(viewProperties, this.frmBBUsersDashboardForm);
  };
  
  /** Need to add more fields when the service is called
    * Method to handle success response of updating user status
    * @param {object} viewModel - which consists of response from backend
    */
  BusinessBankingPresentationController.prototype.showAllAccounts = function (viewModel) {
    //Need to handle the success senario based on the response, when the call is successful
    var viewProperties = {};
    viewProperties.statusSuccess = viewModel;
    viewProperties.progressBar = false;
    this.navManager.updateForm(viewProperties, this.frmBBUsersDashboardForm);
  };

  /**Method to make service call to fetch Accounts
  */
  BusinessBankingPresentationController.prototype.showAllAccounts = function (selectedRole , isEdit) {
    this.navManager.updateForm({
      progressBar: true
    });
    if(this.businessUserManager.getUserAttribute("isEditManger") !== true && !isEdit){
    this.getAllAccountsCall(this.fetchAccountsSuccess.bind(this), this.onServerError.bind(this));
    }
    else {
      this.saveAccountListAndNavigate(this.businessUserManager.userObj.SelectedAccounts);
    }
  };
  
  BusinessBankingPresentationController.prototype.getAllAccountsCall = function( successCallback, failureCallback ) {
    this.businessUserManager.getAllAccounts(successCallback, failureCallback);
  }
  
  
  BusinessBankingPresentationController.prototype.saveAccountListAndNavigate = function (selectedAccounts) {
    this.navManager.updateForm({
      progressBar: true
    });
    var userAggregate = {};  
    var userObj = {};
    var middleName = "";
    var firstName = "";
    var lastName = "";
    if(!kony.sdk.isNullOrUndefined(this.businessUserManager.getUserAttribute("MiddleName"))){
      middleName = this.businessUserManager.getUserAttribute("MiddleName");
    }
    if(!kony.sdk.isNullOrUndefined(this.businessUserManager.getUserAttribute("FirstName"))){
      firstName = this.businessUserManager.getUserAttribute("FirstName");
    }
    if(!kony.sdk.isNullOrUndefined(this.businessUserManager.getUserAttribute("LastName"))){
      lastName = this.businessUserManager.getUserAttribute("LastName");
    }
    userObj["Full Name"] =  firstName+" "+middleName+" "+lastName;
    userObj["Date Of Birth"] = this.businessUserManager.getUserAttribute("DateOfBirth");
    userObj["Email Address"] = this.businessUserManager.getUserAttribute("Email");
    userObj["Social Security Number(SSN)"] = this.businessUserManager.getUserAttribute("Ssn");
    userObj["Driver's License Number"] = this.businessUserManager.getUserAttribute("DrivingLicenseNumber");
    userObj["Registered Phone Number"] = this.businessUserManager.getUserAttribute("Phone");
    userObj["Username"] =this.businessUserManager.getUserAttribute("UserName");
    userAggregate.SelectedRoleName = this.businessUserManager.getUserAttribute("SelectedRoleName");
    userAggregate.SelectedRoleId = this.businessUserManager.getUserAttribute("SelectedRoleId");
    userAggregate.selectedAccounts = selectedAccounts;
    userAggregate.userObject = userObj;
    this.businessUserManager.setUserAttribute("SelectedAccounts",selectedAccounts);
    this.businessUserManager.setUserAttribute("accounts",selectedAccounts);
    this.navManager.updateForm({
			progressBar: false,
			verifyUser: userAggregate,
		}, this.frmUserManagementForm);
  };
  
  
  BusinessBankingPresentationController.prototype.saveAccountListAndNavigateToVerifyUser = function (selectedAccounts,userObjVal) {
    this.navManager.updateForm({
      progressBar: true
    });
    var userAggregate = {};  
    var middleName = "";
    var firstName = "";
    var lastName = "";
			    var userObj = {};
    if(!kony.sdk.isNullOrUndefined(userObjVal.middleName)){
      middleName = userObjVal.middleName;
    }
    if(!kony.sdk.isNullOrUndefined(userObjVal.firstName)){
      firstName = userObjVal.firstName;
    }
    if(!kony.sdk.isNullOrUndefined(userObjVal.lastName)){
      lastName = userObjVal.lastName;
    }
    userObj["Full Name"] =  firstName+" "+middleName+" "+lastName;
    userObj["Date Of Birth"] = CommonUtilities.sendDateToBackend(userObjVal.dob);
    userObj["Email Address"] = userObjVal.email;
    userObj["Social Security Number(SSN)"] = userObjVal.ssn;
    userObj["Driver's License Number"] = userObjVal.driverLicenseNumber;
    userObj["Registered Phone Number"] = userObjVal.phoneNumber;
    userObj["Username"] = userObjVal.userName;
    userAggregate.SelectedRoleName = this.businessUserManager.getUserAttribute("SelectedRoleName");
    userAggregate.SelectedRoleId = this.businessUserManager.getUserAttribute("SelectedRoleId");
    userAggregate.selectedAccounts = selectedAccounts;
    userAggregate.userObject = userObj;
    this.businessUserManager.setUserAttribute("SelectedAccounts",selectedAccounts);
    this.businessUserManager.setUserAttribute("accounts",selectedAccounts);
    this.navManager.updateForm({
			progressBar: false,
			verifyUser: userAggregate,
		}, this.frmUserManagementForm);
  };
  
  /** Method for fetch Accounts success
  * @param {object} response - which consists of response from backend
  */
	BusinessBankingPresentationController.prototype.fetchAccountsSuccess = function(response) {
		if(this.isEditMode()){
      //this.navManager.navigateTo(this.frmUserManagementForm);
    }
		this.navManager.updateForm({
			progressBar: false,
			accounts: response,
			selectedAccounts: this.businessUserManager.getUserAttribute("accounts"),
			id : this.businessUserManager.getUserAttribute("id")
		}, this.frmUserManagementForm);
  };
  /** Method to update the selected accounts in the Business Manager
   * @param {object} selectedAccounts - which consists of list of selected accounts of the user
   */
  BusinessBankingPresentationController.prototype.confirmAccounts = function (selectedAccounts) {
    this.businessUserManager.setUserAttribute("accounts", selectedAccounts);
  };
  /**Method to make service call to fetch Transaction Limits
  */
  BusinessBankingPresentationController.prototype.fetchTransactionLimits = function () {
    this.navManager.updateForm({
      progressBar: true
    }, this.frmUserManagementForm);
    this.businessUserManager.getTransactionLimits(this.businessUserManager.getUserAttribute("Role_id"), this.fetchTransactionLimitsSuccess.bind(this), this.onServerError.bind(this));
  };
  /** Method for fetch Transaction Limit success
   * @param {object} response - which consists of response from backend
   */
  BusinessBankingPresentationController.prototype.fetchTransactionLimitsSuccess = function (response) {
    if (response.length) {
      this.navManager.updateForm({
        progressBar: false,
        transactionLimits: response,
		    selectedLimits: this.businessUserManager.getUserAttribute("services"),
		    id : this.businessUserManager.getUserAttribute("id")
      }, this.frmUserManagementForm);
    } else {
      this.businessUserManager.createOrUpdateUser(this.createOrUpdateUserSuccess.bind(this), this.onServerError.bind(this));
    }
  };
  BusinessBankingPresentationController.prototype.addTransactionLimitsOnBack = function (transactionLimits) {
    this.businessUserManager.setUserAttribute("services", transactionLimits);
  };
  /** Method to update the transaction limits in the Business Manager
   * @param {object} transactionLimits - which consists of list of transaction limits of the user
   */
  BusinessBankingPresentationController.prototype.confirmTransactionLimits = function (transactionLimits) {
    this.businessUserManager.setUserAttribute("services", transactionLimits);
    this.businessUserManager.createOrUpdateUser(this.createOrUpdateUserSuccess.bind(this), this.onServerError.bind(this));
  };
  
  BusinessBankingPresentationController.prototype.updateUser = function (userAggregate, userDataStore,templimitstorejson,tryUpdate) {
    this.navManager.updateForm({
        progressBar: true
      });
	var self = this;
	self.userObj = self.businessUserManager.getUserObject();
	if(tryUpdate===1){
		self.userObj["SelectedAccounts"]=userAggregate["SelectedAccounts"];		
		if(!kony.sdk.isNullOrUndefined(userAggregate["newData"])){
			self.userObj["FirstName"]				=  userAggregate["newData"]["firstName"];
			self.userObj["MiddleName"]               = userAggregate["newData"]["middleName"];
			self.userObj["LastName"]                 = userAggregate["newData"]["lastName"];
			self.userObj["Email"]                    = userAggregate["userObject"]["Email Address"];
			self.userObj["DrivingLicenseNumber"]     = userAggregate["userObject"]["Driver's License Number"];
			self.userObj["Phone"]                    = userAggregate["userObject"]["Registered Phone Number"];
			self.userObj["UserName"]                 = userAggregate["userObject"]["Username"];
			self.userObj["DateOfBirth"]              = userAggregate["userObject"]["Date Of Birth"];
			self.userObj["Ssn"]                      = userAggregate["userObject"]["Social Security Number(SSN)"];
		}
	}
	self.userObjForUpdate = createUserObjectForCreation(userAggregate, self.userObj, userDataStore,templimitstorejson);  
	self.userObjForUpdate["id"]=this.userObj["id"];
  	this.businessUserManager.UpdateBBUser(self.userObjForUpdate,this.createOrUpdateUserSuccess.bind(this,userAggregate), this.onServerError.bind(this));
  };
  /** Method for fetch Transaction Limit success
   * @param {object} response - which consists of response from backend
   */
  BusinessBankingPresentationController.prototype.createOrUpdateUserSuccess = function (userAggregate,response) {
    if (response) {
      var userObj = this.businessUserManager.getUserObject();
      this.navManager.updateForm({
        progressBar: false
      });
      if(response.errorMessage){
        this.navManager.updateForm({
          progressBar: false,
		  updateUserSuccessFailure: true,
          errMsg: response.errorMessage
        });
      } else {
        this.navManager.updateForm({
          progressBar: false,
          updateUserSuccess: true,
		  verifyUser: userAggregate,
          referenceNumber: response.id,
          userObject: userObj
        });
      }
    }
  };
  /** Method to resend Activation Link
  * @param {String} username - contains username
  */
  BusinessBankingPresentationController.prototype.resendActivationLink = function (username) {
    this.businessUserManager.resendActivationLink(username, this.resendActivationLinkSuccess.bind(this), this.resendActivationLinkFailure.bind(this));
  };
  /** Method to handle success response of resendActivationLink
  * @param {object} response - which consists of response from backend
  */
  BusinessBankingPresentationController.prototype.resendActivationLinkSuccess = function () {
    this.navManager.updateForm({
      "activationLinkSuccess": true,
      "progressBar": false
    }, this.frmBBUsersDashboardForm);
  };
  /** Method to handle failure response of resendActivationLink
  */
  BusinessBankingPresentationController.prototype.resendActivationLinkFailure = function () {
    this.navManager.updateForm({
      "serverError": true,
      "progressBar": false
    });
  };
  /** Method to fetch User details
  * @param {String} username - contains username
  */
  BusinessBankingPresentationController.prototype.fetchUserDetails = function (username ,rolename, completionCallback = this.fetchUserDetailsCompletionCallback.bind(this), failureCallback = this.fetchUserDetailsFailure.bind(this),formName = this.frmPermissionsTemplateForm) {
    
     this.navManager.updateForm({
      progressBar : true
    });
        var asyncManager = applicationManager.getAsyncManager();
        asyncManager.callAsync(
            [
                asyncManager.asyncItem(applicationManager.getBusinessUserManager(), "fetchUserDetails", [username]),
                asyncManager.asyncItem(applicationManager.getBusinessUserManager(), "getUserRoleActions", [{"groupId": rolename}]) 
            ],  completionCallback.bind(this,failureCallback,formName));
  };
  
  BusinessBankingPresentationController.prototype.fetchUserDetailsCompletionCallback = function(failureCallback, formName,asyncResponse) {
    var scopeObj = this;
    if (asyncResponse.isAllSuccess()) {
      var userObject = asyncResponse.responses[0].data;
      scopeObj.updateUserObjManager(userObject);
      scopeObj.businessUserManager.setUserAttribute("postShscopeObjowflag", 0);
      var store = segregateFeatureData(asyncResponse.responses[1].data.FeatureActions);
      userObject["defaultLimitsForSelectedRole"] = store;
      if( kony.application.getCurrentForm().id !== formName ){
        applicationManager.getNavigationManager().navigateTo(formName);
      }
      applicationManager.getNavigationManager().updateForm({
        "manageUser": userObject,
        "progressBar": false
      }, formName);
    } else {
           failureCallback();
      }
  }; 

    BusinessBankingPresentationController.prototype.fetchExistingUserDetailsCompletionCallback = function(failureCallback,formName, asyncResponse) {
    var scopeObj = this;
    if (asyncResponse.isAllSuccess()) {
      var userObject = asyncResponse.responses[0].data;
      scopeObj.updateCustomRoleObjManager(userObject);
      var store = segregateFeatureData(asyncResponse.responses[1].data.FeatureActions);
      userObject["defaultLimitsForSelectedRole"] = store;
      
      if( kony.application.getCurrentForm().id !== formName ) {
        applicationManager.getNavigationManager().navigateTo(formName);
      }       
      applicationManager.getNavigationManager().updateForm({
        "selectedUser": userObject,
        "progressBar": false
      }, formName);
    } else {
      var errorData={};
      for(i=0;i<2;i++){
        if(! asyncResponse.responses[i].isSuccess){
          errorData = asyncResponse.responses[i].data;
        }
      }
         failureCallback(errorData);
     }
  };
  
  
  BusinessBankingPresentationController.prototype.updateUserObjHelper = function(userObject) {
    this.businessUserManager.setUserAttribute("FirstName", userObject.FirstName);
    this.businessUserManager.setUserAttribute("MiddleName", userObject.MiddleName);
    this.businessUserManager.setUserAttribute("LastName", userObject.LastName);
    this.businessUserManager.setUserAttribute("Email", userObject.Email);
    this.businessUserManager.setUserAttribute("Ssn", userObject.Ssn);
    this.businessUserManager.setUserAttribute("DrivingLicenseNumber", userObject.DrivingLicenseNumber);
    this.businessUserManager.setUserAttribute("Phone", userObject.Phone);
    this.businessUserManager.setUserAttribute("UserName", userObject.Username);
    this.businessUserManager.setUserAttribute("DateOfBirth",userObject.DateOfBirth);
    this.businessUserManager.setUserAttribute("Role_id", userObject.Group_id);
    this.businessUserManager.setUserAttribute("Group_id", userObject.Group_id);
    this.businessUserManager.setUserAttribute("Group_Name", userObject.Group_Name);
    this.businessUserManager.setUserAttribute("Group_Description", userObject.Group_Description);
    this.businessUserManager.setUserAttribute("accounts", userObject.accounts);
    this.businessUserManager.setUserAttribute("services", userObject.services);
    this.businessUserManager.setUserAttribute("monetary", userObject.monetary);
    this.businessUserManager.setUserAttribute("nonmonetary", userObject.nonmonetary);
  }

  BusinessBankingPresentationController.prototype.fetchExistingUserDetailsForUserCreationCompletionCallback = function(failureCallback,formName, asyncResponse) {
    var scopeObj = this;
    if (asyncResponse.isAllSuccess()) {
      var userObject = asyncResponse.responses[0].data;
      this.businessUserManager.createUserObject();
      this.updateUserObjHelper(userObject);
      this.businessUserManager.setUserAttribute("defaultOrexistingRoleSelected", true);
      var store = segregateFeatureData(asyncResponse.responses[1].data.FeatureActions);
      userObject["defaultLimitsForSelectedRole"] = store;

      if( kony.application.getCurrentForm().id !== formName ) {
        applicationManager.getNavigationManager().navigateTo(formName);
      }       
      applicationManager.getNavigationManager().updateForm({
        "selectedUser": userObject,
        "progressBar": false
      }, formName);
    }else {
      var errorData={};
      for(i=0;i<2;i++){
        if(! asyncResponse.responses[i].isSuccess){
          errorData = asyncResponse.responses[i].data;
        }
      }
      failureCallback(errorData);
    }
  };
  

  BusinessBankingPresentationController.prototype.fetchExistingUserDetailsFailureCallback = function(error){
    this.navManager.updateForm({
      "errorData":error,
      "fetchUserDetailsFailure": true,
      "progressBar": false
    });
  }
  
  /** Method to handle failure response of fetchUserDetails
  */
  BusinessBankingPresentationController.prototype.fetchUserDetailsFailure = function () {
    this.navManager.updateForm({
      "serverError": true,
      "progressBar": false
    });
  };
  
  /** Method to UPDATE User object in manager
   * @param {object} userObj - which consists of the user object
  */
  BusinessBankingPresentationController.prototype.updateUserObjManager = function (userObj) {
    this.businessUserManager.createUserObject();
    this.businessUserManager.setUserAttribute("id", userObj.id);
    this.updateUserObjHelper(userObj);
  };

  BusinessBankingPresentationController.prototype.updateCustomRoleObjManager = function (customRoleObject) {
    this.businessUserManager.createCustomRoleObject();
    this.businessUserManager.setCustomRoleAttribute("Role_id", customRoleObject.Group_id);
    this.businessUserManager.setCustomRoleAttribute("Group_id", customRoleObject.Group_id);
    this.businessUserManager.setCustomRoleAttribute("Group_Name", customRoleObject.Group_Name);
    this.businessUserManager.setCustomRoleAttribute("accounts", customRoleObject.accounts);
    this.businessUserManager.setCustomRoleAttribute("services", customRoleObject.services);
    this.businessUserManager.setCustomRoleAttribute("monetary", customRoleObject.monetary);
    this.businessUserManager.setCustomRoleAttribute("nonmonetary", customRoleObject.nonmonetary);
    this.businessUserManager.setCustomRoleAttribute("SelectedRoleName",customRoleObject.UserName);
    this.businessUserManager.setCustomRoleAttribute("SelectedRoleId",customRoleObject.Group_id);
    this.businessUserManager.setCustomRoleAttribute("isExistingUserSelected", true);
    this.businessUserManager.setCustomRoleAttribute("isPermissionsFromSelectedUserEdited", false);
    this.businessUserManager.setCustomRoleAttribute("defaultOrexistingRoleSelected", true);
  };

  BusinessBankingPresentationController.prototype.showPrintPage = function(data){
    var self = this;
    data.printKeyValueGroupModel.printCallback = function(){
         self.onPrintCancel();
    }
    applicationManager.getNavigationManager().navigateTo('frmPrintTransfer');
    applicationManager.getNavigationManager().updateForm(data, 'frmPrintTransfer');
  };
  BusinessBankingPresentationController.prototype.onPrintCancel = function(){
      this.navManager.navigateTo(this.frmBBUsersDashboardForm);
      this.navManager.updateForm({ "isPrintCancelled": true }, this.frmBBUsersDashboardForm);
  };
  
  BusinessBankingPresentationController.prototype.showEditFeaturePermissions = function (formName,userObject) { //openFeaturePermissions
    this.navManager.updateForm({
      progressBar : true
    });
    this.navManager.updateForm({
      featureActions : true
    }, formName);
    this.navManager.updateForm({
      progressBar : false
    });
  };

  BusinessBankingPresentationController.prototype.showEditTransactionLimits = function (formName) { //openEditTransactionLimits
    this.navManager.updateForm({
      progressBar : true
    });
    this.navManager.updateForm({
      transactionDetails : true
    }, formName);
    this.navManager.updateForm({
      progressBar : false
    });
  };

  BusinessBankingPresentationController.prototype.showAccountLevelPermissions = function (formName,userObject) { //openAccountLevelPermissions
    this.navManager.updateForm({
      progressBar : true
    });
    this.navManager.updateForm({
      accountLevelPermissions : true
    }, formName);
    this.navManager.updateForm({
      progressBar : false
    });
  };
  
  BusinessBankingPresentationController.prototype.navigateToVerifyCustomRole = function (formName,userAggregate) {
    this.navManager.updateForm({
      progressBar: true
    });
    if(!kony.sdk.isNullOrUndefined(applicationManager.getBusinessUserManager().getCustomRoleAttribute("isEditManager")) && applicationManager.getBusinessUserManager().getCustomRoleAttribute("isEditManager")) {
      this.navManager.updateForm({
        verifyCustomRole : userAggregate
      }, formName);
    }
    else{
      this.navManager.updateForm({
        verifyUser : userAggregate
      }, formName);
    }
    this.navManager.updateForm({
      progressBar: false
    });
  };
  
  BusinessBankingPresentationController.prototype.navigateToVerifyUser = function (formName,userAggregate) {
    this.navManager.updateForm({
      progressBar: true
    });
    this.navManager.updateForm({
      verifyUser : userAggregate
    }, formName);

    this.navManager.updateForm({
      progressBar: false
    });
  };
  
  
//Custome role name duplicate check for Primary details page
  
  BusinessBankingPresentationController.prototype.checkDuplicateRoleName = function(custRoleNameVal){
    this.navManager.updateForm({
      progressBar: true
    });
    var custRoleNameObj = {    
		"customRoleName" : custRoleNameVal
	}
    applicationManager.getBusinessUserManager().duplicateCheckCustomRole(custRoleNameObj, this.duplicateCheckCustomeRoleSuccess.bind(this), this.duplicateCheckCustomeRoleFailure.bind(this));
  };
  
  
  BusinessBankingPresentationController.prototype.duplicateCheckCustomeRoleSuccess = function(response){
    this.navManager.updateForm({
      "isDuplicateResponse": response,
      progressBar: false
    }, this.frmPermissionsTemplateForm);
  };
  
  BusinessBankingPresentationController.prototype.duplicateCheckCustomeRoleFailure = function(errorResponse){
    this.navManager.updateForm({
      serverError: true,
      errorMessage: errorResponse.errorMessage,
      progressBar: false
    })
  };
  
// method to create custom role template
  BusinessBankingPresentationController.prototype.createCustomRoleTemplate = function(userAggregate, userDataStore,templimitstorejson) {
    this.navManager.updateForm({
      progressBar: true
    });
    var self = this;
    self.userAggregateObj = userAggregate;
    self.customRoleObj = self.businessUserManager.getCustomRoleObject();
    self.customRoleObjForCreation = createCustomRoleObjectForCreation(userAggregate, self.customRoleObj, userDataStore,templimitstorejson);
    applicationManager.getBusinessUserManager().createCustomRole(self.customRoleObjForCreation, this.createCustomRoleSuccessCallBack.bind(this), this.createCustomRoleFailureCallBack.bind(this));
  };
  
 
 BusinessBankingPresentationController.prototype.createCustomRoleSuccessCallBack = function(response) {
    var self = this;
    self.customRoleObjForCreation["customRoleId"] = response["customRoleId"];
    self.customRoleObjForCreation["createdby"] = response["createdby"];
    self.customRoleObjForCreation["createdts"] = response["createdts"];
    this.navManager.updateForm({
      showAck: self.customRoleObjForCreation
    });
    this.navManager.updateForm({
      progressBar: false
    });
  };
  
  BusinessBankingPresentationController.prototype.createCustomRoleFailureCallBack = function (errorResponse) {
    this.navManager.updateForm({
      serverError: true,
      errorMessage: errorResponse.errorMessage,
      progressBar: false
    })
  };
  
  BusinessBankingPresentationController.prototype.createUser = function(userAggregate, userDataStore,templimitstorejson) {
    this.navManager.updateForm({
      progressBar: true
    });
    var self = this;
    self.userAggregateObj = userAggregate;
    self.userObj = self.businessUserManager.getUserObject();
    self.userObjForCreation = createUserObjectForCreation(userAggregate, self.userObj, userDataStore,templimitstorejson);
    applicationManager.getBusinessUserManager().createBBUser(self.userObjForCreation, this.createUserSuccessCallBack.bind(this), this.createUserFailureCallBack.bind(this));
  };
  
  BusinessBankingPresentationController.prototype.createUserSuccessCallBack = function(response) {
    var self = this;
    self.userObjForCreation["Reference_ID"] = response["id"];
    this.navManager.updateForm({
      showAck: self.userObjForCreation
    });
    this.navManager.updateForm({
      progressBar: false
    });
  };
  
    	BusinessBankingPresentationController.prototype.getBBCampaigns = function(){
        var scope = this;
        var asyncManager = applicationManager.getAsyncManager();
        var accountsManager = applicationManager.getAccountManager();
        var breakpoint = kony.application.getCurrentBreakpoint();
        asyncManager.callAsync(
            [
              asyncManager.asyncItem(accountsManager, 'getCampaigns', [{
              "screenName" : "ACCOUNT_DASHBOARD",
              "scale" : (breakpoint >= 1366) ? "1366" : breakpoint
              }])
        ],
        function (asyncResponses) {
            scope.getCampaigns(asyncResponses.responses[0].data);
        }
        )
    };
   /**
    *Method is used for fetching of campaigns
    * @param {Object}- list of campaigns
    */
  BusinessBankingPresentationController.prototype.getCampaigns = function(response) {
     	if(response.campaignSpecifications)
          this.getCampaignsSuccess(response);
     	else
          this.getCampaignsFailure(response);
   };
    /**
     * Method that gets called when fetching unread messages is successful
     * @param {Object} messagesObj List of messages Object
     */
    BusinessBankingPresentationController.prototype.getCampaignsSuccess = function(res) {
       applicationManager.getNavigationManager().updateForm({
           "campaignRes" : res["campaignSpecifications"]
        });
    };
    /**
     * Method that gets called when there is an error in fetching unread messages for account dashboard
     * @param {Object} error Error Object
     */
    BusinessBankingPresentationController.prototype.getCampaignsFailure = function(error) {
       applicationManager.getNavigationManager().updateForm({
            "campaignError" : error
        });
    };
  
  
  BusinessBankingPresentationController.prototype.createUserFailureCallBack = function (errorResponse) {
    this.navManager.updateForm({
      serverError: true,
      errorMessage: errorResponse.errorMessage,
      progressBar: false
    })
  };
  
  
  /**
   * Method to navigate crete custom role ui.
   * @param {object} userObj - user object /user id for update User - as per final imp .
   */
  BusinessBankingPresentationController.prototype.showCreatePermissionsTemplate = function (userObj,formName,userObject) {    
    var scopeObj = this;
    if (userObj) { //update custom role ui
      this.navManager.updateForm({
        "updateCustomRole": this.businessUserManager.getCustomRoleObject(),
		    "id": this.businessUserManager.setCustomRoleAttribute("id")
      }, this.frmPermissionsTemplateForm);
    } else {
      if(kony.application.getCurrentForm().id !== scopeObj.frmPermissionsTemplateForm){
        this.navManager.navigateTo(this.frmPermissionsTemplateForm);	
      }
	  this.businessUserManager.clearDataMembers();
      this.businessUserManager.createCustomRoleObject();
	  var prevForm=false;
	  if(formName==="fromAllCustomRoles"){
	    prevForm=true;
	  }
      if(userObject === undefined){
        this.navManager.updateForm({
          "createNewCustomRole": "createNewCustomRole",
          "progressBar":true,
          "prevForm":prevForm
        }, this.frmPermissionsTemplateForm);
      }
      else{
        this.navManager.updateForm({
          "userObject":userObject,
          "progressBar":true,
          "prevForm":prevForm
        }, this.frmPermissionsTemplateForm);
      }
    
    }
  }; 
  
  BusinessBankingPresentationController.prototype.updateCustomRole = function (customRoleAggregate, customRoleDataStore,templimitstorejson,tryUpdate) {
    this.navManager.updateForm({
        progressBar: true
      });
	var self = this;
	self.customRoleObj = self.businessUserManager.getCustomRoleObject();
	if( tryUpdate === 1 ) {
		self.customRoleObj["SelectedAccounts"]=customRoleAggregate["SelectedAccounts"];		
		if(!kony.sdk.isNullOrUndefined(customRoleAggregate["newData"])){
			self.customRoleObj["templateName"] =  customRoleAggregate["newData"]["templateName"];
		}
	}
    
    customRoleAggregate["userObject"]["templateName"] = applicationManager.getBusinessUserManager().getCustomRoleAttribute("templateName");
	self.customRoleObjForUpdate = createCustomRoleObjectForCreation(customRoleAggregate, self.customRoleObj, customRoleDataStore,templimitstorejson);  
	self.customRoleObjForUpdate["id"]=this.customRoleObj["id"];
    this.businessUserManager.updateCustomRole(self.customRoleObjForUpdate,this.updateCustomRoleSuccess.bind(this,customRoleAggregate), this.onServerError.bind(this));
  };  
  
   
  /** Method to update the user details.
   * @param {object} response - which consists of response from backend
   */
  BusinessBankingPresentationController.prototype.updateCustomRoleSuccess = function (userAggregate,response) {
    if (response) {
      this.navManager.updateForm({
        progressBar: false
      });
      if(response.errorMessage){
        this.navManager.updateForm({
          progressBar: false,
          updateUserSuccessFailure: true,
          errMsg: response.errorMessage
        }, this.frmPermissionsTemplateForm);
      } else {
		applicationManager.getBusinessUserManager().setCustomRoleAttribute("id", response.customRoleId);        
        this.navManager.updateForm({
          progressBar: false,
          updateUserSuccess: true,
          verifyCustomRole: userAggregate,
          referenceNumber: response.customRoleId,
        }, this.frmPermissionsTemplateForm);
      }
    }
  };
  
  /**
   * Method to update cutome role details in Business User module.
   * @param {object} userObj - user details
   * @param {object} response - respose object
   */
  BusinessBankingPresentationController.prototype.updateCustomRoleDetails = function (customRoleObj, isBack, isManageCustomRole, formName = this.frmPermissionsTemplateForm) {
    this.navManager.updateForm({
      progressBar: true
    }, formName);
    
    if(!kony.sdk.isNullOrUndefined(customRoleObj)){
      this.businessUserManager.setCustomRoleAttribute("templateName", customRoleObj.templateName);
       }
      if ( !isBack ) {
      	this.fetchRoles( this.onFetchDefaultUserRolesSuccess.bind(this),formName );
      }
      else {
        applicationManager.getBusinessUserManager().setCustomRoleAttribute("EditFlagForUpdateCustomRole", true);
		if(!isManageCustomRole){
			this.saveAccountsAndNavigate(this.businessUserManager.getCustomRoleAttribute("SelectedAccounts"));
		}
      }
  };
  
  /**
   * fetch user roles success scenario
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.onFetchDefaultUserRolesSuccess = function (formName,response) {
    var userRoles = [];
	var grecords=[];
    this.businessUserManager.createUserObject();
    var isEditManager = this.businessUserManager.getCustomRoleAttribute("isEditManager");
	grecords = response["GroupRecords"];
    this.businessUserManager.setCustomRoleAttribute("NumRoles",grecords.length);
    for (var i = 0; i < grecords.length; i++)
    {
      var userRole = {
        description : grecords[i].Description,
        imgSelectRole : {"src" : "radioinactivebb.png" , "isVisible" : true},
        lblRoleName: grecords[i].Name,
        lblRoleId: grecords[i].id,
        flxInnerRole : {skin : "sknBGFFFFFBdrE3E3E3BdrRadius2Px"}
      };
      
      userRole.lblRoleName = {
        text : truncateFeatureName(grecords[i].Name,25, false),
        toolTip : grecords[i].Name
      };
      
      if(grecords[i].id === this.businessUserManager.getCustomRoleAttribute("SelectedRoleId")){
         userRole.imgArrow = {isVisible : true};
       }
      else{
        userRole.imgArrow = {isVisible : false};
      }
      userRoles.push(userRole);
    }
    
    var isEditMode = !(this.businessUserManager.getCustomRoleAttribute("id") === undefined || 
                       this.businessUserManager.getCustomRoleAttribute("id") === null || 
                       this.businessUserManager.getCustomRoleAttribute("id") === "");
    
    if( isEditMode && !isEditManager ){
      this.navManager.navigateTo(formName);
    }
    
    this.navManager.updateForm({
      userRoles: userRoles,
      selectedRoleId: this.businessUserManager.getCustomRoleAttribute("SelectedRoleId"),
      id : this.businessUserManager.getCustomRoleAttribute("id"),
      progressBar: false
    }, formName);
  };
  
   /**
   * Method to fetch user role feature-actions set based on the selected rolw.
   */
  BusinessBankingPresentationController.prototype.fetchDefaultRoleActions = function ( selectedRoleId ) {
    this.navManager.updateForm({
      progressBar: true
    });
   this.getUserRoleActionsCall( {"groupId" : selectedRoleId}, this.fetchDefaultrRoleActionsSuccess.bind(this), this.onServerError.bind(this) );
  };
  
   /**
   * Success callback for fetchUserRolesAction
   * @param {object} response - response object
   * */
  BusinessBankingPresentationController.prototype.fetchDefaultrRoleActionsSuccess = function (response) {
    
    /* segregate the data on monetary and non monetary basis and prepare two separate payloads */
    var store = segregateFeatureData(response.FeatureActions);
    var allFeatureData = this.fetchAllFeaturesAction(response);
    var otherFeatureData = this.fetchOtherFeaturesActions(store);
    
    this.navManager.updateForm({
          fetchOtherRoleAction :otherFeatureData,
          fetchRoleAction : allFeatureData,
          storeUserDataTransaction : store,
          progressBar: false
        }, this.frmPermissionsTemplateForm);
  }; 
  
  BusinessBankingPresentationController.prototype.fetchUsersList = function (customRoleObj,formName = this.frmPermissionsTemplateForm) {
    this.navManager.updateForm({
      progressBar: true
    }, formName);
 
   this.navigateToUsers(this.onFetchUsersListSuccess.bind(this),null, formName,this.onFetchUsersListFailure.bind(this));
  };
  
  BusinessBankingPresentationController.prototype.onFetchUsersListFailure = function (errorResponse) {
    var errorMessage  = "";
    if(!kony.sdk.isNullOrUndefined(errorResponse.errorMessage)){
      errorMessage = errorResponse.errorMessage;
    }
    if(!kony.sdk.isNullOrUndefined(errorResponse.dbpErrMsg)){
      errorMessage = errorResponse.dbpErrMsg;
    }
    this.navManager.updateForm({
      fetchUsersListFailure: true,
      errorMessage: errorMessage,
    });
  };
    
  BusinessBankingPresentationController.prototype.onFetchUsersListSuccess = function (searchString, formName, response) {
    var userNamesList = [];
	var userRecords=[];
	userRecords = response;
    for (var i = 0; i < userRecords.length; i++)
    {
      var userRole = {
        imgSelectRole : {"src" : "radioinactivebb.png" , "isVisible" : true},
        lblRoleName: userRecords[i].UserName,
        lblRoleId: userRecords[i].id,
        lblName : userRecords[i].FirstName+" "+ userRecords[i].MiddleName+" "+userRecords[i].LastName,
        flxInnerRole : {skin : "sknBGFFFFFBdrE3E3E3BdrRadius2Px"},
        roleName : userRecords[i].role_name,
        group_id : userRecords[i].Group_id
      };
      
      userRole.lblRoleName = {
        text : truncateFeatureName(userRecords[i].UserName,25, false),
        toolTip : userRecords[i].UserName
      };
      userRole.imgArrow = {isVisible : false};
      userNamesList.push(userRole);
    }
    
     this.navManager.updateForm({
      userNamesList: userNamesList
    }, formName);
  };
  
  /**Method to make service call to fetch Accounts
  */
  BusinessBankingPresentationController.prototype.showAllAvailableAccounts = function (selectedRole , isEdit) {
    this.navManager.updateForm({
      progressBar: true
    });
    if(this.businessUserManager.getCustomRoleAttribute("isEditManger") !== true && !isEdit){
      this.getAllAccountsCall(this.getAllAccountsSuccess.bind(this), this.onServerError.bind(this));
    }
    else {
      this.saveAccountsAndNavigate(this.businessUserManager.customRoleObj.SelectedAccounts);
    }
  };  

    /** Method for getAllAccounts success
  * @param {object} response - which consists of response from backend
  */
	BusinessBankingPresentationController.prototype.getAllAccountsSuccess = function(response) {
		this.navManager.updateForm({
			progressBar: false,
			accounts: response,
			selectedAccounts : this.businessUserManager.getCustomRoleAttribute("accounts"),
			id : this.businessUserManager.getCustomRoleAttribute("id")
		}, this.frmPermissionsTemplateForm);
  };
  
  BusinessBankingPresentationController.prototype.saveAccountsAndNavigate = function (selectedAccounts) {
    this.navManager.updateForm({
      progressBar: true
    });
    var userAggregate = {};  
    var userObj = {};
    var templateName = "";

    if(!kony.sdk.isNullOrUndefined(this.businessUserManager.getCustomRoleAttribute("templateName"))){
      templateName = this.businessUserManager.getCustomRoleAttribute("templateName");
    }
    userObj["templateName"] =  templateName;
    userAggregate.SelectedRoleName = this.businessUserManager.getCustomRoleAttribute("SelectedRoleName");
    userAggregate.SelectedRoleId = this.businessUserManager.getCustomRoleAttribute("SelectedRoleId");
    userAggregate.selectedAccounts = selectedAccounts;
    userAggregate.userObject = userObj;
    this.businessUserManager.setCustomRoleAttribute("SelectedAccounts",selectedAccounts);
    this.navManager.updateForm({
			progressBar: false,
			verifyUser: userAggregate,
		}, this.frmPermissionsTemplateForm);
  };  
  
  /** Method to update the selected accounts in the Business Manager
   * @param {object} selectedAccounts - which consists of list of selected accounts of the user
   */
  BusinessBankingPresentationController.prototype.setSelectedAccounts = function ( selectedAccounts ) {
    this.businessUserManager.setCustomRoleAttribute("accounts", selectedAccounts);
  };  
  
  return BusinessBankingPresentationController;
});
