define(function() {

  return {
    minLabel: '$0', // <str>
    maxLabel: '$100', // <str>
    minVal: 0, // number
    maxVal: 100, // number
    step: 10, // number
    value: 0, //number
    onChangeCallback: null, // executed after the value is selected
    onInputCallback: null, // executed as the value is being changed/input
    postshow: function(){
      this.view.imgThumb.onTouchStart = this._touchStartHandler;
      this.view.imgThumb.onTouchEnd = this._touchEndHandler;
      this.deviceConfig();
    },

    /**
     * Keys:
     * minLabel<string>: label for minimum value
     * maxLable<string>: label for maximum value
     * minVal<int>: minimum value that can be selected
     * maxVal<int>: maximum value that can be selected
     * step<int>: step size to increment/decrement on slider selection
     * onChangeCallback: Called after the slider thumb is released
     * onInputCallback: Called while the thumb is moving during selection
     * @param {Object} data : Data to initialize the widget
     */
    setData(data){
      this.view.lblMinimum.text = data['minLabel'];
      this.view.lblMaximum.text = data['maxLabel'];
      this.minLabel = data['minLabel'];
      this.maxLabel = data['maxLabel'];
      this.minVal = data['minVal'];
      this.maxVal = data['maxVal'];
      this.step = data['step'];
      this.onChangeCallback = data['onChangeCallback'] ? data['onChangeCallback'] : null;
      this.onInputCallback = data['onInputCallback'] ? data['onInputCallback'] : null;
    },

    /**
     * To get the current selected value from Slider
     */
    getValue(){
      let percent = parseFloat(this.view.flxSelection.width.slice(0, -1));
      return percent*(this.maxVal - this.minVal)/100 + this.minVal;
    },

    /**
     * To set the value for slider
     * @param {int} value Value to set in slider
     * @throws Exception when provided value is out of bounds
     */
    setValue(value){
      if(value < this.minVal || value > this.maxVal)
        throw "[Slider Component]: Value " + value + " is out of bounds";
      let left = value / (this.maxVal - this.minVal) * 100;
      this.view.flxImage.left = left + '%';
      this.view.flxSelection.width = left + '%';
      if(this.onChangeCallback)
        this.onChangeCallback();
    },

    _touchEndHandler: function(){
      this.view.flxWrap.onTouchMove = null;
      let stepLength = this.step/(this.maxVal - this.minVal) * 100;
      let left = parseFloat(this.view.flxImage.left.slice(0,-1));
      left = Math.round(left/stepLength) * stepLength;
      this.view.flxImage.left = left + '%';
      this.view.flxSelection.width = left + '%';
      if(this.onChangeCallback)
        this.onChangeCallback();
      this.view.forceLayout();
    },

    _touchStartHandler: function(event, x, y){
      this.view.flxWrap.onTouchMove = this._touchMoveHandle;
    },

    _touchMoveHandle: function(event, x, y){
      let left = x/this.view.flxWrap.frame.width * 100;
      if(left < 0)
        left = 0;
      else if(left  > 100)
        left = 100;
      // set left
      this.view.flxImage.left = left + '%';
      this.view.flxSelection.width = left + "%";

      if(this.onChangeCallback){
        let stepLength = this.step/(this.maxVal - this.minVal) * 100;
        let percent = Math.round(left/stepLength) * stepLength
        this.value = percent*(this.maxVal - this.minVal)/100 + this.minVal
        if(this.onInputCallback)
          this.onInputCallback();
      }

      this.view.forceLayout();
    },

    onBreakpointChange: function(event, width){
      if(width == 640){
        this.view.lblMinimum.skin = 'lblBody13';
        this.view.lblMaximum.skin = 'lblBody13';
      }
      else{
        this.view.lblMinimum.skin = 'lblBody15';
        this.view.lblMaximum.skin = 'lblBody15';
      }
    },
    
    deviceConfig: function(){
      if(kony.os.deviceInfo().name === 'thinclient'){
        this.view.onBreakpointChange = this.onBreakpointChange;
      }
      else{
        this.view.lblMinimum.skin = 'lblBody13';
        this.view.lblMaximum.skin = 'lblBody13';
      }
    }
  };
});