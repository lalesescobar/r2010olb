define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for activateNow **/
    AS_FlexContainer_gf609a22df584ee8ac37aa3e2f8e3183: function AS_FlexContainer_gf609a22df584ee8ac37aa3e2f8e3183(eventobject) {
        var self = this;
        return self.preshow.call(this);
    },
    /** postShow defined for activateNow **/
    AS_FlexContainer_e7805ede8fcd40b48d0a0dff3f424f00: function AS_FlexContainer_e7805ede8fcd40b48d0a0dff3f424f00(eventobject) {
        var self = this;
        return self.onBreakpointChange.call(this);
    }
});