define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** cantSignIn defined for rememberMe **/
    AS_UWI_f2d9d99077f845ac8fcf60ce338d04e1: function AS_UWI_f2d9d99077f845ac8fcf60ce338d04e1() {
        var self = this;
        self.cantSignIn();
    },
    /** preShow defined for login **/
    AS_FlexContainer_h44200ac183d4ca2b5860c9aa00200e7: function AS_FlexContainer_h44200ac183d4ca2b5860c9aa00200e7(eventobject) {
        var self = this;
        return self.preshow.call(this);
    }
});