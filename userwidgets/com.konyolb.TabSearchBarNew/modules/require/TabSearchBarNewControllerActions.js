define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgOption0 **/
    AS_Label_e2180500cdc64481a54c8a5c0bc6b4ee: function AS_Label_e2180500cdc64481a54c8a5c0bc6b4ee(eventobject, x, y) {
        var self = this;
        var viewConstants = require('ViewConstants');
        if (this.view.imgOption0.text === viewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO) {
            this.view.imgOption0.text = viewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
            this.view.imgOption1.text = viewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
            this.view.flxToggleOptions.forceLayout();
            if (this.onOptionToggle !== undefined && this.onOptionToggle !== null) {
                this.onOptionToggle();
            }
        }
    },
    /** onTouchStart defined for imgOption1 **/
    AS_Label_d45c0c60442348efa739eee5c613a4b8: function AS_Label_d45c0c60442348efa739eee5c613a4b8(eventobject, x, y) {
        var self = this;
        var viewConstants = require('ViewConstants');
        if (this.view.imgOption1.text === viewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO) {
            this.view.imgOption1.text = viewConstants.FONT_ICONS.RADIO_BUTTON_SELECTED_NUO;
            this.view.imgOption0.text = viewConstants.FONT_ICONS.RADIO_BUTTON_UNSELECTED_NUO;
            this.view.flxToggleOptions.forceLayout();
            if (this.onOptionToggle !== undefined && this.onOptionToggle !== null) {
                this.onOptionToggle();
            }
        }
    },
    /** preShow defined for TabSearchBarNew **/
    AS_FlexContainer_a4a4390e20d74defbc8707b39f620b48: function AS_FlexContainer_a4a4390e20d74defbc8707b39f620b48(eventobject) {
        var self = this;
        //this.invokePreShow();
    }
});