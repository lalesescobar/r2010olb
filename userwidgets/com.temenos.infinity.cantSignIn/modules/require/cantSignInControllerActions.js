define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for cantSignIn **/
    AS_FlexContainer_c8c496f15d7d4a0491c0bcb8412df78f: function AS_FlexContainer_c8c496f15d7d4a0491c0bcb8412df78f(eventobject) {
        var self = this;
        return self.preshow.call(this);
    },
    /** postShow defined for cantSignIn **/
    AS_FlexContainer_c6a95a421cb44204bf2e5bd3f59103a5: function AS_FlexContainer_c6a95a421cb44204bf2e5bd3f59103a5(eventobject) {
        var self = this;
        return self.postShow.call(this);
    }
});