define(['CommonUtilities'], function(CommonUtilities) {

	return {
     renderCalendar: function()
      {
        var context1={"widget":this.view.calSendOn,"anchor":"bottom"};       
        this.view.calSendOn.setContext(context1);
        var context2={"widget":this.view.CalDeliverBy,"anchor":"bottom"};       
        this.view.CalDeliverBy.setContext(context2);
        // accessibility setup
        let widgets = [
          [this.view.txtSearch, this.view.flxEnterAmount],
          [this.view.txtSearchmod, this.view.flxEnterAmountmod]
        ]
        for(let i=0; i<widgets.length; i++){
          CommonUtilities.setA11yFoucsHandlers(widgets[i][0], widgets[i][1], this)
        }
      }

	};
});