define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnTab1 **/
    AS_Button_h80556e9d2834519bc200ae74561ca8b: function AS_Button_h80556e9d2834519bc200ae74561ca8b(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab2 **/
    AS_Button_f63e64d075854924b873d3bb295e4313: function AS_Button_f63e64d075854924b873d3bb295e4313(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab3 **/
    AS_Button_ge8f1970cf7240a7bdb249adcd6b3d5d: function AS_Button_ge8f1970cf7240a7bdb249adcd6b3d5d(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab4 **/
    AS_Button_e9dd04f339b04df78046b090d3f27a26: function AS_Button_e9dd04f339b04df78046b090d3f27a26(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab5 **/
    AS_Button_a05e63e207ac4eb98cea48eaba9f2a42: function AS_Button_a05e63e207ac4eb98cea48eaba9f2a42(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** onClick defined for btnTab6 **/
    AS_Button_dfc4b13c20b14ef2b7f6b7d8f6dba4b7: function AS_Button_dfc4b13c20b14ef2b7f6b7d8f6dba4b7(eventobject) {
        var self = this;
        this.clickTab(eventobject);
    },
    /** preShow defined for TabsHeaderNew **/
    AS_FlexScrollContainer_c54138f7252541e0a148d21f513dca54: function AS_FlexScrollContainer_c54138f7252541e0a148d21f513dca54(eventobject) {
        var self = this;
        //this.invokePreShow();
    }
});